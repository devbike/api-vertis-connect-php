<?php
/**
 * ModelParcInfITFVertisTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace VertisConnect;

/**
 * ModelParcInfITFVertisTest Class Doc Comment
 *
 * @category    Class
 * @description ModelParcInfITFVertis
 * @package     VertisConnect
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ModelParcInfITFVertisTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ModelParcInfITFVertis"
     */
    public function testModelParcInfITFVertis()
    {
    }

    /**
     * Test attribute "cod_parceiro"
     */
    public function testPropertyCodParceiro()
    {
    }

    /**
     * Test attribute "ind_tip_parceiro"
     */
    public function testPropertyIndTipParceiro()
    {
    }

    /**
     * Test attribute "path_arquivo_connection"
     */
    public function testPropertyPathArquivoConnection()
    {
    }

    /**
     * Test attribute "path_arquivo_connection_local"
     */
    public function testPropertyPathArquivoConnectionLocal()
    {
    }

    /**
     * Test attribute "ind_exporta_itfconnection"
     */
    public function testPropertyIndExportaItfconnection()
    {
    }

    /**
     * Test attribute "recordcount"
     */
    public function testPropertyRecordcount()
    {
    }
}
