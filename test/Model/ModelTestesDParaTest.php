<?php
/**
 * ModelTestesDParaTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace VertisConnect;

/**
 * ModelTestesDParaTest Class Doc Comment
 *
 * @category    Class
 * @description ModelTestesDPara
 * @package     VertisConnect
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ModelTestesDParaTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ModelTestesDPara"
     */
    public function testModelTestesDPara()
    {
    }

    /**
     * Test attribute "cod_exa_teste_de_para"
     */
    public function testPropertyCodExaTesteDePara()
    {
    }

    /**
     * Test attribute "fkcod_unid_negoc"
     */
    public function testPropertyFkcodUnidNegoc()
    {
    }

    /**
     * Test attribute "fkcod_unid_oper"
     */
    public function testPropertyFkcodUnidOper()
    {
    }

    /**
     * Test attribute "fkcod_parceiro"
     */
    public function testPropertyFkcodParceiro()
    {
    }

    /**
     * Test attribute "cod_exame_parc"
     */
    public function testPropertyCodExameParc()
    {
    }

    /**
     * Test attribute "cod_teste_parc"
     */
    public function testPropertyCodTesteParc()
    {
    }

    /**
     * Test attribute "nom_teste_parc"
     */
    public function testPropertyNomTesteParc()
    {
    }

    /**
     * Test attribute "fkcod_exa_teste"
     */
    public function testPropertyFkcodExaTeste()
    {
    }

    /**
     * Test attribute "dth_inclusao"
     */
    public function testPropertyDthInclusao()
    {
    }

    /**
     * Test attribute "recordcount"
     */
    public function testPropertyRecordcount()
    {
    }
}
