<?php
/**
 * ModelPsaFaturaTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace VertisConnect;

/**
 * ModelPsaFaturaTest Class Doc Comment
 *
 * @category    Class
 * @description ModelPsaFatura
 * @package     VertisConnect
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ModelPsaFaturaTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ModelPsaFatura"
     */
    public function testModelPsaFatura()
    {
    }

    /**
     * Test attribute "cod_psa_fatura"
     */
    public function testPropertyCodPsaFatura()
    {
    }

    /**
     * Test attribute "cod_plano_x_animal"
     */
    public function testPropertyCodPlanoXAnimal()
    {
    }

    /**
     * Test attribute "cod_plano_saude"
     */
    public function testPropertyCodPlanoSaude()
    {
    }

    /**
     * Test attribute "nom_plano_saude"
     */
    public function testPropertyNomPlanoSaude()
    {
    }

    /**
     * Test attribute "cod_animal"
     */
    public function testPropertyCodAnimal()
    {
    }

    /**
     * Test attribute "nom_animal"
     */
    public function testPropertyNomAnimal()
    {
    }

    /**
     * Test attribute "cod_proprietario"
     */
    public function testPropertyCodProprietario()
    {
    }

    /**
     * Test attribute "nom_proprietario"
     */
    public function testPropertyNomProprietario()
    {
    }

    /**
     * Test attribute "vlr_fatura"
     */
    public function testPropertyVlrFatura()
    {
    }

    /**
     * Test attribute "ind_meio_pgto"
     */
    public function testPropertyIndMeioPgto()
    {
    }

    /**
     * Test attribute "nro_parcela"
     */
    public function testPropertyNroParcela()
    {
    }

    /**
     * Test attribute "ind_envia_email"
     */
    public function testPropertyIndEnviaEmail()
    {
    }

    /**
     * Test attribute "dth_inclusao"
     */
    public function testPropertyDthInclusao()
    {
    }

    /**
     * Test attribute "dth_exclusao"
     */
    public function testPropertyDthExclusao()
    {
    }

    /**
     * Test attribute "recordcount"
     */
    public function testPropertyRecordcount()
    {
    }
}
