<?php
/**
 * ModelExamesTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace VertisConnect;

/**
 * ModelExamesTest Class Doc Comment
 *
 * @category    Class
 * @description ModelExames
 * @package     VertisConnect
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ModelExamesTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ModelExames"
     */
    public function testModelExames()
    {
    }

    /**
     * Test attribute "cod_prod_exame"
     */
    public function testPropertyCodProdExame()
    {
    }

    /**
     * Test attribute "nom_exame"
     */
    public function testPropertyNomExame()
    {
    }

    /**
     * Test attribute "nom_reduzido"
     */
    public function testPropertyNomReduzido()
    {
    }

    /**
     * Test attribute "nom_exa_ingles"
     */
    public function testPropertyNomExaIngles()
    {
    }

    /**
     * Test attribute "fkcod_metodologia"
     */
    public function testPropertyFkcodMetodologia()
    {
    }

    /**
     * Test attribute "fkcod_material"
     */
    public function testPropertyFkcodMaterial()
    {
    }

    /**
     * Test attribute "vol_material"
     */
    public function testPropertyVolMaterial()
    {
    }

    /**
     * Test attribute "ind_mult_amostras"
     */
    public function testPropertyIndMultAmostras()
    {
    }

    /**
     * Test attribute "qtd_amostras"
     */
    public function testPropertyQtdAmostras()
    {
    }

    /**
     * Test attribute "tmp_intervalo_coleta"
     */
    public function testPropertyTmpIntervaloColeta()
    {
    }

    /**
     * Test attribute "unid_vol_mat"
     */
    public function testPropertyUnidVolMat()
    {
    }

    /**
     * Test attribute "fkcod_recipiente"
     */
    public function testPropertyFkcodRecipiente()
    {
    }

    /**
     * Test attribute "ind_id_amostra_auto"
     */
    public function testPropertyIndIdAmostraAuto()
    {
    }

    /**
     * Test attribute "ind_fmt_etq"
     */
    public function testPropertyIndFmtEtq()
    {
    }

    /**
     * Test attribute "ind_pzo_entrega"
     */
    public function testPropertyIndPzoEntrega()
    {
    }

    /**
     * Test attribute "prazo_entrega"
     */
    public function testPropertyPrazoEntrega()
    {
    }

    /**
     * Test attribute "ind_pzo_urgencia"
     */
    public function testPropertyIndPzoUrgencia()
    {
    }

    /**
     * Test attribute "pzo_reg_urgencia"
     */
    public function testPropertyPzoRegUrgencia()
    {
    }

    /**
     * Test attribute "fkcod_clinica_execucao"
     */
    public function testPropertyFkcodClinicaExecucao()
    {
    }

    /**
     * Test attribute "fkcod_antibiograma"
     */
    public function testPropertyFkcodAntibiograma()
    {
    }

    /**
     * Test attribute "ind_ativo"
     */
    public function testPropertyIndAtivo()
    {
    }

    /**
     * Test attribute "ind_tip_exame"
     */
    public function testPropertyIndTipExame()
    {
    }

    /**
     * Test attribute "ind_realiza_cmed"
     */
    public function testPropertyIndRealizaCmed()
    {
    }

    /**
     * Test attribute "ind_exibe_qdr"
     */
    public function testPropertyIndExibeQdr()
    {
    }

    /**
     * Test attribute "ind_desc_progres"
     */
    public function testPropertyIndDescProgres()
    {
    }

    /**
     * Test attribute "ind_mostra_laudo"
     */
    public function testPropertyIndMostraLaudo()
    {
    }

    /**
     * Test attribute "ind_anexa_arq_laudo"
     */
    public function testPropertyIndAnexaArqLaudo()
    {
    }

    /**
     * Test attribute "ind_posicao_resultados"
     */
    public function testPropertyIndPosicaoResultados()
    {
    }

    /**
     * Test attribute "ind_layout_img_laudo"
     */
    public function testPropertyIndLayoutImgLaudo()
    {
    }

    /**
     * Test attribute "ind_exibe_laudo_evolutivo"
     */
    public function testPropertyIndExibeLaudoEvolutivo()
    {
    }

    /**
     * Test attribute "ind_modalidade"
     */
    public function testPropertyIndModalidade()
    {
    }

    /**
     * Test attribute "vlr_produto"
     */
    public function testPropertyVlrProduto()
    {
    }

    /**
     * Test attribute "cod_apoio"
     */
    public function testPropertyCodApoio()
    {
    }

    /**
     * Test attribute "desc_recipiente"
     */
    public function testPropertyDescRecipiente()
    {
    }

    /**
     * Test attribute "nom_grp_produto"
     */
    public function testPropertyNomGrpProduto()
    {
    }

    /**
     * Test attribute "nom_material"
     */
    public function testPropertyNomMaterial()
    {
    }

    /**
     * Test attribute "ind_tip_material"
     */
    public function testPropertyIndTipMaterial()
    {
    }

    /**
     * Test attribute "ind_jejum"
     */
    public function testPropertyIndJejum()
    {
    }

    /**
     * Test attribute "itens_perfil"
     */
    public function testPropertyItensPerfil()
    {
    }

    /**
     * Test attribute "cod_produto"
     */
    public function testPropertyCodProduto()
    {
    }

    /**
     * Test attribute "id_produto"
     */
    public function testPropertyIdProduto()
    {
    }

    /**
     * Test attribute "fkcod_grp_prod"
     */
    public function testPropertyFkcodGrpProd()
    {
    }

    /**
     * Test attribute "fkcod_setor"
     */
    public function testPropertyFkcodSetor()
    {
    }

    /**
     * Test attribute "cod_ant_produto"
     */
    public function testPropertyCodAntProduto()
    {
    }

    /**
     * Test attribute "ind_tip_produto"
     */
    public function testPropertyIndTipProduto()
    {
    }

    /**
     * Test attribute "ind_sit_produto"
     */
    public function testPropertyIndSitProduto()
    {
    }

    /**
     * Test attribute "dth_situacao"
     */
    public function testPropertyDthSituacao()
    {
    }

    /**
     * Test attribute "nom_prod_compra"
     */
    public function testPropertyNomProdCompra()
    {
    }

    /**
     * Test attribute "nom_usual"
     */
    public function testPropertyNomUsual()
    {
    }

    /**
     * Test attribute "ord_impressao"
     */
    public function testPropertyOrdImpressao()
    {
    }

    /**
     * Test attribute "fkcod_prod_lcto_conta"
     */
    public function testPropertyFkcodProdLctoConta()
    {
    }

    /**
     * Test attribute "nro_casas_decimais"
     */
    public function testPropertyNroCasasDecimais()
    {
    }

    /**
     * Test attribute "ind_comercial"
     */
    public function testPropertyIndComercial()
    {
    }

    /**
     * Test attribute "ind_gera_bonus"
     */
    public function testPropertyIndGeraBonus()
    {
    }

    /**
     * Test attribute "ind_aceita_bonus"
     */
    public function testPropertyIndAceitaBonus()
    {
    }

    /**
     * Test attribute "ind_exig_profis"
     */
    public function testPropertyIndExigProfis()
    {
    }

    /**
     * Test attribute "ind_dig_valor"
     */
    public function testPropertyIndDigValor()
    {
    }

    /**
     * Test attribute "ind_rel_tab_preco"
     */
    public function testPropertyIndRelTabPreco()
    {
    }

    /**
     * Test attribute "ind_habilita_incidencia"
     */
    public function testPropertyIndHabilitaIncidencia()
    {
    }

    /**
     * Test attribute "ind_exec_aux"
     */
    public function testPropertyIndExecAux()
    {
    }

    /**
     * Test attribute "ind_precificacao"
     */
    public function testPropertyIndPrecificacao()
    {
    }

    /**
     * Test attribute "ind_desc_plan_desc"
     */
    public function testPropertyIndDescPlanDesc()
    {
    }

    /**
     * Test attribute "ind_emitir_etq"
     */
    public function testPropertyIndEmitirEtq()
    {
    }

    /**
     * Test attribute "ind_terceirizado"
     */
    public function testPropertyIndTerceirizado()
    {
    }

    /**
     * Test attribute "recordcount"
     */
    public function testPropertyRecordcount()
    {
    }
}
