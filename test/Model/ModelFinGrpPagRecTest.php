<?php
/**
 * ModelFinGrpPagRecTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace VertisConnect;

/**
 * ModelFinGrpPagRecTest Class Doc Comment
 *
 * @category    Class
 * @description ModelFinGrpPagRec
 * @package     VertisConnect
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ModelFinGrpPagRecTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ModelFinGrpPagRec"
     */
    public function testModelFinGrpPagRec()
    {
    }

    /**
     * Test attribute "id_grp_pagrec"
     */
    public function testPropertyIdGrpPagrec()
    {
    }

    /**
     * Test attribute "cod_unid_negoc"
     */
    public function testPropertyCodUnidNegoc()
    {
    }

    /**
     * Test attribute "cod_unid_oper"
     */
    public function testPropertyCodUnidOper()
    {
    }

    /**
     * Test attribute "cod_grp_pagrec"
     */
    public function testPropertyCodGrpPagrec()
    {
    }

    /**
     * Test attribute "desc_grp_pagrec"
     */
    public function testPropertyDescGrpPagrec()
    {
    }

    /**
     * Test attribute "ind_tip_grupo"
     */
    public function testPropertyIndTipGrupo()
    {
    }

    /**
     * Test attribute "id_conta_cred"
     */
    public function testPropertyIdContaCred()
    {
    }

    /**
     * Test attribute "id_conta_deb"
     */
    public function testPropertyIdContaDeb()
    {
    }

    /**
     * Test attribute "id_conta_cor"
     */
    public function testPropertyIdContaCor()
    {
    }

    /**
     * Test attribute "ind_nivel"
     */
    public function testPropertyIndNivel()
    {
    }

    /**
     * Test attribute "id_grp_pagrec_pai"
     */
    public function testPropertyIdGrpPagrecPai()
    {
    }

    /**
     * Test attribute "cod_parceiro"
     */
    public function testPropertyCodParceiro()
    {
    }

    /**
     * Test attribute "ind_fluxo_caixa"
     */
    public function testPropertyIndFluxoCaixa()
    {
    }

    /**
     * Test attribute "desc_sigla"
     */
    public function testPropertyDescSigla()
    {
    }

    /**
     * Test attribute "ind_situacao"
     */
    public function testPropertyIndSituacao()
    {
    }

    /**
     * Test attribute "cod_dre"
     */
    public function testPropertyCodDre()
    {
    }

    /**
     * Test attribute "tmp_depreciacao"
     */
    public function testPropertyTmpDepreciacao()
    {
    }

    /**
     * Test attribute "cod_ccusto"
     */
    public function testPropertyCodCcusto()
    {
    }

    /**
     * Test attribute "obriga_ccusto"
     */
    public function testPropertyObrigaCcusto()
    {
    }

    /**
     * Test attribute "recordcount"
     */
    public function testPropertyRecordcount()
    {
    }
}
