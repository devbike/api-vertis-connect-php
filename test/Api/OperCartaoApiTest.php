<?php
/**
 * OperCartaoApiTest
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace VertisConnect;

use \VertisConnect\Configuration;
use \VertisConnect\ApiException;
use \VertisConnect\ObjectSerializer;

/**
 * OperCartaoApiTest Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class OperCartaoApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createTModelOperCartao
     *
     * .
     *
     */
    public function testCreateTModelOperCartao()
    {
    }

    /**
     * Test case for deleteTModelOperCartao
     *
     * .
     *
     */
    public function testDeleteTModelOperCartao()
    {
    }

    /**
     * Test case for getTModelOperCartao
     *
     * .
     *
     */
    public function testGetTModelOperCartao()
    {
    }

    /**
     * Test case for getTModelOperCartaos
     *
     * .
     *
     */
    public function testGetTModelOperCartaos()
    {
    }

    /**
     * Test case for updateTModelOperCartao
     *
     * .
     *
     */
    public function testUpdateTModelOperCartao()
    {
    }
}
