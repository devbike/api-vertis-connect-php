<?php
/**
 * ParametrizaoFiscalProprietrioApiTest
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace VertisConnect;

use \VertisConnect\Configuration;
use \VertisConnect\ApiException;
use \VertisConnect\ObjectSerializer;

/**
 * ParametrizaoFiscalProprietrioApiTest Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ParametrizaoFiscalProprietrioApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for doCreateInfoFiscalProp
     *
     * .
     *
     */
    public function testDoCreateInfoFiscalProp()
    {
    }

    /**
     * Test case for doDeleteInfoFiscalPro
     *
     * .
     *
     */
    public function testDoDeleteInfoFiscalPro()
    {
    }

    /**
     * Test case for doGetInfoFiscalProp
     *
     * .
     *
     */
    public function testDoGetInfoFiscalProp()
    {
    }

    /**
     * Test case for doUpdateInfoFiscalProp
     *
     * .
     *
     */
    public function testDoUpdateInfoFiscalProp()
    {
    }
}
