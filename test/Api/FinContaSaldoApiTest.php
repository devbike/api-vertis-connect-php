<?php
/**
 * FinContaSaldoApiTest
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace VertisConnect;

use \VertisConnect\Configuration;
use \VertisConnect\ApiException;
use \VertisConnect\ObjectSerializer;

/**
 * FinContaSaldoApiTest Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class FinContaSaldoApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createfincontasaldo
     *
     * .
     *
     */
    public function testCreatefincontasaldo()
    {
    }

    /**
     * Test case for deletefincontasaldo
     *
     * .
     *
     */
    public function testDeletefincontasaldo()
    {
    }

    /**
     * Test case for getfincontasaldo
     *
     * .
     *
     */
    public function testGetfincontasaldo()
    {
    }

    /**
     * Test case for getfincontasaldos
     *
     * .
     *
     */
    public function testGetfincontasaldos()
    {
    }

    /**
     * Test case for updatefincontasaldo
     *
     * .
     *
     */
    public function testUpdatefincontasaldo()
    {
    }
}
