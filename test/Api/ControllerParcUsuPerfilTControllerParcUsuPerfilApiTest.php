<?php
/**
 * ControllerParcUsuPerfilTControllerParcUsuPerfilApiTest
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace VertisConnect;

use \VertisConnect\Configuration;
use \VertisConnect\ApiException;
use \VertisConnect\ObjectSerializer;

/**
 * ControllerParcUsuPerfilTControllerParcUsuPerfilApiTest Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ControllerParcUsuPerfilTControllerParcUsuPerfilApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for apiV11ParcUsuPerfilGet
     *
     * .
     *
     */
    public function testApiV11ParcUsuPerfilGet()
    {
    }

    /**
     * Test case for apiV11ParcUsuPerfilIdDelete
     *
     * .
     *
     */
    public function testApiV11ParcUsuPerfilIdDelete()
    {
    }

    /**
     * Test case for apiV11ParcUsuPerfilIdGet
     *
     * .
     *
     */
    public function testApiV11ParcUsuPerfilIdGet()
    {
    }

    /**
     * Test case for apiV11ParcUsuPerfilIdPut
     *
     * .
     *
     */
    public function testApiV11ParcUsuPerfilIdPut()
    {
    }

    /**
     * Test case for apiV11ParcUsuPerfilPost
     *
     * .
     *
     */
    public function testApiV11ParcUsuPerfilPost()
    {
    }
}
