# ModelEspecie

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_especie** | **int** | #field_definition# | 
**nom_especie** | **string** | #field_definition# | 
**nom_popular** | **string** | #field_definition# | 
**nom_familia** | **string** | #field_definition# | 
**cod_especie_imp** | **int** | #field_definition# | 
**num_hematocrito_esp** | **int** | #field_definition# | 
**nom_ingles** | **string** | #field_definition# | 
**ind_especie** | **string** | #field_definition# | 
**img_dermograma** | **string** | #field_definition# | 
**img_odontograma** | **string** | #field_definition# | 
**dth_inclusao** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


