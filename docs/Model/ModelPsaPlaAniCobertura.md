# ModelPsaPlaAniCobertura

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_pla_ani_coberturas** | **int** | #field_definition# | 
**cod_plano_saude** | **int** | #field_definition# | 
**cod_animal** | **int** | #field_definition# | 
**cod_coberturas** | **int** | #field_definition# | 
**qtd_coberta** | **int** | #field_definition# | 
**qtd_utilizada** | **int** | #field_definition# | 
**dth_vigencia** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**motivo_exclusao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


