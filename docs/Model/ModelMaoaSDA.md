# ModelMaoaSDA

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_prod_exame** | **int** | #field_definition# | [optional] 
**ind_tipo_mapa_sda** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


