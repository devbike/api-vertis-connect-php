# ModelParcInfFaturamento

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_parceiro** | **int** | #field_definition# | [optional] 
**ind_tip_parceiro** | **string** | #field_definition# | [optional] 
**num_dias_fat** | **int** | #field_definition# | [optional] 
**ind_tipo_vcto** | **string** | #field_definition# | [optional] 
**ind_fmt_pgto_conv** | **string** | #field_definition# | [optional] 
**ind_nome_boleto** | **string** | #field_definition# | [optional] 
**ind_meio_fat_conv** | **string** | #field_definition# | [optional] 
**email1_fat_conv** | **string** | #field_definition# | [optional] 
**email2_fat_conv** | **string** | #field_definition# | [optional] 
**ind_gerar_rps** | **string** | #field_definition# | [optional] 
**ind_anexar_rps** | **string** | #field_definition# | [optional] 
**ind_anexar_nfse** | **string** | #field_definition# | [optional] 
**ind_anexar_boleto** | **string** | #field_definition# | [optional] 
**nom_logra_cob** | **string** | #field_definition# | [optional] 
**nro_logra_cob** | **string** | #field_definition# | [optional] 
**compl_logra_cob** | **string** | #field_definition# | [optional] 
**nom_bairro_cob** | **string** | #field_definition# | [optional] 
**nom_cidade_cob** | **string** | #field_definition# | [optional] 
**sigla_uf_cob** | **string** | #field_definition# | [optional] 
**cod_cep_cob** | **string** | #field_definition# | [optional] 
**cod_cep_compl_cob** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


