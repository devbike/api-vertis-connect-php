# ModelOrdemServicoItensOs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_ord_serv_item** | **int** | #field_definition# | [optional] 
**pkcod_ord_servico** | **int** | #field_definition# | [optional] 
**pkcod_ord_serv_item_solic** | **string** | #field_definition# | [optional] 
**pkcod_ord_serv_item_vinc** | **int** | #field_definition# | [optional] 
**pkcod_unid_negoc** | **int** | #field_definition# | 
**pkcod_unid_oper** | **int** | #field_definition# | 
**dth_lancamento** | **string** | #field_definition# | 
**dth_hor_agenda** | **string** | #field_definition# | [optional] 
**pkcod_deposito** | **int** | #field_definition# | [optional] 
**pkcod_setor_consumo** | **int** | #field_definition# | [optional] 
**pkcod_unid_requisit** | **int** | #field_definition# | [optional] 
**pkcod_solicitante** | **int** | #field_definition# | [optional] 
**pkcod_executante** | **int** | #field_definition# | [optional] 
**ind_sit_exec** | **string** | #field_definition# | [optional] 
**pkcod_produto** | **int** | #field_definition# | 
**nom_produto** | **string** | #field_definition# | [optional] 
**pkcod_prod_vinculado** | **int** | #field_definition# | [optional] 
**pkcod_tab_preco** | **int** | #field_definition# | 
**pkcod_tab_preco_orig** | **int** | #field_definition# | [optional] 
**qtd_produto** | **float** | #field_definition# | 
**vlr_produto** | **float** | #field_definition# | 
**pkcod_usu_desconto** | **int** | #field_definition# | [optional] 
**vlr_produto_desc** | **float** | #field_definition# | [optional] 
**vlr_complementar** | **float** | #field_definition# | [optional] 
**vlr_desc_comis** | **float** | #field_definition# | [optional] 
**vlr_desc_apoio** | **float** | #field_definition# | [optional] 
**ind_tip_execucao** | **string** | #field_definition# | [optional] 
**ind_frm_execucao** | **string** | #field_definition# | [optional] 
**ind_cobra_adic** | **string** | #field_definition# | [optional] 
**ind_sit_comissao** | **string** | #field_definition# | [optional] 
**ind_origem_lcto** | **string** | #field_definition# | [optional] 
**ind_kit_fechado** | **string** | #field_definition# | [optional] 
**ind_descarta_desconto** | **string** | #field_definition# | [optional] 
**ind_terceirizado** | **string** | #field_definition# | [optional] 
**ind_sat_gerado** | **string** | #field_definition# | [optional] 
**pkcod_doc_fiscal** | **int** | #field_definition# | [optional] 
**obs_produto** | **string** | #field_definition# | [optional] 
**pkcod_seq_pedido** | **int** | #field_definition# | [optional] 
**id_pedido_item** | **int** | #field_definition# | [optional] 
**usr_codigo** | **int** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**cod_perfil** | **int** | #field_definition# | [optional] 
**seq_exa_vinculado** | **int** | #field_definition# | [optional] 
**cod_unid_oper_prestadora** | **int** |  | [optional] 
**item_exame** | [**\VertisConnect\Model\ModelOrdemServicoItemExame**](ModelOrdemServicoItemExame.md) |  | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


