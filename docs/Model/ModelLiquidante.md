# ModelLiquidante

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_liquidante** | **int** | #field_definition# | 
**dsc_liquidante** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


