# ModelFinConta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_conta** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**cod_conta** | **string** | #field_definition# | 
**desc_conta** | **string** | #field_definition# | 
**ind_clas_contabil** | **string** | #field_definition# | 
**ind_tipo_conta** | **string** | #field_definition# | 
**ind_nivel** | **string** | #field_definition# | 
**id_conta_pai** | **int** | #field_definition# | 
**ind_situacao** | **string** | #field_definition# | 
**cod_unid_negoc** | **int** | #field_definition# | 
**ind_totaliza** | **string** | #field_definition# | 
**cod_banco** | **string** | #field_definition# | 
**cod_agencia** | **string** | #field_definition# | 
**cod_conta_corrente** | **string** | #field_definition# | 
**ind_deposito** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


