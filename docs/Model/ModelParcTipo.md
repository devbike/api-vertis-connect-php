# ModelParcTipo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_parceiria** | **int** | #field_definition# | [optional] 
**des_parc_neg** | **string** | #field_definition# | [optional] 
**ind_tip_pessoa** | **string** | #field_definition# | [optional] 
**ind_tip_parceiro** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


