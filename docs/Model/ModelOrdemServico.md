# ModelOrdemServico

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_ord_servico** | **int** | #field_definition# | [optional] 
**cod_ord_serv_solic** | **string** | #field_definition# | [optional] 
**fkcod_unid_negoc** | **int** | #field_definition# | 
**fkcod_unid_oper** | **int** | #field_definition# | 
**fkcod_unid_oper_prestadora** | **int** | #field_definition# | [optional] 
**ind_serv_prod** | **string** | #field_definition# | [optional] 
**ind_liquidante** | **string** | #field_definition# | [optional] 
**cpf_liquidante** | **string** | #field_definition# | [optional] 
**fkcod_parc_liquidante** | **int** | #field_definition# | [optional] 
**info_liquidante** | [**\VertisConnect\Model\ModelOrdemServicoInfoLiquidante**](ModelOrdemServicoInfoLiquidante.md) |  | [optional] 
**fkcod_tab_preco** | **int** | #field_definition# | 
**tip_faturamento** | **string** | #field_definition# | 
**dth_vencimento** | **string** | #field_definition# | [optional] 
**fkcod_faturamento** | **int** | #field_definition# | [optional] 
**fkusr_cod_criacao_os** | **int** | #field_definition# | [optional] 
**fkusr_cod_modificou_os** | **int** | #field_definition# | [optional] 
**obs_ordem_servico** | **string** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**ind_retira_amostra** | **string** | #field_definition# | 
**nro_protocolo** | **string** | #field_definition# | 
**vlr_adiantamento** | **float** | #field_definition# | 
**lock_vlr_adiantamento** | **string** | #field_definition# | 
**os_med** | [**\VertisConnect\Model\ModelOrdemServicoOsMed**](ModelOrdemServicoOsMed.md) |  | 
**itens_os** | [**\VertisConnect\Model\ModelOrdemServicoItensOs[]**](ModelOrdemServicoItensOs.md) | #field_definition# | 
**ind_permite_excluir** | **string** |  | [optional] 
**statusinicial** | **string** |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


