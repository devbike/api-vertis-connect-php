# ModelPsaPrecoPlano

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_preco_plano** | **int** | #field_definition# | 
**cod_plano_saude** | **int** | #field_definition# | 
**nom_plano_saude** | **string** | #field_definition# | 
**vlr_plano** | **float** | #field_definition# | 
**dth_vigencia** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


