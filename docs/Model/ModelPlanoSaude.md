# ModelPlanoSaude

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_plano_saude** | **int** | #field_definition# | 
**desc_plano_saude** | **string** | #field_definition# | 
**vlr_plano** | **float** | #field_definition# | 
**ind_periodo_plano** | **string** | #field_definition# | 
**ind_tip_plano** | **string** | #field_definition# | 
**dth_inclusao** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**cod_tab_preco** | **int** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


