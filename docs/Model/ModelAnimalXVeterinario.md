# ModelAnimalXVeterinario

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_anim_x_vet** | **int** | #field_definition# | [optional] 
**fkcod_animal** | **int** | #field_definition# | [optional] 
**fkcod_veterinario** | **int** | #field_definition# | [optional] 
**dth_ult_vinculo** | **string** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | [optional] 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**nom_parceiro** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


