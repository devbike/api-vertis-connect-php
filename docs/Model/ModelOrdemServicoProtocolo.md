# ModelOrdemServicoProtocolo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nro_protocolo** | **string** |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


