# ModelParcTempExec

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_parceiria** | **int** | #field_definition# | [optional] 
**cod_parceiro** | **int** | #field_definition# | [optional] 
**cod_produto** | **int** | #field_definition# | [optional] 
**tempo_execucao** | **int** | #field_definition# | [optional] 
**ind_segunda** | **string** | #field_definition# | [optional] 
**ind_terca** | **string** | #field_definition# | [optional] 
**ind_quarta** | **string** | #field_definition# | [optional] 
**ind_quinta** | **string** | #field_definition# | [optional] 
**ind_sexta** | **string** | #field_definition# | [optional] 
**ind_sabado** | **string** | #field_definition# | [optional] 
**ind_domingo** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


