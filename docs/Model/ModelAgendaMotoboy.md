# ModelAgendaMotoboy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_agenda_mtb** | **int** | #field_definition# | [optional] 
**ind_sit_coleta** | **string** | #field_definition# | [optional] 
**ind_reg_coleta** | **string** | #field_definition# | [optional] 
**ind_nec_insumo** | **string** | #field_definition# | [optional] 
**dth_solic_coleta** | **string** | #field_definition# | [optional] 
**dth_prev_coleta** | **string** | #field_definition# | [optional] 
**cod_parceiro** | **int** | #field_definition# | [optional] 
**num_telefone** | **string** | #field_definition# | [optional] 
**nom_cliente** | **string** | #field_definition# | [optional] 
**end_coleta** | **string** | #field_definition# | [optional] 
**cod_motoboy** | **int** | #field_definition# | [optional] 
**cod_usu_rec_solic** | **int** | #field_definition# | [optional] 
**dth_com_coleta** | **string** | #field_definition# | [optional] 
**cod_usu_com_mtb** | **int** | #field_definition# | [optional] 
**dth_rec_amostra** | **string** | #field_definition# | [optional] 
**dth_enc_coleta** | **string** | #field_definition# | [optional] 
**dth_canc_coleta** | **string** | #field_definition# | [optional] 
**dth_comun_canc** | **string** | #field_definition# | [optional] 
**cod_usu_enc_solic** | **int** | #field_definition# | [optional] 
**obs_solicitacao** | **string** | #field_definition# | [optional] 
**cod_mot_cancel** | **int** | #field_definition# | [optional] 
**nom_usu_contato** | **string** | #field_definition# | [optional] 
**cod_usu_inf_rec_amostra** | **int** | #field_definition# | [optional] 
**cod_usu_canc_coleta** | **int** | #field_definition# | [optional] 
**cod_usu_com_cancel** | **int** | #field_definition# | [optional] 
**ind_tipo_parceiro** | **string** | #field_definition# | [optional] 
**nro_protocolo** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


