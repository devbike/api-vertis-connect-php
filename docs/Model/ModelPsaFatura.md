# ModelPsaFatura

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_psa_fatura** | **int** | #field_definition# | 
**cod_plano_x_animal** | **int** | #field_definition# | 
**cod_plano_saude** | **int** | #field_definition# | 
**nom_plano_saude** | **string** | #field_definition# | 
**cod_animal** | **int** | #field_definition# | 
**nom_animal** | **string** | #field_definition# | 
**cod_proprietario** | **int** | #field_definition# | 
**nom_proprietario** | **string** | #field_definition# | 
**vlr_fatura** | **float** | #field_definition# | 
**ind_meio_pgto** | **string** | #field_definition# | 
**nro_parcela** | **int** | #field_definition# | 
**ind_envia_email** | **string** | #field_definition# | 
**dth_inclusao** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


