# ModelResultadoPadrao

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_res_padrao** | **string** | #field_definition# | [optional] 
**desc_rp_linha** | **string** | #field_definition# | [optional] 
**desc_res_padrao** | **string** | #field_definition# | [optional] 
**ind_fmt_resultado** | **string** | #field_definition# | [optional] 
**fkcod_setor** | **int** | #field_definition# | [optional] 
**ind_situacao** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


