# ModelExameReferencia

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_exa_referencia** | **int** | #field_definition# | [optional] 
**fkcod_prod_exame** | **int** | #field_definition# | [optional] 
**fkcod_exa_teste** | **int** | #field_definition# | [optional] 
**fkcod_especie** | **int** | #field_definition# | [optional] 
**ind_sexo** | **string** | #field_definition# | [optional] 
**idade_inicial** | **int** | #field_definition# | [optional] 
**des_idade** | **string** | #field_definition# | [optional] 
**vlr_ref_inicial** | **float** | #field_definition# | [optional] 
**idade_final** | **int** | #field_definition# | [optional] 
**vlr_ref_final** | **float** | #field_definition# | [optional] 
**des_referencia** | **string** | #field_definition# | [optional] 
**ind_idade** | **string** | #field_definition# | [optional] 
**unid_medida** | **string** | #field_definition# | [optional] 
**vlr_min_refer** | **float** | #field_definition# | [optional] 
**vlr_max_refer** | **float** | #field_definition# | [optional] 
**vlr_ref_abs_inicial** | **float** | #field_definition# | [optional] 
**vlr_ref_abs_final** | **float** | #field_definition# | [optional] 
**des_referencia_absoluto** | **string** | #field_definition# | [optional] 
**unid_medida_absoluto** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


