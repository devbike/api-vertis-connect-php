# ModelProdutoConversao

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_produto_origem** | **int** | #field_definition# | [optional] 
**cod_produto_destino** | **int** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


