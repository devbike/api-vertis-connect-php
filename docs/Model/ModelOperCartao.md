# ModelOperCartao

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_operadora** | **int** | #field_definition# | 
**nom_operadora** | **string** | #field_definition# | 
**num_dias_compensacao** | **int** | #field_definition# | 
**perc_taxa_admin** | **float** | #field_definition# | 
**ind_tipo_operadora** | **string** | #field_definition# | 
**ind_dias_corridos** | **string** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**num_max_parcela** | **int** | #field_definition# | 
**cod_maquina** | **int** | #field_definition# | 
**cod_bandeira** | **string** | #field_definition# | 
**ind_situacao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


