# ModelCRMVeterinarios

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_orgao_classe** | **int** | #field_definition# | 
**cod_veterinario** | **int** | #field_definition# | 
**orgao_classe** | **string** | #field_definition# | 
**num_orgao_classe** | **string** | #field_definition# | 
**uf_orgao_classe** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


