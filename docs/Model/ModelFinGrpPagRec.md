# ModelFinGrpPagRec

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_grp_pagrec** | **int** | #field_definition# | 
**cod_unid_negoc** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**cod_grp_pagrec** | **string** | #field_definition# | 
**desc_grp_pagrec** | **string** | #field_definition# | 
**ind_tip_grupo** | **string** | #field_definition# | 
**id_conta_cred** | **int** | #field_definition# | 
**id_conta_deb** | **int** | #field_definition# | 
**id_conta_cor** | **int** | #field_definition# | 
**ind_nivel** | **string** | #field_definition# | 
**id_grp_pagrec_pai** | **int** | #field_definition# | 
**cod_parceiro** | **int** | #field_definition# | 
**ind_fluxo_caixa** | **string** | #field_definition# | 
**desc_sigla** | **string** | #field_definition# | 
**ind_situacao** | **string** | #field_definition# | 
**cod_dre** | **int** | #field_definition# | 
**tmp_depreciacao** | **int** | #field_definition# | 
**cod_ccusto** | **int** | #field_definition# | 
**obriga_ccusto** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


