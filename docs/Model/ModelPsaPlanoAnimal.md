# ModelPsaPlanoAnimal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_plano_x_animal** | **int** | #field_definition# | 
**cod_plano_saude** | **int** | #field_definition# | 
**nom_plano_saude** | **string** | #field_definition# | 
**cod_animal** | **int** | #field_definition# | 
**nom_animal** | **string** | #field_definition# | 
**nom_especie** | **string** | #field_definition# | 
**nom_raca** | **string** | #field_definition# | 
**dth_aniversario** | **string** | #field_definition# | 
**idade** | **string** | #field_definition# | 
**ind_sexo** | **string** | #field_definition# | 
**ind_castrado** | **string** | #field_definition# | 
**cod_proprietario** | **int** | #field_definition# | 
**nom_proprietario** | **string** | #field_definition# | 
**num_cnpj_cpf** | **string** | #field_definition# | 
**email1** | **string** | #field_definition# | 
**vlr_plano** | **float** | #field_definition# | 
**dth_vigencia** | **string** | #field_definition# | 
**dia_vencimento** | **int** | #field_definition# | 
**ind_frm_pgto** | **string** | #field_definition# | 
**ind_meio_pgto** | **string** | #field_definition# | 
**ind_renova_automatico** | **string** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**cancelamento** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


