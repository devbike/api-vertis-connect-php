# ModelExameAntibiotico

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_antibiotico** | **int** | #field_definition# | [optional] 
**fkcod_unid_negoc** | **int** | #field_definition# | [optional] 
**desc_antibiotico** | **string** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | [optional] 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


