# ModelServicos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_prod_servico** | **int** | #field_definition# | [optional] 
**fkcod_servico** | **string** | #field_definition# | [optional] 
**tmp_med_execucao** | **int** | #field_definition# | [optional] 
**ind_agenda** | **string** | #field_definition# | [optional] 
**ind_regime_urg** | **string** | #field_definition# | [optional] 
**ind_desc_progres** | **string** | #field_definition# | [optional] 
**ind_item_conta** | **string** | #field_definition# | [optional] 
**ind_prod_banho_tosa** | **string** | #field_definition# | [optional] 
**desc_instrucoes** | **string** | #field_definition# | [optional] 
**cod_produto** | **int** | #field_definition# | [optional] 
**id_produto** | **string** | #field_definition# | [optional] 
**fkcod_grp_prod** | **int** | #field_definition# | [optional] 
**fkcod_setor** | **int** | #field_definition# | [optional] 
**cod_ant_produto** | **string** | #field_definition# | [optional] 
**ind_tip_produto** | **string** | #field_definition# | [optional] 
**ind_sit_produto** | **string** | #field_definition# | [optional] 
**dth_situacao** | **string** | #field_definition# | [optional] 
**nom_prod_compra** | **string** | #field_definition# | [optional] 
**nom_usual** | **string** | #field_definition# | [optional] 
**ord_impressao** | **int** | #field_definition# | [optional] 
**fkcod_prod_lcto_conta** | **int** | #field_definition# | [optional] 
**nro_casas_decimais** | **int** | #field_definition# | [optional] 
**ind_comercial** | **string** | #field_definition# | [optional] 
**ind_gera_bonus** | **string** | #field_definition# | [optional] 
**ind_aceita_bonus** | **string** | #field_definition# | [optional] 
**ind_exig_profis** | **string** | #field_definition# | [optional] 
**ind_dig_valor** | **string** | #field_definition# | [optional] 
**ind_rel_tab_preco** | **string** | #field_definition# | [optional] 
**ind_habilita_incidencia** | **string** | #field_definition# | [optional] 
**ind_exec_aux** | **string** | #field_definition# | [optional] 
**ind_precificacao** | **string** | #field_definition# | [optional] 
**ind_desc_plan_desc** | **string** | #field_definition# | [optional] 
**ind_emitir_etq** | **string** | #field_definition# | [optional] 
**ind_terceirizado** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


