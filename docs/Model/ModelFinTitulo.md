# ModelFinTitulo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_titulo** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**id_grp_pagrec** | **int** | #field_definition# | 
**cod_parceiro** | **int** | #field_definition# | 
**cod_faturamento** | **int** | #field_definition# | 
**dth_emis_titulo** | **string** | #field_definition# | 
**ind_tipo_titulo** | **string** | #field_definition# | 
**ind_clas_titulo** | **string** | #field_definition# | 
**ind_provisao** | **string** | #field_definition# | 
**ind_tip_imposto** | **string** | #field_definition# | 
**vlr_bruto_tit** | **float** | #field_definition# | 
**vlr_titulo** | **float** | #field_definition# | 
**dat_vcto_titulo** | **string** | #field_definition# | 
**obs_hist_titulo** | **string** | #field_definition# | 
**vlr_liq_titulo** | **float** | #field_definition# | 
**dat_liq_titulo** | **string** | #field_definition# | 
**nro_boleto** | **string** | #field_definition# | 
**vlr_desconto** | **float** | #field_definition# | 
**vlr_juros_multa** | **float** | #field_definition# | 
**ind_sit_titulo** | **string** | #field_definition# | 
**vlr_dinheiro** | **float** | #field_definition# | 
**vlr_cartao_credito** | **float** | #field_definition# | 
**vlr_cartao_debito** | **float** | #field_definition# | 
**vlr_cheque** | **float** | #field_definition# | 
**cod_conta_receber** | **int** | #field_definition# | 
**vlr_imposto** | **float** | #field_definition# | 
**id_conta_cor** | **int** | #field_definition# | 
**vlr_boleto** | **float** | #field_definition# | 
**vlr_deposito** | **float** | #field_definition# | 
**nro_parcela** | **int** | #field_definition# | 
**cod_ctrl_receb** | **int** | #field_definition# | 
**ind_meio_pagamento** | **string** | #field_definition# | 
**cod_unid_negoc** | **int** | #field_definition# | 
**vlr_desc_titulo** | **float** | #field_definition# | 
**cod_usu_cadastro** | **int** | #field_definition# | 
**cod_usu_liquidacao** | **int** | #field_definition# | 
**vlr_vale** | **float** | #field_definition# | 
**vlr_bonus** | **float** | #field_definition# | 
**cod_regra_rateio** | **int** | #field_definition# | 
**dat_deposito** | **string** | #field_definition# | 
**ind_meio_pgto_alterado** | **string** | #field_definition# | 
**ind_origem** | **string** | #field_definition# | 
**cod_titulo_principal** | **int** | #field_definition# | 
**cod_ccusto** | **int** | #field_definition# | 
**ind_autorizado** | **string** | #field_definition# | 
**ind_conciliado** | **string** | #field_definition# | 
**vlr_taxa_admin** | **float** | #field_definition# | 
**cod_operadora** | **int** | #field_definition# | 
**num_cartao** | **string** | #field_definition# | 
**num_operacao** | **string** | #field_definition# | 
**dat_compensacao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


