# ModelUnidadeOperacional

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_unid_oper** | **int** | #field_definition# | 
**cod_unid_negoc** | **int** | #field_definition# | 
**cnpj_unid_oper** | **string** | #field_definition# | 
**tip_unid_operac** | **string** | #field_definition# | 
**nom_unid_oper** | **string** | #field_definition# | 
**cap_hosp_plan** | **int** | #field_definition# | 
**cap_hosp_oper** | **int** | #field_definition# | 
**cap_hosp_emer** | **int** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**nom_raz_social** | **string** | #field_definition# | 
**nom_lograd** | **string** | #field_definition# | 
**num_lograd** | **string** | #field_definition# | 
**compl_lograd** | **string** | #field_definition# | 
**nom_bairro** | **string** | #field_definition# | 
**nom_cidade** | **string** | #field_definition# | 
**sigla_uf** | **string** | #field_definition# | 
**cod_cep** | **string** | #field_definition# | 
**cod_cep_compl** | **string** | #field_definition# | 
**num_telefone1** | **string** | #field_definition# | 
**compl_fone1** | **string** | #field_definition# | 
**ind_tip_fone1** | **string** | #field_definition# | 
**num_telefone2** | **string** | #field_definition# | 
**compl_fone2** | **string** | #field_definition# | 
**ind_tip_fone2** | **string** | #field_definition# | 
**num_telefone3** | **string** | #field_definition# | 
**compl_fone3** | **string** | #field_definition# | 
**ind_tip_fone3** | **string** | #field_definition# | 
**num_telefone4** | **string** | #field_definition# | 
**compl_fone4** | **string** | #field_definition# | 
**num_insc_est** | **string** | #field_definition# | 
**num_insc_mun** | **string** | #field_definition# | 
**num_insc_susep** | **string** | #field_definition# | 
**ind_tip_fone4** | **string** | #field_definition# | 
**vlr_ch_padrao** | **float** | #field_definition# | 
**vlr_m2_filme** | **float** | #field_definition# | 
**mascara_ccusto** | **string** | #field_definition# | 
**hr_inicio** | **string** | #field_definition# | 
**home_page** | **string** | #field_definition# | 
**email1** | **string** | #field_definition# | 
**hr_termino** | **string** | #field_definition# | 
**path_dbxini_interface** | **string** | #field_definition# | 
**num_crmv** | **string** | #field_definition# | 
**caminho_certificado** | **string** | #field_definition# | 
**senha_certificado** | **string** | #field_definition# | 
**num_serie_certificado** | **string** | #field_definition# | 
**schemas** | **string** | #field_definition# | 
**nom_prefeitura** | **string** | #field_definition# | 
**path_logo_prestador** | **string** | #field_definition# | [optional] 
**path_logo_prefeitura** | **string** | #field_definition# | [optional] 
**ind_salvar_log** | **bool** | #field_definition# | [optional] 
**path_log** | **string** | #field_definition# | [optional] 
**ind_tipo_ambiente** | **string** | #field_definition# | [optional] 
**ind_visualizar_msg** | **bool** | #field_definition# | 
**senha_webservice** | **string** | #field_definition# | 
**host_proxy** | **string** | #field_definition# | 
**porta_proxy** | **int** | #field_definition# | 
**nom_usu_proxy** | **string** | #field_definition# | 
**senha_proxy** | **string** | #field_definition# | 
**smtp_servidor** | **string** | #field_definition# | 
**porta_smtp** | **int** | #field_definition# | 
**nom_usu_smtp** | **string** | #field_definition# | 
**senha_smtp** | **string** | #field_definition# | 
**ind_conexao_segura** | **bool** | #field_definition# | 
**desc_titulo_email** | **string** | #field_definition# | 
**desc_msg_email** | **string** | #field_definition# | 
**ind_opt_simples** | **string** | #field_definition# | 
**ind_incentiv_cult** | **string** | #field_definition# | 
**reg_esp_tributacao** | **int** | #field_definition# | 
**ind_iss_retido** | **string** | #field_definition# | 
**serie_rps** | **string** | #field_definition# | 
**cod_tipo_rps** | **int** | #field_definition# | 
**cod_municipio** | **int** | #field_definition# | 
**cod_tributacao_municipal** | **string** | #field_definition# | 
**cod_cnae** | **string** | #field_definition# | 
**cod_lista_servico** | **string** | #field_definition# | 
**cod_clinica_vinc** | **int** | #field_definition# | 
**orig_tab_preco** | **int** | #field_definition# | 
**cod_unid_oper_master** | **int** | #field_definition# | 
**nom_unid_oper_reduzido** | **string** | #field_definition# | 
**ind_situacao** | **string** | #field_definition# | 
**ind_utiliza_sat** | **string** | #field_definition# | 
**nro_reg_mapa** | **string** | #field_definition# | 
**cor_tela** | **int** | #field_definition# | 
**cod_nat_operacao** | **int** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


