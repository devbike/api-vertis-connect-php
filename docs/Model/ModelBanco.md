# ModelBanco

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_banco** | **string** | #field_definition# | 
**nom_banco** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


