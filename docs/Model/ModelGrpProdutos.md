# ModelGrpProdutos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_grp_prod** | **int** | #field_definition# | [optional] 
**cod_unid_negoc** | **int** | #field_definition# | [optional] 
**nom_grp_produto** | **string** | #field_definition# | [optional] 
**sigla_grp_prod** | **string** | #field_definition# | [optional] 
**ind_tip_produto** | **string** | #field_definition# | [optional] 
**ind_frm_apura_prc** | **string** | #field_definition# | [optional] 
**cod_grp_prod_pai** | **int** | #field_definition# | [optional] 
**perc_custo_prod** | **float** | #field_definition# | [optional] 
**ind_tab_preco** | **string** | #field_definition# | [optional] 
**ind_lista_in35** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


