# ModelOperTaxaVigencia

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_taxa_vigencia** | **int** | #field_definition# | 
**cod_operadora** | **int** | #field_definition# | 
**perc_taxa_admin** | **float** | #field_definition# | 
**dth_vigencia** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


