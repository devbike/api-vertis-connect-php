# ModelCodigoServico

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_servico** | **string** | #field_definition# | [optional] 
**desc_servico** | **string** | #field_definition# | [optional] 
**ind_exige_intermediario** | **string** | #field_definition# | [optional] 
**cod_ibpt** | **int** | #field_definition# | [optional] 
**ind_categoria** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


