# ModelParametros

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_parametro** | **int** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**ind_situacao** | **string** | #field_definition# | 
**desc_parametro** | **string** | #field_definition# | 
**ind_tipo_dado** | **string** | #field_definition# | 
**vlr_parametro** | **string** | #field_definition# | 
**nro_decimais** | **int** | #field_definition# | 
**desc_completa** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


