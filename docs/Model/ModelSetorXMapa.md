# ModelSetorXMapa

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_setor_x_mapa** | **int** | #field_definition# | [optional] 
**fkcod_setor_mapa** | **int** | #field_definition# | [optional] 
**desc_exame_setor_mapa** | **string** | #field_definition# | [optional] 
**tipo_mapa** | **string** | #field_definition# | [optional] 
**ind_setor_exame** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


