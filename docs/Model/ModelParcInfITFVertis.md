# ModelParcInfITFVertis

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_parceiro** | **int** | #field_definition# | [optional] 
**ind_tip_parceiro** | **string** | #field_definition# | [optional] 
**path_arquivo_connection** | **string** | #field_definition# | [optional] 
**path_arquivo_connection_local** | **string** | #field_definition# | [optional] 
**ind_exporta_itfconnection** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


