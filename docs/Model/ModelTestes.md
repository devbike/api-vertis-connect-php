# ModelTestes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_exa_teste** | **int** | #field_definition# | [optional] 
**fkcod_prod_exame** | **int** | #field_definition# | [optional] 
**nom_teste** | **string** | #field_definition# | [optional] 
**nom_teste_laudo** | **string** | #field_definition# | [optional] 
**fkcod_unid_medida** | **int** | #field_definition# | [optional] 
**fkcod_res_padrao** | **string** | #field_definition# | [optional] 
**seq_impressao** | **string** | #field_definition# | [optional] 
**ind_situacao** | **string** | #field_definition# | [optional] 
**ind_cabec** | **string** | #field_definition# | [optional] 
**ind_resultado** | **string** | #field_definition# | [optional] 
**ind_alinhamento_esquerda** | **string** | #field_definition# | [optional] 
**ind_saltar_linha** | **string** | #field_definition# | [optional] 
**ind_exibe_laudo** | **string** | #field_definition# | [optional] 
**niv_identacao** | **int** | #field_definition# | [optional] 
**tipo_fonte** | **string** | #field_definition# | [optional] 
**fmt_fonte** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


