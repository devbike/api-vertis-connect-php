# ModelExamePerfil

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_prod_exa_perfil** | **int** | #field_definition# | [optional] 
**fkcod_unid_negoc** | **int** | #field_definition# | [optional] 
**fkcod_exa_perfil** | **int** | #field_definition# | [optional] 
**fkcod_prod_exame** | **int** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


