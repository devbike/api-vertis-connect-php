# ModelExamesItensPerfil

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_prod_exame_perfil** | **int** |  | [optional] 
**nom_exame_perfil** | **string** |  | [optional] 
**vlr_produto_perfil** | **string** |  | [optional] 
**fkcod_material_perfil** | **int** |  | [optional] 
**fkcod_metodologia_perfil** | **int** |  | [optional] 
**nom_reduzido_perfil** | **string** |  | [optional] 
**fkcod_recipiente_perfil** | **int** |  | [optional] 
**nom_grp_produto_perfil** | **string** |  | [optional] 
**desc_recipiente_perfil** | **string** |  | [optional] 
**nom_material_perfil** | **string** |  | [optional] 
**ind_tip_material_perfil** | **string** |  | [optional] 
**cod_perfil** | **int** |  | [optional] 
**fkcod_setor** | **int** |  | [optional] 
**fkcod_setor_mapa_trab** | **int** |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


