# ModelOrdemServicoOsMedComplExame

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_ord_serv_exa** | **int** | #field_definition# | [optional] 
**ind_public_imagem** | **string** | #field_definition# | [optional] 
**ind_publica_laudo** | **string** | #field_definition# | [optional] 
**emails_laudos** | **string** | #field_definition# | [optional] 
**desc_email_copia_reenvio** | **string** | #field_definition# | [optional] 
**img_ass_qrcode** | **string** | #field_definition# | [optional] 
**desc_motivo_alt_mapa_sda** | **string** | #field_definition# | [optional] 
**ind_prot_conf** | **string** | #field_definition# | [optional] 
**ind_etiqueta** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


