# ModelTestesDPara

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_exa_teste_de_para** | **int** | #field_definition# | 
**fkcod_unid_negoc** | **int** | #field_definition# | 
**fkcod_unid_oper** | **int** | #field_definition# | 
**fkcod_parceiro** | **int** | #field_definition# | 
**cod_exame_parc** | **string** | #field_definition# | 
**cod_teste_parc** | **string** | #field_definition# | 
**nom_teste_parc** | **string** | #field_definition# | 
**fkcod_exa_teste** | **int** | #field_definition# | 
**dth_inclusao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


