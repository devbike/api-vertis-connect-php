# ModelRaca

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_raca** | **int** | #field_definition# | [optional] 
**cod_especie** | **int** | #field_definition# | [optional] 
**cod_pelagem** | **int** | #field_definition# | [optional] 
**nom_raca** | **string** | #field_definition# | [optional] 
**ind_ani_silvestre** | **string** | #field_definition# | [optional] 
**ind_porte** | **string** | #field_definition# | [optional] 
**ind_tip_pelagem** | **string** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | [optional] 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


