# ModelUnidadeNegocio

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_unid_negoc** | **int** | Código sequencial da entidade, obrigatório, chave primária | 
**nom_unid_negoc** | **string** | Nome da unidade de negócio | 
**tip_unid_negoc** | **string** | Determina o tipo da unidade de negócio | 
**sigla_unid_negoc** | **string** | Sigla utilizada pela unidade de negócio | 
**num_reg_ans** | **int** | Número do registro junto a ANS | 
**dth_exclusao** | **string** | Data de exclusão lógica | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


