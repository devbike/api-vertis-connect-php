# ModelKitMsda

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_exa_msda_kit** | **int** | #field_definition# | [optional] 
**ind_tip_kit** | **int** | #field_definition# | [optional] 
**ind_equino** | **string** | #field_definition# | [optional] 
**ind_asinino_muar** | **string** | #field_definition# | [optional] 
**fkcod_fabricante** | **int** | #field_definition# | [optional] 
**nom_comercial_kit** | **string** | #field_definition# | [optional] 
**nro_partida_lote** | **string** | #field_definition# | [optional] 
**nro_validade** | **string** | #field_definition# | [optional] 
**ind_situacao** | **string** | #field_definition# | [optional] 
**qtd_capacidade_kit** | **int** | #field_definition# | [optional] 
**qtd_utilizada** | **int** | #field_definition# | [optional] 
**preferencial** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


