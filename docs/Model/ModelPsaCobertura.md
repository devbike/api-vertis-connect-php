# ModelPsaCobertura

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_coberturas** | **int** | #field_definition# | 
**cod_plano_saude** | **int** | #field_definition# | 
**nom_plano_saude** | **string** | #field_definition# | 
**cod_produto** | **int** | #field_definition# | 
**nom_produto** | **string** | #field_definition# | 
**dth_vigencia** | **string** | #field_definition# | 
**qtd_eventos** | **int** | #field_definition# | 
**qtd_dias_carencia** | **int** | #field_definition# | 
**dth_exclusao** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


