# ModelOrdemServicoItemExame

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_ord_serv_item_exa** | **int** | #field_definition# | [optional] 
**fkcod_material** | **int** | #field_definition# | 
**fkcod_metodologia** | **int** | #field_definition# | 
**fkcod_equipamento** | **int** | #field_definition# | 
**fkcod_setor_mapa_trab** | **int** | #field_definition# | [optional] 
**nom_setor_mapa_trab** | **string** | #field_definition# | [optional] 
**dth_atend_inicio** | **string** | #field_definition# | [optional] 
**dth_atend_final** | **string** | #field_definition# | [optional] 
**ind_sit_mapa_trab** | **string** | #field_definition# | [optional] 
**id_amostra** | **string** | #field_definition# | [optional] 
**id_accession_number** | **string** | #field_definition# | [optional] 
**ind_colher_amostra** | **string** | #field_definition# | [optional] 
**ind_etq_impressa** | **string** | #field_definition# | [optional] 
**ind_integrado** | **string** | #field_definition# | [optional] 
**ind_terceirizado** | **string** | #field_definition# | [optional] 
**obs_rejeicao** | **string** | #field_definition# | [optional] 
**dth_prev_entrega** | **string** | #field_definition# | [optional] 
**dth_assinatura** | **string** | #field_definition# | [optional] 
**dth_publicacao** | **string** | #field_definition# | [optional] 
**arq_anexo_laudo** | **string** | #field_definition# | [optional] 
**txt_assinatura_digital** | **string** | #field_definition# | [optional] 
**fkcod_vet_ass_digital_1** | **int** | #field_definition# | [optional] 
**fkcod_vet_ass_digital_2** | **int** | #field_definition# | [optional] 
**public_propr** | **string** | #field_definition# | [optional] 
**dth_public_propr** | **string** | #field_definition# | [optional] 
**public_clin** | **string** | #field_definition# | [optional] 
**dth_public_clin** | **string** | #field_definition# | [optional] 
**public_vet** | **string** | #field_definition# | [optional] 
**dth_public_vet** | **string** | #field_definition# | [optional] 
**versao_itf** | **int** | #field_definition# | [optional] 
**ind_tip_exame** | **string** |  | [optional] 
**cod_perfil** | **int** |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


