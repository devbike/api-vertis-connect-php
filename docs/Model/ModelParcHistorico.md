# ModelParcHistorico

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_historico** | **int** | #field_definition# | [optional] 
**cod_parceiro** | **int** | #field_definition# | [optional] 
**cod_parceiria** | **int** | #field_definition# | [optional] 
**dth_historico** | **string** | #field_definition# | [optional] 
**obs_historico** | **string** | #field_definition# | [optional] 
**txt_inicio_historico** | **string** | #field_definition# | [optional] 
**ind_mostra_alerta** | **string** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | [optional] 
**cod_usuario** | **int** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


