# ModelCtaRecControle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_ctrl_receb** | **int** | #field_definition# | 
**ind_tip_recebimento** | **string** | #field_definition# | 
**cod_agencia** | **string** | #field_definition# | 
**conta_corrente** | **string** | #field_definition# | 
**num_cheque** | **string** | #field_definition# | 
**vlr_recebido** | **float** | #field_definition# | 
**nro_parcelas** | **int** | #field_definition# | 
**num_cartao** | **string** | #field_definition# | 
**num_operacao** | **string** | #field_definition# | 
**dat_emissao** | **string** | #field_definition# | 
**dat_compensacao** | **string** | #field_definition# | 
**cod_operadora** | **int** | #field_definition# | 
**cod_banco** | **string** | #field_definition# | 
**ind_sit_cheque** | **string** | #field_definition# | 
**cod_titulo** | **int** | #field_definition# | 
**his_repasse** | **string** | #field_definition# | 
**cod_unid_oper** | **int** | #field_definition# | 
**vlr_antecipacao** | **float** | #field_definition# | 
**cod_unid_oper_tpv** | **int** | #field_definition# | 
**perc_taxa_admin** | **float** | #field_definition# | 
**ind_tef** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


