# ModelEquipamentosUnidades

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_equip_x_unid** | **int** | #field_definition# | [optional] 
**fkcod_unid_negoc** | **int** | #field_definition# | [optional] 
**fkcod_unid_oper** | **int** | #field_definition# | [optional] 
**fkcod_equipamento** | **int** | #field_definition# | [optional] 
**fkcod_prod_exame** | **int** | #field_definition# | [optional] 
**cod_unid_oper_execucao** | **int** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


