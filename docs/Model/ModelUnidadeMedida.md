# ModelUnidadeMedida

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_unid_medida** | **int** | #field_definition# | [optional] 
**nom_unid_medida** | **string** | #field_definition# | [optional] 
**sigla_unid_medida** | **string** | #field_definition# | [optional] 
**dat_exclusao** | **string** | #field_definition# | [optional] 
**ind_util_prescricao** | **string** | #field_definition# | [optional] 
**ind_frm_calculo** | **string** | #field_definition# | [optional] 
**cod_unid_med_dose_calc** | **int** | #field_definition# | [optional] 
**ind_uso_continuo_dia** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


