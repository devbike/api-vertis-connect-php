# ModelOrdemServicoOsMed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_ord_serv_med** | **int** | #field_definition# | [optional] 
**dth_req_exame** | **string** | #field_definition# | 
**fkcod_outro_liquidante** | **int** | #field_definition# | [optional] 
**fkcod_vet_solicitante** | **int** | #field_definition# | [optional] 
**compl_vet_solicitante** | [**\VertisConnect\Model\ModelOrdemServicoOsMedComplVetSolicitante**](ModelOrdemServicoOsMedComplVetSolicitante.md) |  | [optional] 
**fkcod_clinica** | **int** | #field_definition# | 
**clinica** | [**\VertisConnect\Model\ModelOrdemServicoOsMedClinica**](ModelOrdemServicoOsMedClinica.md) |  | [optional] 
**fkcod_proprietario** | **int** | #field_definition# | [optional] 
**tutor** | [**\VertisConnect\Model\ModelOrdemServicoOsMedTutor**](ModelOrdemServicoOsMedTutor.md) |  | [optional] 
**fkcod_animal** | **int** | #field_definition# | 
**animal** | [**\VertisConnect\Model\ModelOrdemServicoOsMedAnimal**](ModelOrdemServicoOsMedAnimal.md) |  | [optional] 
**fkcod_vet_encaminhou** | **int** | #field_definition# | [optional] 
**fkcod_clinica_encaminhou** | **int** | #field_definition# | [optional] 
**fkcod_plano_saude** | **int** | #field_definition# | [optional] 
**fkcod_clin_plano_saude** | **int** | #field_definition# | [optional] 
**fkcod_usu_atend_susp** | **int** | #field_definition# | [optional] 
**compl_exame** | [**\VertisConnect\Model\ModelOrdemServicoOsMedComplExame**](ModelOrdemServicoOsMedComplExame.md) |  | [optional] 
**compl_pet** | [**\VertisConnect\Model\ModelOrdemServicoOsMedComplPet**](ModelOrdemServicoOsMedComplPet.md) |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


