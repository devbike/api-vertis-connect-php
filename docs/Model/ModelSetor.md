# ModelSetor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_setor** | **int** | #field_definition# | [optional] 
**nom_setor** | **string** | #field_definition# | [optional] 
**ord_impressao** | **string** | #field_definition# | [optional] 
**ind_impr_cab** | **string** | #field_definition# | [optional] 
**ind_impr_rod** | **string** | #field_definition# | [optional] 
**cod_sub_setor** | **int** | #field_definition# | [optional] 
**ind_prot_interno** | **string** | #field_definition# | [optional] 
**ind_prot_externo** | **string** | #field_definition# | [optional] 
**ind_imprime_etiqueta** | **string** | #field_definition# | [optional] 
**ind_prioridade** | **int** | #field_definition# | [optional] 
**cod_ccusto** | **int** | #field_definition# | [optional] 
**ind_deposito** | **string** | #field_definition# | [optional] 
**ind_sit_setor** | **string** | #field_definition# | [optional] 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**nom_impressora1** | **string** | #field_definition# | [optional] 
**nom_impressora2** | **string** | #field_definition# | [optional] 
**nom_impressora3** | **string** | #field_definition# | [optional] 
**cod_deposito_padrao_old** | **int** | #field_definition# | [optional] 
**ind_agenda_exames_agrupados** | **string** | #field_definition# | [optional] 
**mod_impressora1** | **string** | #field_definition# | [optional] 
**etq_impressora1** | **string** | #field_definition# | [optional] 
**mod_impressora2** | **string** | #field_definition# | [optional] 
**etq_impressora2** | **string** | #field_definition# | [optional] 
**mod_impressora3** | **string** | #field_definition# | [optional] 
**etq_impressora3** | **string** | #field_definition# | [optional] 
**tmp_max_espera** | **int** | #field_definition# | [optional] 
**ind_laudo_bioquimico** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


