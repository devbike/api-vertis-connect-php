# ModelExameEquipamentos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_equipamento** | **int** | #field_definition# | [optional] 
**cod_equip_interface** | **int** | #field_definition# | [optional] 
**fkcod_unid_negoc** | **int** | #field_definition# | [optional] 
**fkcod_unid_oper** | **int** | #field_definition# | [optional] 
**fkcod_setor** | **int** | #field_definition# | [optional] 
**nom_equipamento** | **string** | #field_definition# | [optional] 
**ind_tip_interface** | **string** | #field_definition# | [optional] 
**id_equipamento** | **string** | #field_definition# | [optional] 
**ind_fmt_interface** | **string** | #field_definition# | [optional] 
**ind_tip_comunicacao** | **string** | #field_definition# | [optional] 
**ind_itf_ativa** | **string** | #field_definition# | [optional] 
**ind_id_amostra_auto** | **string** | #field_definition# | [optional] 
**ind_porta_comunicacao** | **int** | #field_definition# | [optional] 
**ind_baud_rate** | **int** | #field_definition# | [optional] 
**ind_stop_bits** | **int** | #field_definition# | [optional] 
**ind_data_bits** | **int** | #field_definition# | [optional] 
**ind_parity** | **int** | #field_definition# | [optional] 
**ind_flow_control** | **int** | #field_definition# | [optional] 
**end_tcp_equipamento** | **string** | #field_definition# | [optional] 
**cod_porta_comunicacao** | **int** | #field_definition# | [optional] 
**path_envio** | **string** | #field_definition# | [optional] 
**timer_envio** | **int** | #field_definition# | [optional] 
**path_retorno** | **string** | #field_definition# | [optional] 
**timer_retorno** | **int** | #field_definition# | [optional] 
**path_arquivo_pdf** | **string** | #field_definition# | [optional] 
**path_copia_resultados** | **string** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | [optional] 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


