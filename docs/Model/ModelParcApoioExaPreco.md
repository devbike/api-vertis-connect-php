# ModelParcApoioExaPreco

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_prc_exa_apoio** | **int** | #field_definition# | [optional] 
**cod_unid_oper** | **int** | #field_definition# | [optional] 
**cod_apoio** | **int** | #field_definition# | [optional] 
**cod_exame** | **int** | #field_definition# | [optional] 
**dat_vigencia** | **string** | #field_definition# | [optional] 
**vlr_exame** | **float** | #field_definition# | [optional] 
**vlr_exame_urg** | **float** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


