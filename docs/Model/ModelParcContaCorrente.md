# ModelParcContaCorrente

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_parc_conta** | **int** | #field_definition# | [optional] 
**cod_parceiro** | **int** | #field_definition# | [optional] 
**dth_lancamento** | **string** | #field_definition# | [optional] 
**ind_tip_lancamento** | **string** | #field_definition# | [optional] 
**vlr_lancamento** | **float** | #field_definition# | [optional] 
**id_ctrl_pgto** | **int** | #field_definition# | [optional] 
**historico_lcto** | **string** | #field_definition# | [optional] 
**id_senha_cc** | **string** | #field_definition# | [optional] 
**dat_validade** | **string** | #field_definition# | [optional] 
**cod_term_pv** | **int** | #field_definition# | [optional] 
**vlr_dinheiro** | **float** | #field_definition# | [optional] 
**vlr_cheque** | **float** | #field_definition# | [optional] 
**vlr_cartao_credito** | **float** | #field_definition# | [optional] 
**vlr_cartao_debito** | **float** | #field_definition# | [optional] 
**vlr_boleto** | **float** | #field_definition# | [optional] 
**vlr_deposito** | **float** | #field_definition# | [optional] 
**cod_titulo** | **int** | #field_definition# | [optional] 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**cod_usu_exclusao** | **int** | #field_definition# | [optional] 
**desc_motivo_exclusao** | **string** | #field_definition# | [optional] 
**cod_usu_lcto** | **int** | #field_definition# | [optional] 
**vlr_desconto** | **float** | #field_definition# | [optional] 
**cod_parc_conta_orig** | **int** | #field_definition# | [optional] 
**cod_parc_conta_desc** | **int** | #field_definition# | [optional] 
**cod_conta_receber** | **int** | #field_definition# | [optional] 
**vlr_bonus_utiliz** | **float** | #field_definition# | [optional] 
**vlr_baixa_vcto** | **float** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


