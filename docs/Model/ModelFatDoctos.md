# ModelFatDoctos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_fat_doctos** | **int** | #field_definition# | 
**fkid_faturamento** | **int** | #field_definition# | 
**ind_tipo_doc** | **string** | #field_definition# | 
**link_docto** | **string** | #field_definition# | 
**dsc_docto** | **string** | #field_definition# | 
**docto_b64** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


