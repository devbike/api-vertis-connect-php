# ModelFiscal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_parceiro** | **int** | #field_definition# | [optional] 
**ind_tip_parceiro** | **string** | #field_definition# | [optional] 
**ind_sit_rps** | **string** | #field_definition# | [optional] 
**ind_opt_simples** | **string** | #field_definition# | [optional] 
**nom_repr_simples** | **string** | #field_definition# | [optional] 
**fkcod_nat_operacao** | **int** | #field_definition# | [optional] 
**reg_esp_tributacao** | **int** | #field_definition# | [optional] 
**ind_frm_irrfpj** | **string** | #field_definition# | [optional] 
**ind_susp_exig_pis** | **string** | #field_definition# | [optional] 
**dat_susp_exig_pis** | **string** | #field_definition# | [optional] 
**ind_susp_exig_cofins** | **string** | #field_definition# | [optional] 
**dat_susp_exig_cofins** | **string** | #field_definition# | [optional] 
**ind_susp_exig_csll** | **string** | #field_definition# | [optional] 
**dat_susp_exig_csll** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


