# ModelFinContaSaldo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_conta** | **int** | #field_definition# | 
**dat_abert_saldo** | **string** | #field_definition# | 
**vlr_sdo_inicial** | **float** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


