# ModelVetTmpExec

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_veterinario** | **int** | #field_definition# | [optional] 
**cod_exame** | **int** | #field_definition# | [optional] 
**tmp_med_execucao** | **int** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


