# ModelFaturamentoConvenio

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_faturamento** | **int** | #field_definition# | 
**id_unid_oper** | **int** | #field_definition# | 
**dth_emis_fatura** | **string** | #field_definition# | 
**dta_vencimento** | **string** | #field_definition# | 
**dta_liquidacao** | **string** | #field_definition# | 
**nro_fatura** | **int** | #field_definition# | 
**nro_parcela** | **int** | #field_definition# | 
**vlr_fatura** | **float** | #field_definition# | 
**ind_forma_pagto** | **string** | #field_definition# | 
**ind_situacao_fat** | **string** | #field_definition# | 
**id_parceiro** | **int** | #field_definition# | 
**local_unid_oper** | **int** |  | [optional] 
**cod_unid_neg** | **int** |  | [optional] 
**doc_anexos** | [**\VertisConnect\Model\ModelFaturamentoConvenioDocAnexos[]**](ModelFaturamentoConvenioDocAnexos.md) |  | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


