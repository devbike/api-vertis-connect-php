# ModelProdutoKit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_prod_kit** | **int** | #field_definition# | [optional] 
**fkcod_produto_kit** | **int** | #field_definition# | [optional] 
**cod_prod_item_kit** | **int** | #field_definition# | [optional] 
**qtd_utilizada** | **float** | #field_definition# | [optional] 
**fkcod_unid_consumo** | **int** | #field_definition# | [optional] 
**ind_prod_opcional** | **string** | #field_definition# | [optional] 
**dth_inclusao** | **string** | #field_definition# | [optional] 
**dth_exclusao** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


