# ModelOrdemServicoOsMedComplPet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_ord_serv_pet** | **int** | #field_definition# | [optional] 
**nom_proprietario** | **string** | #field_definition# | 
**nom_animal** | **string** | #field_definition# | 
**fkcod_especie** | **int** | #field_definition# | 
**fkcod_raca** | **int** | #field_definition# | 
**dat_aniversario** | **string** | #field_definition# | 
**ind_sexo** | **string** | #field_definition# | 
**ind_castrado** | **string** | #field_definition# | 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


