# ModelMotoboyClinica

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_motoboy** | **int** | #field_definition# | [optional] 
**cod_clinica** | **int** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


