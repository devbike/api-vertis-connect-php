# ModelFornecedorProduto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cod_fornecedor** | **int** | #field_definition# | [optional] 
**cod_produto** | **int** | #field_definition# | [optional] 
**vlr_custo** | **float** | #field_definition# | [optional] 
**dt_cadastro** | **string** | #field_definition# | [optional] 
**recordcount** | **int** | Total de registros | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


