# VertisConnect\ExamesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExame**](ExamesApi.md#createExame) | **POST** /api/V1.1/exames | 
[**deleteExame**](ExamesApi.md#deleteExame) | **DELETE** /api/V1.1/exames/{id} | 
[**findExamesPortal**](ExamesApi.md#findExamesPortal) | **GET** /api/V1.1/exames-portal/{id} | 
[**getExame**](ExamesApi.md#getExame) | **GET** /api/V1.1/exames/{id} | 
[**getExames**](ExamesApi.md#getExames) | **GET** /api/V1.1/exames | 
[**getExamesPortal**](ExamesApi.md#getExamesPortal) | **GET** /api/V1.1/exames-portal | 
[**updateExame**](ExamesApi.md#updateExame) | **PUT** /api/V1.1/exames/{id} | 


# **createExame**
> \VertisConnect\Model\ModelExames createExame($exames)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exames = new \VertisConnect\Model\ModelExames(); // \VertisConnect\Model\ModelExames | Objeto prod_exame_nv

try {
    $result = $apiInstance->createExame($exames);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesApi->createExame: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exames** | [**\VertisConnect\Model\ModelExames**](../Model/ModelExames.md)| Objeto prod_exame_nv |

### Return type

[**\VertisConnect\Model\ModelExames**](../Model/ModelExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteExame**
> \VertisConnect\Model\ModelExames deleteExame($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exames

try {
    $result = $apiInstance->deleteExame($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesApi->deleteExame: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exames |

### Return type

[**\VertisConnect\Model\ModelExames**](../Model/ModelExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **findExamesPortal**
> \VertisConnect\Model\ModelExames[] findExamesPortal($id)



Retorna todos os exames disponíveis para serem solicitados através do portal

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->findExamesPortal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesApi->findExamesPortal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelExames[]**](../Model/ModelExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExame**
> \VertisConnect\Model\ModelExames getExame($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exames

try {
    $result = $apiInstance->getExame($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesApi->getExame: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exames |

### Return type

[**\VertisConnect\Model\ModelExames**](../Model/ModelExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExames**
> \VertisConnect\Model\ModelExames[] getExames()



Retorna registros do objeto prod_exame_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExames();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesApi->getExames: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelExames[]**](../Model/ModelExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExamesPortal**
> \VertisConnect\Model\ModelExames[] getExamesPortal()



Retorna todos os exames disponíveis para serem solicitados através do portal

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExamesPortal();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesApi->getExamesPortal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelExames[]**](../Model/ModelExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExame**
> \VertisConnect\Model\ModelExames updateExame($exames, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exames = new \VertisConnect\Model\ModelExames(); // \VertisConnect\Model\ModelExames | Objeto prod_exame_nv
$id = 56; // int | ID do Exames

try {
    $result = $apiInstance->updateExame($exames, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesApi->updateExame: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exames** | [**\VertisConnect\Model\ModelExames**](../Model/ModelExames.md)| Objeto prod_exame_nv |
 **id** | **int**| ID do Exames |

### Return type

[**\VertisConnect\Model\ModelExames**](../Model/ModelExames.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

