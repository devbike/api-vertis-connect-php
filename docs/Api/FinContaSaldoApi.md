# VertisConnect\FinContaSaldoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createfincontasaldo**](FinContaSaldoApi.md#createfincontasaldo) | **POST** /api/V1.1/fin-contas-saldo | 
[**deletefincontasaldo**](FinContaSaldoApi.md#deletefincontasaldo) | **DELETE** /api/V1.1/fin-contas-saldo/{id} | 
[**getfincontasaldo**](FinContaSaldoApi.md#getfincontasaldo) | **GET** /api/V1.1/fin-contas-saldo/{id} | 
[**getfincontasaldos**](FinContaSaldoApi.md#getfincontasaldos) | **GET** /api/V1.1/fin-contas-saldo | 
[**updatefincontasaldo**](FinContaSaldoApi.md#updatefincontasaldo) | **PUT** /api/V1.1/fin-contas-saldo/{id} | 


# **createfincontasaldo**
> \VertisConnect\Model\ModelFinContaSaldo createfincontasaldo($fin_conta_saldo)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaSaldoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_conta_saldo = new \VertisConnect\Model\ModelFinContaSaldo(); // \VertisConnect\Model\ModelFinContaSaldo | Objeto fin_conta_saldo

try {
    $result = $apiInstance->createfincontasaldo($fin_conta_saldo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaSaldoApi->createfincontasaldo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_conta_saldo** | [**\VertisConnect\Model\ModelFinContaSaldo**](../Model/ModelFinContaSaldo.md)| Objeto fin_conta_saldo |

### Return type

[**\VertisConnect\Model\ModelFinContaSaldo**](../Model/ModelFinContaSaldo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletefincontasaldo**
> \VertisConnect\Model\ModelFinContaSaldo deletefincontasaldo($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaSaldoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinContaSaldo

try {
    $result = $apiInstance->deletefincontasaldo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaSaldoApi->deletefincontasaldo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinContaSaldo |

### Return type

[**\VertisConnect\Model\ModelFinContaSaldo**](../Model/ModelFinContaSaldo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfincontasaldo**
> \VertisConnect\Model\ModelFinContaSaldo getfincontasaldo($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaSaldoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinContaSaldo

try {
    $result = $apiInstance->getfincontasaldo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaSaldoApi->getfincontasaldo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinContaSaldo |

### Return type

[**\VertisConnect\Model\ModelFinContaSaldo**](../Model/ModelFinContaSaldo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfincontasaldos**
> \VertisConnect\Model\ModelFinContaSaldo[] getfincontasaldos()



Retorna registros do objeto fin_conta_saldo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaSaldoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getfincontasaldos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaSaldoApi->getfincontasaldos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelFinContaSaldo[]**](../Model/ModelFinContaSaldo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatefincontasaldo**
> \VertisConnect\Model\ModelFinContaSaldo updatefincontasaldo($fin_conta_saldo, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaSaldoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_conta_saldo = new \VertisConnect\Model\ModelFinContaSaldo(); // \VertisConnect\Model\ModelFinContaSaldo | Objeto fin_conta_saldo
$id = 56; // int | ID do FinContaSaldo

try {
    $result = $apiInstance->updatefincontasaldo($fin_conta_saldo, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaSaldoApi->updatefincontasaldo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_conta_saldo** | [**\VertisConnect\Model\ModelFinContaSaldo**](../Model/ModelFinContaSaldo.md)| Objeto fin_conta_saldo |
 **id** | **int**| ID do FinContaSaldo |

### Return type

[**\VertisConnect\Model\ModelFinContaSaldo**](../Model/ModelFinContaSaldo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

