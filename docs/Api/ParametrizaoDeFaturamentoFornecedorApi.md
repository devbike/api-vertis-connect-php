# VertisConnect\ParametrizaoDeFaturamentoFornecedorApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfFatForn**](ParametrizaoDeFaturamentoFornecedorApi.md#doCreateInfFatForn) | **POST** /api/V1.1/fornecedores/{id}/paramsfat | 
[**doDeleteInfFatFor**](ParametrizaoDeFaturamentoFornecedorApi.md#doDeleteInfFatFor) | **DELETE** /api/V1.1/fornecedores/{id}/paramsfat | 
[**doGetInfFatForn**](ParametrizaoDeFaturamentoFornecedorApi.md#doGetInfFatForn) | **GET** /api/V1.1/fornecedores/{id}/paramsfat | 
[**doUpdateInfFatForn**](ParametrizaoDeFaturamentoFornecedorApi.md#doUpdateInfFatForn) | **PUT** /api/V1.1/fornecedores/{id}/paramsfat | 


# **doCreateInfFatForn**
> \VertisConnect\Model\ModelParcInfFaturamento doCreateInfFatForn($parametrizao_de_faturamento, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_de_faturamento = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID do Fornecedor

try {
    $result = $apiInstance->doCreateInfFatForn($parametrizao_de_faturamento, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoFornecedorApi->doCreateInfFatForn: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_de_faturamento** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID do Fornecedor |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfFatFor**
> \VertisConnect\Model\ModelParcInfFaturamento doDeleteInfFatFor($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfFatFor($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoFornecedorApi->doDeleteInfFatFor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfFatForn**
> \VertisConnect\Model\ModelParcInfFaturamento doGetInfFatForn($id)



Retorna configurações de faturamento para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfFatForn($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoFornecedorApi->doGetInfFatForn: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfFatForn**
> \VertisConnect\Model\ModelParcInfFaturamento doUpdateInfFatForn($parametros_fiscais___fornecedor, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___fornecedor = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfFatForn($parametros_fiscais___fornecedor, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoFornecedorApi->doUpdateInfFatForn: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___fornecedor** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

