# VertisConnect\MedicamentosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMedicamento**](MedicamentosApi.md#createMedicamento) | **POST** /api/V1.1/medicamentos | 
[**deleteMedicamento**](MedicamentosApi.md#deleteMedicamento) | **DELETE** /api/V1.1/medicamentos/{id} | 
[**getMedicamento**](MedicamentosApi.md#getMedicamento) | **GET** /api/V1.1/medicamentos/{id} | 
[**getMedicamentos**](MedicamentosApi.md#getMedicamentos) | **GET** /api/V1.1/medicamentos | 
[**updateMedicamento**](MedicamentosApi.md#updateMedicamento) | **PUT** /api/V1.1/medicamentos/{id} | 


# **createMedicamento**
> \VertisConnect\Model\ModelMedicamentos createMedicamento($medicamentos)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MedicamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$medicamentos = new \VertisConnect\Model\ModelMedicamentos(); // \VertisConnect\Model\ModelMedicamentos | Objeto prod_medicamento_nv

try {
    $result = $apiInstance->createMedicamento($medicamentos);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MedicamentosApi->createMedicamento: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **medicamentos** | [**\VertisConnect\Model\ModelMedicamentos**](../Model/ModelMedicamentos.md)| Objeto prod_medicamento_nv |

### Return type

[**\VertisConnect\Model\ModelMedicamentos**](../Model/ModelMedicamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMedicamento**
> \VertisConnect\Model\ModelMedicamentos deleteMedicamento($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MedicamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Medicamentos

try {
    $result = $apiInstance->deleteMedicamento($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MedicamentosApi->deleteMedicamento: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Medicamentos |

### Return type

[**\VertisConnect\Model\ModelMedicamentos**](../Model/ModelMedicamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMedicamento**
> \VertisConnect\Model\ModelMedicamentos getMedicamento($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MedicamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Medicamentos

try {
    $result = $apiInstance->getMedicamento($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MedicamentosApi->getMedicamento: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Medicamentos |

### Return type

[**\VertisConnect\Model\ModelMedicamentos**](../Model/ModelMedicamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMedicamentos**
> \VertisConnect\Model\ModelMedicamentos[] getMedicamentos()



Retorna registros do objeto prod_medicamento_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MedicamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getMedicamentos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MedicamentosApi->getMedicamentos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelMedicamentos[]**](../Model/ModelMedicamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateMedicamento**
> \VertisConnect\Model\ModelMedicamentos updateMedicamento($medicamentos, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MedicamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$medicamentos = new \VertisConnect\Model\ModelMedicamentos(); // \VertisConnect\Model\ModelMedicamentos | Objeto prod_medicamento_nv
$id = 56; // int | ID do Medicamentos

try {
    $result = $apiInstance->updateMedicamento($medicamentos, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MedicamentosApi->updateMedicamento: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **medicamentos** | [**\VertisConnect\Model\ModelMedicamentos**](../Model/ModelMedicamentos.md)| Objeto prod_medicamento_nv |
 **id** | **int**| ID do Medicamentos |

### Return type

[**\VertisConnect\Model\ModelMedicamentos**](../Model/ModelMedicamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

