# VertisConnect\UnidadesDeMedidaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUnidadeMedida**](UnidadesDeMedidaApi.md#createUnidadeMedida) | **POST** /api/V1.1/unidades_medida | 
[**deleteUnidadeMedida**](UnidadesDeMedidaApi.md#deleteUnidadeMedida) | **DELETE** /api/V1.1/unidades_medida/{id} | 
[**getUnidadeMedida**](UnidadesDeMedidaApi.md#getUnidadeMedida) | **GET** /api/V1.1/unidades_medida/{id} | 
[**getUnidadeMedidas**](UnidadesDeMedidaApi.md#getUnidadeMedidas) | **GET** /api/V1.1/unidades_medida | 
[**updateUnidadeMedida**](UnidadesDeMedidaApi.md#updateUnidadeMedida) | **PUT** /api/V1.1/unidades_medida/{id} | 


# **createUnidadeMedida**
> \VertisConnect\Model\ModelUnidadeMedida createUnidadeMedida($unidades_de_medida)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadesDeMedidaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidades_de_medida = new \VertisConnect\Model\ModelUnidadeMedida(); // \VertisConnect\Model\ModelUnidadeMedida | Objeto prod_unid_medida

try {
    $result = $apiInstance->createUnidadeMedida($unidades_de_medida);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadesDeMedidaApi->createUnidadeMedida: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidades_de_medida** | [**\VertisConnect\Model\ModelUnidadeMedida**](../Model/ModelUnidadeMedida.md)| Objeto prod_unid_medida |

### Return type

[**\VertisConnect\Model\ModelUnidadeMedida**](../Model/ModelUnidadeMedida.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUnidadeMedida**
> \VertisConnect\Model\ModelUnidadeMedida deleteUnidadeMedida($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadesDeMedidaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unidades de Medida

try {
    $result = $apiInstance->deleteUnidadeMedida($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadesDeMedidaApi->deleteUnidadeMedida: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unidades de Medida |

### Return type

[**\VertisConnect\Model\ModelUnidadeMedida**](../Model/ModelUnidadeMedida.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadeMedida**
> \VertisConnect\Model\ModelUnidadeMedida getUnidadeMedida($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadesDeMedidaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unidades de Medida

try {
    $result = $apiInstance->getUnidadeMedida($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadesDeMedidaApi->getUnidadeMedida: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unidades de Medida |

### Return type

[**\VertisConnect\Model\ModelUnidadeMedida**](../Model/ModelUnidadeMedida.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadeMedidas**
> \VertisConnect\Model\ModelUnidadeMedida[] getUnidadeMedidas()



Retorna registros do objeto prod_unid_medida

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadesDeMedidaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getUnidadeMedidas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadesDeMedidaApi->getUnidadeMedidas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelUnidadeMedida[]**](../Model/ModelUnidadeMedida.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUnidadeMedida**
> \VertisConnect\Model\ModelUnidadeMedida updateUnidadeMedida($unidades_de_medida, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadesDeMedidaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidades_de_medida = new \VertisConnect\Model\ModelUnidadeMedida(); // \VertisConnect\Model\ModelUnidadeMedida | Objeto prod_unid_medida
$id = 56; // int | ID do Unidades de Medida

try {
    $result = $apiInstance->updateUnidadeMedida($unidades_de_medida, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadesDeMedidaApi->updateUnidadeMedida: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidades_de_medida** | [**\VertisConnect\Model\ModelUnidadeMedida**](../Model/ModelUnidadeMedida.md)| Objeto prod_unid_medida |
 **id** | **int**| ID do Unidades de Medida |

### Return type

[**\VertisConnect\Model\ModelUnidadeMedida**](../Model/ModelUnidadeMedida.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

