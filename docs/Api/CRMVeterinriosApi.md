# VertisConnect\CRMVeterinriosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCRMVeterinario**](CRMVeterinriosApi.md#createCRMVeterinario) | **POST** /api/V1.1/crmveterinarios | 
[**deleteCRMVeterinario**](CRMVeterinriosApi.md#deleteCRMVeterinario) | **DELETE** /api/V1.1/crmveterinarios/{id} | 
[**getCRMVeterinario**](CRMVeterinriosApi.md#getCRMVeterinario) | **GET** /api/V1.1/crmveterinarios/{id} | 
[**getCRMVeterinarios**](CRMVeterinriosApi.md#getCRMVeterinarios) | **GET** /api/V1.1/crmveterinarios | 
[**updateCRMVeterinario**](CRMVeterinriosApi.md#updateCRMVeterinario) | **PUT** /api/V1.1/crmveterinarios/{id} | 


# **createCRMVeterinario**
> \VertisConnect\Model\ModelCRMVeterinarios createCRMVeterinario($crm_veterinrios)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CRMVeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$crm_veterinrios = new \VertisConnect\Model\ModelCRMVeterinarios(); // \VertisConnect\Model\ModelCRMVeterinarios | Objeto parc_vet_orgao_classe

try {
    $result = $apiInstance->createCRMVeterinario($crm_veterinrios);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CRMVeterinriosApi->createCRMVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **crm_veterinrios** | [**\VertisConnect\Model\ModelCRMVeterinarios**](../Model/ModelCRMVeterinarios.md)| Objeto parc_vet_orgao_classe |

### Return type

[**\VertisConnect\Model\ModelCRMVeterinarios**](../Model/ModelCRMVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCRMVeterinario**
> \VertisConnect\Model\ModelCRMVeterinarios deleteCRMVeterinario($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CRMVeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do CRM Veterinários

try {
    $result = $apiInstance->deleteCRMVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CRMVeterinriosApi->deleteCRMVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do CRM Veterinários |

### Return type

[**\VertisConnect\Model\ModelCRMVeterinarios**](../Model/ModelCRMVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCRMVeterinario**
> \VertisConnect\Model\ModelCRMVeterinarios getCRMVeterinario($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CRMVeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do CRM Veterinários

try {
    $result = $apiInstance->getCRMVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CRMVeterinriosApi->getCRMVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do CRM Veterinários |

### Return type

[**\VertisConnect\Model\ModelCRMVeterinarios**](../Model/ModelCRMVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCRMVeterinarios**
> \VertisConnect\Model\ModelCRMVeterinarios[] getCRMVeterinarios()



Retorna registros do objeto parc_vet_orgao_classe

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CRMVeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getCRMVeterinarios();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CRMVeterinriosApi->getCRMVeterinarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelCRMVeterinarios[]**](../Model/ModelCRMVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCRMVeterinario**
> \VertisConnect\Model\ModelCRMVeterinarios updateCRMVeterinario($crm_veterinrios, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CRMVeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$crm_veterinrios = new \VertisConnect\Model\ModelCRMVeterinarios(); // \VertisConnect\Model\ModelCRMVeterinarios | Objeto parc_vet_orgao_classe
$id = 56; // int | ID do CRM Veterinários

try {
    $result = $apiInstance->updateCRMVeterinario($crm_veterinrios, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CRMVeterinriosApi->updateCRMVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **crm_veterinrios** | [**\VertisConnect\Model\ModelCRMVeterinarios**](../Model/ModelCRMVeterinarios.md)| Objeto parc_vet_orgao_classe |
 **id** | **int**| ID do CRM Veterinários |

### Return type

[**\VertisConnect\Model\ModelCRMVeterinarios**](../Model/ModelCRMVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

