# VertisConnect\AntibioticosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExameAntibiotico**](AntibioticosApi.md#createExameAntibiotico) | **POST** /api/V1.1/exameantibioticos | 
[**deleteExameAntibiotico**](AntibioticosApi.md#deleteExameAntibiotico) | **DELETE** /api/V1.1/exameantibioticos/{id} | 
[**getExameAntibiotico**](AntibioticosApi.md#getExameAntibiotico) | **GET** /api/V1.1/exameantibioticos/{id} | 
[**getExameAntibioticos**](AntibioticosApi.md#getExameAntibioticos) | **GET** /api/V1.1/exameantibioticos | 
[**updateExameAntibiotico**](AntibioticosApi.md#updateExameAntibiotico) | **PUT** /api/V1.1/exameantibioticos/{id} | 


# **createExameAntibiotico**
> \VertisConnect\Model\ModelExameAntibiotico createExameAntibiotico($antibioticos)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AntibioticosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$antibioticos = new \VertisConnect\Model\ModelExameAntibiotico(); // \VertisConnect\Model\ModelExameAntibiotico | Objeto prod_exa_antibiotico_nv

try {
    $result = $apiInstance->createExameAntibiotico($antibioticos);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AntibioticosApi->createExameAntibiotico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **antibioticos** | [**\VertisConnect\Model\ModelExameAntibiotico**](../Model/ModelExameAntibiotico.md)| Objeto prod_exa_antibiotico_nv |

### Return type

[**\VertisConnect\Model\ModelExameAntibiotico**](../Model/ModelExameAntibiotico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteExameAntibiotico**
> \VertisConnect\Model\ModelExameAntibiotico deleteExameAntibiotico($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AntibioticosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Antibioticos

try {
    $result = $apiInstance->deleteExameAntibiotico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AntibioticosApi->deleteExameAntibiotico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Antibioticos |

### Return type

[**\VertisConnect\Model\ModelExameAntibiotico**](../Model/ModelExameAntibiotico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameAntibiotico**
> \VertisConnect\Model\ModelExameAntibiotico getExameAntibiotico($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AntibioticosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Antibioticos

try {
    $result = $apiInstance->getExameAntibiotico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AntibioticosApi->getExameAntibiotico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Antibioticos |

### Return type

[**\VertisConnect\Model\ModelExameAntibiotico**](../Model/ModelExameAntibiotico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameAntibioticos**
> \VertisConnect\Model\ModelExameAntibiotico[] getExameAntibioticos()



Retorna registros do objeto prod_exa_antibiotico_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AntibioticosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExameAntibioticos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AntibioticosApi->getExameAntibioticos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelExameAntibiotico[]**](../Model/ModelExameAntibiotico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExameAntibiotico**
> \VertisConnect\Model\ModelExameAntibiotico updateExameAntibiotico($antibioticos, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AntibioticosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$antibioticos = new \VertisConnect\Model\ModelExameAntibiotico(); // \VertisConnect\Model\ModelExameAntibiotico | Objeto prod_exa_antibiotico_nv
$id = 56; // int | ID do Antibioticos

try {
    $result = $apiInstance->updateExameAntibiotico($antibioticos, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AntibioticosApi->updateExameAntibiotico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **antibioticos** | [**\VertisConnect\Model\ModelExameAntibiotico**](../Model/ModelExameAntibiotico.md)| Objeto prod_exa_antibiotico_nv |
 **id** | **int**| ID do Antibioticos |

### Return type

[**\VertisConnect\Model\ModelExameAntibiotico**](../Model/ModelExameAntibiotico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

