# VertisConnect\AnimaisXClinicasIdAnimalclnicasApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAnimalXClinicas**](AnimaisXClinicasIdAnimalclnicasApi.md#getAnimalXClinicas) | **GET** /api/V1.1/animais_x_clinicas/{idAnimal}/clinicas | 


# **getAnimalXClinicas**
> \VertisConnect\Model\ModelAnimalXClinica getAnimalXClinicas($id_animal)



Retorna informações de todos as clínicas de um animal.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisXClinicasIdAnimalclnicasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id_animal = 56; // int | ID do Animal

try {
    $result = $apiInstance->getAnimalXClinicas($id_animal);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisXClinicasIdAnimalclnicasApi->getAnimalXClinicas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_animal** | **int**| ID do Animal |

### Return type

[**\VertisConnect\Model\ModelAnimalXClinica**](../Model/ModelAnimalXClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

