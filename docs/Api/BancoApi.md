# VertisConnect\BancoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBanco**](BancoApi.md#createBanco) | **POST** /api/V1.1/bancos | 
[**deleteBanco**](BancoApi.md#deleteBanco) | **DELETE** /api/V1.1/bancos/{id} | 
[**getBanco**](BancoApi.md#getBanco) | **GET** /api/V1.1/bancos/{id} | 
[**getBancos**](BancoApi.md#getBancos) | **GET** /api/V1.1/bancos | 
[**updateBanco**](BancoApi.md#updateBanco) | **PUT** /api/V1.1/bancos/{id} | 


# **createBanco**
> \VertisConnect\Model\ModelBanco createBanco($banco)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\BancoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$banco = new \VertisConnect\Model\ModelBanco(); // \VertisConnect\Model\ModelBanco | Objeto banco

try {
    $result = $apiInstance->createBanco($banco);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BancoApi->createBanco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **banco** | [**\VertisConnect\Model\ModelBanco**](../Model/ModelBanco.md)| Objeto banco |

### Return type

[**\VertisConnect\Model\ModelBanco**](../Model/ModelBanco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBanco**
> \VertisConnect\Model\ModelBanco deleteBanco($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\BancoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Banco

try {
    $result = $apiInstance->deleteBanco($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BancoApi->deleteBanco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Banco |

### Return type

[**\VertisConnect\Model\ModelBanco**](../Model/ModelBanco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBanco**
> \VertisConnect\Model\ModelBanco getBanco($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\BancoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Banco

try {
    $result = $apiInstance->getBanco($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BancoApi->getBanco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Banco |

### Return type

[**\VertisConnect\Model\ModelBanco**](../Model/ModelBanco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBancos**
> \VertisConnect\Model\ModelBanco[] getBancos()



Retorna registros do objeto banco

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\BancoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getBancos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BancoApi->getBancos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelBanco[]**](../Model/ModelBanco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBanco**
> \VertisConnect\Model\ModelBanco updateBanco($banco, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\BancoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$banco = new \VertisConnect\Model\ModelBanco(); // \VertisConnect\Model\ModelBanco | Objeto banco
$id = 56; // int | ID do Banco

try {
    $result = $apiInstance->updateBanco($banco, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BancoApi->updateBanco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **banco** | [**\VertisConnect\Model\ModelBanco**](../Model/ModelBanco.md)| Objeto banco |
 **id** | **int**| ID do Banco |

### Return type

[**\VertisConnect\Model\ModelBanco**](../Model/ModelBanco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

