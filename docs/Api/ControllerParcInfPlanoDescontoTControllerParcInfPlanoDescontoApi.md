# VertisConnect\ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11PropriedadesIdPlanosDescontoDelete**](ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi.md#apiV11PropriedadesIdPlanosDescontoDelete) | **DELETE** /api/V1.1/propriedades/{id}/planos-desconto | 
[**apiV11PropriedadesIdPlanosDescontoGet**](ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi.md#apiV11PropriedadesIdPlanosDescontoGet) | **GET** /api/V1.1/propriedades/{id}/planos-desconto | 
[**apiV11PropriedadesIdPlanosDescontoPost**](ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi.md#apiV11PropriedadesIdPlanosDescontoPost) | **POST** /api/V1.1/propriedades/{id}/planos-desconto | 
[**apiV11PropriedadesIdPlanosDescontoPut**](ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi.md#apiV11PropriedadesIdPlanosDescontoPut) | **PUT** /api/V1.1/propriedades/{id}/planos-desconto | 


# **apiV11PropriedadesIdPlanosDescontoDelete**
> apiV11PropriedadesIdPlanosDescontoDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PropriedadesIdPlanosDescontoDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi->apiV11PropriedadesIdPlanosDescontoDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PropriedadesIdPlanosDescontoGet**
> apiV11PropriedadesIdPlanosDescontoGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PropriedadesIdPlanosDescontoGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi->apiV11PropriedadesIdPlanosDescontoGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PropriedadesIdPlanosDescontoPost**
> apiV11PropriedadesIdPlanosDescontoPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11PropriedadesIdPlanosDescontoPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi->apiV11PropriedadesIdPlanosDescontoPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PropriedadesIdPlanosDescontoPut**
> apiV11PropriedadesIdPlanosDescontoPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PropriedadesIdPlanosDescontoPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcInfPlanoDescontoTControllerParcInfPlanoDescontoApi->apiV11PropriedadesIdPlanosDescontoPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

