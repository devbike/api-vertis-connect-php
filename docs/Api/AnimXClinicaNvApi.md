# VertisConnect\AnimXClinicaNvApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAnimalXClinica**](AnimXClinicaNvApi.md#deleteAnimalXClinica) | **DELETE** /api/V1.1/animais_x_clinicas/{id} | 
[**updateAnimalXClinica**](AnimXClinicaNvApi.md#updateAnimalXClinica) | **PUT** /api/V1.1/animais_x_clinicas/{id} | 


# **deleteAnimalXClinica**
> \VertisConnect\Model\ModelAnimalXClinica deleteAnimalXClinica($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimXClinicaNvApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animal_x_Clinica

try {
    $result = $apiInstance->deleteAnimalXClinica($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimXClinicaNvApi->deleteAnimalXClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animal_x_Clinica |

### Return type

[**\VertisConnect\Model\ModelAnimalXClinica**](../Model/ModelAnimalXClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAnimalXClinica**
> \VertisConnect\Model\ModelAnimalXClinica updateAnimalXClinica($anim_x_clinica_nv, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimXClinicaNvApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$anim_x_clinica_nv = new \VertisConnect\Model\ModelAnimalXClinica(); // \VertisConnect\Model\ModelAnimalXClinica | Objeto anim_x_clinica_nv
$id = 56; // int | ID do Animal_x_Clinica

try {
    $result = $apiInstance->updateAnimalXClinica($anim_x_clinica_nv, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimXClinicaNvApi->updateAnimalXClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anim_x_clinica_nv** | [**\VertisConnect\Model\ModelAnimalXClinica**](../Model/ModelAnimalXClinica.md)| Objeto anim_x_clinica_nv |
 **id** | **int**| ID do Animal_x_Clinica |

### Return type

[**\VertisConnect\Model\ModelAnimalXClinica**](../Model/ModelAnimalXClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

