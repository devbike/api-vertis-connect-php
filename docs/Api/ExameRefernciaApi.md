# VertisConnect\ExameRefernciaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExameReferencia**](ExameRefernciaApi.md#createExameReferencia) | **POST** /api/V1.1/examereferencia | 
[**deleteExameReferencia**](ExameRefernciaApi.md#deleteExameReferencia) | **DELETE** /api/V1.1/examereferencia/{id} | 
[**getExameReferencia**](ExameRefernciaApi.md#getExameReferencia) | **GET** /api/V1.1/examereferencia/{id} | 
[**getExameReferencias**](ExameRefernciaApi.md#getExameReferencias) | **GET** /api/V1.1/examereferencia | 
[**updateExameReferencia**](ExameRefernciaApi.md#updateExameReferencia) | **PUT** /api/V1.1/examereferencia/{id} | 


# **createExameReferencia**
> \VertisConnect\Model\ModelExameReferencia createExameReferencia($exame_referncia)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameRefernciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exame_referncia = new \VertisConnect\Model\ModelExameReferencia(); // \VertisConnect\Model\ModelExameReferencia | Objeto prod_exa_referencia_nv

try {
    $result = $apiInstance->createExameReferencia($exame_referncia);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameRefernciaApi->createExameReferencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exame_referncia** | [**\VertisConnect\Model\ModelExameReferencia**](../Model/ModelExameReferencia.md)| Objeto prod_exa_referencia_nv |

### Return type

[**\VertisConnect\Model\ModelExameReferencia**](../Model/ModelExameReferencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteExameReferencia**
> \VertisConnect\Model\ModelExameReferencia deleteExameReferencia($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameRefernciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame Referência

try {
    $result = $apiInstance->deleteExameReferencia($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameRefernciaApi->deleteExameReferencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame Referência |

### Return type

[**\VertisConnect\Model\ModelExameReferencia**](../Model/ModelExameReferencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameReferencia**
> \VertisConnect\Model\ModelExameReferencia getExameReferencia($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameRefernciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame Referência

try {
    $result = $apiInstance->getExameReferencia($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameRefernciaApi->getExameReferencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame Referência |

### Return type

[**\VertisConnect\Model\ModelExameReferencia**](../Model/ModelExameReferencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameReferencias**
> \VertisConnect\Model\ModelExameReferencia[] getExameReferencias()



Retorna registros do objeto prod_exa_referencia_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameRefernciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExameReferencias();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameRefernciaApi->getExameReferencias: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelExameReferencia[]**](../Model/ModelExameReferencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExameReferencia**
> \VertisConnect\Model\ModelExameReferencia updateExameReferencia($exame_referncia, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameRefernciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exame_referncia = new \VertisConnect\Model\ModelExameReferencia(); // \VertisConnect\Model\ModelExameReferencia | Objeto prod_exa_referencia_nv
$id = 56; // int | ID do Exame Referência

try {
    $result = $apiInstance->updateExameReferencia($exame_referncia, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameRefernciaApi->updateExameReferencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exame_referncia** | [**\VertisConnect\Model\ModelExameReferencia**](../Model/ModelExameReferencia.md)| Objeto prod_exa_referencia_nv |
 **id** | **int**| ID do Exame Referência |

### Return type

[**\VertisConnect\Model\ModelExameReferencia**](../Model/ModelExameReferencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

