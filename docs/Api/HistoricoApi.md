# VertisConnect\HistoricoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createParcHistorico**](HistoricoApi.md#createParcHistorico) | **POST** /api/V1.1/parc-historicos | 
[**deleteParcHistorico**](HistoricoApi.md#deleteParcHistorico) | **DELETE** /api/V1.1/parc-historicos/{id} | 
[**getParcHistorico**](HistoricoApi.md#getParcHistorico) | **GET** /api/V1.1/parc-historicos/{id} | 
[**getParcHistoricos**](HistoricoApi.md#getParcHistoricos) | **GET** /api/V1.1/parc-historicos | 
[**updateParcHistorico**](HistoricoApi.md#updateParcHistorico) | **PUT** /api/V1.1/parc-historicos/{id} | 


# **createParcHistorico**
> \VertisConnect\Model\ModelParcHistorico createParcHistorico($historico)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\HistoricoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$historico = new \VertisConnect\Model\ModelParcHistorico(); // \VertisConnect\Model\ModelParcHistorico | Objeto parc_historico

try {
    $result = $apiInstance->createParcHistorico($historico);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HistoricoApi->createParcHistorico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **historico** | [**\VertisConnect\Model\ModelParcHistorico**](../Model/ModelParcHistorico.md)| Objeto parc_historico |

### Return type

[**\VertisConnect\Model\ModelParcHistorico**](../Model/ModelParcHistorico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteParcHistorico**
> \VertisConnect\Model\ModelParcHistorico deleteParcHistorico($id)



Exclui o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\HistoricoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->deleteParcHistorico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HistoricoApi->deleteParcHistorico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcHistorico**](../Model/ModelParcHistorico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParcHistorico**
> \VertisConnect\Model\ModelParcHistorico getParcHistorico($id)



Retorna informações de um registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\HistoricoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do regoistro

try {
    $result = $apiInstance->getParcHistorico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HistoricoApi->getParcHistorico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do regoistro |

### Return type

[**\VertisConnect\Model\ModelParcHistorico**](../Model/ModelParcHistorico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParcHistoricos**
> \VertisConnect\Model\ModelParcHistorico[] getParcHistoricos()



Lista todo historico.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\HistoricoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getParcHistoricos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HistoricoApi->getParcHistoricos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelParcHistorico[]**](../Model/ModelParcHistorico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateParcHistorico**
> \VertisConnect\Model\ModelParcHistorico updateParcHistorico($historico, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\HistoricoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$historico = new \VertisConnect\Model\ModelParcHistorico(); // \VertisConnect\Model\ModelParcHistorico | Objeto parc_historico
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateParcHistorico($historico, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HistoricoApi->updateParcHistorico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **historico** | [**\VertisConnect\Model\ModelParcHistorico**](../Model/ModelParcHistorico.md)| Objeto parc_historico |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcHistorico**](../Model/ModelParcHistorico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

