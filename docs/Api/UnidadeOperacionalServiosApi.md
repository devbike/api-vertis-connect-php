# VertisConnect\UnidadeOperacionalServiosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUnidOperServicos**](UnidadeOperacionalServiosApi.md#createUnidOperServicos) | **POST** /api/V1.1/unidoper_servicos/{uo} | 
[**deleteUnidOperServicos**](UnidadeOperacionalServiosApi.md#deleteUnidOperServicos) | **DELETE** /api/V1.1/unidoper_servicos/{uo}/produtos/{id} | 
[**getUnidOperServicos**](UnidadeOperacionalServiosApi.md#getUnidOperServicos) | **GET** /api/V1.1/unidoper_servicos/{uo}/produtos/{id} | 
[**getUnidsOperServicos**](UnidadeOperacionalServiosApi.md#getUnidsOperServicos) | **GET** /api/V1.1/unidoper_servicos/{uo}/produtos | 
[**updateUnidOperServicos**](UnidadeOperacionalServiosApi.md#updateUnidOperServicos) | **PUT** /api/V1.1/unidoper_servicos/{uo}/produtos/{id} | 


# **createUnidOperServicos**
> \VertisConnect\Model\ModelUnidOperServicos createUnidOperServicos($unidade_operacional___servios, $uo)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidade_operacional___servios = new \VertisConnect\Model\ModelUnidOperServicos(); // \VertisConnect\Model\ModelUnidOperServicos | Objeto unid_oper_servicos
$uo = 56; // int | 

try {
    $result = $apiInstance->createUnidOperServicos($unidade_operacional___servios, $uo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalServiosApi->createUnidOperServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidade_operacional___servios** | [**\VertisConnect\Model\ModelUnidOperServicos**](../Model/ModelUnidOperServicos.md)| Objeto unid_oper_servicos |
 **uo** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelUnidOperServicos**](../Model/ModelUnidOperServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUnidOperServicos**
> \VertisConnect\Model\ModelUnidOperServicos deleteUnidOperServicos($id, $uo)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unid Oper Serviços
$uo = 56; // int | 

try {
    $result = $apiInstance->deleteUnidOperServicos($id, $uo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalServiosApi->deleteUnidOperServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unid Oper Serviços |
 **uo** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelUnidOperServicos**](../Model/ModelUnidOperServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidOperServicos**
> \VertisConnect\Model\ModelUnidOperServicos getUnidOperServicos($id, $uo)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unid Oper Serviços
$uo = 56; // int | 

try {
    $result = $apiInstance->getUnidOperServicos($id, $uo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalServiosApi->getUnidOperServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unid Oper Serviços |
 **uo** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelUnidOperServicos**](../Model/ModelUnidOperServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidsOperServicos**
> \VertisConnect\Model\ModelUnidOperServicos[] getUnidsOperServicos($uo)



Retorna registros do objeto unid_oper_servicos

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uo = 56; // int | 

try {
    $result = $apiInstance->getUnidsOperServicos($uo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalServiosApi->getUnidsOperServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uo** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelUnidOperServicos[]**](../Model/ModelUnidOperServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUnidOperServicos**
> \VertisConnect\Model\ModelUnidOperServicos updateUnidOperServicos($id, $unidade_operacional___servios, $uo)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unid Oper Serviços
$unidade_operacional___servios = new \VertisConnect\Model\ModelUnidOperServicos(); // \VertisConnect\Model\ModelUnidOperServicos | Objeto unid_oper_servicos
$uo = 56; // int | 

try {
    $result = $apiInstance->updateUnidOperServicos($id, $unidade_operacional___servios, $uo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalServiosApi->updateUnidOperServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unid Oper Serviços |
 **unidade_operacional___servios** | [**\VertisConnect\Model\ModelUnidOperServicos**](../Model/ModelUnidOperServicos.md)| Objeto unid_oper_servicos |
 **uo** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelUnidOperServicos**](../Model/ModelUnidOperServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

