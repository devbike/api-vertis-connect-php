# VertisConnect\SetoresApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSetores**](SetoresApi.md#createSetores) | **POST** /api/V1.1/setores | 
[**deleteSetores**](SetoresApi.md#deleteSetores) | **DELETE** /api/V1.1/setores/{id} | 
[**getSetores**](SetoresApi.md#getSetores) | **GET** /api/V1.1/setores/{id} | 
[**getSetoress**](SetoresApi.md#getSetoress) | **GET** /api/V1.1/setores | 
[**updateSetores**](SetoresApi.md#updateSetores) | **PUT** /api/V1.1/setores/{id} | 


# **createSetores**
> \VertisConnect\Model\ModelSetor createSetores($setores)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$setores = new \VertisConnect\Model\ModelSetor(); // \VertisConnect\Model\ModelSetor | Objeto setor

try {
    $result = $apiInstance->createSetores($setores);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetoresApi->createSetores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setores** | [**\VertisConnect\Model\ModelSetor**](../Model/ModelSetor.md)| Objeto setor |

### Return type

[**\VertisConnect\Model\ModelSetor**](../Model/ModelSetor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSetores**
> \VertisConnect\Model\ModelSetor deleteSetores($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Setores

try {
    $result = $apiInstance->deleteSetores($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetoresApi->deleteSetores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Setores |

### Return type

[**\VertisConnect\Model\ModelSetor**](../Model/ModelSetor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSetores**
> \VertisConnect\Model\ModelSetor getSetores($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Setores

try {
    $result = $apiInstance->getSetores($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetoresApi->getSetores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Setores |

### Return type

[**\VertisConnect\Model\ModelSetor**](../Model/ModelSetor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSetoress**
> \VertisConnect\Model\ModelSetor[] getSetoress()



Retorna registros do objeto setor

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getSetoress();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetoresApi->getSetoress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelSetor[]**](../Model/ModelSetor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSetores**
> \VertisConnect\Model\ModelSetor updateSetores($setores, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$setores = new \VertisConnect\Model\ModelSetor(); // \VertisConnect\Model\ModelSetor | Objeto setor
$id = 56; // int | ID do Setores

try {
    $result = $apiInstance->updateSetores($setores, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetoresApi->updateSetores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setores** | [**\VertisConnect\Model\ModelSetor**](../Model/ModelSetor.md)| Objeto setor |
 **id** | **int**| ID do Setores |

### Return type

[**\VertisConnect\Model\ModelSetor**](../Model/ModelSetor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

