# VertisConnect\OperTaxaVigenciaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOperTaxaVigencia**](OperTaxaVigenciaApi.md#createOperTaxaVigencia) | **POST** /api/V1.1/oper-taxa-vigencia | 
[**deleteOperTaxaVigencia**](OperTaxaVigenciaApi.md#deleteOperTaxaVigencia) | **DELETE** /api/V1.1/oper-taxa-vigencia/{id} | 
[**getOperTaxaVigencia**](OperTaxaVigenciaApi.md#getOperTaxaVigencia) | **GET** /api/V1.1/oper-taxa-vigencia/{id} | 
[**getOperTaxaVigencias**](OperTaxaVigenciaApi.md#getOperTaxaVigencias) | **GET** /api/V1.1/oper-taxa-vigencia | 
[**updateOperTaxaVigencia**](OperTaxaVigenciaApi.md#updateOperTaxaVigencia) | **PUT** /api/V1.1/oper-taxa-vigencia/{id} | 


# **createOperTaxaVigencia**
> \VertisConnect\Model\ModelOperTaxaVigencia createOperTaxaVigencia($oper_taxa_vigencia)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperTaxaVigenciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$oper_taxa_vigencia = new \VertisConnect\Model\ModelOperTaxaVigencia(); // \VertisConnect\Model\ModelOperTaxaVigencia | Objeto operadora_taxa_vigencia

try {
    $result = $apiInstance->createOperTaxaVigencia($oper_taxa_vigencia);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperTaxaVigenciaApi->createOperTaxaVigencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **oper_taxa_vigencia** | [**\VertisConnect\Model\ModelOperTaxaVigencia**](../Model/ModelOperTaxaVigencia.md)| Objeto operadora_taxa_vigencia |

### Return type

[**\VertisConnect\Model\ModelOperTaxaVigencia**](../Model/ModelOperTaxaVigencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteOperTaxaVigencia**
> \VertisConnect\Model\ModelOperTaxaVigencia deleteOperTaxaVigencia($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperTaxaVigenciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do OperTaxaVigencia

try {
    $result = $apiInstance->deleteOperTaxaVigencia($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperTaxaVigenciaApi->deleteOperTaxaVigencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do OperTaxaVigencia |

### Return type

[**\VertisConnect\Model\ModelOperTaxaVigencia**](../Model/ModelOperTaxaVigencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOperTaxaVigencia**
> \VertisConnect\Model\ModelOperTaxaVigencia getOperTaxaVigencia($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperTaxaVigenciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do OperTaxaVigencia

try {
    $result = $apiInstance->getOperTaxaVigencia($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperTaxaVigenciaApi->getOperTaxaVigencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do OperTaxaVigencia |

### Return type

[**\VertisConnect\Model\ModelOperTaxaVigencia**](../Model/ModelOperTaxaVigencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOperTaxaVigencias**
> \VertisConnect\Model\ModelOperTaxaVigencia[] getOperTaxaVigencias()



Retorna registros do objeto operadora_taxa_vigencia

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperTaxaVigenciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getOperTaxaVigencias();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperTaxaVigenciaApi->getOperTaxaVigencias: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelOperTaxaVigencia[]**](../Model/ModelOperTaxaVigencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOperTaxaVigencia**
> \VertisConnect\Model\ModelOperTaxaVigencia updateOperTaxaVigencia($oper_taxa_vigencia, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperTaxaVigenciaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$oper_taxa_vigencia = new \VertisConnect\Model\ModelOperTaxaVigencia(); // \VertisConnect\Model\ModelOperTaxaVigencia | Objeto operadora_taxa_vigencia
$id = 56; // int | ID do OperTaxaVigencia

try {
    $result = $apiInstance->updateOperTaxaVigencia($oper_taxa_vigencia, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperTaxaVigenciaApi->updateOperTaxaVigencia: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **oper_taxa_vigencia** | [**\VertisConnect\Model\ModelOperTaxaVigencia**](../Model/ModelOperTaxaVigencia.md)| Objeto operadora_taxa_vigencia |
 **id** | **int**| ID do OperTaxaVigencia |

### Return type

[**\VertisConnect\Model\ModelOperTaxaVigencia**](../Model/ModelOperTaxaVigencia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

