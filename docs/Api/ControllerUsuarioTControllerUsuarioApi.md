# VertisConnect\ControllerUsuarioTControllerUsuarioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11UsuariosGet**](ControllerUsuarioTControllerUsuarioApi.md#apiV11UsuariosGet) | **GET** /api/V1.1/usuarios | 
[**apiV11UsuariosIdDelete**](ControllerUsuarioTControllerUsuarioApi.md#apiV11UsuariosIdDelete) | **DELETE** /api/V1.1/usuarios/{id} | 
[**apiV11UsuariosIdGet**](ControllerUsuarioTControllerUsuarioApi.md#apiV11UsuariosIdGet) | **GET** /api/V1.1/usuarios/{id} | 
[**apiV11UsuariosIdPut**](ControllerUsuarioTControllerUsuarioApi.md#apiV11UsuariosIdPut) | **PUT** /api/V1.1/usuarios/{id} | 
[**apiV11UsuariosPost**](ControllerUsuarioTControllerUsuarioApi.md#apiV11UsuariosPost) | **POST** /api/V1.1/usuarios | 


# **apiV11UsuariosGet**
> apiV11UsuariosGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11UsuariosGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->apiV11UsuariosGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11UsuariosIdDelete**
> apiV11UsuariosIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11UsuariosIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->apiV11UsuariosIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11UsuariosIdGet**
> apiV11UsuariosIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11UsuariosIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->apiV11UsuariosIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11UsuariosIdPut**
> apiV11UsuariosIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11UsuariosIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->apiV11UsuariosIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11UsuariosPost**
> apiV11UsuariosPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerUsuarioTControllerUsuarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11UsuariosPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerUsuarioTControllerUsuarioApi->apiV11UsuariosPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

