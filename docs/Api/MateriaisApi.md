# VertisConnect\MateriaisApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExameMaterial**](MateriaisApi.md#createExameMaterial) | **POST** /api/V1.1/examemateriais | 
[**deleteExameMaterial**](MateriaisApi.md#deleteExameMaterial) | **DELETE** /api/V1.1/examemateriais/{id} | 
[**getExameMaterial**](MateriaisApi.md#getExameMaterial) | **GET** /api/V1.1/examemateriais/{id} | 
[**getExameMaterials**](MateriaisApi.md#getExameMaterials) | **GET** /api/V1.1/examemateriais | 
[**updateExameMaterial**](MateriaisApi.md#updateExameMaterial) | **PUT** /api/V1.1/examemateriais/{id} | 


# **createExameMaterial**
> \VertisConnect\Model\ModelMaterialExame createExameMaterial($materiais)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MateriaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$materiais = new \VertisConnect\Model\ModelMaterialExame(); // \VertisConnect\Model\ModelMaterialExame | Objeto prod_exa_material_nv

try {
    $result = $apiInstance->createExameMaterial($materiais);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MateriaisApi->createExameMaterial: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materiais** | [**\VertisConnect\Model\ModelMaterialExame**](../Model/ModelMaterialExame.md)| Objeto prod_exa_material_nv |

### Return type

[**\VertisConnect\Model\ModelMaterialExame**](../Model/ModelMaterialExame.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteExameMaterial**
> \VertisConnect\Model\ModelMaterialExame deleteExameMaterial($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MateriaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Materiais

try {
    $result = $apiInstance->deleteExameMaterial($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MateriaisApi->deleteExameMaterial: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Materiais |

### Return type

[**\VertisConnect\Model\ModelMaterialExame**](../Model/ModelMaterialExame.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameMaterial**
> \VertisConnect\Model\ModelMaterialExame getExameMaterial($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MateriaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Materiais

try {
    $result = $apiInstance->getExameMaterial($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MateriaisApi->getExameMaterial: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Materiais |

### Return type

[**\VertisConnect\Model\ModelMaterialExame**](../Model/ModelMaterialExame.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameMaterials**
> \VertisConnect\Model\ModelMaterialExame[] getExameMaterials()



Retorna registros do objeto prod_exa_material_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MateriaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExameMaterials();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MateriaisApi->getExameMaterials: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelMaterialExame[]**](../Model/ModelMaterialExame.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExameMaterial**
> \VertisConnect\Model\ModelMaterialExame updateExameMaterial($materiais, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MateriaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$materiais = new \VertisConnect\Model\ModelMaterialExame(); // \VertisConnect\Model\ModelMaterialExame | Objeto prod_exa_material_nv
$id = 56; // int | ID do Materiais

try {
    $result = $apiInstance->updateExameMaterial($materiais, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MateriaisApi->updateExameMaterial: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **materiais** | [**\VertisConnect\Model\ModelMaterialExame**](../Model/ModelMaterialExame.md)| Objeto prod_exa_material_nv |
 **id** | **int**| ID do Materiais |

### Return type

[**\VertisConnect\Model\ModelMaterialExame**](../Model/ModelMaterialExame.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

