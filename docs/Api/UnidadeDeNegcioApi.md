# VertisConnect\UnidadeDeNegcioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUnidadesNegocio**](UnidadeDeNegcioApi.md#createUnidadesNegocio) | **POST** /api/V1.1/unidadesnegocio | 
[**deleteUnidadesNegocio**](UnidadeDeNegcioApi.md#deleteUnidadesNegocio) | **DELETE** /api/V1.1/unidadesnegocio/{id} | 
[**getUnidadesNegocio**](UnidadeDeNegcioApi.md#getUnidadesNegocio) | **GET** /api/V1.1/unidadesnegocio/{id} | 
[**getUnidadesNegocios**](UnidadeDeNegcioApi.md#getUnidadesNegocios) | **GET** /api/V1.1/unidadesnegocio | 
[**updateUnidadesNegocio**](UnidadeDeNegcioApi.md#updateUnidadesNegocio) | **PUT** /api/V1.1/unidadesnegocio/{id} | 


# **createUnidadesNegocio**
> \VertisConnect\Model\ModelUnidadeNegocio createUnidadesNegocio($unidade_de_negcio)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidade_de_negcio = new \VertisConnect\Model\ModelUnidadeNegocio(); // \VertisConnect\Model\ModelUnidadeNegocio | Objeto unid_negocio

try {
    $result = $apiInstance->createUnidadesNegocio($unidade_de_negcio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeDeNegcioApi->createUnidadesNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidade_de_negcio** | [**\VertisConnect\Model\ModelUnidadeNegocio**](../Model/ModelUnidadeNegocio.md)| Objeto unid_negocio |

### Return type

[**\VertisConnect\Model\ModelUnidadeNegocio**](../Model/ModelUnidadeNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUnidadesNegocio**
> \VertisConnect\Model\ModelUnidadeNegocio deleteUnidadesNegocio($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unidade de Negócio

try {
    $result = $apiInstance->deleteUnidadesNegocio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeDeNegcioApi->deleteUnidadesNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unidade de Negócio |

### Return type

[**\VertisConnect\Model\ModelUnidadeNegocio**](../Model/ModelUnidadeNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadesNegocio**
> \VertisConnect\Model\ModelUnidadeNegocio getUnidadesNegocio($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unidade de Negócio

try {
    $result = $apiInstance->getUnidadesNegocio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeDeNegcioApi->getUnidadesNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unidade de Negócio |

### Return type

[**\VertisConnect\Model\ModelUnidadeNegocio**](../Model/ModelUnidadeNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadesNegocios**
> \VertisConnect\Model\ModelUnidadeNegocio[] getUnidadesNegocios()



Retorna registros do objeto unid_negocio

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getUnidadesNegocios();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeDeNegcioApi->getUnidadesNegocios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelUnidadeNegocio[]**](../Model/ModelUnidadeNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUnidadesNegocio**
> \VertisConnect\Model\ModelUnidadeNegocio updateUnidadesNegocio($unidade_de_negcio, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeDeNegcioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidade_de_negcio = new \VertisConnect\Model\ModelUnidadeNegocio(); // \VertisConnect\Model\ModelUnidadeNegocio | Objeto unid_negocio
$id = 56; // int | ID do Unidade de Negócio

try {
    $result = $apiInstance->updateUnidadesNegocio($unidade_de_negcio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeDeNegcioApi->updateUnidadesNegocio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidade_de_negcio** | [**\VertisConnect\Model\ModelUnidadeNegocio**](../Model/ModelUnidadeNegocio.md)| Objeto unid_negocio |
 **id** | **int**| ID do Unidade de Negócio |

### Return type

[**\VertisConnect\Model\ModelUnidadeNegocio**](../Model/ModelUnidadeNegocio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

