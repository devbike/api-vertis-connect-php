# VertisConnect\UnidadeOperacionalApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUnidadeOperacional**](UnidadeOperacionalApi.md#createUnidadeOperacional) | **POST** /api/V1.1/unidadesoperacionais | 
[**deleteUnidadeOperacional**](UnidadeOperacionalApi.md#deleteUnidadeOperacional) | **DELETE** /api/V1.1/unidadesoperacionais/{id} | 
[**getUnidadeOperacional**](UnidadeOperacionalApi.md#getUnidadeOperacional) | **GET** /api/V1.1/unidadesoperacionais/{id} | 
[**getUnidadeOperacionals**](UnidadeOperacionalApi.md#getUnidadeOperacionals) | **GET** /api/V1.1/unidadesoperacionais | 
[**updateUnidadeOperacional**](UnidadeOperacionalApi.md#updateUnidadeOperacional) | **PUT** /api/V1.1/unidadesoperacionais/{id} | 


# **createUnidadeOperacional**
> \VertisConnect\Model\ModelUnidadeOperacional createUnidadeOperacional($unidade_operacional)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidade_operacional = new \VertisConnect\Model\ModelUnidadeOperacional(); // \VertisConnect\Model\ModelUnidadeOperacional | Objeto unid_operacional

try {
    $result = $apiInstance->createUnidadeOperacional($unidade_operacional);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->createUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidade_operacional** | [**\VertisConnect\Model\ModelUnidadeOperacional**](../Model/ModelUnidadeOperacional.md)| Objeto unid_operacional |

### Return type

[**\VertisConnect\Model\ModelUnidadeOperacional**](../Model/ModelUnidadeOperacional.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUnidadeOperacional**
> \VertisConnect\Model\ModelUnidadeOperacional deleteUnidadeOperacional($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unidade Operacional

try {
    $result = $apiInstance->deleteUnidadeOperacional($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->deleteUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unidade Operacional |

### Return type

[**\VertisConnect\Model\ModelUnidadeOperacional**](../Model/ModelUnidadeOperacional.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadeOperacional**
> \VertisConnect\Model\ModelUnidadeOperacional getUnidadeOperacional($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Unidade Operacional

try {
    $result = $apiInstance->getUnidadeOperacional($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->getUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Unidade Operacional |

### Return type

[**\VertisConnect\Model\ModelUnidadeOperacional**](../Model/ModelUnidadeOperacional.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUnidadeOperacionals**
> \VertisConnect\Model\ModelUnidadeOperacional[] getUnidadeOperacionals()



Retorna registros do objeto unid_operacional

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getUnidadeOperacionals();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->getUnidadeOperacionals: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelUnidadeOperacional[]**](../Model/ModelUnidadeOperacional.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUnidadeOperacional**
> \VertisConnect\Model\ModelUnidadeOperacional updateUnidadeOperacional($unidade_operacional, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\UnidadeOperacionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unidade_operacional = new \VertisConnect\Model\ModelUnidadeOperacional(); // \VertisConnect\Model\ModelUnidadeOperacional | Objeto unid_operacional
$id = 56; // int | ID do Unidade Operacional

try {
    $result = $apiInstance->updateUnidadeOperacional($unidade_operacional, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnidadeOperacionalApi->updateUnidadeOperacional: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unidade_operacional** | [**\VertisConnect\Model\ModelUnidadeOperacional**](../Model/ModelUnidadeOperacional.md)| Objeto unid_operacional |
 **id** | **int**| ID do Unidade Operacional |

### Return type

[**\VertisConnect\Model\ModelUnidadeOperacional**](../Model/ModelUnidadeOperacional.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

