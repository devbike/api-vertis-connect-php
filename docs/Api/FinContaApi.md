# VertisConnect\FinContaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createfinconta**](FinContaApi.md#createfinconta) | **POST** /api/V1.1/fin-contas | 
[**deletefinconta**](FinContaApi.md#deletefinconta) | **DELETE** /api/V1.1/fin-contas/{id} | 
[**getfinconta**](FinContaApi.md#getfinconta) | **GET** /api/V1.1/fin-contas/{id} | 
[**getfincontas**](FinContaApi.md#getfincontas) | **GET** /api/V1.1/fin-contas | 
[**updatefinconta**](FinContaApi.md#updatefinconta) | **PUT** /api/V1.1/fin-contas/{id} | 


# **createfinconta**
> \VertisConnect\Model\ModelFinConta createfinconta($fin_conta)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_conta = new \VertisConnect\Model\ModelFinConta(); // \VertisConnect\Model\ModelFinConta | Objeto fin_conta

try {
    $result = $apiInstance->createfinconta($fin_conta);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaApi->createfinconta: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_conta** | [**\VertisConnect\Model\ModelFinConta**](../Model/ModelFinConta.md)| Objeto fin_conta |

### Return type

[**\VertisConnect\Model\ModelFinConta**](../Model/ModelFinConta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletefinconta**
> \VertisConnect\Model\ModelFinConta deletefinconta($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinConta

try {
    $result = $apiInstance->deletefinconta($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaApi->deletefinconta: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinConta |

### Return type

[**\VertisConnect\Model\ModelFinConta**](../Model/ModelFinConta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfinconta**
> \VertisConnect\Model\ModelFinConta getfinconta($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinConta

try {
    $result = $apiInstance->getfinconta($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaApi->getfinconta: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinConta |

### Return type

[**\VertisConnect\Model\ModelFinConta**](../Model/ModelFinConta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfincontas**
> \VertisConnect\Model\ModelFinConta[] getfincontas()



Retorna registros do objeto fin_conta

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getfincontas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaApi->getfincontas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelFinConta[]**](../Model/ModelFinConta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatefinconta**
> \VertisConnect\Model\ModelFinConta updatefinconta($fin_conta, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinContaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_conta = new \VertisConnect\Model\ModelFinConta(); // \VertisConnect\Model\ModelFinConta | Objeto fin_conta
$id = 56; // int | ID do FinConta

try {
    $result = $apiInstance->updatefinconta($fin_conta, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinContaApi->updatefinconta: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_conta** | [**\VertisConnect\Model\ModelFinConta**](../Model/ModelFinConta.md)| Objeto fin_conta |
 **id** | **int**| ID do FinConta |

### Return type

[**\VertisConnect\Model\ModelFinConta**](../Model/ModelFinConta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

