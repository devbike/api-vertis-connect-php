# VertisConnect\ExamesTestesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTeste**](ExamesTestesApi.md#createTeste) | **POST** /api/V1.1/exames/{ex}/testes | 
[**deleteTeste**](ExamesTestesApi.md#deleteTeste) | **DELETE** /api/V1.1/exames/{ex}/testes/{id} | 
[**getTeste**](ExamesTestesApi.md#getTeste) | **GET** /api/V1.1/exames/{ex}/testes/{id} | 
[**getTestes**](ExamesTestesApi.md#getTestes) | **GET** /api/V1.1/exames/{ex}/testes | 
[**updateTeste**](ExamesTestesApi.md#updateTeste) | **PUT** /api/V1.1/exames/{ex}/testes/{id} | 


# **createTeste**
> \VertisConnect\Model\ModelTestes createTeste($exames___testes, $ex)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesTestesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exames___testes = new \VertisConnect\Model\ModelTestes(); // \VertisConnect\Model\ModelTestes | Objeto prod_exa_teste_nv
$ex = 56; // int | 

try {
    $result = $apiInstance->createTeste($exames___testes, $ex);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesTestesApi->createTeste: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exames___testes** | [**\VertisConnect\Model\ModelTestes**](../Model/ModelTestes.md)| Objeto prod_exa_teste_nv |
 **ex** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelTestes**](../Model/ModelTestes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteTeste**
> \VertisConnect\Model\ModelTestes deleteTeste($id, $ex)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesTestesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Testes
$ex = 56; // int | 

try {
    $result = $apiInstance->deleteTeste($id, $ex);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesTestesApi->deleteTeste: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Testes |
 **ex** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelTestes**](../Model/ModelTestes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTeste**
> \VertisConnect\Model\ModelTestes getTeste($id, $ex)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesTestesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Testes
$ex = 56; // int | 

try {
    $result = $apiInstance->getTeste($id, $ex);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesTestesApi->getTeste: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Testes |
 **ex** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelTestes**](../Model/ModelTestes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTestes**
> \VertisConnect\Model\ModelTestes[] getTestes($ex)



Retorna registros do objeto prod_exa_teste_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesTestesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ex = 56; // int | 

try {
    $result = $apiInstance->getTestes($ex);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesTestesApi->getTestes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ex** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelTestes[]**](../Model/ModelTestes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTeste**
> \VertisConnect\Model\ModelTestes updateTeste($id, $exames___testes, $ex)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesTestesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Testes
$exames___testes = new \VertisConnect\Model\ModelTestes(); // \VertisConnect\Model\ModelTestes | Objeto prod_exa_teste_nv
$ex = 56; // int | 

try {
    $result = $apiInstance->updateTeste($id, $exames___testes, $ex);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesTestesApi->updateTeste: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Testes |
 **exames___testes** | [**\VertisConnect\Model\ModelTestes**](../Model/ModelTestes.md)| Objeto prod_exa_teste_nv |
 **ex** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelTestes**](../Model/ModelTestes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

