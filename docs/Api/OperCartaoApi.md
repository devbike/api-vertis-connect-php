# VertisConnect\OperCartaoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTModelOperCartao**](OperCartaoApi.md#createTModelOperCartao) | **POST** /api/V1.1/oper-cartao | 
[**deleteTModelOperCartao**](OperCartaoApi.md#deleteTModelOperCartao) | **DELETE** /api/V1.1/oper-cartao/{id} | 
[**getTModelOperCartao**](OperCartaoApi.md#getTModelOperCartao) | **GET** /api/V1.1/oper-cartao/{id} | 
[**getTModelOperCartaos**](OperCartaoApi.md#getTModelOperCartaos) | **GET** /api/V1.1/oper-cartao | 
[**updateTModelOperCartao**](OperCartaoApi.md#updateTModelOperCartao) | **PUT** /api/V1.1/oper-cartao/{id} | 


# **createTModelOperCartao**
> \VertisConnect\Model\ModelOperCartao createTModelOperCartao($oper_cartao)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperCartaoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$oper_cartao = new \VertisConnect\Model\ModelOperCartao(); // \VertisConnect\Model\ModelOperCartao | Objeto operadora_cartao

try {
    $result = $apiInstance->createTModelOperCartao($oper_cartao);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperCartaoApi->createTModelOperCartao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **oper_cartao** | [**\VertisConnect\Model\ModelOperCartao**](../Model/ModelOperCartao.md)| Objeto operadora_cartao |

### Return type

[**\VertisConnect\Model\ModelOperCartao**](../Model/ModelOperCartao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteTModelOperCartao**
> \VertisConnect\Model\ModelOperCartao deleteTModelOperCartao($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperCartaoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do OperCartao

try {
    $result = $apiInstance->deleteTModelOperCartao($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperCartaoApi->deleteTModelOperCartao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do OperCartao |

### Return type

[**\VertisConnect\Model\ModelOperCartao**](../Model/ModelOperCartao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTModelOperCartao**
> \VertisConnect\Model\ModelOperCartao getTModelOperCartao($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperCartaoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do OperCartao

try {
    $result = $apiInstance->getTModelOperCartao($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperCartaoApi->getTModelOperCartao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do OperCartao |

### Return type

[**\VertisConnect\Model\ModelOperCartao**](../Model/ModelOperCartao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTModelOperCartaos**
> \VertisConnect\Model\ModelOperCartao[] getTModelOperCartaos()



Retorna registros do objeto operadora_cartao

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperCartaoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getTModelOperCartaos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperCartaoApi->getTModelOperCartaos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelOperCartao[]**](../Model/ModelOperCartao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTModelOperCartao**
> \VertisConnect\Model\ModelOperCartao updateTModelOperCartao($oper_cartao, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OperCartaoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$oper_cartao = new \VertisConnect\Model\ModelOperCartao(); // \VertisConnect\Model\ModelOperCartao | Objeto operadora_cartao
$id = 56; // int | ID do OperCartao

try {
    $result = $apiInstance->updateTModelOperCartao($oper_cartao, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperCartaoApi->updateTModelOperCartao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **oper_cartao** | [**\VertisConnect\Model\ModelOperCartao**](../Model/ModelOperCartao.md)| Objeto operadora_cartao |
 **id** | **int**| ID do OperCartao |

### Return type

[**\VertisConnect\Model\ModelOperCartao**](../Model/ModelOperCartao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

