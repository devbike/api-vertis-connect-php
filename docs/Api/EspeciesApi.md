# VertisConnect\EspeciesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createEspecie**](EspeciesApi.md#createEspecie) | **POST** /api/V1.1/Especies | 
[**deleteEspecie**](EspeciesApi.md#deleteEspecie) | **DELETE** /api/V1.1/Especies/{id} | 
[**getEspecie**](EspeciesApi.md#getEspecie) | **GET** /api/V1.1/Especies/{id} | 
[**getEspecies**](EspeciesApi.md#getEspecies) | **GET** /api/V1.1/Especies | 
[**updateEspecie**](EspeciesApi.md#updateEspecie) | **PUT** /api/V1.1/Especies/{id} | 


# **createEspecie**
> \VertisConnect\Model\ModelEspecie createEspecie($especies)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\EspeciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$especies = new \VertisConnect\Model\ModelEspecie(); // \VertisConnect\Model\ModelEspecie | Objeto anim_Especie_nv

try {
    $result = $apiInstance->createEspecie($especies);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EspeciesApi->createEspecie: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **especies** | [**\VertisConnect\Model\ModelEspecie**](../Model/ModelEspecie.md)| Objeto anim_Especie_nv |

### Return type

[**\VertisConnect\Model\ModelEspecie**](../Model/ModelEspecie.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteEspecie**
> \VertisConnect\Model\ModelEspecie deleteEspecie($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\EspeciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Especies

try {
    $result = $apiInstance->deleteEspecie($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EspeciesApi->deleteEspecie: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Especies |

### Return type

[**\VertisConnect\Model\ModelEspecie**](../Model/ModelEspecie.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEspecie**
> \VertisConnect\Model\ModelEspecie getEspecie($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\EspeciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Especies

try {
    $result = $apiInstance->getEspecie($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EspeciesApi->getEspecie: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Especies |

### Return type

[**\VertisConnect\Model\ModelEspecie**](../Model/ModelEspecie.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEspecies**
> \VertisConnect\Model\ModelEspecie[] getEspecies()



Retorna registros do objeto anim_Especie_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\EspeciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getEspecies();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EspeciesApi->getEspecies: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelEspecie[]**](../Model/ModelEspecie.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateEspecie**
> \VertisConnect\Model\ModelEspecie updateEspecie($especies, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\EspeciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$especies = new \VertisConnect\Model\ModelEspecie(); // \VertisConnect\Model\ModelEspecie | Objeto anim_Especie_nv
$id = 56; // int | ID do Especies

try {
    $result = $apiInstance->updateEspecie($especies, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EspeciesApi->updateEspecie: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **especies** | [**\VertisConnect\Model\ModelEspecie**](../Model/ModelEspecie.md)| Objeto anim_Especie_nv |
 **id** | **int**| ID do Especies |

### Return type

[**\VertisConnect\Model\ModelEspecie**](../Model/ModelEspecie.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

