# VertisConnect\ControllerResultadoTControllerResultadoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11ResultadosPost**](ControllerResultadoTControllerResultadoApi.md#apiV11ResultadosPost) | **POST** /api/V1.1/resultados | 


# **apiV11ResultadosPost**
> apiV11ResultadosPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerResultadoTControllerResultadoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11ResultadosPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerResultadoTControllerResultadoApi->apiV11ResultadosPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

