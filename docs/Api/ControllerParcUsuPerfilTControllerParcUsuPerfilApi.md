# VertisConnect\ControllerParcUsuPerfilTControllerParcUsuPerfilApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11ParcUsuPerfilGet**](ControllerParcUsuPerfilTControllerParcUsuPerfilApi.md#apiV11ParcUsuPerfilGet) | **GET** /api/V1.1/parc-usu-perfil | 
[**apiV11ParcUsuPerfilIdDelete**](ControllerParcUsuPerfilTControllerParcUsuPerfilApi.md#apiV11ParcUsuPerfilIdDelete) | **DELETE** /api/V1.1/parc-usu-perfil/{id} | 
[**apiV11ParcUsuPerfilIdGet**](ControllerParcUsuPerfilTControllerParcUsuPerfilApi.md#apiV11ParcUsuPerfilIdGet) | **GET** /api/V1.1/parc-usu-perfil/{id} | 
[**apiV11ParcUsuPerfilIdPut**](ControllerParcUsuPerfilTControllerParcUsuPerfilApi.md#apiV11ParcUsuPerfilIdPut) | **PUT** /api/V1.1/parc-usu-perfil/{id} | 
[**apiV11ParcUsuPerfilPost**](ControllerParcUsuPerfilTControllerParcUsuPerfilApi.md#apiV11ParcUsuPerfilPost) | **POST** /api/V1.1/parc-usu-perfil | 


# **apiV11ParcUsuPerfilGet**
> apiV11ParcUsuPerfilGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcUsuPerfilTControllerParcUsuPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11ParcUsuPerfilGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcUsuPerfilTControllerParcUsuPerfilApi->apiV11ParcUsuPerfilGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ParcUsuPerfilIdDelete**
> apiV11ParcUsuPerfilIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcUsuPerfilTControllerParcUsuPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11ParcUsuPerfilIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcUsuPerfilTControllerParcUsuPerfilApi->apiV11ParcUsuPerfilIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ParcUsuPerfilIdGet**
> apiV11ParcUsuPerfilIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcUsuPerfilTControllerParcUsuPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11ParcUsuPerfilIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcUsuPerfilTControllerParcUsuPerfilApi->apiV11ParcUsuPerfilIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ParcUsuPerfilIdPut**
> apiV11ParcUsuPerfilIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcUsuPerfilTControllerParcUsuPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11ParcUsuPerfilIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcUsuPerfilTControllerParcUsuPerfilApi->apiV11ParcUsuPerfilIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ParcUsuPerfilPost**
> apiV11ParcUsuPerfilPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerParcUsuPerfilTControllerParcUsuPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11ParcUsuPerfilPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerParcUsuPerfilTControllerParcUsuPerfilApi->apiV11ParcUsuPerfilPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

