# VertisConnect\ProprietriosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProprietarios**](ProprietriosApi.md#createProprietarios) | **POST** /api/V1.1/proprietarios | 
[**deleteProprietarios**](ProprietriosApi.md#deleteProprietarios) | **DELETE** /api/V1.1/proprietarios/{id} | 
[**findTutorByDoc**](ProprietriosApi.md#findTutorByDoc) | **GET** /api/V1.1/buscatutor/{id} | 
[**getProprietarios**](ProprietriosApi.md#getProprietarios) | **GET** /api/V1.1/proprietarios/{id} | 
[**getProprietarioss**](ProprietriosApi.md#getProprietarioss) | **GET** /api/V1.1/proprietarios | 
[**updateProprietarios**](ProprietriosApi.md#updateProprietarios) | **PUT** /api/V1.1/proprietarios/{id} | 


# **createProprietarios**
> \VertisConnect\Model\ModelProprietarios createProprietarios($proprietrios)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$proprietrios = new \VertisConnect\Model\ModelProprietarios(); // \VertisConnect\Model\ModelProprietarios | Objeto parc_proprietario

try {
    $result = $apiInstance->createProprietarios($proprietrios);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->createProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **proprietrios** | [**\VertisConnect\Model\ModelProprietarios**](../Model/ModelProprietarios.md)| Objeto parc_proprietario |

### Return type

[**\VertisConnect\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProprietarios**
> \VertisConnect\Model\ModelProprietarios deleteProprietarios($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Tutor

try {
    $result = $apiInstance->deleteProprietarios($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->deleteProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Tutor |

### Return type

[**\VertisConnect\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **findTutorByDoc**
> \VertisConnect\Model\ModelProprietarios[] findTutorByDoc($id)



Retorna registros do objeto parc_proprietario

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | CPF/CNPJ do Tutor

try {
    $result = $apiInstance->findTutorByDoc($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->findTutorByDoc: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| CPF/CNPJ do Tutor |

### Return type

[**\VertisConnect\Model\ModelProprietarios[]**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProprietarios**
> \VertisConnect\Model\ModelProprietarios getProprietarios($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Tutor

try {
    $result = $apiInstance->getProprietarios($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->getProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Tutor |

### Return type

[**\VertisConnect\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProprietarioss**
> \VertisConnect\Model\ModelProprietarios[] getProprietarioss()



Retorna registros do objeto parc_proprietario

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getProprietarioss();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->getProprietarioss: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelProprietarios[]**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProprietarios**
> \VertisConnect\Model\ModelProprietarios updateProprietarios($proprietrios, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProprietriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$proprietrios = new \VertisConnect\Model\ModelProprietarios(); // \VertisConnect\Model\ModelProprietarios | Objeto parc_proprietario
$id = 56; // int | ID do Tutor

try {
    $result = $apiInstance->updateProprietarios($proprietrios, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProprietriosApi->updateProprietarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **proprietrios** | [**\VertisConnect\Model\ModelProprietarios**](../Model/ModelProprietarios.md)| Objeto parc_proprietario |
 **id** | **int**| ID do Tutor |

### Return type

[**\VertisConnect\Model\ModelProprietarios**](../Model/ModelProprietarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

