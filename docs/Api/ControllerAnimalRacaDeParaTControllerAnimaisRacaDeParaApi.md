# VertisConnect\ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11AnimaisRacaDeParaGet**](ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi.md#apiV11AnimaisRacaDeParaGet) | **GET** /api/V1.1/animais_raca_de_para | 
[**apiV11AnimaisRacaDeParaIdDelete**](ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi.md#apiV11AnimaisRacaDeParaIdDelete) | **DELETE** /api/V1.1/animais_raca_de_para/{id} | 
[**apiV11AnimaisRacaDeParaIdGet**](ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi.md#apiV11AnimaisRacaDeParaIdGet) | **GET** /api/V1.1/animais_raca_de_para/{id} | 
[**apiV11AnimaisRacaDeParaIdPut**](ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi.md#apiV11AnimaisRacaDeParaIdPut) | **PUT** /api/V1.1/animais_raca_de_para/{id} | 
[**apiV11AnimaisRacaDeParaPost**](ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi.md#apiV11AnimaisRacaDeParaPost) | **POST** /api/V1.1/animais_raca_de_para | 


# **apiV11AnimaisRacaDeParaGet**
> apiV11AnimaisRacaDeParaGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11AnimaisRacaDeParaGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi->apiV11AnimaisRacaDeParaGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11AnimaisRacaDeParaIdDelete**
> apiV11AnimaisRacaDeParaIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11AnimaisRacaDeParaIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi->apiV11AnimaisRacaDeParaIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11AnimaisRacaDeParaIdGet**
> apiV11AnimaisRacaDeParaIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11AnimaisRacaDeParaIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi->apiV11AnimaisRacaDeParaIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11AnimaisRacaDeParaIdPut**
> apiV11AnimaisRacaDeParaIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11AnimaisRacaDeParaIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi->apiV11AnimaisRacaDeParaIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11AnimaisRacaDeParaPost**
> apiV11AnimaisRacaDeParaPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11AnimaisRacaDeParaPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerAnimalRacaDeParaTControllerAnimaisRacaDeParaApi->apiV11AnimaisRacaDeParaPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

