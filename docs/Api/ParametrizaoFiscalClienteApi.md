# VertisConnect\ParametrizaoFiscalClienteApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoFiscalCli**](ParametrizaoFiscalClienteApi.md#doCreateInfoFiscalCli) | **POST** /api/V1.1/clientes/{id}/paramsfiscais | 
[**doDeleteInfoFiscalCli**](ParametrizaoFiscalClienteApi.md#doDeleteInfoFiscalCli) | **DELETE** /api/V1.1/clientes/{id}/paramsfiscais | 
[**doGetInfoFiscalCli**](ParametrizaoFiscalClienteApi.md#doGetInfoFiscalCli) | **GET** /api/V1.1/clientes/{id}/paramsfiscais | 
[**doUpdateInfoFiscalCli**](ParametrizaoFiscalClienteApi.md#doUpdateInfoFiscalCli) | **PUT** /api/V1.1/clientes/{id}/paramsfiscais | 


# **doCreateInfoFiscalCli**
> \VertisConnect\Model\ModelFiscal doCreateInfoFiscalCli($parametrizao_fiscal, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_fiscal = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do Cliente

try {
    $result = $apiInstance->doCreateInfoFiscalCli($parametrizao_fiscal, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalClienteApi->doCreateInfoFiscalCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_fiscal** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do Cliente |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoFiscalCli**
> \VertisConnect\Model\ModelFiscal doDeleteInfoFiscalCli($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoFiscalCli($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalClienteApi->doDeleteInfoFiscalCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoFiscalCli**
> \VertisConnect\Model\ModelFiscal doGetInfoFiscalCli($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoFiscalCli($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalClienteApi->doGetInfoFiscalCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoFiscalCli**
> \VertisConnect\Model\ModelFiscal doUpdateInfoFiscalCli($parametros_fiscais___cliente, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___cliente = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoFiscalCli($parametros_fiscais___cliente, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalClienteApi->doUpdateInfoFiscalCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___cliente** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

