# VertisConnect\PsaPlanoAnimalApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPsaPlanoAnimal**](PsaPlanoAnimalApi.md#createPsaPlanoAnimal) | **POST** /api/V1.1/psa-plano-animal | 
[**deletePsaPlanoAnimal**](PsaPlanoAnimalApi.md#deletePsaPlanoAnimal) | **DELETE** /api/V1.1/psa-plano-animal/{id} | 
[**getPsaPlanoAnimal**](PsaPlanoAnimalApi.md#getPsaPlanoAnimal) | **GET** /api/V1.1/psa-plano-animal/{id} | 
[**getPsaPlanoAnimals**](PsaPlanoAnimalApi.md#getPsaPlanoAnimals) | **GET** /api/V1.1/psa-plano-animal | 
[**updatePsaPlanoAnimal**](PsaPlanoAnimalApi.md#updatePsaPlanoAnimal) | **PUT** /api/V1.1/psa-plano-animal/{id} | 


# **createPsaPlanoAnimal**
> \VertisConnect\Model\ModelPsaPlanoAnimal createPsaPlanoAnimal($psa_plano_animal)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlanoAnimalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_plano_animal = new \VertisConnect\Model\ModelPsaPlanoAnimal(); // \VertisConnect\Model\ModelPsaPlanoAnimal | Objeto fin_conta

try {
    $result = $apiInstance->createPsaPlanoAnimal($psa_plano_animal);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlanoAnimalApi->createPsaPlanoAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_plano_animal** | [**\VertisConnect\Model\ModelPsaPlanoAnimal**](../Model/ModelPsaPlanoAnimal.md)| Objeto fin_conta |

### Return type

[**\VertisConnect\Model\ModelPsaPlanoAnimal**](../Model/ModelPsaPlanoAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePsaPlanoAnimal**
> \VertisConnect\Model\ModelPsaPlanoAnimal deletePsaPlanoAnimal($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlanoAnimalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaPlanoAnimal

try {
    $result = $apiInstance->deletePsaPlanoAnimal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlanoAnimalApi->deletePsaPlanoAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaPlanoAnimal |

### Return type

[**\VertisConnect\Model\ModelPsaPlanoAnimal**](../Model/ModelPsaPlanoAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaPlanoAnimal**
> \VertisConnect\Model\ModelPsaPlanoAnimal getPsaPlanoAnimal($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlanoAnimalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaPlanoAnimal

try {
    $result = $apiInstance->getPsaPlanoAnimal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlanoAnimalApi->getPsaPlanoAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaPlanoAnimal |

### Return type

[**\VertisConnect\Model\ModelPsaPlanoAnimal**](../Model/ModelPsaPlanoAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaPlanoAnimals**
> \VertisConnect\Model\ModelPsaPlanoAnimal[] getPsaPlanoAnimals()



Retorna registros do objeto fin_conta

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlanoAnimalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPsaPlanoAnimals();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlanoAnimalApi->getPsaPlanoAnimals: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelPsaPlanoAnimal[]**](../Model/ModelPsaPlanoAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePsaPlanoAnimal**
> \VertisConnect\Model\ModelPsaPlanoAnimal updatePsaPlanoAnimal($psa_plano_animal, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlanoAnimalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_plano_animal = new \VertisConnect\Model\ModelPsaPlanoAnimal(); // \VertisConnect\Model\ModelPsaPlanoAnimal | Objeto fin_conta
$id = 56; // int | ID do PsaPlanoAnimal

try {
    $result = $apiInstance->updatePsaPlanoAnimal($psa_plano_animal, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlanoAnimalApi->updatePsaPlanoAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_plano_animal** | [**\VertisConnect\Model\ModelPsaPlanoAnimal**](../Model/ModelPsaPlanoAnimal.md)| Objeto fin_conta |
 **id** | **int**| ID do PsaPlanoAnimal |

### Return type

[**\VertisConnect\Model\ModelPsaPlanoAnimal**](../Model/ModelPsaPlanoAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

