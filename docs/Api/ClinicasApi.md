# VertisConnect\ClinicasApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createClinica**](ClinicasApi.md#createClinica) | **POST** /api/V1.1/clinicas | 
[**deleteClinica**](ClinicasApi.md#deleteClinica) | **DELETE** /api/V1.1/clinicas/{id} | 
[**getClinica**](ClinicasApi.md#getClinica) | **GET** /api/V1.1/clinicas/{id} | 
[**getClinicaData**](ClinicasApi.md#getClinicaData) | **GET** /api/V1.1/clinicas-alt/{data} | 
[**getClinicas**](ClinicasApi.md#getClinicas) | **GET** /api/V1.1/clinicas | 
[**updateClinica**](ClinicasApi.md#updateClinica) | **PUT** /api/V1.1/clinicas/{id} | 


# **createClinica**
> \VertisConnect\Model\ModelClinicas createClinica($clinicas)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ClinicasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clinicas = new \VertisConnect\Model\ModelClinicas(); // \VertisConnect\Model\ModelClinicas | Objeto parc_clinica_nv

try {
    $result = $apiInstance->createClinica($clinicas);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicasApi->createClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clinicas** | [**\VertisConnect\Model\ModelClinicas**](../Model/ModelClinicas.md)| Objeto parc_clinica_nv |

### Return type

[**\VertisConnect\Model\ModelClinicas**](../Model/ModelClinicas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteClinica**
> \VertisConnect\Model\ModelClinicas deleteClinica($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ClinicasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Clinicas

try {
    $result = $apiInstance->deleteClinica($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicasApi->deleteClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Clinicas |

### Return type

[**\VertisConnect\Model\ModelClinicas**](../Model/ModelClinicas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinica**
> \VertisConnect\Model\ModelClinicas getClinica($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ClinicasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Clinicas

try {
    $result = $apiInstance->getClinica($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicasApi->getClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Clinicas |

### Return type

[**\VertisConnect\Model\ModelClinicas**](../Model/ModelClinicas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinicaData**
> \VertisConnect\Model\ModelClinicas getClinicaData($data)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ClinicasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$data = "data_example"; // string | data de alteracao Clinicas

try {
    $result = $apiInstance->getClinicaData($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicasApi->getClinicaData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | **string**| data de alteracao Clinicas |

### Return type

[**\VertisConnect\Model\ModelClinicas**](../Model/ModelClinicas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinicas**
> \VertisConnect\Model\ModelClinicas[] getClinicas()



Retorna registros do objeto parc_clinica_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ClinicasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getClinicas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicasApi->getClinicas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelClinicas[]**](../Model/ModelClinicas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateClinica**
> \VertisConnect\Model\ModelClinicas updateClinica($clinicas, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ClinicasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$clinicas = new \VertisConnect\Model\ModelClinicas(); // \VertisConnect\Model\ModelClinicas | Objeto parc_clinica_nv
$id = 56; // int | ID do Clinicas

try {
    $result = $apiInstance->updateClinica($clinicas, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClinicasApi->updateClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clinicas** | [**\VertisConnect\Model\ModelClinicas**](../Model/ModelClinicas.md)| Objeto parc_clinica_nv |
 **id** | **int**| ID do Clinicas |

### Return type

[**\VertisConnect\Model\ModelClinicas**](../Model/ModelClinicas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

