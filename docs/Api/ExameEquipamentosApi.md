# VertisConnect\ExameEquipamentosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExameEquipamentos**](ExameEquipamentosApi.md#createExameEquipamentos) | **POST** /api/V1.1/equipamentos | 
[**deleteExameEquipamentos**](ExameEquipamentosApi.md#deleteExameEquipamentos) | **DELETE** /api/V1.1/equipamentos/{id} | 
[**getExameEquipamento**](ExameEquipamentosApi.md#getExameEquipamento) | **GET** /api/V1.1/equipamentos/{id} | 
[**getExameEquipamentos**](ExameEquipamentosApi.md#getExameEquipamentos) | **GET** /api/V1.1/equipamentos | 
[**updateExameEquipamentos**](ExameEquipamentosApi.md#updateExameEquipamentos) | **PUT** /api/V1.1/equipamentos/{id} | 


# **createExameEquipamentos**
> \VertisConnect\Model\ModelExameEquipamentos createExameEquipamentos($exame_equipamentos)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exame_equipamentos = new \VertisConnect\Model\ModelExameEquipamentos(); // \VertisConnect\Model\ModelExameEquipamentos | Objeto prod_exa_equipamento_nv

try {
    $result = $apiInstance->createExameEquipamentos($exame_equipamentos);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosApi->createExameEquipamentos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exame_equipamentos** | [**\VertisConnect\Model\ModelExameEquipamentos**](../Model/ModelExameEquipamentos.md)| Objeto prod_exa_equipamento_nv |

### Return type

[**\VertisConnect\Model\ModelExameEquipamentos**](../Model/ModelExameEquipamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteExameEquipamentos**
> \VertisConnect\Model\ModelExameEquipamentos deleteExameEquipamentos($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame Equipamentos

try {
    $result = $apiInstance->deleteExameEquipamentos($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosApi->deleteExameEquipamentos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame Equipamentos |

### Return type

[**\VertisConnect\Model\ModelExameEquipamentos**](../Model/ModelExameEquipamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameEquipamento**
> \VertisConnect\Model\ModelExameEquipamentos getExameEquipamento($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame Equipamentos

try {
    $result = $apiInstance->getExameEquipamento($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosApi->getExameEquipamento: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame Equipamentos |

### Return type

[**\VertisConnect\Model\ModelExameEquipamentos**](../Model/ModelExameEquipamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameEquipamentos**
> \VertisConnect\Model\ModelExameEquipamentos[] getExameEquipamentos()



Retorna registros do objeto prod_exa_equipamento_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExameEquipamentos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosApi->getExameEquipamentos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelExameEquipamentos[]**](../Model/ModelExameEquipamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExameEquipamentos**
> \VertisConnect\Model\ModelExameEquipamentos updateExameEquipamentos($prod_exa_equipamento_nv, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$prod_exa_equipamento_nv = new \VertisConnect\Model\ModelExameEquipamentos(); // \VertisConnect\Model\ModelExameEquipamentos | Objeto prod_exa_equipamento_nv
$id = 56; // int | ID do Exame Equipamentos

try {
    $result = $apiInstance->updateExameEquipamentos($prod_exa_equipamento_nv, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosApi->updateExameEquipamentos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **prod_exa_equipamento_nv** | [**\VertisConnect\Model\ModelExameEquipamentos**](../Model/ModelExameEquipamentos.md)| Objeto prod_exa_equipamento_nv |
 **id** | **int**| ID do Exame Equipamentos |

### Return type

[**\VertisConnect\Model\ModelExameEquipamentos**](../Model/ModelExameEquipamentos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

