# VertisConnect\ControllerCRMClinicaTControllerCRMClinicaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11CrmclinicasGet**](ControllerCRMClinicaTControllerCRMClinicaApi.md#apiV11CrmclinicasGet) | **GET** /api/V1.1/crmclinicas | 
[**apiV11CrmclinicasIdDelete**](ControllerCRMClinicaTControllerCRMClinicaApi.md#apiV11CrmclinicasIdDelete) | **DELETE** /api/V1.1/crmclinicas/{id} | 
[**apiV11CrmclinicasIdGet**](ControllerCRMClinicaTControllerCRMClinicaApi.md#apiV11CrmclinicasIdGet) | **GET** /api/V1.1/crmclinicas/{id} | 
[**apiV11CrmclinicasIdPut**](ControllerCRMClinicaTControllerCRMClinicaApi.md#apiV11CrmclinicasIdPut) | **PUT** /api/V1.1/crmclinicas/{id} | 
[**apiV11CrmclinicasPost**](ControllerCRMClinicaTControllerCRMClinicaApi.md#apiV11CrmclinicasPost) | **POST** /api/V1.1/crmclinicas | 


# **apiV11CrmclinicasGet**
> apiV11CrmclinicasGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerCRMClinicaTControllerCRMClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11CrmclinicasGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerCRMClinicaTControllerCRMClinicaApi->apiV11CrmclinicasGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11CrmclinicasIdDelete**
> apiV11CrmclinicasIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerCRMClinicaTControllerCRMClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11CrmclinicasIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerCRMClinicaTControllerCRMClinicaApi->apiV11CrmclinicasIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11CrmclinicasIdGet**
> apiV11CrmclinicasIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerCRMClinicaTControllerCRMClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11CrmclinicasIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerCRMClinicaTControllerCRMClinicaApi->apiV11CrmclinicasIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11CrmclinicasIdPut**
> apiV11CrmclinicasIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerCRMClinicaTControllerCRMClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11CrmclinicasIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerCRMClinicaTControllerCRMClinicaApi->apiV11CrmclinicasIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11CrmclinicasPost**
> apiV11CrmclinicasPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerCRMClinicaTControllerCRMClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11CrmclinicasPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerCRMClinicaTControllerCRMClinicaApi->apiV11CrmclinicasPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

