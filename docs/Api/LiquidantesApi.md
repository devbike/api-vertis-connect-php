# VertisConnect\LiquidantesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLiquidante**](LiquidantesApi.md#createLiquidante) | **POST** /api/V1.1/liquidantes | 
[**deleteLiquidante**](LiquidantesApi.md#deleteLiquidante) | **DELETE** /api/V1.1/liquidantes/{id} | 
[**getLiquidante**](LiquidantesApi.md#getLiquidante) | **GET** /api/V1.1/liquidantes/{id} | 
[**getLiquidantes**](LiquidantesApi.md#getLiquidantes) | **GET** /api/V1.1/liquidantes | 
[**updateLiquidante**](LiquidantesApi.md#updateLiquidante) | **PUT** /api/V1.1/liquidantes/{id} | 


# **createLiquidante**
> \VertisConnect\Model\ModelLiquidante createLiquidante($liquidantes)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LiquidantesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$liquidantes = new \VertisConnect\Model\ModelLiquidante(); // \VertisConnect\Model\ModelLiquidante | Objeto liquidante

try {
    $result = $apiInstance->createLiquidante($liquidantes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LiquidantesApi->createLiquidante: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **liquidantes** | [**\VertisConnect\Model\ModelLiquidante**](../Model/ModelLiquidante.md)| Objeto liquidante |

### Return type

[**\VertisConnect\Model\ModelLiquidante**](../Model/ModelLiquidante.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteLiquidante**
> \VertisConnect\Model\ModelLiquidante deleteLiquidante($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LiquidantesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Liquidantes

try {
    $result = $apiInstance->deleteLiquidante($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LiquidantesApi->deleteLiquidante: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Liquidantes |

### Return type

[**\VertisConnect\Model\ModelLiquidante**](../Model/ModelLiquidante.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLiquidante**
> \VertisConnect\Model\ModelLiquidante getLiquidante($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LiquidantesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Liquidantes

try {
    $result = $apiInstance->getLiquidante($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LiquidantesApi->getLiquidante: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Liquidantes |

### Return type

[**\VertisConnect\Model\ModelLiquidante**](../Model/ModelLiquidante.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLiquidantes**
> \VertisConnect\Model\ModelLiquidante[] getLiquidantes()



Retorna registros do objeto liquidante

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LiquidantesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getLiquidantes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LiquidantesApi->getLiquidantes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelLiquidante[]**](../Model/ModelLiquidante.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateLiquidante**
> \VertisConnect\Model\ModelLiquidante updateLiquidante($liquidantes, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LiquidantesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$liquidantes = new \VertisConnect\Model\ModelLiquidante(); // \VertisConnect\Model\ModelLiquidante | Objeto liquidante
$id = 56; // int | ID do Liquidantes

try {
    $result = $apiInstance->updateLiquidante($liquidantes, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LiquidantesApi->updateLiquidante: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **liquidantes** | [**\VertisConnect\Model\ModelLiquidante**](../Model/ModelLiquidante.md)| Objeto liquidante |
 **id** | **int**| ID do Liquidantes |

### Return type

[**\VertisConnect\Model\ModelLiquidante**](../Model/ModelLiquidante.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

