# VertisConnect\ControllerRacaDParaTControllerRacaDParaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11RacasdparaGet**](ControllerRacaDParaTControllerRacaDParaApi.md#apiV11RacasdparaGet) | **GET** /api/V1.1/racasdpara | 
[**apiV11RacasdparaIdDelete**](ControllerRacaDParaTControllerRacaDParaApi.md#apiV11RacasdparaIdDelete) | **DELETE** /api/V1.1/racasdpara/{id} | 
[**apiV11RacasdparaIdGet**](ControllerRacaDParaTControllerRacaDParaApi.md#apiV11RacasdparaIdGet) | **GET** /api/V1.1/racasdpara/{id} | 
[**apiV11RacasdparaIdPut**](ControllerRacaDParaTControllerRacaDParaApi.md#apiV11RacasdparaIdPut) | **PUT** /api/V1.1/racasdpara/{id} | 
[**apiV11RacasdparaPost**](ControllerRacaDParaTControllerRacaDParaApi.md#apiV11RacasdparaPost) | **POST** /api/V1.1/racasdpara | 


# **apiV11RacasdparaGet**
> apiV11RacasdparaGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerRacaDParaTControllerRacaDParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11RacasdparaGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerRacaDParaTControllerRacaDParaApi->apiV11RacasdparaGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RacasdparaIdDelete**
> apiV11RacasdparaIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerRacaDParaTControllerRacaDParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11RacasdparaIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerRacaDParaTControllerRacaDParaApi->apiV11RacasdparaIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RacasdparaIdGet**
> apiV11RacasdparaIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerRacaDParaTControllerRacaDParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11RacasdparaIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerRacaDParaTControllerRacaDParaApi->apiV11RacasdparaIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RacasdparaIdPut**
> apiV11RacasdparaIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerRacaDParaTControllerRacaDParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11RacasdparaIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerRacaDParaTControllerRacaDParaApi->apiV11RacasdparaIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RacasdparaPost**
> apiV11RacasdparaPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerRacaDParaTControllerRacaDParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11RacasdparaPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerRacaDParaTControllerRacaDParaApi->apiV11RacasdparaPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

