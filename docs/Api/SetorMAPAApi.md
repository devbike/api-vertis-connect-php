# VertisConnect\SetorMAPAApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSetorXMapa**](SetorMAPAApi.md#createSetorXMapa) | **POST** /api/V1.1/setormapas | 
[**deleteSetorXMapa**](SetorMAPAApi.md#deleteSetorXMapa) | **DELETE** /api/V1.1/setormapas/{id} | 
[**getSetorXMapa**](SetorMAPAApi.md#getSetorXMapa) | **GET** /api/V1.1/setormapas/{id} | 
[**getSetorXMapas**](SetorMAPAApi.md#getSetorXMapas) | **GET** /api/V1.1/setormapas | 
[**updateSetorXMapa**](SetorMAPAApi.md#updateSetorXMapa) | **PUT** /api/V1.1/setormapas/{id} | 


# **createSetorXMapa**
> \VertisConnect\Model\ModelSetorXMapa createSetorXMapa($setor_mapa)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetorMAPAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$setor_mapa = new \VertisConnect\Model\ModelSetorXMapa(); // \VertisConnect\Model\ModelSetorXMapa | Objeto prod_exa_setor_x_mapa_nv

try {
    $result = $apiInstance->createSetorXMapa($setor_mapa);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetorMAPAApi->createSetorXMapa: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setor_mapa** | [**\VertisConnect\Model\ModelSetorXMapa**](../Model/ModelSetorXMapa.md)| Objeto prod_exa_setor_x_mapa_nv |

### Return type

[**\VertisConnect\Model\ModelSetorXMapa**](../Model/ModelSetorXMapa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSetorXMapa**
> \VertisConnect\Model\ModelSetorXMapa deleteSetorXMapa($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetorMAPAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Setor MAPA

try {
    $result = $apiInstance->deleteSetorXMapa($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetorMAPAApi->deleteSetorXMapa: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Setor MAPA |

### Return type

[**\VertisConnect\Model\ModelSetorXMapa**](../Model/ModelSetorXMapa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSetorXMapa**
> \VertisConnect\Model\ModelSetorXMapa getSetorXMapa($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetorMAPAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Setor MAPA

try {
    $result = $apiInstance->getSetorXMapa($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetorMAPAApi->getSetorXMapa: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Setor MAPA |

### Return type

[**\VertisConnect\Model\ModelSetorXMapa**](../Model/ModelSetorXMapa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSetorXMapas**
> \VertisConnect\Model\ModelSetorXMapa[] getSetorXMapas()



Retorna registros do objeto prod_exa_setor_x_mapa_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetorMAPAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getSetorXMapas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetorMAPAApi->getSetorXMapas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelSetorXMapa[]**](../Model/ModelSetorXMapa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSetorXMapa**
> \VertisConnect\Model\ModelSetorXMapa updateSetorXMapa($setor_mapa, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\SetorMAPAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$setor_mapa = new \VertisConnect\Model\ModelSetorXMapa(); // \VertisConnect\Model\ModelSetorXMapa | Objeto prod_exa_setor_x_mapa_nv
$id = 56; // int | ID do Setor MAPA

try {
    $result = $apiInstance->updateSetorXMapa($setor_mapa, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SetorMAPAApi->updateSetorXMapa: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setor_mapa** | [**\VertisConnect\Model\ModelSetorXMapa**](../Model/ModelSetorXMapa.md)| Objeto prod_exa_setor_x_mapa_nv |
 **id** | **int**| ID do Setor MAPA |

### Return type

[**\VertisConnect\Model\ModelSetorXMapa**](../Model/ModelSetorXMapa.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

