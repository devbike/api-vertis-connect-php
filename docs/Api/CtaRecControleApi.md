# VertisConnect\CtaRecControleApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCtaRecControle**](CtaRecControleApi.md#createCtaRecControle) | **POST** /api/V1.1/cta-rec-controle | 
[**deleteCtaRecControle**](CtaRecControleApi.md#deleteCtaRecControle) | **DELETE** /api/V1.1/cta-rec-controle/{id} | 
[**getCtaRecControle**](CtaRecControleApi.md#getCtaRecControle) | **GET** /api/V1.1/cta-rec-controle/{id} | 
[**getCtaRecControles**](CtaRecControleApi.md#getCtaRecControles) | **GET** /api/V1.1/cta-rec-controle | 
[**updateCtaRecControle**](CtaRecControleApi.md#updateCtaRecControle) | **PUT** /api/V1.1/cta-rec-controle/{id} | 


# **createCtaRecControle**
> \VertisConnect\Model\ModelCtaRecControle createCtaRecControle($cta_rec_controle)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CtaRecControleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cta_rec_controle = new \VertisConnect\Model\ModelCtaRecControle(); // \VertisConnect\Model\ModelCtaRecControle | Objeto cta_rec_controle

try {
    $result = $apiInstance->createCtaRecControle($cta_rec_controle);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CtaRecControleApi->createCtaRecControle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cta_rec_controle** | [**\VertisConnect\Model\ModelCtaRecControle**](../Model/ModelCtaRecControle.md)| Objeto cta_rec_controle |

### Return type

[**\VertisConnect\Model\ModelCtaRecControle**](../Model/ModelCtaRecControle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCtaRecControle**
> \VertisConnect\Model\ModelCtaRecControle deleteCtaRecControle($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CtaRecControleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do CtaRecControle

try {
    $result = $apiInstance->deleteCtaRecControle($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CtaRecControleApi->deleteCtaRecControle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do CtaRecControle |

### Return type

[**\VertisConnect\Model\ModelCtaRecControle**](../Model/ModelCtaRecControle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCtaRecControle**
> \VertisConnect\Model\ModelCtaRecControle getCtaRecControle($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CtaRecControleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do CtaRecControle

try {
    $result = $apiInstance->getCtaRecControle($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CtaRecControleApi->getCtaRecControle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do CtaRecControle |

### Return type

[**\VertisConnect\Model\ModelCtaRecControle**](../Model/ModelCtaRecControle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCtaRecControles**
> \VertisConnect\Model\ModelCtaRecControle[] getCtaRecControles()



Retorna registros do objeto cta_rec_controle

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CtaRecControleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getCtaRecControles();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CtaRecControleApi->getCtaRecControles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelCtaRecControle[]**](../Model/ModelCtaRecControle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCtaRecControle**
> \VertisConnect\Model\ModelCtaRecControle updateCtaRecControle($cta_rec_controle, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CtaRecControleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cta_rec_controle = new \VertisConnect\Model\ModelCtaRecControle(); // \VertisConnect\Model\ModelCtaRecControle | Objeto cta_rec_controle
$id = 56; // int | ID do CtaRecControle

try {
    $result = $apiInstance->updateCtaRecControle($cta_rec_controle, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CtaRecControleApi->updateCtaRecControle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cta_rec_controle** | [**\VertisConnect\Model\ModelCtaRecControle**](../Model/ModelCtaRecControle.md)| Objeto cta_rec_controle |
 **id** | **int**| ID do CtaRecControle |

### Return type

[**\VertisConnect\Model\ModelCtaRecControle**](../Model/ModelCtaRecControle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

