# VertisConnect\RelacionamentoClnicaMotoboyApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createClinica**](RelacionamentoClnicaMotoboyApi.md#createClinica) | **POST** /api/V1.1/motoboys/{mtb}/clinicas | 
[**deleteClinica**](RelacionamentoClnicaMotoboyApi.md#deleteClinica) | **DELETE** /api/V1.1/motoboys/{mtb}/clinicas/{cln} | 
[**getClinica**](RelacionamentoClnicaMotoboyApi.md#getClinica) | **GET** /api/V1.1/motoboys/{mtb}/clinicas/{cln} | 
[**getClinicas**](RelacionamentoClnicaMotoboyApi.md#getClinicas) | **GET** /api/V1.1/motoboys/{mtb}/clinicas | 
[**updateClinica**](RelacionamentoClnicaMotoboyApi.md#updateClinica) | **PUT** /api/V1.1/motoboys/{mtb}/clinicas/{cln} | 


# **createClinica**
> \VertisConnect\Model\ModelMotoboyClinica createClinica($relacionamento_clnica_motoboy, $mtb)



Insere um novo registro de clínica para o motoboy definido no parâmetro MTB.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RelacionamentoClnicaMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$relacionamento_clnica_motoboy = new \VertisConnect\Model\ModelMotoboyClinica(); // \VertisConnect\Model\ModelMotoboyClinica | parc_mtb_clin
$mtb = 56; // int | 

try {
    $result = $apiInstance->createClinica($relacionamento_clnica_motoboy, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RelacionamentoClnicaMotoboyApi->createClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **relacionamento_clnica_motoboy** | [**\VertisConnect\Model\ModelMotoboyClinica**](../Model/ModelMotoboyClinica.md)| parc_mtb_clin |
 **mtb** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelMotoboyClinica**](../Model/ModelMotoboyClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteClinica**
> \VertisConnect\Model\ModelMotoboyClinica deleteClinica($cln, $mtb)



Exclui o registro determinado no parâmetro CLN.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RelacionamentoClnicaMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cln = 56; // int | ID chave da Clínica
$mtb = 56; // int | ID chave do Motoboy

try {
    $result = $apiInstance->deleteClinica($cln, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RelacionamentoClnicaMotoboyApi->deleteClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cln** | **int**| ID chave da Clínica |
 **mtb** | **int**| ID chave do Motoboy |

### Return type

[**\VertisConnect\Model\ModelMotoboyClinica**](../Model/ModelMotoboyClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinica**
> \VertisConnect\Model\ModelMotoboyClinica getClinica($cln, $mtb)



Retorna informações do relacionamento selecionado atrvés dp parâmetro CLN.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RelacionamentoClnicaMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cln = 56; // int | ID chave da Clínica
$mtb = 56; // int | ID chave do Motoboy

try {
    $result = $apiInstance->getClinica($cln, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RelacionamentoClnicaMotoboyApi->getClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cln** | **int**| ID chave da Clínica |
 **mtb** | **int**| ID chave do Motoboy |

### Return type

[**\VertisConnect\Model\ModelMotoboyClinica**](../Model/ModelMotoboyClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinicas**
> \VertisConnect\Model\ModelMotoboyClinica[] getClinicas($mtb)



Retorna todas as clínicas relacionadas ao motoboy definido no parâmetro MTB.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RelacionamentoClnicaMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mtb = 56; // int | 

try {
    $result = $apiInstance->getClinicas($mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RelacionamentoClnicaMotoboyApi->getClinicas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mtb** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelMotoboyClinica[]**](../Model/ModelMotoboyClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateClinica**
> \VertisConnect\Model\ModelMotoboyClinica updateClinica($cln, $relacionamento_clnica_motoboy, $mtb)



Atualiza o registro determinado no parâmetro CLN.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RelacionamentoClnicaMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cln = 56; // int | ID chave da Clínica
$relacionamento_clnica_motoboy = new \VertisConnect\Model\ModelMotoboyClinica(); // \VertisConnect\Model\ModelMotoboyClinica | Objeto parc_mtb_clin
$mtb = 56; // int | ID chave do Motoboy

try {
    $result = $apiInstance->updateClinica($cln, $relacionamento_clnica_motoboy, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RelacionamentoClnicaMotoboyApi->updateClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cln** | **int**| ID chave da Clínica |
 **relacionamento_clnica_motoboy** | [**\VertisConnect\Model\ModelMotoboyClinica**](../Model/ModelMotoboyClinica.md)| Objeto parc_mtb_clin |
 **mtb** | **int**| ID chave do Motoboy |

### Return type

[**\VertisConnect\Model\ModelMotoboyClinica**](../Model/ModelMotoboyClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

