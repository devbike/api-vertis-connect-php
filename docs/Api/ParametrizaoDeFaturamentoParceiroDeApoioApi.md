# VertisConnect\ParametrizaoDeFaturamentoParceiroDeApoioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfFatApoio**](ParametrizaoDeFaturamentoParceiroDeApoioApi.md#doCreateInfFatApoio) | **POST** /api/V1.1/apoios/{id}/paramsfat | 
[**doDeleteInfFatApoio**](ParametrizaoDeFaturamentoParceiroDeApoioApi.md#doDeleteInfFatApoio) | **DELETE** /api/V1.1/apoios/{id}/paramsfat | 
[**doGetInfFatApoio**](ParametrizaoDeFaturamentoParceiroDeApoioApi.md#doGetInfFatApoio) | **GET** /api/V1.1/apoios/{id}/paramsfat | 
[**doUpdateInfFatApoio**](ParametrizaoDeFaturamentoParceiroDeApoioApi.md#doUpdateInfFatApoio) | **PUT** /api/V1.1/apoios/{id}/paramsfat | 


# **doCreateInfFatApoio**
> \VertisConnect\Model\ModelParcInfFaturamento doCreateInfFatApoio($parametrizao_de_faturamento, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_de_faturamento = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do Parceiro de Apoio

try {
    $result = $apiInstance->doCreateInfFatApoio($parametrizao_de_faturamento, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoParceiroDeApoioApi->doCreateInfFatApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_de_faturamento** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do Parceiro de Apoio |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfFatApoio**
> \VertisConnect\Model\ModelParcInfFaturamento doDeleteInfFatApoio($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfFatApoio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoParceiroDeApoioApi->doDeleteInfFatApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfFatApoio**
> \VertisConnect\Model\ModelParcInfFaturamento doGetInfFatApoio($id)



Retorna configurações de faturamento para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfFatApoio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoParceiroDeApoioApi->doGetInfFatApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfFatApoio**
> \VertisConnect\Model\ModelParcInfFaturamento doUpdateInfFatApoio($parametros_fiscais___parceiro_de_apoio, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___parceiro_de_apoio = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfFatApoio($parametros_fiscais___parceiro_de_apoio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoParceiroDeApoioApi->doUpdateInfFatApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___parceiro_de_apoio** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

