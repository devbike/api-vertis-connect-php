# VertisConnect\LocalDeTrabalhoDoUsurioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUsuarioLocalTrabalho**](LocalDeTrabalhoDoUsurioApi.md#createUsuarioLocalTrabalho) | **POST** /api/V1.1/usuarios_localtrabalho | 
[**deleteUsuarioLocalTrabalho**](LocalDeTrabalhoDoUsurioApi.md#deleteUsuarioLocalTrabalho) | **DELETE** /api/V1.1/usuarios_localtrabalho/{id} | 
[**getUsuarioLocalTrabalho**](LocalDeTrabalhoDoUsurioApi.md#getUsuarioLocalTrabalho) | **GET** /api/V1.1/usuarios_localtrabalho/{id} | 
[**getUsuariosLocalTrabalho**](LocalDeTrabalhoDoUsurioApi.md#getUsuariosLocalTrabalho) | **GET** /api/V1.1/usuarios_localtrabalho | 
[**updateUsuarioLocalTrabalho**](LocalDeTrabalhoDoUsurioApi.md#updateUsuarioLocalTrabalho) | **PUT** /api/V1.1/usuarios_localtrabalho/{id} | 


# **createUsuarioLocalTrabalho**
> \VertisConnect\Model\ModelParcUsuLocalTrabalho createUsuarioLocalTrabalho($local_de_trabalho_do_usurio)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LocalDeTrabalhoDoUsurioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$local_de_trabalho_do_usurio = new \VertisConnect\Model\ModelParcUsuLocalTrabalho(); // \VertisConnect\Model\ModelParcUsuLocalTrabalho | Objeto parc_usu_loc_trab_altern

try {
    $result = $apiInstance->createUsuarioLocalTrabalho($local_de_trabalho_do_usurio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocalDeTrabalhoDoUsurioApi->createUsuarioLocalTrabalho: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **local_de_trabalho_do_usurio** | [**\VertisConnect\Model\ModelParcUsuLocalTrabalho**](../Model/ModelParcUsuLocalTrabalho.md)| Objeto parc_usu_loc_trab_altern |

### Return type

[**\VertisConnect\Model\ModelParcUsuLocalTrabalho**](../Model/ModelParcUsuLocalTrabalho.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUsuarioLocalTrabalho**
> \VertisConnect\Model\ModelParcUsuLocalTrabalho deleteUsuarioLocalTrabalho($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LocalDeTrabalhoDoUsurioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Local de Trabalho do Usuario

try {
    $result = $apiInstance->deleteUsuarioLocalTrabalho($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocalDeTrabalhoDoUsurioApi->deleteUsuarioLocalTrabalho: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Local de Trabalho do Usuario |

### Return type

[**\VertisConnect\Model\ModelParcUsuLocalTrabalho**](../Model/ModelParcUsuLocalTrabalho.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUsuarioLocalTrabalho**
> \VertisConnect\Model\ModelParcUsuLocalTrabalho getUsuarioLocalTrabalho($un, $id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LocalDeTrabalhoDoUsurioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$un = 56; // int | Unidade Operacional de Trabalho do Usuario
$id = 56; // int | ID do Usuario

try {
    $result = $apiInstance->getUsuarioLocalTrabalho($un, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocalDeTrabalhoDoUsurioApi->getUsuarioLocalTrabalho: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **un** | **int**| Unidade Operacional de Trabalho do Usuario |
 **id** | **int**| ID do Usuario |

### Return type

[**\VertisConnect\Model\ModelParcUsuLocalTrabalho**](../Model/ModelParcUsuLocalTrabalho.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUsuariosLocalTrabalho**
> \VertisConnect\Model\ModelParcUsuLocalTrabalho[] getUsuariosLocalTrabalho()



Retorna registros do objeto parc_usu_loc_trab_altern

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LocalDeTrabalhoDoUsurioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getUsuariosLocalTrabalho();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocalDeTrabalhoDoUsurioApi->getUsuariosLocalTrabalho: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelParcUsuLocalTrabalho[]**](../Model/ModelParcUsuLocalTrabalho.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUsuarioLocalTrabalho**
> \VertisConnect\Model\ModelParcUsuLocalTrabalho updateUsuarioLocalTrabalho($local_de_trabalho_do_usurio, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\LocalDeTrabalhoDoUsurioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$local_de_trabalho_do_usurio = new \VertisConnect\Model\ModelParcUsuLocalTrabalho(); // \VertisConnect\Model\ModelParcUsuLocalTrabalho | Objeto parc_usu_loc_trab_altern
$id = 56; // int | ID do Local de Trabalho do Usuario

try {
    $result = $apiInstance->updateUsuarioLocalTrabalho($local_de_trabalho_do_usurio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LocalDeTrabalhoDoUsurioApi->updateUsuarioLocalTrabalho: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **local_de_trabalho_do_usurio** | [**\VertisConnect\Model\ModelParcUsuLocalTrabalho**](../Model/ModelParcUsuLocalTrabalho.md)| Objeto parc_usu_loc_trab_altern |
 **id** | **int**| ID do Local de Trabalho do Usuario |

### Return type

[**\VertisConnect\Model\ModelParcUsuLocalTrabalho**](../Model/ModelParcUsuLocalTrabalho.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

