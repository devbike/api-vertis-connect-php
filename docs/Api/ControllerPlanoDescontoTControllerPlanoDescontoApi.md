# VertisConnect\ControllerPlanoDescontoTControllerPlanoDescontoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11PlanosdescontosGet**](ControllerPlanoDescontoTControllerPlanoDescontoApi.md#apiV11PlanosdescontosGet) | **GET** /api/V1.1/planosdescontos | 
[**apiV11PlanosdescontosIdDelete**](ControllerPlanoDescontoTControllerPlanoDescontoApi.md#apiV11PlanosdescontosIdDelete) | **DELETE** /api/V1.1/planosdescontos/{id} | 
[**apiV11PlanosdescontosIdGet**](ControllerPlanoDescontoTControllerPlanoDescontoApi.md#apiV11PlanosdescontosIdGet) | **GET** /api/V1.1/planosdescontos/{id} | 
[**apiV11PlanosdescontosIdPut**](ControllerPlanoDescontoTControllerPlanoDescontoApi.md#apiV11PlanosdescontosIdPut) | **PUT** /api/V1.1/planosdescontos/{id} | 
[**apiV11PlanosdescontosPost**](ControllerPlanoDescontoTControllerPlanoDescontoApi.md#apiV11PlanosdescontosPost) | **POST** /api/V1.1/planosdescontos | 


# **apiV11PlanosdescontosGet**
> apiV11PlanosdescontosGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPlanoDescontoTControllerPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11PlanosdescontosGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerPlanoDescontoTControllerPlanoDescontoApi->apiV11PlanosdescontosGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PlanosdescontosIdDelete**
> apiV11PlanosdescontosIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPlanoDescontoTControllerPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PlanosdescontosIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerPlanoDescontoTControllerPlanoDescontoApi->apiV11PlanosdescontosIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PlanosdescontosIdGet**
> apiV11PlanosdescontosIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPlanoDescontoTControllerPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PlanosdescontosIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerPlanoDescontoTControllerPlanoDescontoApi->apiV11PlanosdescontosIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PlanosdescontosIdPut**
> apiV11PlanosdescontosIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPlanoDescontoTControllerPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PlanosdescontosIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerPlanoDescontoTControllerPlanoDescontoApi->apiV11PlanosdescontosIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PlanosdescontosPost**
> apiV11PlanosdescontosPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPlanoDescontoTControllerPlanoDescontoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11PlanosdescontosPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerPlanoDescontoTControllerPlanoDescontoApi->apiV11PlanosdescontosPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

