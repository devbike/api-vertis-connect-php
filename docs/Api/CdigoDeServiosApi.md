# VertisConnect\CdigoDeServiosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCodigoServico**](CdigoDeServiosApi.md#createCodigoServico) | **POST** /api/V1.1/codigoservicos | 
[**deleteCodigoServico**](CdigoDeServiosApi.md#deleteCodigoServico) | **DELETE** /api/V1.1/codigoservicos/{id} | 
[**getCodigoServico**](CdigoDeServiosApi.md#getCodigoServico) | **GET** /api/V1.1/codigoservicos/{id} | 
[**getCodigoServicos**](CdigoDeServiosApi.md#getCodigoServicos) | **GET** /api/V1.1/codigoservicos | 
[**updateCodigoServico**](CdigoDeServiosApi.md#updateCodigoServico) | **PUT** /api/V1.1/codigoservicos/{id} | 


# **createCodigoServico**
> \VertisConnect\Model\ModelCodigoServico createCodigoServico($cdigo_de_servios)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CdigoDeServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cdigo_de_servios = new \VertisConnect\Model\ModelCodigoServico(); // \VertisConnect\Model\ModelCodigoServico | Objeto unid_oper_cod_servicos

try {
    $result = $apiInstance->createCodigoServico($cdigo_de_servios);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CdigoDeServiosApi->createCodigoServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cdigo_de_servios** | [**\VertisConnect\Model\ModelCodigoServico**](../Model/ModelCodigoServico.md)| Objeto unid_oper_cod_servicos |

### Return type

[**\VertisConnect\Model\ModelCodigoServico**](../Model/ModelCodigoServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCodigoServico**
> \VertisConnect\Model\ModelCodigoServico deleteCodigoServico($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CdigoDeServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Codigo Serviços

try {
    $result = $apiInstance->deleteCodigoServico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CdigoDeServiosApi->deleteCodigoServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Codigo Serviços |

### Return type

[**\VertisConnect\Model\ModelCodigoServico**](../Model/ModelCodigoServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCodigoServico**
> \VertisConnect\Model\ModelCodigoServico getCodigoServico($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CdigoDeServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Codigo Serviços

try {
    $result = $apiInstance->getCodigoServico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CdigoDeServiosApi->getCodigoServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Codigo Serviços |

### Return type

[**\VertisConnect\Model\ModelCodigoServico**](../Model/ModelCodigoServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCodigoServicos**
> \VertisConnect\Model\ModelCodigoServico[] getCodigoServicos()



Retorna registros do objeto unid_oper_cod_servicos

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CdigoDeServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getCodigoServicos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CdigoDeServiosApi->getCodigoServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelCodigoServico[]**](../Model/ModelCodigoServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCodigoServico**
> \VertisConnect\Model\ModelCodigoServico updateCodigoServico($cdigo_de_servios, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\CdigoDeServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$cdigo_de_servios = new \VertisConnect\Model\ModelCodigoServico(); // \VertisConnect\Model\ModelCodigoServico | Objeto unid_oper_cod_servicos
$id = 56; // int | ID do Codigo Serviços

try {
    $result = $apiInstance->updateCodigoServico($cdigo_de_servios, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CdigoDeServiosApi->updateCodigoServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cdigo_de_servios** | [**\VertisConnect\Model\ModelCodigoServico**](../Model/ModelCodigoServico.md)| Objeto unid_oper_cod_servicos |
 **id** | **int**| ID do Codigo Serviços |

### Return type

[**\VertisConnect\Model\ModelCodigoServico**](../Model/ModelCodigoServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

