# VertisConnect\ServiosInstruesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createInstrucoes**](ServiosInstruesApi.md#createInstrucoes) | **POST** /api/V1.1/instrucoes | 
[**deleteInstrucoes**](ServiosInstruesApi.md#deleteInstrucoes) | **DELETE** /api/V1.1/instrucoes/{id} | 
[**getInstrucoes**](ServiosInstruesApi.md#getInstrucoes) | **GET** /api/V1.1/instrucoes/{id} | 
[**getInstrucoess**](ServiosInstruesApi.md#getInstrucoess) | **GET** /api/V1.1/instrucoes | 
[**updateInstrucoes**](ServiosInstruesApi.md#updateInstrucoes) | **PUT** /api/V1.1/instrucoes/{id} | 


# **createInstrucoes**
> \VertisConnect\Model\ModelServInstrucoes createInstrucoes($servios___instrues)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosInstruesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$servios___instrues = new \VertisConnect\Model\ModelServInstrucoes(); // \VertisConnect\Model\ModelServInstrucoes | Objeto prod_serv_instrucoes_nv

try {
    $result = $apiInstance->createInstrucoes($servios___instrues);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosInstruesApi->createInstrucoes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **servios___instrues** | [**\VertisConnect\Model\ModelServInstrucoes**](../Model/ModelServInstrucoes.md)| Objeto prod_serv_instrucoes_nv |

### Return type

[**\VertisConnect\Model\ModelServInstrucoes**](../Model/ModelServInstrucoes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteInstrucoes**
> \VertisConnect\Model\ModelServInstrucoes deleteInstrucoes($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosInstruesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Serviços Instruções

try {
    $result = $apiInstance->deleteInstrucoes($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosInstruesApi->deleteInstrucoes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Serviços Instruções |

### Return type

[**\VertisConnect\Model\ModelServInstrucoes**](../Model/ModelServInstrucoes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInstrucoes**
> \VertisConnect\Model\ModelServInstrucoes getInstrucoes($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosInstruesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Serviços Instruções

try {
    $result = $apiInstance->getInstrucoes($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosInstruesApi->getInstrucoes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Serviços Instruções |

### Return type

[**\VertisConnect\Model\ModelServInstrucoes**](../Model/ModelServInstrucoes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInstrucoess**
> \VertisConnect\Model\ModelServInstrucoes[] getInstrucoess()



Retorna registros do objeto prod_serv_instrucoes_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosInstruesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getInstrucoess();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosInstruesApi->getInstrucoess: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelServInstrucoes[]**](../Model/ModelServInstrucoes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateInstrucoes**
> \VertisConnect\Model\ModelServInstrucoes updateInstrucoes($servios___instrues, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosInstruesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$servios___instrues = new \VertisConnect\Model\ModelServInstrucoes(); // \VertisConnect\Model\ModelServInstrucoes | Objeto prod_serv_instrucoes_nv
$id = 56; // int | ID do Serviços Instruções

try {
    $result = $apiInstance->updateInstrucoes($servios___instrues, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosInstruesApi->updateInstrucoes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **servios___instrues** | [**\VertisConnect\Model\ModelServInstrucoes**](../Model/ModelServInstrucoes.md)| Objeto prod_serv_instrucoes_nv |
 **id** | **int**| ID do Serviços Instruções |

### Return type

[**\VertisConnect\Model\ModelServInstrucoes**](../Model/ModelServInstrucoes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

