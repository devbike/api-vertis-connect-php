# VertisConnect\ControllerExameDParaTControllerExameDeParaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11ExamesdparaGet**](ControllerExameDParaTControllerExameDeParaApi.md#apiV11ExamesdparaGet) | **GET** /api/V1.1/examesdpara | 
[**apiV11ExamesdparaIdDelete**](ControllerExameDParaTControllerExameDeParaApi.md#apiV11ExamesdparaIdDelete) | **DELETE** /api/V1.1/examesdpara/{id} | 
[**apiV11ExamesdparaIdGet**](ControllerExameDParaTControllerExameDeParaApi.md#apiV11ExamesdparaIdGet) | **GET** /api/V1.1/examesdpara/{id} | 
[**apiV11ExamesdparaIdPut**](ControllerExameDParaTControllerExameDeParaApi.md#apiV11ExamesdparaIdPut) | **PUT** /api/V1.1/examesdpara/{id} | 
[**apiV11ExamesdparaPost**](ControllerExameDParaTControllerExameDeParaApi.md#apiV11ExamesdparaPost) | **POST** /api/V1.1/examesdpara | 
[**apiV11Get**](ControllerExameDParaTControllerExameDeParaApi.md#apiV11Get) | **GET** /api/V1.1 | 
[**apiV11ReversedstringsValueGet**](ControllerExameDParaTControllerExameDeParaApi.md#apiV11ReversedstringsValueGet) | **GET** /api/V1.1/reversedstrings/{Value} | 


# **apiV11ExamesdparaGet**
> apiV11ExamesdparaGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExameDParaTControllerExameDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11ExamesdparaGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerExameDParaTControllerExameDeParaApi->apiV11ExamesdparaGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ExamesdparaIdDelete**
> apiV11ExamesdparaIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExameDParaTControllerExameDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11ExamesdparaIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerExameDParaTControllerExameDeParaApi->apiV11ExamesdparaIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ExamesdparaIdGet**
> apiV11ExamesdparaIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExameDParaTControllerExameDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11ExamesdparaIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerExameDParaTControllerExameDeParaApi->apiV11ExamesdparaIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ExamesdparaIdPut**
> apiV11ExamesdparaIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExameDParaTControllerExameDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11ExamesdparaIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerExameDParaTControllerExameDeParaApi->apiV11ExamesdparaIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ExamesdparaPost**
> apiV11ExamesdparaPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExameDParaTControllerExameDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11ExamesdparaPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerExameDParaTControllerExameDeParaApi->apiV11ExamesdparaPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11Get**
> apiV11Get()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExameDParaTControllerExameDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11Get();
} catch (Exception $e) {
    echo 'Exception when calling ControllerExameDParaTControllerExameDeParaApi->apiV11Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11ReversedstringsValueGet**
> apiV11ReversedstringsValueGet($value)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExameDParaTControllerExameDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$value = "value_example"; // string | 

try {
    $apiInstance->apiV11ReversedstringsValueGet($value);
} catch (Exception $e) {
    echo 'Exception when calling ControllerExameDParaTControllerExameDeParaApi->apiV11ReversedstringsValueGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

