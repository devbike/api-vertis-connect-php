# VertisConnect\ExamesPerfilApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExamePerfil**](ExamesPerfilApi.md#createExamePerfil) | **POST** /api/V1.1/exameperfil | 
[**deleteExamePerfil**](ExamesPerfilApi.md#deleteExamePerfil) | **DELETE** /api/V1.1/exameperfil/{id} | 
[**getExamePerfil**](ExamesPerfilApi.md#getExamePerfil) | **GET** /api/V1.1/exameperfil/{id} | 
[**getExamePerfis**](ExamesPerfilApi.md#getExamePerfis) | **GET** /api/V1.1/exameperfil | 
[**getExamesPerfil**](ExamesPerfilApi.md#getExamesPerfil) | **GET** /api/V1.1/perfil/{id}/exames | 
[**getPerfisExame**](ExamesPerfilApi.md#getPerfisExame) | **GET** /api/V1.1/exames/{id}/perfil | 
[**getPerfisUnidade**](ExamesPerfilApi.md#getPerfisUnidade) | **GET** /api/V1.1/unidneg/{id}/perfis | 
[**updateExamePerfil**](ExamesPerfilApi.md#updateExamePerfil) | **PUT** /api/V1.1/exameperfil/{id} | 


# **createExamePerfil**
> \VertisConnect\Model\ModelExamePerfil createExamePerfil($exames___perfil)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exames___perfil = new \VertisConnect\Model\ModelExamePerfil(); // \VertisConnect\Model\ModelExamePerfil | Objeto prod_exa_perfil_nv

try {
    $result = $apiInstance->createExamePerfil($exames___perfil);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->createExamePerfil: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exames___perfil** | [**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)| Objeto prod_exa_perfil_nv |

### Return type

[**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteExamePerfil**
> \VertisConnect\Model\ModelExamePerfil deleteExamePerfil($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Perfil

try {
    $result = $apiInstance->deleteExamePerfil($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->deleteExamePerfil: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Perfil |

### Return type

[**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExamePerfil**
> \VertisConnect\Model\ModelExamePerfil getExamePerfil($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Perfil

try {
    $result = $apiInstance->getExamePerfil($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->getExamePerfil: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Perfil |

### Return type

[**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExamePerfis**
> \VertisConnect\Model\ModelExamePerfil[] getExamePerfis()



Retorna registros do objeto prod_exa_perfil_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExamePerfis();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->getExamePerfis: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelExamePerfil[]**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExamesPerfil**
> \VertisConnect\Model\ModelExamePerfil getExamesPerfil($id)



Retorna todos os exames de um perfil determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Perfil

try {
    $result = $apiInstance->getExamesPerfil($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->getExamesPerfil: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Perfil |

### Return type

[**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPerfisExame**
> \VertisConnect\Model\ModelExamePerfil getPerfisExame($id)



Retorna todos os perfis de um exame determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame

try {
    $result = $apiInstance->getPerfisExame($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->getPerfisExame: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame |

### Return type

[**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPerfisUnidade**
> \VertisConnect\Model\ModelExamePerfil getPerfisUnidade($id)



Retorna todos os perfis de uma unidade de negocio determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID da Unidade de Negocio

try {
    $result = $apiInstance->getPerfisUnidade($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->getPerfisUnidade: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID da Unidade de Negocio |

### Return type

[**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExamePerfil**
> \VertisConnect\Model\ModelExamePerfil updateExamePerfil($exames___perfil, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesPerfilApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exames___perfil = new \VertisConnect\Model\ModelExamePerfil(); // \VertisConnect\Model\ModelExamePerfil | Objeto prod_exa_perfil_nv
$id = 56; // int | ID do Perfil

try {
    $result = $apiInstance->updateExamePerfil($exames___perfil, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesPerfilApi->updateExamePerfil: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exames___perfil** | [**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)| Objeto prod_exa_perfil_nv |
 **id** | **int**| ID do Perfil |

### Return type

[**\VertisConnect\Model\ModelExamePerfil**](../Model/ModelExamePerfil.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

