# VertisConnect\PsaFaturaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPsaFatura**](PsaFaturaApi.md#createPsaFatura) | **POST** /api/V1.1/psa-fatura | 
[**deletePsaFatura**](PsaFaturaApi.md#deletePsaFatura) | **DELETE** /api/V1.1/psa-fatura/{id} | 
[**getPsaFatura**](PsaFaturaApi.md#getPsaFatura) | **GET** /api/V1.1/psa-fatura/{id} | 
[**getPsaFaturas**](PsaFaturaApi.md#getPsaFaturas) | **GET** /api/V1.1/psa-fatura | 
[**updatePsaFatura**](PsaFaturaApi.md#updatePsaFatura) | **PUT** /api/V1.1/psa-fatura/{id} | 


# **createPsaFatura**
> \VertisConnect\Model\ModelPsaFatura createPsaFatura($psa_fatura)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaFaturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_fatura = new \VertisConnect\Model\ModelPsaFatura(); // \VertisConnect\Model\ModelPsaFatura | Objeto fin_conta

try {
    $result = $apiInstance->createPsaFatura($psa_fatura);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaFaturaApi->createPsaFatura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_fatura** | [**\VertisConnect\Model\ModelPsaFatura**](../Model/ModelPsaFatura.md)| Objeto fin_conta |

### Return type

[**\VertisConnect\Model\ModelPsaFatura**](../Model/ModelPsaFatura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePsaFatura**
> \VertisConnect\Model\ModelPsaFatura deletePsaFatura($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaFaturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaFatura

try {
    $result = $apiInstance->deletePsaFatura($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaFaturaApi->deletePsaFatura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaFatura |

### Return type

[**\VertisConnect\Model\ModelPsaFatura**](../Model/ModelPsaFatura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaFatura**
> \VertisConnect\Model\ModelPsaFatura getPsaFatura($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaFaturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaFatura

try {
    $result = $apiInstance->getPsaFatura($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaFaturaApi->getPsaFatura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaFatura |

### Return type

[**\VertisConnect\Model\ModelPsaFatura**](../Model/ModelPsaFatura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaFaturas**
> \VertisConnect\Model\ModelPsaFatura[] getPsaFaturas()



Retorna registros do objeto fin_conta

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaFaturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPsaFaturas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaFaturaApi->getPsaFaturas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelPsaFatura[]**](../Model/ModelPsaFatura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePsaFatura**
> \VertisConnect\Model\ModelPsaFatura updatePsaFatura($psa_fatura, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaFaturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_fatura = new \VertisConnect\Model\ModelPsaFatura(); // \VertisConnect\Model\ModelPsaFatura | Objeto fin_conta
$id = 56; // int | ID do PsaFatura

try {
    $result = $apiInstance->updatePsaFatura($psa_fatura, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaFaturaApi->updatePsaFatura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_fatura** | [**\VertisConnect\Model\ModelPsaFatura**](../Model/ModelPsaFatura.md)| Objeto fin_conta |
 **id** | **int**| ID do PsaFatura |

### Return type

[**\VertisConnect\Model\ModelPsaFatura**](../Model/ModelPsaFatura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

