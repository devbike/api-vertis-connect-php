# VertisConnect\AnimXVeterinarioNvApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAnimalXVeterinario**](AnimXVeterinarioNvApi.md#deleteAnimalXVeterinario) | **DELETE** /api/V1.1/animais_x_veterinarios/{id} | 
[**updateAnimalXVeterinario**](AnimXVeterinarioNvApi.md#updateAnimalXVeterinario) | **PUT** /api/V1.1/animais_x_veterinarios/{id} | 


# **deleteAnimalXVeterinario**
> \VertisConnect\Model\ModelAnimalXVeterinario deleteAnimalXVeterinario($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimXVeterinarioNvApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animal_x_Veterinario

try {
    $result = $apiInstance->deleteAnimalXVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimXVeterinarioNvApi->deleteAnimalXVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animal_x_Veterinario |

### Return type

[**\VertisConnect\Model\ModelAnimalXVeterinario**](../Model/ModelAnimalXVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAnimalXVeterinario**
> \VertisConnect\Model\ModelAnimalXVeterinario updateAnimalXVeterinario($anim_x_veterinario_nv, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimXVeterinarioNvApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$anim_x_veterinario_nv = new \VertisConnect\Model\ModelAnimalXVeterinario(); // \VertisConnect\Model\ModelAnimalXVeterinario | Objeto anim_x_veterinario_nv
$id = 56; // int | ID do Animal_x_Veterinario

try {
    $result = $apiInstance->updateAnimalXVeterinario($anim_x_veterinario_nv, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimXVeterinarioNvApi->updateAnimalXVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anim_x_veterinario_nv** | [**\VertisConnect\Model\ModelAnimalXVeterinario**](../Model/ModelAnimalXVeterinario.md)| Objeto anim_x_veterinario_nv |
 **id** | **int**| ID do Animal_x_Veterinario |

### Return type

[**\VertisConnect\Model\ModelAnimalXVeterinario**](../Model/ModelAnimalXVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

