# VertisConnect\ProdutosEFornecedoresApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFornecedorProduto**](ProdutosEFornecedoresApi.md#createFornecedorProduto) | **POST** /api/V1.1/fornecedores/{f}/produtos | 
[**deleteFornecedorProduto**](ProdutosEFornecedoresApi.md#deleteFornecedorProduto) | **DELETE** /api/V1.1/fornecedores/{f}/produtos/{id} | 
[**getFornecedorProduto**](ProdutosEFornecedoresApi.md#getFornecedorProduto) | **GET** /api/V1.1/fornecedores/{f}/produtos/{id} | 
[**getFornecedorProdutos**](ProdutosEFornecedoresApi.md#getFornecedorProdutos) | **GET** /api/V1.1/fornecedores/{f}/produtos | 
[**updateFornecedorProduto**](ProdutosEFornecedoresApi.md#updateFornecedorProduto) | **PUT** /api/V1.1/fornecedores/{f}/produtos/{id} | 


# **createFornecedorProduto**
> \VertisConnect\Model\ModelFornecedorProduto createFornecedorProduto($produtos_e_fornecedores, $f)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosEFornecedoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$produtos_e_fornecedores = new \VertisConnect\Model\ModelFornecedorProduto(); // \VertisConnect\Model\ModelFornecedorProduto | Objeto parc_forn_produto
$f = 56; // int | 

try {
    $result = $apiInstance->createFornecedorProduto($produtos_e_fornecedores, $f);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosEFornecedoresApi->createFornecedorProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtos_e_fornecedores** | [**\VertisConnect\Model\ModelFornecedorProduto**](../Model/ModelFornecedorProduto.md)| Objeto parc_forn_produto |
 **f** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelFornecedorProduto**](../Model/ModelFornecedorProduto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteFornecedorProduto**
> \VertisConnect\Model\ModelFornecedorProduto deleteFornecedorProduto($id, $f)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosEFornecedoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Produtos por Fornecedor
$f = 56; // int | 

try {
    $result = $apiInstance->deleteFornecedorProduto($id, $f);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosEFornecedoresApi->deleteFornecedorProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Produtos por Fornecedor |
 **f** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelFornecedorProduto**](../Model/ModelFornecedorProduto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFornecedorProduto**
> \VertisConnect\Model\ModelFornecedorProduto getFornecedorProduto($id, $f)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosEFornecedoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Produtos por Fornecedor
$f = 56; // int | 

try {
    $result = $apiInstance->getFornecedorProduto($id, $f);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosEFornecedoresApi->getFornecedorProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Produtos por Fornecedor |
 **f** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelFornecedorProduto**](../Model/ModelFornecedorProduto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFornecedorProdutos**
> \VertisConnect\Model\ModelFornecedorProduto[] getFornecedorProdutos($f)



Retorna registros do objeto parc_forn_produto

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosEFornecedoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$f = 56; // int | 

try {
    $result = $apiInstance->getFornecedorProdutos($f);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosEFornecedoresApi->getFornecedorProdutos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **f** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelFornecedorProduto[]**](../Model/ModelFornecedorProduto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateFornecedorProduto**
> \VertisConnect\Model\ModelFornecedorProduto updateFornecedorProduto($id, $produtos_e_fornecedores, $f)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosEFornecedoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Produtos por Fornecedor
$produtos_e_fornecedores = new \VertisConnect\Model\ModelFornecedorProduto(); // \VertisConnect\Model\ModelFornecedorProduto | Objeto parc_forn_produto
$f = 56; // int | 

try {
    $result = $apiInstance->updateFornecedorProduto($id, $produtos_e_fornecedores, $f);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosEFornecedoresApi->updateFornecedorProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Produtos por Fornecedor |
 **produtos_e_fornecedores** | [**\VertisConnect\Model\ModelFornecedorProduto**](../Model/ModelFornecedorProduto.md)| Objeto parc_forn_produto |
 **f** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelFornecedorProduto**](../Model/ModelFornecedorProduto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

