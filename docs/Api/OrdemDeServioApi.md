# VertisConnect\OrdemDeServioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrdemServico**](OrdemDeServioApi.md#createOrdemServico) | **POST** /api/V1.1/ordens-servico | 
[**createOrdemServicoPortal**](OrdemDeServioApi.md#createOrdemServicoPortal) | **POST** /api/V1.1/exames-internet | 
[**deleteOrdemServico**](OrdemDeServioApi.md#deleteOrdemServico) | **DELETE** /api/V1.1/ordens-servico/{id} | 
[**deleteOsItem**](OrdemDeServioApi.md#deleteOsItem) | **DELETE** /api/V1.1/exames-internet/removeitem/{id} | 
[**determinaIdAmostra**](OrdemDeServioApi.md#determinaIdAmostra) | **GET** /api/V1.1/exames-internet/necessita_idamostra | 
[**getExamesOS**](OrdemDeServioApi.md#getExamesOS) | **GET** /api/V1.1/exames-internet | 
[**getExamesOSById**](OrdemDeServioApi.md#getExamesOSById) | **GET** /api/V1.1/exames-internet/{id} | 
[**getOSProtocolo**](OrdemDeServioApi.md#getOSProtocolo) | **GET** /api/V1.1/ordens-servico/protocolo | 
[**getOrdemServico**](OrdemDeServioApi.md#getOrdemServico) | **GET** /api/V1.1/ordens-servico/{id} | 
[**getOrdemServicos**](OrdemDeServioApi.md#getOrdemServicos) | **GET** /api/V1.1/ordens-servico | 
[**updateExamesOS**](OrdemDeServioApi.md#updateExamesOS) | **PUT** /api/V1.1/exames-internet/{id} | 
[**updateOrdemServico**](OrdemDeServioApi.md#updateOrdemServico) | **PUT** /api/V1.1/ordens-servico/{id} | 


# **createOrdemServico**
> \VertisConnect\Model\ModelOrdemServico createOrdemServico($ordem_de_servio)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ordem_de_servio = new \VertisConnect\Model\ModelOrdemServico(); // \VertisConnect\Model\ModelOrdemServico | Objeto ordem_servico_nv

try {
    $result = $apiInstance->createOrdemServico($ordem_de_servio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->createOrdemServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ordem_de_servio** | [**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)| Objeto ordem_servico_nv |

### Return type

[**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createOrdemServicoPortal**
> \VertisConnect\Model\ModelOrdemServico createOrdemServicoPortal($solicitar_exames_via_portal)



Cria uma solicitação de exames originaria do portal do cliente.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$solicitar_exames_via_portal = new \VertisConnect\Model\ModelOrdemServico(); // \VertisConnect\Model\ModelOrdemServico | Envolve diversos objetos

try {
    $result = $apiInstance->createOrdemServicoPortal($solicitar_exames_via_portal);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->createOrdemServicoPortal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **solicitar_exames_via_portal** | [**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)| Envolve diversos objetos |

### Return type

[**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteOrdemServico**
> \VertisConnect\Model\ModelOrdemServico deleteOrdemServico($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID da Ordem de Serviço

try {
    $result = $apiInstance->deleteOrdemServico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->deleteOrdemServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID da Ordem de Serviço |

### Return type

[**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteOsItem**
> \VertisConnect\Model\ModelOrdemServico[] deleteOsItem($id)



Exclui o item determinado no parametro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Item da Ordem de Serviço

try {
    $result = $apiInstance->deleteOsItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->deleteOsItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Item da Ordem de Serviço |

### Return type

[**\VertisConnect\Model\ModelOrdemServico[]**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **determinaIdAmostra**
> \VertisConnect\Model\ModelOrdemServico[] determinaIdAmostra()



Retorna a boolean para necessidade de informar o id da amostra

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->determinaIdAmostra();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->determinaIdAmostra: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelOrdemServico[]**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExamesOS**
> \VertisConnect\Model\ModelOrdemServico[] getExamesOS()



Retorna a relação de exames solicitados pelo usuario

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExamesOS();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->getExamesOS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelOrdemServico[]**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExamesOSById**
> \VertisConnect\Model\ModelOrdemServico[] getExamesOSById($id)



Retorna uma Ordem de Serviço, determinada pelo parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Ordem de Serviço

try {
    $result = $apiInstance->getExamesOSById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->getExamesOSById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Ordem de Serviço |

### Return type

[**\VertisConnect\Model\ModelOrdemServico[]**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOSProtocolo**
> \VertisConnect\Model\ModelOrdemServicoProtocolo[] getOSProtocolo()



Retorna número de protocolo que deverá ser utilizado para abertura de OS durante a sessão ativa

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getOSProtocolo();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->getOSProtocolo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelOrdemServicoProtocolo[]**](../Model/ModelOrdemServicoProtocolo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOrdemServico**
> \VertisConnect\Model\ModelOrdemServico getOrdemServico($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Ordem de Serviço

try {
    $result = $apiInstance->getOrdemServico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->getOrdemServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Ordem de Serviço |

### Return type

[**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOrdemServicos**
> \VertisConnect\Model\ModelOrdemServico[] getOrdemServicos()



Retorna registros do objeto ordem_servico_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getOrdemServicos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->getOrdemServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelOrdemServico[]**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExamesOS**
> \VertisConnect\Model\ModelOrdemServico[] updateExamesOS($id)



Atualiza a ordem de serviço selecionada

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID da Ordem de Serviço

try {
    $result = $apiInstance->updateExamesOS($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->updateExamesOS: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID da Ordem de Serviço |

### Return type

[**\VertisConnect\Model\ModelOrdemServico[]**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateOrdemServico**
> \VertisConnect\Model\ModelOrdemServico updateOrdemServico($ordem_de_servio, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\OrdemDeServioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ordem_de_servio = new \VertisConnect\Model\ModelOrdemServico(); // \VertisConnect\Model\ModelOrdemServico | Objeto ordem_servico_nv
$id = 56; // int | ID do Ordem de Serviço

try {
    $result = $apiInstance->updateOrdemServico($ordem_de_servio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdemDeServioApi->updateOrdemServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ordem_de_servio** | [**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)| Objeto ordem_servico_nv |
 **id** | **int**| ID do Ordem de Serviço |

### Return type

[**\VertisConnect\Model\ModelOrdemServico**](../Model/ModelOrdemServico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

