# VertisConnect\ControllerExpeciePelagemTControllerEspeciePelagemApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11RelespspelsDelete**](ControllerExpeciePelagemTControllerEspeciePelagemApi.md#apiV11RelespspelsDelete) | **DELETE** /api/V1.1/relespspels | 
[**apiV11RelespspelsGet**](ControllerExpeciePelagemTControllerEspeciePelagemApi.md#apiV11RelespspelsGet) | **GET** /api/V1.1/relespspels | 
[**apiV11RelespspelsIdGet**](ControllerExpeciePelagemTControllerEspeciePelagemApi.md#apiV11RelespspelsIdGet) | **GET** /api/V1.1/relespspels/{id} | 
[**apiV11RelespspelsPost**](ControllerExpeciePelagemTControllerEspeciePelagemApi.md#apiV11RelespspelsPost) | **POST** /api/V1.1/relespspels | 
[**apiV11RelespspelsPut**](ControllerExpeciePelagemTControllerEspeciePelagemApi.md#apiV11RelespspelsPut) | **PUT** /api/V1.1/relespspels | 


# **apiV11RelespspelsDelete**
> apiV11RelespspelsDelete()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExpeciePelagemTControllerEspeciePelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11RelespspelsDelete();
} catch (Exception $e) {
    echo 'Exception when calling ControllerExpeciePelagemTControllerEspeciePelagemApi->apiV11RelespspelsDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RelespspelsGet**
> apiV11RelespspelsGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExpeciePelagemTControllerEspeciePelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11RelespspelsGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerExpeciePelagemTControllerEspeciePelagemApi->apiV11RelespspelsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RelespspelsIdGet**
> apiV11RelespspelsIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExpeciePelagemTControllerEspeciePelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11RelespspelsIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerExpeciePelagemTControllerEspeciePelagemApi->apiV11RelespspelsIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RelespspelsPost**
> apiV11RelespspelsPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExpeciePelagemTControllerEspeciePelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11RelespspelsPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerExpeciePelagemTControllerEspeciePelagemApi->apiV11RelespspelsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11RelespspelsPut**
> apiV11RelespspelsPut()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerExpeciePelagemTControllerEspeciePelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11RelespspelsPut();
} catch (Exception $e) {
    echo 'Exception when calling ControllerExpeciePelagemTControllerEspeciePelagemApi->apiV11RelespspelsPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

