# VertisConnect\FaturamentoDeConvnioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDocFaturamento**](FaturamentoDeConvnioApi.md#getDocFaturamento) | **GET** /api/V1.1/fat-convenio/{id}/doctos | 
[**getDocFaturamentoById**](FaturamentoDeConvnioApi.md#getDocFaturamentoById) | **GET** /api/V1.1/fat-convenio/{id}/doctos/{doc} | 
[**getFaturamento**](FaturamentoDeConvnioApi.md#getFaturamento) | **GET** /api/V1.1/fat-convenio/{id} | 
[**getFaturamentos**](FaturamentoDeConvnioApi.md#getFaturamentos) | **GET** /api/V1.1/fat-convenio | 


# **getDocFaturamento**
> \VertisConnect\Model\ModelFaturamentoConvenio[] getDocFaturamento($id)



Retorna os documentos do faturamento indicado no parametro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FaturamentoDeConvnioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Faturamento de Convênio

try {
    $result = $apiInstance->getDocFaturamento($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FaturamentoDeConvnioApi->getDocFaturamento: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Faturamento de Convênio |

### Return type

[**\VertisConnect\Model\ModelFaturamentoConvenio[]**](../Model/ModelFaturamentoConvenio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDocFaturamentoById**
> \VertisConnect\Model\ModelFatDoctos getDocFaturamentoById($doc, $id)



Retorna informações de um único documento (DOC) do faturamento determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FaturamentoDeConvnioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$doc = "doc_example"; // string | Tipo do Documento Anexado ao Faturamento de Convênio
$id = 56; // int | ID do Faturamento de Convênio

try {
    $result = $apiInstance->getDocFaturamentoById($doc, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FaturamentoDeConvnioApi->getDocFaturamentoById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc** | **string**| Tipo do Documento Anexado ao Faturamento de Convênio |
 **id** | **int**| ID do Faturamento de Convênio |

### Return type

[**\VertisConnect\Model\ModelFatDoctos**](../Model/ModelFatDoctos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFaturamento**
> \VertisConnect\Model\ModelFaturamentoConvenio getFaturamento($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FaturamentoDeConvnioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Faturamento de Convênio

try {
    $result = $apiInstance->getFaturamento($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FaturamentoDeConvnioApi->getFaturamento: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Faturamento de Convênio |

### Return type

[**\VertisConnect\Model\ModelFaturamentoConvenio**](../Model/ModelFaturamentoConvenio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFaturamentos**
> \VertisConnect\Model\ModelFaturamentoConvenio[] getFaturamentos()



Retorna registros do objeto nl_faturamento

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FaturamentoDeConvnioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getFaturamentos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FaturamentoDeConvnioApi->getFaturamentos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelFaturamentoConvenio[]**](../Model/ModelFaturamentoConvenio.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

