# VertisConnect\ConversoDeProdutosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProdutoConversao**](ConversoDeProdutosApi.md#createProdutoConversao) | **POST** /api/V1.1/produtoconversao | 
[**deleteProdutoConversao**](ConversoDeProdutosApi.md#deleteProdutoConversao) | **DELETE** /api/V1.1/produtos/{origem}/conversao/{destino} | 
[**getProdutoConversao**](ConversoDeProdutosApi.md#getProdutoConversao) | **GET** /api/V1.1/produtos/{origem}/conversao/{destino} | 
[**getProdutosConversao**](ConversoDeProdutosApi.md#getProdutosConversao) | **GET** /api/V1.1/produtos/{id}/conversao | 
[**updateProdutoConversao**](ConversoDeProdutosApi.md#updateProdutoConversao) | **PUT** /api/V1.1/produtos/{origem}/conversao/{destino} | 


# **createProdutoConversao**
> \VertisConnect\Model\ModelProdutoConversao createProdutoConversao($converso_de_produtos)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ConversoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$converso_de_produtos = new \VertisConnect\Model\ModelProdutoConversao(); // \VertisConnect\Model\ModelProdutoConversao | Objeto prod_conversao

try {
    $result = $apiInstance->createProdutoConversao($converso_de_produtos);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConversoDeProdutosApi->createProdutoConversao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **converso_de_produtos** | [**\VertisConnect\Model\ModelProdutoConversao**](../Model/ModelProdutoConversao.md)| Objeto prod_conversao |

### Return type

[**\VertisConnect\Model\ModelProdutoConversao**](../Model/ModelProdutoConversao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProdutoConversao**
> \VertisConnect\Model\ModelProdutoConversao deleteProdutoConversao($destino, $id, $origem)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ConversoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destino = 56; // int | 
$id = 56; // int | ID do Conversão de Produtos
$origem = 56; // int | 

try {
    $result = $apiInstance->deleteProdutoConversao($destino, $id, $origem);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConversoDeProdutosApi->deleteProdutoConversao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destino** | **int**|  |
 **id** | **int**| ID do Conversão de Produtos |
 **origem** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelProdutoConversao**](../Model/ModelProdutoConversao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProdutoConversao**
> \VertisConnect\Model\ModelProdutoConversao getProdutoConversao($destino, $id, $origem)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ConversoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destino = 56; // int | 
$id = 56; // int | ID do Conversão de Produtos
$origem = 56; // int | 

try {
    $result = $apiInstance->getProdutoConversao($destino, $id, $origem);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConversoDeProdutosApi->getProdutoConversao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destino** | **int**|  |
 **id** | **int**| ID do Conversão de Produtos |
 **origem** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelProdutoConversao**](../Model/ModelProdutoConversao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProdutosConversao**
> \VertisConnect\Model\ModelProdutoConversao[] getProdutosConversao($id)



Retorna registros do objeto prod_conversao

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ConversoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->getProdutosConversao($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConversoDeProdutosApi->getProdutosConversao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelProdutoConversao[]**](../Model/ModelProdutoConversao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProdutoConversao**
> \VertisConnect\Model\ModelProdutoConversao updateProdutoConversao($destino, $converso_de_produtos, $id, $origem)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ConversoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$destino = 56; // int | 
$converso_de_produtos = new \VertisConnect\Model\ModelProdutoConversao(); // \VertisConnect\Model\ModelProdutoConversao | Objeto prod_conversao
$id = 56; // int | ID do Conversão de Produtos
$origem = 56; // int | 

try {
    $result = $apiInstance->updateProdutoConversao($destino, $converso_de_produtos, $id, $origem);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConversoDeProdutosApi->updateProdutoConversao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destino** | **int**|  |
 **converso_de_produtos** | [**\VertisConnect\Model\ModelProdutoConversao**](../Model/ModelProdutoConversao.md)| Objeto prod_conversao |
 **id** | **int**| ID do Conversão de Produtos |
 **origem** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelProdutoConversao**](../Model/ModelProdutoConversao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

