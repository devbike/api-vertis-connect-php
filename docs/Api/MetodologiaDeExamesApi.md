# VertisConnect\MetodologiaDeExamesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createExameMetodologias**](MetodologiaDeExamesApi.md#createExameMetodologias) | **POST** /api/V1.1/examemetodologias | 
[**deleteExameMetodologias**](MetodologiaDeExamesApi.md#deleteExameMetodologias) | **DELETE** /api/V1.1/examemetodologias/{id} | 
[**getExameMetodologias**](MetodologiaDeExamesApi.md#getExameMetodologias) | **GET** /api/V1.1/examemetodologias/{id} | 
[**getExameMetodologiass**](MetodologiaDeExamesApi.md#getExameMetodologiass) | **GET** /api/V1.1/examemetodologias | 
[**updateExameMetodologias**](MetodologiaDeExamesApi.md#updateExameMetodologias) | **PUT** /api/V1.1/examemetodologias/{id} | 


# **createExameMetodologias**
> \VertisConnect\Model\ModelExameMetodologia createExameMetodologias($metodologia_de_exames)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MetodologiaDeExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$metodologia_de_exames = new \VertisConnect\Model\ModelExameMetodologia(); // \VertisConnect\Model\ModelExameMetodologia | Objeto prod_exa_metodologia_nv

try {
    $result = $apiInstance->createExameMetodologias($metodologia_de_exames);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MetodologiaDeExamesApi->createExameMetodologias: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **metodologia_de_exames** | [**\VertisConnect\Model\ModelExameMetodologia**](../Model/ModelExameMetodologia.md)| Objeto prod_exa_metodologia_nv |

### Return type

[**\VertisConnect\Model\ModelExameMetodologia**](../Model/ModelExameMetodologia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteExameMetodologias**
> \VertisConnect\Model\ModelExameMetodologia deleteExameMetodologias($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MetodologiaDeExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Metodologias

try {
    $result = $apiInstance->deleteExameMetodologias($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MetodologiaDeExamesApi->deleteExameMetodologias: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Metodologias |

### Return type

[**\VertisConnect\Model\ModelExameMetodologia**](../Model/ModelExameMetodologia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameMetodologias**
> \VertisConnect\Model\ModelExameMetodologia getExameMetodologias($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MetodologiaDeExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Metodologias

try {
    $result = $apiInstance->getExameMetodologias($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MetodologiaDeExamesApi->getExameMetodologias: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Metodologias |

### Return type

[**\VertisConnect\Model\ModelExameMetodologia**](../Model/ModelExameMetodologia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getExameMetodologiass**
> \VertisConnect\Model\ModelExameMetodologia[] getExameMetodologiass()



Retorna registros do objeto prod_exa_metodologia_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MetodologiaDeExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getExameMetodologiass();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MetodologiaDeExamesApi->getExameMetodologiass: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelExameMetodologia[]**](../Model/ModelExameMetodologia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateExameMetodologias**
> \VertisConnect\Model\ModelExameMetodologia updateExameMetodologias($metodologia_de_exames, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MetodologiaDeExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$metodologia_de_exames = new \VertisConnect\Model\ModelExameMetodologia(); // \VertisConnect\Model\ModelExameMetodologia | Objeto prod_exa_metodologia_nv
$id = 56; // int | ID do Metodologias

try {
    $result = $apiInstance->updateExameMetodologias($metodologia_de_exames, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MetodologiaDeExamesApi->updateExameMetodologias: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **metodologia_de_exames** | [**\VertisConnect\Model\ModelExameMetodologia**](../Model/ModelExameMetodologia.md)| Objeto prod_exa_metodologia_nv |
 **id** | **int**| ID do Metodologias |

### Return type

[**\VertisConnect\Model\ModelExameMetodologia**](../Model/ModelExameMetodologia.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

