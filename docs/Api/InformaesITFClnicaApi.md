# VertisConnect\InformaesITFClnicaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoITFVertisClin**](InformaesITFClnicaApi.md#doCreateInfoITFVertisClin) | **POST** /api/V1.1/clinicas/{id}/itf-vertis | 
[**doDeleteInfoITFVertisClint**](InformaesITFClnicaApi.md#doDeleteInfoITFVertisClint) | **DELETE** /api/V1.1/clinicas/{id}/itf-vertis | 
[**doGetInfoITFVertisClin**](InformaesITFClnicaApi.md#doGetInfoITFVertisClin) | **GET** /api/V1.1/clinicas/{id}/itf-vertis | 
[**doUpdateInfoITFVertisClin**](InformaesITFClnicaApi.md#doUpdateInfoITFVertisClin) | **PUT** /api/V1.1/clinicas/{id}/itf-vertis | 


# **doCreateInfoITFVertisClin**
> \VertisConnect\Model\ModelParcInfITFVertis doCreateInfoITFVertisClin($informaes_itf, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$informaes_itf = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doCreateInfoITFVertisClin($informaes_itf, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFClnicaApi->doCreateInfoITFVertisClin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **informaes_itf** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoITFVertisClint**
> \VertisConnect\Model\ModelParcInfITFVertis doDeleteInfoITFVertisClint($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoITFVertisClint($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFClnicaApi->doDeleteInfoITFVertisClint: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoITFVertisClin**
> \VertisConnect\Model\ModelParcInfITFVertis doGetInfoITFVertisClin($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoITFVertisClin($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFClnicaApi->doGetInfoITFVertisClin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoITFVertisClin**
> \VertisConnect\Model\ModelParcInfITFVertis doUpdateInfoITFVertisClin($parametros_fiscais___clnica, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___clnica = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoITFVertisClin($parametros_fiscais___clnica, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFClnicaApi->doUpdateInfoITFVertisClin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___clnica** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

