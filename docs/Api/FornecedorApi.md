# VertisConnect\FornecedorApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFornecedor**](FornecedorApi.md#createFornecedor) | **POST** /api/V1.1/fornecedores | 
[**deleteFornecedor**](FornecedorApi.md#deleteFornecedor) | **DELETE** /api/V1.1/fornecedores/{id} | 
[**getFornecedor**](FornecedorApi.md#getFornecedor) | **GET** /api/V1.1/fornecedores/{id} | 
[**getFornecedores**](FornecedorApi.md#getFornecedores) | **GET** /api/V1.1/fornecedores | 
[**updateFornecedor**](FornecedorApi.md#updateFornecedor) | **PUT** /api/V1.1/fornecedores/{id} | 


# **createFornecedor**
> \VertisConnect\Model\ModelFornecedor createFornecedor($fornecedores)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fornecedores = new \VertisConnect\Model\ModelFornecedor(); // \VertisConnect\Model\ModelFornecedor | Objeto parc_fornecedor

try {
    $result = $apiInstance->createFornecedor($fornecedores);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornecedorApi->createFornecedor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fornecedores** | [**\VertisConnect\Model\ModelFornecedor**](../Model/ModelFornecedor.md)| Objeto parc_fornecedor |

### Return type

[**\VertisConnect\Model\ModelFornecedor**](../Model/ModelFornecedor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteFornecedor**
> \VertisConnect\Model\ModelFornecedor deleteFornecedor($id)



Exclui o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->deleteFornecedor($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornecedorApi->deleteFornecedor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFornecedor**](../Model/ModelFornecedor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFornecedor**
> \VertisConnect\Model\ModelFornecedor getFornecedor($id)



Retorna informações de um registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do regoistro

try {
    $result = $apiInstance->getFornecedor($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornecedorApi->getFornecedor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do regoistro |

### Return type

[**\VertisConnect\Model\ModelFornecedor**](../Model/ModelFornecedor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFornecedores**
> \VertisConnect\Model\ModelFornecedor[] getFornecedores()



Retorna agendas para o motoboy selecionado.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getFornecedores();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornecedorApi->getFornecedores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelFornecedor[]**](../Model/ModelFornecedor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateFornecedor**
> \VertisConnect\Model\ModelFornecedor updateFornecedor($fornecedor, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fornecedor = new \VertisConnect\Model\ModelFornecedor(); // \VertisConnect\Model\ModelFornecedor | Objeto parc_fornecedor
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateFornecedor($fornecedor, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FornecedorApi->updateFornecedor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fornecedor** | [**\VertisConnect\Model\ModelFornecedor**](../Model/ModelFornecedor.md)| Objeto parc_fornecedor |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFornecedor**](../Model/ModelFornecedor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

