# VertisConnect\ParametrizaoFiscalParceiroDeApoioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoFiscalApoio**](ParametrizaoFiscalParceiroDeApoioApi.md#doCreateInfoFiscalApoio) | **POST** /api/V1.1/apoios/{id}/paramsfiscais | 
[**doDeleteInfoFiscalApoio**](ParametrizaoFiscalParceiroDeApoioApi.md#doDeleteInfoFiscalApoio) | **DELETE** /api/V1.1/apoios/{id}/paramsfiscais | 
[**doGetInfoFiscalApoio**](ParametrizaoFiscalParceiroDeApoioApi.md#doGetInfoFiscalApoio) | **GET** /api/V1.1/apoios/{id}/paramsfiscais | 
[**doUpdateInfoFiscalApoio**](ParametrizaoFiscalParceiroDeApoioApi.md#doUpdateInfoFiscalApoio) | **PUT** /api/V1.1/apoios/{id}/paramsfiscais | 


# **doCreateInfoFiscalApoio**
> \VertisConnect\Model\ModelFiscal doCreateInfoFiscalApoio($parametrizao_fiscal, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_fiscal = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do Parceiro de Apoio

try {
    $result = $apiInstance->doCreateInfoFiscalApoio($parametrizao_fiscal, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalParceiroDeApoioApi->doCreateInfoFiscalApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_fiscal** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do Parceiro de Apoio |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoFiscalApoio**
> \VertisConnect\Model\ModelFiscal doDeleteInfoFiscalApoio($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoFiscalApoio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalParceiroDeApoioApi->doDeleteInfoFiscalApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoFiscalApoio**
> \VertisConnect\Model\ModelFiscal doGetInfoFiscalApoio($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoFiscalApoio($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalParceiroDeApoioApi->doGetInfoFiscalApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoFiscalApoio**
> \VertisConnect\Model\ModelFiscal doUpdateInfoFiscalApoio($parametros_fiscais___parceiro_de_apoio, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalParceiroDeApoioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___parceiro_de_apoio = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoFiscalApoio($parametros_fiscais___parceiro_de_apoio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalParceiroDeApoioApi->doUpdateInfoFiscalApoio: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___parceiro_de_apoio** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

