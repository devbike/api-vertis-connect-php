# VertisConnect\MapaSDAApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMapaSDA**](MapaSDAApi.md#createMapaSDA) | **POST** /api/V1.1/mapas_sda | 
[**deleteMapaSDA**](MapaSDAApi.md#deleteMapaSDA) | **DELETE** /api/V1.1/mapas_sda/{id} | 
[**getMapaSDA**](MapaSDAApi.md#getMapaSDA) | **GET** /api/V1.1/mapas_sda/{id} | 
[**getMapaSDAs**](MapaSDAApi.md#getMapaSDAs) | **GET** /api/V1.1/mapas_sda | 
[**updateMapaSDA**](MapaSDAApi.md#updateMapaSDA) | **PUT** /api/V1.1/mapas_sda/{id} | 


# **createMapaSDA**
> \VertisConnect\Model\ModelMaoaSDA createMapaSDA($mapa_sda)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MapaSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mapa_sda = new \VertisConnect\Model\ModelMaoaSDA(); // \VertisConnect\Model\ModelMaoaSDA | Objeto prod_exa_mapa_sda_nv

try {
    $result = $apiInstance->createMapaSDA($mapa_sda);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MapaSDAApi->createMapaSDA: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mapa_sda** | [**\VertisConnect\Model\ModelMaoaSDA**](../Model/ModelMaoaSDA.md)| Objeto prod_exa_mapa_sda_nv |

### Return type

[**\VertisConnect\Model\ModelMaoaSDA**](../Model/ModelMaoaSDA.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMapaSDA**
> \VertisConnect\Model\ModelMaoaSDA deleteMapaSDA($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MapaSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Mapa SDA

try {
    $result = $apiInstance->deleteMapaSDA($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MapaSDAApi->deleteMapaSDA: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Mapa SDA |

### Return type

[**\VertisConnect\Model\ModelMaoaSDA**](../Model/ModelMaoaSDA.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMapaSDA**
> \VertisConnect\Model\ModelMaoaSDA getMapaSDA($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MapaSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Mapa SDA

try {
    $result = $apiInstance->getMapaSDA($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MapaSDAApi->getMapaSDA: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Mapa SDA |

### Return type

[**\VertisConnect\Model\ModelMaoaSDA**](../Model/ModelMaoaSDA.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMapaSDAs**
> \VertisConnect\Model\ModelMaoaSDA[] getMapaSDAs()



Retorna registros do objeto prod_exa_mapa_sda_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MapaSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getMapaSDAs();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MapaSDAApi->getMapaSDAs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelMaoaSDA[]**](../Model/ModelMaoaSDA.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateMapaSDA**
> \VertisConnect\Model\ModelMaoaSDA updateMapaSDA($mapa_sda, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MapaSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mapa_sda = new \VertisConnect\Model\ModelMaoaSDA(); // \VertisConnect\Model\ModelMaoaSDA | Objeto prod_exa_mapa_sda_nv
$id = 56; // int | ID do Mapa SDA

try {
    $result = $apiInstance->updateMapaSDA($mapa_sda, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MapaSDAApi->updateMapaSDA: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mapa_sda** | [**\VertisConnect\Model\ModelMaoaSDA**](../Model/ModelMaoaSDA.md)| Objeto prod_exa_mapa_sda_nv |
 **id** | **int**| ID do Mapa SDA |

### Return type

[**\VertisConnect\Model\ModelMaoaSDA**](../Model/ModelMaoaSDA.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

