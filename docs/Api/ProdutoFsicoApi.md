# VertisConnect\ProdutoFsicoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProdutoFisico**](ProdutoFsicoApi.md#createProdutoFisico) | **POST** /api/V1.1/produtosfisicos | 
[**deleteProdutoFisico**](ProdutoFsicoApi.md#deleteProdutoFisico) | **DELETE** /api/V1.1/produtosfisicos/{id} | 
[**getProdutoFisico**](ProdutoFsicoApi.md#getProdutoFisico) | **GET** /api/V1.1/produtosfisicos/{id} | 
[**getProdutoFisicos**](ProdutoFsicoApi.md#getProdutoFisicos) | **GET** /api/V1.1/produtosfisicos | 
[**updateProdutoFisico**](ProdutoFsicoApi.md#updateProdutoFisico) | **PUT** /api/V1.1/produtosfisicos/{id} | 


# **createProdutoFisico**
> \VertisConnect\Model\ModelProdutoFisico createProdutoFisico($produto_fsico)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutoFsicoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$produto_fsico = new \VertisConnect\Model\ModelProdutoFisico(); // \VertisConnect\Model\ModelProdutoFisico | Objeto prod_fisico_nv

try {
    $result = $apiInstance->createProdutoFisico($produto_fsico);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutoFsicoApi->createProdutoFisico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produto_fsico** | [**\VertisConnect\Model\ModelProdutoFisico**](../Model/ModelProdutoFisico.md)| Objeto prod_fisico_nv |

### Return type

[**\VertisConnect\Model\ModelProdutoFisico**](../Model/ModelProdutoFisico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProdutoFisico**
> \VertisConnect\Model\ModelProdutoFisico deleteProdutoFisico($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutoFsicoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Produto

try {
    $result = $apiInstance->deleteProdutoFisico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutoFsicoApi->deleteProdutoFisico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Produto |

### Return type

[**\VertisConnect\Model\ModelProdutoFisico**](../Model/ModelProdutoFisico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProdutoFisico**
> \VertisConnect\Model\ModelProdutoFisico getProdutoFisico($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutoFsicoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Produto Físico

try {
    $result = $apiInstance->getProdutoFisico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutoFsicoApi->getProdutoFisico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Produto Físico |

### Return type

[**\VertisConnect\Model\ModelProdutoFisico**](../Model/ModelProdutoFisico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProdutoFisicos**
> \VertisConnect\Model\ModelProdutoFisico[] getProdutoFisicos()



Retorna registros do objeto prod_fisico_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutoFsicoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getProdutoFisicos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutoFsicoApi->getProdutoFisicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelProdutoFisico[]**](../Model/ModelProdutoFisico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProdutoFisico**
> \VertisConnect\Model\ModelProdutoFisico updateProdutoFisico($produto_fsico, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutoFsicoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$produto_fsico = new \VertisConnect\Model\ModelProdutoFisico(); // \VertisConnect\Model\ModelProdutoFisico | Objeto prod_fisico_nv
$id = 56; // int | ID do Produto

try {
    $result = $apiInstance->updateProdutoFisico($produto_fsico, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutoFsicoApi->updateProdutoFisico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produto_fsico** | [**\VertisConnect\Model\ModelProdutoFisico**](../Model/ModelProdutoFisico.md)| Objeto prod_fisico_nv |
 **id** | **int**| ID do Produto |

### Return type

[**\VertisConnect\Model\ModelProdutoFisico**](../Model/ModelProdutoFisico.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

