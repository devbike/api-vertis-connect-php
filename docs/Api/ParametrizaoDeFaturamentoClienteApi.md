# VertisConnect\ParametrizaoDeFaturamentoClienteApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfFatCli**](ParametrizaoDeFaturamentoClienteApi.md#doCreateInfFatCli) | **POST** /api/V1.1/clientes/{id}/paramsfat | 
[**doDeleteInfFatCli**](ParametrizaoDeFaturamentoClienteApi.md#doDeleteInfFatCli) | **DELETE** /api/V1.1/clientes/{id}/paramsfat | 
[**doGetInfFatCli**](ParametrizaoDeFaturamentoClienteApi.md#doGetInfFatCli) | **GET** /api/V1.1/clientes/{id}/paramsfat | 
[**doUpdateInfFatCli**](ParametrizaoDeFaturamentoClienteApi.md#doUpdateInfFatCli) | **PUT** /api/V1.1/clientes/{id}/paramsfat | 


# **doCreateInfFatCli**
> \VertisConnect\Model\ModelParcInfFaturamento doCreateInfFatCli($parametrizao_de_faturamento, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_de_faturamento = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do Cliente

try {
    $result = $apiInstance->doCreateInfFatCli($parametrizao_de_faturamento, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClienteApi->doCreateInfFatCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_de_faturamento** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do Cliente |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfFatCli**
> \VertisConnect\Model\ModelParcInfFaturamento doDeleteInfFatCli($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfFatCli($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClienteApi->doDeleteInfFatCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfFatCli**
> \VertisConnect\Model\ModelParcInfFaturamento doGetInfFatCli($id)



Retorna configurações de faturamento para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfFatCli($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClienteApi->doGetInfFatCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfFatCli**
> \VertisConnect\Model\ModelParcInfFaturamento doUpdateInfFatCli($parametros_fiscais___cliente, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___cliente = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfFatCli($parametros_fiscais___cliente, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClienteApi->doUpdateInfFatCli: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___cliente** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

