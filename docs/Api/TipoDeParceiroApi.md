# VertisConnect\TipoDeParceiroApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createParcTipo**](TipoDeParceiroApi.md#createParcTipo) | **POST** /api/V1.1/parc-tipos | 
[**deleteParcTipo**](TipoDeParceiroApi.md#deleteParcTipo) | **DELETE** /api/V1.1/parc-tipos/{id} | 
[**getParcTipo**](TipoDeParceiroApi.md#getParcTipo) | **GET** /api/V1.1/parc-tipos/{id} | 
[**getParcTipos**](TipoDeParceiroApi.md#getParcTipos) | **GET** /api/V1.1/parc-tipos | 
[**updateParcTipo**](TipoDeParceiroApi.md#updateParcTipo) | **PUT** /api/V1.1/parc-tipos/{id} | 


# **createParcTipo**
> \VertisConnect\Model\ModelParcTipo createParcTipo($tipo_de_parceiro)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TipoDeParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$tipo_de_parceiro = new \VertisConnect\Model\ModelParcTipo(); // \VertisConnect\Model\ModelParcTipo | Objeto parc_tipo

try {
    $result = $apiInstance->createParcTipo($tipo_de_parceiro);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TipoDeParceiroApi->createParcTipo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipo_de_parceiro** | [**\VertisConnect\Model\ModelParcTipo**](../Model/ModelParcTipo.md)| Objeto parc_tipo |

### Return type

[**\VertisConnect\Model\ModelParcTipo**](../Model/ModelParcTipo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteParcTipo**
> \VertisConnect\Model\ModelParcTipo deleteParcTipo($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TipoDeParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->deleteParcTipo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TipoDeParceiroApi->deleteParcTipo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcTipo**](../Model/ModelParcTipo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParcTipo**
> \VertisConnect\Model\ModelParcTipo getParcTipo($id)



Retorna informações de um registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TipoDeParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do regoistro

try {
    $result = $apiInstance->getParcTipo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TipoDeParceiroApi->getParcTipo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do regoistro |

### Return type

[**\VertisConnect\Model\ModelParcTipo**](../Model/ModelParcTipo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParcTipos**
> \VertisConnect\Model\ModelParcTipo[] getParcTipos()



Retorna todos os tipos de parceiro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TipoDeParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getParcTipos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TipoDeParceiroApi->getParcTipos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelParcTipo[]**](../Model/ModelParcTipo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateParcTipo**
> \VertisConnect\Model\ModelParcTipo updateParcTipo($tipo_de_parceiro, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TipoDeParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$tipo_de_parceiro = new \VertisConnect\Model\ModelParcTipo(); // \VertisConnect\Model\ModelParcTipo | Objeto parc_tipo
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateParcTipo($tipo_de_parceiro, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TipoDeParceiroApi->updateParcTipo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipo_de_parceiro** | [**\VertisConnect\Model\ModelParcTipo**](../Model/ModelParcTipo.md)| Objeto parc_tipo |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcTipo**](../Model/ModelParcTipo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

