# VertisConnect\ContaCorrenteApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createContaCorrente**](ContaCorrenteApi.md#createContaCorrente) | **POST** /api/V1.1/contascorrentes | 
[**deleteContaCorrente**](ContaCorrenteApi.md#deleteContaCorrente) | **DELETE** /api/V1.1/contascorrentes/{id} | 
[**getContaCorrente**](ContaCorrenteApi.md#getContaCorrente) | **GET** /api/V1.1/contascorrentes/{id} | 
[**getContaCorrentes**](ContaCorrenteApi.md#getContaCorrentes) | **GET** /api/V1.1/contascorrentes | 
[**updateContaCorrente**](ContaCorrenteApi.md#updateContaCorrente) | **PUT** /api/V1.1/contascorrentes/{id} | 


# **createContaCorrente**
> \VertisConnect\Model\ModelParcContaCorrente createContaCorrente($conta_corrente)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ContaCorrenteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$conta_corrente = new \VertisConnect\Model\ModelParcContaCorrente(); // \VertisConnect\Model\ModelParcContaCorrente | Objeto parc_cta_corrente

try {
    $result = $apiInstance->createContaCorrente($conta_corrente);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContaCorrenteApi->createContaCorrente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conta_corrente** | [**\VertisConnect\Model\ModelParcContaCorrente**](../Model/ModelParcContaCorrente.md)| Objeto parc_cta_corrente |

### Return type

[**\VertisConnect\Model\ModelParcContaCorrente**](../Model/ModelParcContaCorrente.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteContaCorrente**
> \VertisConnect\Model\ModelParcContaCorrente deleteContaCorrente($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ContaCorrenteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Conta Corrente

try {
    $result = $apiInstance->deleteContaCorrente($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContaCorrenteApi->deleteContaCorrente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Conta Corrente |

### Return type

[**\VertisConnect\Model\ModelParcContaCorrente**](../Model/ModelParcContaCorrente.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getContaCorrente**
> \VertisConnect\Model\ModelParcContaCorrente getContaCorrente($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ContaCorrenteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Conta Corrente

try {
    $result = $apiInstance->getContaCorrente($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContaCorrenteApi->getContaCorrente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Conta Corrente |

### Return type

[**\VertisConnect\Model\ModelParcContaCorrente**](../Model/ModelParcContaCorrente.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getContaCorrentes**
> \VertisConnect\Model\ModelParcContaCorrente[] getContaCorrentes()



Retorna registros do objeto parc_cta_corrente

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ContaCorrenteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getContaCorrentes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContaCorrenteApi->getContaCorrentes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelParcContaCorrente[]**](../Model/ModelParcContaCorrente.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateContaCorrente**
> \VertisConnect\Model\ModelParcContaCorrente updateContaCorrente($conta_corrente, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ContaCorrenteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$conta_corrente = new \VertisConnect\Model\ModelParcContaCorrente(); // \VertisConnect\Model\ModelParcContaCorrente | Objeto parc_cta_corrente
$id = 56; // int | ID do Conta Corrente

try {
    $result = $apiInstance->updateContaCorrente($conta_corrente, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContaCorrenteApi->updateContaCorrente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conta_corrente** | [**\VertisConnect\Model\ModelParcContaCorrente**](../Model/ModelParcContaCorrente.md)| Objeto parc_cta_corrente |
 **id** | **int**| ID do Conta Corrente |

### Return type

[**\VertisConnect\Model\ModelParcContaCorrente**](../Model/ModelParcContaCorrente.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

