# VertisConnect\DiariaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiaria**](DiariaApi.md#createDiaria) | **POST** /api/V1.1/diarias | 
[**deleteDiaria**](DiariaApi.md#deleteDiaria) | **DELETE** /api/V1.1/diarias/{id} | 
[**getDiaria**](DiariaApi.md#getDiaria) | **GET** /api/V1.1/diarias/{id} | 
[**getDiarias**](DiariaApi.md#getDiarias) | **GET** /api/V1.1/diarias | 
[**updateDiaria**](DiariaApi.md#updateDiaria) | **PUT** /api/V1.1/diarias/{id} | 


# **createDiaria**
> \VertisConnect\Model\ModelDiarias createDiaria($diarias)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\DiariaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$diarias = new \VertisConnect\Model\ModelDiarias(); // \VertisConnect\Model\ModelDiarias | Objeto prod_diaria_nv

try {
    $result = $apiInstance->createDiaria($diarias);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiariaApi->createDiaria: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **diarias** | [**\VertisConnect\Model\ModelDiarias**](../Model/ModelDiarias.md)| Objeto prod_diaria_nv |

### Return type

[**\VertisConnect\Model\ModelDiarias**](../Model/ModelDiarias.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDiaria**
> \VertisConnect\Model\ModelDiarias deleteDiaria($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\DiariaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID da Agenda

try {
    $result = $apiInstance->deleteDiaria($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiariaApi->deleteDiaria: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID da Agenda |

### Return type

[**\VertisConnect\Model\ModelDiarias**](../Model/ModelDiarias.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDiaria**
> \VertisConnect\Model\ModelDiarias getDiaria($id)



Retorna informações de um registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\DiariaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->getDiaria($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiariaApi->getDiaria: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelDiarias**](../Model/ModelDiarias.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDiarias**
> \VertisConnect\Model\ModelDiarias[] getDiarias()



Retorna diarias cadastradas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\DiariaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDiarias();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiariaApi->getDiarias: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelDiarias[]**](../Model/ModelDiarias.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDiaria**
> \VertisConnect\Model\ModelDiarias updateDiaria($diaria, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\DiariaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$diaria = new \VertisConnect\Model\ModelDiarias(); // \VertisConnect\Model\ModelDiarias | Objeto prod_diarias_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateDiaria($diaria, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiariaApi->updateDiaria: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **diaria** | [**\VertisConnect\Model\ModelDiarias**](../Model/ModelDiarias.md)| Objeto prod_diarias_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelDiarias**](../Model/ModelDiarias.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

