# VertisConnect\AnimalXClinicaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAnimalXClinica**](AnimalXClinicaApi.md#createAnimalXClinica) | **POST** /api/V1.1/animais_x_clinicas | 
[**getAnimaisXClinicas**](AnimalXClinicaApi.md#getAnimaisXClinicas) | **GET** /api/V1.1/animais_x_clinicas | 
[**getAnimalXClinica**](AnimalXClinicaApi.md#getAnimalXClinica) | **GET** /api/V1.1/animais_x_clinicas/{id} | 


# **createAnimalXClinica**
> \VertisConnect\Model\ModelAnimalXClinica createAnimalXClinica($anim_x_clinica_nv)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimalXClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$anim_x_clinica_nv = new \VertisConnect\Model\ModelAnimalXClinica(); // \VertisConnect\Model\ModelAnimalXClinica | Objeto anim_x_clinica_nv

try {
    $result = $apiInstance->createAnimalXClinica($anim_x_clinica_nv);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimalXClinicaApi->createAnimalXClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anim_x_clinica_nv** | [**\VertisConnect\Model\ModelAnimalXClinica**](../Model/ModelAnimalXClinica.md)| Objeto anim_x_clinica_nv |

### Return type

[**\VertisConnect\Model\ModelAnimalXClinica**](../Model/ModelAnimalXClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimaisXClinicas**
> \VertisConnect\Model\ModelAnimalXClinica[] getAnimaisXClinicas()



Retorna registros do objeto anim_x_clinica_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimalXClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAnimaisXClinicas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimalXClinicaApi->getAnimaisXClinicas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelAnimalXClinica[]**](../Model/ModelAnimalXClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimalXClinica**
> \VertisConnect\Model\ModelAnimalXClinica getAnimalXClinica($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimalXClinicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animal_x_Clinica

try {
    $result = $apiInstance->getAnimalXClinica($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimalXClinicaApi->getAnimalXClinica: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animal_x_Clinica |

### Return type

[**\VertisConnect\Model\ModelAnimalXClinica**](../Model/ModelAnimalXClinica.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

