# VertisConnect\RecipienteApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRecipiente**](RecipienteApi.md#createRecipiente) | **POST** /api/V1.1/recipiente | 
[**deleteRecipiente**](RecipienteApi.md#deleteRecipiente) | **DELETE** /api/V1.1/recipiente/{id} | 
[**getRecipiente**](RecipienteApi.md#getRecipiente) | **GET** /api/V1.1/recipiente/{id} | 
[**getRecipientes**](RecipienteApi.md#getRecipientes) | **GET** /api/V1.1/recipiente | 
[**updateRecipiente**](RecipienteApi.md#updateRecipiente) | **PUT** /api/V1.1/recipiente/{id} | 


# **createRecipiente**
> \VertisConnect\Model\ModelRecipientes createRecipiente($recipiente)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RecipienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$recipiente = new \VertisConnect\Model\ModelRecipientes(); // \VertisConnect\Model\ModelRecipientes | Objeto prod_exa_recipiente_nv

try {
    $result = $apiInstance->createRecipiente($recipiente);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecipienteApi->createRecipiente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipiente** | [**\VertisConnect\Model\ModelRecipientes**](../Model/ModelRecipientes.md)| Objeto prod_exa_recipiente_nv |

### Return type

[**\VertisConnect\Model\ModelRecipientes**](../Model/ModelRecipientes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRecipiente**
> \VertisConnect\Model\ModelRecipientes deleteRecipiente($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RecipienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Recipiente

try {
    $result = $apiInstance->deleteRecipiente($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecipienteApi->deleteRecipiente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Recipiente |

### Return type

[**\VertisConnect\Model\ModelRecipientes**](../Model/ModelRecipientes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRecipiente**
> \VertisConnect\Model\ModelRecipientes getRecipiente($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RecipienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Recipiente

try {
    $result = $apiInstance->getRecipiente($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecipienteApi->getRecipiente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Recipiente |

### Return type

[**\VertisConnect\Model\ModelRecipientes**](../Model/ModelRecipientes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRecipientes**
> \VertisConnect\Model\ModelRecipientes[] getRecipientes()



Retorna registros do objeto prod_exa_recipiente_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RecipienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getRecipientes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecipienteApi->getRecipientes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelRecipientes[]**](../Model/ModelRecipientes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRecipiente**
> \VertisConnect\Model\ModelRecipientes updateRecipiente($recipiente, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RecipienteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$recipiente = new \VertisConnect\Model\ModelRecipientes(); // \VertisConnect\Model\ModelRecipientes | Objeto prod_exa_recipiente_nv
$id = 56; // int | ID do Recipiente

try {
    $result = $apiInstance->updateRecipiente($recipiente, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecipienteApi->updateRecipiente: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recipiente** | [**\VertisConnect\Model\ModelRecipientes**](../Model/ModelRecipientes.md)| Objeto prod_exa_recipiente_nv |
 **id** | **int**| ID do Recipiente |

### Return type

[**\VertisConnect\Model\ModelRecipientes**](../Model/ModelRecipientes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

