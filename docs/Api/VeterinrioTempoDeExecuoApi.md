# VertisConnect\VeterinrioTempoDeExecuoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRunTime**](VeterinrioTempoDeExecuoApi.md#createRunTime) | **POST** /api/V1.1/veterinarios/{vet}/tmpexec | 
[**deleteRunTime**](VeterinrioTempoDeExecuoApi.md#deleteRunTime) | **DELETE** /api/V1.1/veterinarios/{vet}/tmpexec/{id} | 
[**getRunTime**](VeterinrioTempoDeExecuoApi.md#getRunTime) | **GET** /api/V1.1/veterinarios/{vet}/tmpexec/{id} | 
[**getRunTimes**](VeterinrioTempoDeExecuoApi.md#getRunTimes) | **GET** /api/V1.1/veterinarios/{vet}/tmpexec | 
[**updateRunTime**](VeterinrioTempoDeExecuoApi.md#updateRunTime) | **PUT** /api/V1.1/veterinarios/{vet}/tmpexec/{id} | 


# **createRunTime**
> \VertisConnect\Model\ModelVetTmpExec createRunTime($veterinrio___tempo_de_execuo, $vet)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinrioTempoDeExecuoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$veterinrio___tempo_de_execuo = new \VertisConnect\Model\ModelVetTmpExec(); // \VertisConnect\Model\ModelVetTmpExec | Objeto parc_vet_tmp_exec
$vet = 56; // int | 

try {
    $result = $apiInstance->createRunTime($veterinrio___tempo_de_execuo, $vet);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinrioTempoDeExecuoApi->createRunTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **veterinrio___tempo_de_execuo** | [**\VertisConnect\Model\ModelVetTmpExec**](../Model/ModelVetTmpExec.md)| Objeto parc_vet_tmp_exec |
 **vet** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelVetTmpExec**](../Model/ModelVetTmpExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRunTime**
> \VertisConnect\Model\ModelVetTmpExec deleteRunTime($id, $vet)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinrioTempoDeExecuoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Vet. Tempo Execução
$vet = 56; // int | 

try {
    $result = $apiInstance->deleteRunTime($id, $vet);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinrioTempoDeExecuoApi->deleteRunTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Vet. Tempo Execução |
 **vet** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelVetTmpExec**](../Model/ModelVetTmpExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRunTime**
> \VertisConnect\Model\ModelVetTmpExec getRunTime($id, $vet)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinrioTempoDeExecuoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Vet. Tempo Execução
$vet = 56; // int | 

try {
    $result = $apiInstance->getRunTime($id, $vet);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinrioTempoDeExecuoApi->getRunTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Vet. Tempo Execução |
 **vet** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelVetTmpExec**](../Model/ModelVetTmpExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRunTimes**
> \VertisConnect\Model\ModelVetTmpExec[] getRunTimes($vet)



Retorna registros do objeto parc_vet_tmp_exec

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinrioTempoDeExecuoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$vet = 56; // int | 

try {
    $result = $apiInstance->getRunTimes($vet);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinrioTempoDeExecuoApi->getRunTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vet** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelVetTmpExec[]**](../Model/ModelVetTmpExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRunTime**
> \VertisConnect\Model\ModelVetTmpExec updateRunTime($id, $veterinrio___tempo_de_execuo, $vet)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinrioTempoDeExecuoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Vet. Tempo Execução
$veterinrio___tempo_de_execuo = new \VertisConnect\Model\ModelVetTmpExec(); // \VertisConnect\Model\ModelVetTmpExec | Objeto parc_vet_tmp_exec
$vet = 56; // int | 

try {
    $result = $apiInstance->updateRunTime($id, $veterinrio___tempo_de_execuo, $vet);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinrioTempoDeExecuoApi->updateRunTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Vet. Tempo Execução |
 **veterinrio___tempo_de_execuo** | [**\VertisConnect\Model\ModelVetTmpExec**](../Model/ModelVetTmpExec.md)| Objeto parc_vet_tmp_exec |
 **vet** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelVetTmpExec**](../Model/ModelVetTmpExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

