# VertisConnect\AgendaDeMotoboyApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAgendasMotoboy**](AgendaDeMotoboyApi.md#createAgendasMotoboy) | **POST** /api/V1.1/motoboys/{mtb}/agendas | 
[**deleteAgendasMotoboy**](AgendaDeMotoboyApi.md#deleteAgendasMotoboy) | **DELETE** /api/V1.1/motoboys/{mtb}/agendas/{age} | 
[**getAgendaMotoboy**](AgendaDeMotoboyApi.md#getAgendaMotoboy) | **GET** /api/V1.1/motoboys/{mtb}/agendas/{age} | 
[**getAgendasMotoboy**](AgendaDeMotoboyApi.md#getAgendasMotoboy) | **GET** /api/V1.1/motoboys/{mtb}/agendas | 
[**updateAgendasMotoboy**](AgendaDeMotoboyApi.md#updateAgendasMotoboy) | **PUT** /api/V1.1/motoboys/{mtb}/agendas/{age} | 


# **createAgendasMotoboy**
> \VertisConnect\Model\ModelAgendaMotoboy createAgendasMotoboy($agenda_de_motoboy, $mtb)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AgendaDeMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$agenda_de_motoboy = new \VertisConnect\Model\ModelAgendaMotoboy(); // \VertisConnect\Model\ModelAgendaMotoboy | Objeto parc_mtb_agenda
$mtb = 56; // int | 

try {
    $result = $apiInstance->createAgendasMotoboy($agenda_de_motoboy, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgendaDeMotoboyApi->createAgendasMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agenda_de_motoboy** | [**\VertisConnect\Model\ModelAgendaMotoboy**](../Model/ModelAgendaMotoboy.md)| Objeto parc_mtb_agenda |
 **mtb** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelAgendaMotoboy**](../Model/ModelAgendaMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAgendasMotoboy**
> \VertisConnect\Model\ModelAgendaMotoboy deleteAgendasMotoboy($age, $mtb)



Apaga o registro determinado no parâmetro AGE

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AgendaDeMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$age = 56; // int | ID da Agenda
$mtb = 56; // int | ID do Motoboy

try {
    $result = $apiInstance->deleteAgendasMotoboy($age, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgendaDeMotoboyApi->deleteAgendasMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **age** | **int**| ID da Agenda |
 **mtb** | **int**| ID do Motoboy |

### Return type

[**\VertisConnect\Model\ModelAgendaMotoboy**](../Model/ModelAgendaMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAgendaMotoboy**
> \VertisConnect\Model\ModelAgendaMotoboy getAgendaMotoboy($age, $mtb)



Retorna informações de uma agenda determinada no parâmetro AGE.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AgendaDeMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$age = 56; // int | ID da Agenda
$mtb = 56; // int | ID do Motoboy

try {
    $result = $apiInstance->getAgendaMotoboy($age, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgendaDeMotoboyApi->getAgendaMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **age** | **int**| ID da Agenda |
 **mtb** | **int**| ID do Motoboy |

### Return type

[**\VertisConnect\Model\ModelAgendaMotoboy**](../Model/ModelAgendaMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAgendasMotoboy**
> \VertisConnect\Model\ModelAgendaMotoboy[] getAgendasMotoboy($mtb)



Retorna agendas para o motoboy selecionado.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AgendaDeMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mtb = 56; // int | 

try {
    $result = $apiInstance->getAgendasMotoboy($mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgendaDeMotoboyApi->getAgendasMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mtb** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelAgendaMotoboy[]**](../Model/ModelAgendaMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAgendasMotoboy**
> \VertisConnect\Model\ModelAgendaMotoboy updateAgendasMotoboy($age, $agenda_de_motoboy, $mtb)



Atualiza o registro de agenda determinado no parâmetro AGE

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AgendaDeMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$age = 56; // int | ID da Agenda
$agenda_de_motoboy = new \VertisConnect\Model\ModelAgendaMotoboy(); // \VertisConnect\Model\ModelAgendaMotoboy | Objeto parc_mtb_agenda
$mtb = 56; // int | ID do Motoboy

try {
    $result = $apiInstance->updateAgendasMotoboy($age, $agenda_de_motoboy, $mtb);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgendaDeMotoboyApi->updateAgendasMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **age** | **int**| ID da Agenda |
 **agenda_de_motoboy** | [**\VertisConnect\Model\ModelAgendaMotoboy**](../Model/ModelAgendaMotoboy.md)| Objeto parc_mtb_agenda |
 **mtb** | **int**| ID do Motoboy |

### Return type

[**\VertisConnect\Model\ModelAgendaMotoboy**](../Model/ModelAgendaMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

