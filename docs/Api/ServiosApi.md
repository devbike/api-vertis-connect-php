# VertisConnect\ServiosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createServico**](ServiosApi.md#createServico) | **POST** /api/V1.1/servicos | 
[**deleteServico**](ServiosApi.md#deleteServico) | **DELETE** /api/V1.1/servicos/{id} | 
[**getServico**](ServiosApi.md#getServico) | **GET** /api/V1.1/servicos/{id} | 
[**getServicos**](ServiosApi.md#getServicos) | **GET** /api/V1.1/servicos | 
[**updateServico**](ServiosApi.md#updateServico) | **PUT** /api/V1.1/servicos/{id} | 


# **createServico**
> \VertisConnect\Model\ModelServicos createServico($servios)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$servios = new \VertisConnect\Model\ModelServicos(); // \VertisConnect\Model\ModelServicos | Objeto prod_servico_nv

try {
    $result = $apiInstance->createServico($servios);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosApi->createServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **servios** | [**\VertisConnect\Model\ModelServicos**](../Model/ModelServicos.md)| Objeto prod_servico_nv |

### Return type

[**\VertisConnect\Model\ModelServicos**](../Model/ModelServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteServico**
> \VertisConnect\Model\ModelServicos deleteServico($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Serviços

try {
    $result = $apiInstance->deleteServico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosApi->deleteServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Serviços |

### Return type

[**\VertisConnect\Model\ModelServicos**](../Model/ModelServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getServico**
> \VertisConnect\Model\ModelServicos getServico($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Serviços

try {
    $result = $apiInstance->getServico($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosApi->getServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Serviços |

### Return type

[**\VertisConnect\Model\ModelServicos**](../Model/ModelServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getServicos**
> \VertisConnect\Model\ModelServicos[] getServicos()



Retorna registros do objeto prod_servico_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getServicos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosApi->getServicos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelServicos[]**](../Model/ModelServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateServico**
> \VertisConnect\Model\ModelServicos updateServico($servios, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ServiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$servios = new \VertisConnect\Model\ModelServicos(); // \VertisConnect\Model\ModelServicos | Objeto prod_servico_nv
$id = 56; // int | ID do Serviços

try {
    $result = $apiInstance->updateServico($servios, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ServiosApi->updateServico: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **servios** | [**\VertisConnect\Model\ModelServicos**](../Model/ModelServicos.md)| Objeto prod_servico_nv |
 **id** | **int**| ID do Serviços |

### Return type

[**\VertisConnect\Model\ModelServicos**](../Model/ModelServicos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

