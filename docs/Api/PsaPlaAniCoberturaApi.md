# VertisConnect\PsaPlaAniCoberturaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPsaPlaAniCobertura**](PsaPlaAniCoberturaApi.md#createPsaPlaAniCobertura) | **POST** /api/V1.1/psa-pla-ani-cobertura | 
[**deletePsaPlaAniCobertura**](PsaPlaAniCoberturaApi.md#deletePsaPlaAniCobertura) | **DELETE** /api/V1.1/psa-pla-ani-cobertura/{id} | 
[**getPsaPlaAniCobertura**](PsaPlaAniCoberturaApi.md#getPsaPlaAniCobertura) | **GET** /api/V1.1/psa-pla-ani-cobertura/{id} | 
[**getPsaPlaAniCoberturas**](PsaPlaAniCoberturaApi.md#getPsaPlaAniCoberturas) | **GET** /api/V1.1/psa-pla-ani-cobertura | 
[**updatePsaPlaAniCobertura**](PsaPlaAniCoberturaApi.md#updatePsaPlaAniCobertura) | **PUT** /api/V1.1/psa-pla-ani-cobertura/{id} | 


# **createPsaPlaAniCobertura**
> \VertisConnect\Model\ModelPsaPlaAniCobertura createPsaPlaAniCobertura($psa_pla_ani_cobertura)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlaAniCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_pla_ani_cobertura = new \VertisConnect\Model\ModelPsaPlaAniCobertura(); // \VertisConnect\Model\ModelPsaPlaAniCobertura | Objeto fin_conta

try {
    $result = $apiInstance->createPsaPlaAniCobertura($psa_pla_ani_cobertura);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlaAniCoberturaApi->createPsaPlaAniCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_pla_ani_cobertura** | [**\VertisConnect\Model\ModelPsaPlaAniCobertura**](../Model/ModelPsaPlaAniCobertura.md)| Objeto fin_conta |

### Return type

[**\VertisConnect\Model\ModelPsaPlaAniCobertura**](../Model/ModelPsaPlaAniCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePsaPlaAniCobertura**
> \VertisConnect\Model\ModelPsaPlaAniCobertura deletePsaPlaAniCobertura($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlaAniCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaPlaAniCobertura

try {
    $result = $apiInstance->deletePsaPlaAniCobertura($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlaAniCoberturaApi->deletePsaPlaAniCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaPlaAniCobertura |

### Return type

[**\VertisConnect\Model\ModelPsaPlaAniCobertura**](../Model/ModelPsaPlaAniCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaPlaAniCobertura**
> \VertisConnect\Model\ModelPsaPlaAniCobertura getPsaPlaAniCobertura($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlaAniCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaPlaAniCobertura

try {
    $result = $apiInstance->getPsaPlaAniCobertura($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlaAniCoberturaApi->getPsaPlaAniCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaPlaAniCobertura |

### Return type

[**\VertisConnect\Model\ModelPsaPlaAniCobertura**](../Model/ModelPsaPlaAniCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaPlaAniCoberturas**
> \VertisConnect\Model\ModelPsaPlaAniCobertura[] getPsaPlaAniCoberturas()



Retorna registros do objeto fin_conta

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlaAniCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPsaPlaAniCoberturas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlaAniCoberturaApi->getPsaPlaAniCoberturas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelPsaPlaAniCobertura[]**](../Model/ModelPsaPlaAniCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePsaPlaAniCobertura**
> \VertisConnect\Model\ModelPsaPlaAniCobertura updatePsaPlaAniCobertura($psa_pla_ani_cobertura, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPlaAniCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_pla_ani_cobertura = new \VertisConnect\Model\ModelPsaPlaAniCobertura(); // \VertisConnect\Model\ModelPsaPlaAniCobertura | Objeto fin_conta
$id = 56; // int | ID do PsaPlaAniCobertura

try {
    $result = $apiInstance->updatePsaPlaAniCobertura($psa_pla_ani_cobertura, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPlaAniCoberturaApi->updatePsaPlaAniCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_pla_ani_cobertura** | [**\VertisConnect\Model\ModelPsaPlaAniCobertura**](../Model/ModelPsaPlaAniCobertura.md)| Objeto fin_conta |
 **id** | **int**| ID do PsaPlaAniCobertura |

### Return type

[**\VertisConnect\Model\ModelPsaPlaAniCobertura**](../Model/ModelPsaPlaAniCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

