# VertisConnect\VeterinriosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVeterinario**](VeterinriosApi.md#createVeterinario) | **POST** /api/V1.1/veterinarios | 
[**deleteVeterinario**](VeterinriosApi.md#deleteVeterinario) | **DELETE** /api/V1.1/veterinarios/{id} | 
[**getVeterinario**](VeterinriosApi.md#getVeterinario) | **GET** /api/V1.1/veterinarios/{id} | 
[**getVeterinarios**](VeterinriosApi.md#getVeterinarios) | **GET** /api/V1.1/veterinarios | 
[**updateVeterinario**](VeterinriosApi.md#updateVeterinario) | **PUT** /api/V1.1/veterinarios/{id} | 


# **createVeterinario**
> \VertisConnect\Model\ModelVeterinarios createVeterinario($veterinrio)



Cria novo Veterinário

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$veterinrio = new \VertisConnect\Model\ModelVeterinarios(); // \VertisConnect\Model\ModelVeterinarios | Objeto Veterinário

try {
    $result = $apiInstance->createVeterinario($veterinrio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->createVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **veterinrio** | [**\VertisConnect\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)| Objeto Veterinário |

### Return type

[**\VertisConnect\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVeterinario**
> \VertisConnect\Model\ModelVeterinarios deleteVeterinario($id)



Deleta o veterinário do código informado

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Veterinário

try {
    $result = $apiInstance->deleteVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->deleteVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Veterinário |

### Return type

[**\VertisConnect\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVeterinario**
> \VertisConnect\Model\ModelVeterinarios getVeterinario($id)



Retorna o veterinário do código informado

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Veterinário

try {
    $result = $apiInstance->getVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->getVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Veterinário |

### Return type

[**\VertisConnect\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVeterinarios**
> \VertisConnect\Model\ModelVeterinarios[] getVeterinarios()



Retorna todos os veterinários cadastrados

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getVeterinarios();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->getVeterinarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelVeterinarios[]**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVeterinario**
> \VertisConnect\Model\ModelVeterinarios updateVeterinario($veterinrio, $id)



Atualiza o veterinário do código informado

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinriosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$veterinrio = new \VertisConnect\Model\ModelVeterinarios(); // \VertisConnect\Model\ModelVeterinarios | Objeto Veterinário
$id = 56; // int | ID do Veterinário

try {
    $result = $apiInstance->updateVeterinario($veterinrio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinriosApi->updateVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **veterinrio** | [**\VertisConnect\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)| Objeto Veterinário |
 **id** | **int**| ID do Veterinário |

### Return type

[**\VertisConnect\Model\ModelVeterinarios**](../Model/ModelVeterinarios.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

