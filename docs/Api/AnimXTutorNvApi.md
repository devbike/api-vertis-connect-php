# VertisConnect\AnimXTutorNvApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAnimalXTutor**](AnimXTutorNvApi.md#deleteAnimalXTutor) | **DELETE** /api/V1.1/animais_x_tutores/{id} | 
[**updateAnimalXTutor**](AnimXTutorNvApi.md#updateAnimalXTutor) | **PUT** /api/V1.1/animais_x_tutores/{id} | 


# **deleteAnimalXTutor**
> \VertisConnect\Model\ModelAnimalXTutor deleteAnimalXTutor($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimXTutorNvApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animais_x_Tutores

try {
    $result = $apiInstance->deleteAnimalXTutor($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimXTutorNvApi->deleteAnimalXTutor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animais_x_Tutores |

### Return type

[**\VertisConnect\Model\ModelAnimalXTutor**](../Model/ModelAnimalXTutor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAnimalXTutor**
> \VertisConnect\Model\ModelAnimalXTutor updateAnimalXTutor($anim_x_tutor_nv, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimXTutorNvApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$anim_x_tutor_nv = new \VertisConnect\Model\ModelAnimalXTutor(); // \VertisConnect\Model\ModelAnimalXTutor | Objeto anim_x_tutor_nv
$id = 56; // int | ID do Animais_x_Tutores

try {
    $result = $apiInstance->updateAnimalXTutor($anim_x_tutor_nv, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimXTutorNvApi->updateAnimalXTutor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anim_x_tutor_nv** | [**\VertisConnect\Model\ModelAnimalXTutor**](../Model/ModelAnimalXTutor.md)| Objeto anim_x_tutor_nv |
 **id** | **int**| ID do Animais_x_Tutores |

### Return type

[**\VertisConnect\Model\ModelAnimalXTutor**](../Model/ModelAnimalXTutor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

