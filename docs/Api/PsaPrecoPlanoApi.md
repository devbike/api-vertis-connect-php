# VertisConnect\PsaPrecoPlanoApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPsaPrecoPlano**](PsaPrecoPlanoApi.md#createPsaPrecoPlano) | **POST** /api/V1.1/psa-preco-plano | 
[**deletePsaPrecoPlano**](PsaPrecoPlanoApi.md#deletePsaPrecoPlano) | **DELETE** /api/V1.1/psa-preco-plano/{id} | 
[**getPsaPrecoPlano**](PsaPrecoPlanoApi.md#getPsaPrecoPlano) | **GET** /api/V1.1/psa-preco-plano/{id} | 
[**getPsaPrecoPlanos**](PsaPrecoPlanoApi.md#getPsaPrecoPlanos) | **GET** /api/V1.1/psa-preco-plano | 
[**updatePsaPrecoPlano**](PsaPrecoPlanoApi.md#updatePsaPrecoPlano) | **PUT** /api/V1.1/psa-preco-plano/{id} | 


# **createPsaPrecoPlano**
> \VertisConnect\Model\ModelPsaPrecoPlano createPsaPrecoPlano($psa_preco_plano)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPrecoPlanoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_preco_plano = new \VertisConnect\Model\ModelPsaPrecoPlano(); // \VertisConnect\Model\ModelPsaPrecoPlano | Objeto fin_conta

try {
    $result = $apiInstance->createPsaPrecoPlano($psa_preco_plano);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPrecoPlanoApi->createPsaPrecoPlano: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_preco_plano** | [**\VertisConnect\Model\ModelPsaPrecoPlano**](../Model/ModelPsaPrecoPlano.md)| Objeto fin_conta |

### Return type

[**\VertisConnect\Model\ModelPsaPrecoPlano**](../Model/ModelPsaPrecoPlano.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePsaPrecoPlano**
> \VertisConnect\Model\ModelPsaPrecoPlano deletePsaPrecoPlano($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPrecoPlanoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaPrecoPlano

try {
    $result = $apiInstance->deletePsaPrecoPlano($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPrecoPlanoApi->deletePsaPrecoPlano: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaPrecoPlano |

### Return type

[**\VertisConnect\Model\ModelPsaPrecoPlano**](../Model/ModelPsaPrecoPlano.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaPrecoPlano**
> \VertisConnect\Model\ModelPsaPrecoPlano getPsaPrecoPlano($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPrecoPlanoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaPrecoPlano

try {
    $result = $apiInstance->getPsaPrecoPlano($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPrecoPlanoApi->getPsaPrecoPlano: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaPrecoPlano |

### Return type

[**\VertisConnect\Model\ModelPsaPrecoPlano**](../Model/ModelPsaPrecoPlano.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaPrecoPlanos**
> \VertisConnect\Model\ModelPsaPrecoPlano[] getPsaPrecoPlanos()



Retorna registros do objeto fin_conta

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPrecoPlanoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPsaPrecoPlanos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPrecoPlanoApi->getPsaPrecoPlanos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelPsaPrecoPlano[]**](../Model/ModelPsaPrecoPlano.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePsaPrecoPlano**
> \VertisConnect\Model\ModelPsaPrecoPlano updatePsaPrecoPlano($psa_preco_plano, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaPrecoPlanoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_preco_plano = new \VertisConnect\Model\ModelPsaPrecoPlano(); // \VertisConnect\Model\ModelPsaPrecoPlano | Objeto fin_conta
$id = 56; // int | ID do PsaPrecoPlano

try {
    $result = $apiInstance->updatePsaPrecoPlano($psa_preco_plano, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaPrecoPlanoApi->updatePsaPrecoPlano: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_preco_plano** | [**\VertisConnect\Model\ModelPsaPrecoPlano**](../Model/ModelPsaPrecoPlano.md)| Objeto fin_conta |
 **id** | **int**| ID do PsaPrecoPlano |

### Return type

[**\VertisConnect\Model\ModelPsaPrecoPlano**](../Model/ModelPsaPrecoPlano.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

