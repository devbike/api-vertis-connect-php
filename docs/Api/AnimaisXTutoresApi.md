# VertisConnect\AnimaisXTutoresApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAnimalXTutor**](AnimaisXTutoresApi.md#createAnimalXTutor) | **POST** /api/V1.1/animais_x_tutores | 
[**getAnimaisXTutores**](AnimaisXTutoresApi.md#getAnimaisXTutores) | **GET** /api/V1.1/animais_x_tutores | 
[**getAnimalXTutor**](AnimaisXTutoresApi.md#getAnimalXTutor) | **GET** /api/V1.1/animais_x_tutores/{id} | 


# **createAnimalXTutor**
> \VertisConnect\Model\ModelAnimalXTutor createAnimalXTutor($anim_x_tutor_nv)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisXTutoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$anim_x_tutor_nv = new \VertisConnect\Model\ModelAnimalXTutor(); // \VertisConnect\Model\ModelAnimalXTutor | Objeto anim_x_tutor_nv

try {
    $result = $apiInstance->createAnimalXTutor($anim_x_tutor_nv);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisXTutoresApi->createAnimalXTutor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anim_x_tutor_nv** | [**\VertisConnect\Model\ModelAnimalXTutor**](../Model/ModelAnimalXTutor.md)| Objeto anim_x_tutor_nv |

### Return type

[**\VertisConnect\Model\ModelAnimalXTutor**](../Model/ModelAnimalXTutor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimaisXTutores**
> \VertisConnect\Model\ModelAnimalXTutor[] getAnimaisXTutores()



Retorna registros do objeto anim_x_tutor_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisXTutoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAnimaisXTutores();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisXTutoresApi->getAnimaisXTutores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelAnimalXTutor[]**](../Model/ModelAnimalXTutor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimalXTutor**
> \VertisConnect\Model\ModelAnimalXTutor getAnimalXTutor($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisXTutoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animais_x_Tutores

try {
    $result = $apiInstance->getAnimalXTutor($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisXTutoresApi->getAnimalXTutor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animais_x_Tutores |

### Return type

[**\VertisConnect\Model\ModelAnimalXTutor**](../Model/ModelAnimalXTutor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

