# VertisConnect\ParametrizaoFiscalPropriedadeApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createInfoFiscal**](ParametrizaoFiscalPropriedadeApi.md#createInfoFiscal) | **POST** /api/V1.1/propriedades/{id}/paramsfiscais | 
[**deleteInfoFiscal**](ParametrizaoFiscalPropriedadeApi.md#deleteInfoFiscal) | **DELETE** /api/V1.1/propriedades/{id}/paramsfiscais | 
[**getInfoFiscal**](ParametrizaoFiscalPropriedadeApi.md#getInfoFiscal) | **GET** /api/V1.1/propriedades/{id}/paramsfiscais | 
[**updateInfoFiscal**](ParametrizaoFiscalPropriedadeApi.md#updateInfoFiscal) | **PUT** /api/V1.1/propriedades/{id}/paramsfiscais | 


# **createInfoFiscal**
> \VertisConnect\Model\ModelFiscal createInfoFiscal($parametrizao_fiscal, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_fiscal = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave da Properiedade

try {
    $result = $apiInstance->createInfoFiscal($parametrizao_fiscal, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalPropriedadeApi->createInfoFiscal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_fiscal** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave da Properiedade |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteInfoFiscal**
> \VertisConnect\Model\ModelFiscal deleteInfoFiscal($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->deleteInfoFiscal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalPropriedadeApi->deleteInfoFiscal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInfoFiscal**
> \VertisConnect\Model\ModelFiscal getInfoFiscal($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->getInfoFiscal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalPropriedadeApi->getInfoFiscal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateInfoFiscal**
> \VertisConnect\Model\ModelFiscal updateInfoFiscal($parametros_fiscais___propriedade, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___propriedade = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateInfoFiscal($parametros_fiscais___propriedade, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalPropriedadeApi->updateInfoFiscal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___propriedade** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

