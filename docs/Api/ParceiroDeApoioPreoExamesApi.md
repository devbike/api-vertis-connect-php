# VertisConnect\ParceiroDeApoioPreoExamesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createApoioExaPreco**](ParceiroDeApoioPreoExamesApi.md#createApoioExaPreco) | **POST** /api/V1.1/apoio_exa_precos | 
[**deleteApoioExaPreco**](ParceiroDeApoioPreoExamesApi.md#deleteApoioExaPreco) | **DELETE** /api/V1.1/apoio_exa_precos/{id} | 
[**getApoioExaPreco**](ParceiroDeApoioPreoExamesApi.md#getApoioExaPreco) | **GET** /api/V1.1/apoio_exa_precos/{id} | 
[**getApoioExaPrecos**](ParceiroDeApoioPreoExamesApi.md#getApoioExaPrecos) | **GET** /api/V1.1/apoio_exa_precos | 
[**updateApoioExaPreco**](ParceiroDeApoioPreoExamesApi.md#updateApoioExaPreco) | **PUT** /api/V1.1/apoio_exa_precos/{id} | 


# **createApoioExaPreco**
> \VertisConnect\Model\ModelParcApoioExaPreco createApoioExaPreco($parceiro_de__apoio____preo_exames)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParceiroDeApoioPreoExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parceiro_de__apoio____preo_exames = new \VertisConnect\Model\ModelParcApoioExaPreco(); // \VertisConnect\Model\ModelParcApoioExaPreco | Objeto parc_apoio_exa_preco

try {
    $result = $apiInstance->createApoioExaPreco($parceiro_de__apoio____preo_exames);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeApoioPreoExamesApi->createApoioExaPreco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parceiro_de__apoio____preo_exames** | [**\VertisConnect\Model\ModelParcApoioExaPreco**](../Model/ModelParcApoioExaPreco.md)| Objeto parc_apoio_exa_preco |

### Return type

[**\VertisConnect\Model\ModelParcApoioExaPreco**](../Model/ModelParcApoioExaPreco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteApoioExaPreco**
> \VertisConnect\Model\ModelParcApoioExaPreco deleteApoioExaPreco($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParceiroDeApoioPreoExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Parc. Apoio Exame Preço

try {
    $result = $apiInstance->deleteApoioExaPreco($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeApoioPreoExamesApi->deleteApoioExaPreco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Parc. Apoio Exame Preço |

### Return type

[**\VertisConnect\Model\ModelParcApoioExaPreco**](../Model/ModelParcApoioExaPreco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getApoioExaPreco**
> \VertisConnect\Model\ModelParcApoioExaPreco getApoioExaPreco($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParceiroDeApoioPreoExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Parc. Apoio Exame Preço

try {
    $result = $apiInstance->getApoioExaPreco($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeApoioPreoExamesApi->getApoioExaPreco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Parc. Apoio Exame Preço |

### Return type

[**\VertisConnect\Model\ModelParcApoioExaPreco**](../Model/ModelParcApoioExaPreco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getApoioExaPrecos**
> \VertisConnect\Model\ModelParcApoioExaPreco[] getApoioExaPrecos()



Retorna registros do objeto parc_apoio_exa_preco

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParceiroDeApoioPreoExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getApoioExaPrecos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeApoioPreoExamesApi->getApoioExaPrecos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelParcApoioExaPreco[]**](../Model/ModelParcApoioExaPreco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateApoioExaPreco**
> \VertisConnect\Model\ModelParcApoioExaPreco updateApoioExaPreco($parceiro_de__apoio____preo_exames, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParceiroDeApoioPreoExamesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parceiro_de__apoio____preo_exames = new \VertisConnect\Model\ModelParcApoioExaPreco(); // \VertisConnect\Model\ModelParcApoioExaPreco | Objeto parc_apoio_exa_preco
$id = 56; // int | ID do Parc. Apoio Exame Preço

try {
    $result = $apiInstance->updateApoioExaPreco($parceiro_de__apoio____preo_exames, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParceiroDeApoioPreoExamesApi->updateApoioExaPreco: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parceiro_de__apoio____preo_exames** | [**\VertisConnect\Model\ModelParcApoioExaPreco**](../Model/ModelParcApoioExaPreco.md)| Objeto parc_apoio_exa_preco |
 **id** | **int**| ID do Parc. Apoio Exame Preço |

### Return type

[**\VertisConnect\Model\ModelParcApoioExaPreco**](../Model/ModelParcApoioExaPreco.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

