# VertisConnect\FinTituloApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createfintitulo**](FinTituloApi.md#createfintitulo) | **POST** /api/V1.1/fin-titulos | 
[**deletefintitulo**](FinTituloApi.md#deletefintitulo) | **DELETE** /api/V1.1/fin-titulos/{id} | 
[**getfintitulo**](FinTituloApi.md#getfintitulo) | **GET** /api/V1.1/fin-titulos/{id} | 
[**getfintitulos**](FinTituloApi.md#getfintitulos) | **GET** /api/V1.1/fin-titulos | 
[**liquidafintitulo**](FinTituloApi.md#liquidafintitulo) | **PUT** /api/V1.1/fin-titulos/liquida/{id} | 
[**updatefintitulo**](FinTituloApi.md#updatefintitulo) | **PUT** /api/V1.1/fin-titulos/{id} | 


# **createfintitulo**
> \VertisConnect\Model\ModelFinTitulo createfintitulo($fin_titulo)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinTituloApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_titulo = new \VertisConnect\Model\ModelFinTitulo(); // \VertisConnect\Model\ModelFinTitulo | Objeto fin_titulo

try {
    $result = $apiInstance->createfintitulo($fin_titulo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinTituloApi->createfintitulo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_titulo** | [**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)| Objeto fin_titulo |

### Return type

[**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletefintitulo**
> \VertisConnect\Model\ModelFinTitulo deletefintitulo($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinTituloApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinTitulo

try {
    $result = $apiInstance->deletefintitulo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinTituloApi->deletefintitulo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinTitulo |

### Return type

[**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfintitulo**
> \VertisConnect\Model\ModelFinTitulo getfintitulo($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinTituloApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinTitulo

try {
    $result = $apiInstance->getfintitulo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinTituloApi->getfintitulo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinTitulo |

### Return type

[**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfintitulos**
> \VertisConnect\Model\ModelFinTitulo[] getfintitulos()



Retorna registros do objeto fin_titulo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinTituloApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getfintitulos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinTituloApi->getfintitulos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelFinTitulo[]**](../Model/ModelFinTitulo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **liquidafintitulo**
> \VertisConnect\Model\ModelFinTitulo liquidafintitulo($fin_titulo, $id)



Liquida o título

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinTituloApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_titulo = new \VertisConnect\Model\ModelFinTitulo(); // \VertisConnect\Model\ModelFinTitulo | Objeto fin_titulo
$id = 56; // int | ID do FinTitulo

try {
    $result = $apiInstance->liquidafintitulo($fin_titulo, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinTituloApi->liquidafintitulo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_titulo** | [**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)| Objeto fin_titulo |
 **id** | **int**| ID do FinTitulo |

### Return type

[**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatefintitulo**
> \VertisConnect\Model\ModelFinTitulo updatefintitulo($fin_titulo, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinTituloApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_titulo = new \VertisConnect\Model\ModelFinTitulo(); // \VertisConnect\Model\ModelFinTitulo | Objeto fin_titulo
$id = 56; // int | ID do FinTitulo

try {
    $result = $apiInstance->updatefintitulo($fin_titulo, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinTituloApi->updatefintitulo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_titulo** | [**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)| Objeto fin_titulo |
 **id** | **int**| ID do FinTitulo |

### Return type

[**\VertisConnect\Model\ModelFinTitulo**](../Model/ModelFinTitulo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

