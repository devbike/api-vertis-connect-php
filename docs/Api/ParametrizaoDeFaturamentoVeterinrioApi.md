# VertisConnect\ParametrizaoDeFaturamentoVeterinrioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfFatVet**](ParametrizaoDeFaturamentoVeterinrioApi.md#doCreateInfFatVet) | **POST** /api/V1.1/veterinarios/{id}/paramsfat | 
[**doDeleteInfFatVet**](ParametrizaoDeFaturamentoVeterinrioApi.md#doDeleteInfFatVet) | **DELETE** /api/V1.1/veterinarios/{id}/paramsfat | 
[**doGetInfFatVet**](ParametrizaoDeFaturamentoVeterinrioApi.md#doGetInfFatVet) | **GET** /api/V1.1/veterinarios/{id}/paramsfat | 
[**doUpdateInfFatVet**](ParametrizaoDeFaturamentoVeterinrioApi.md#doUpdateInfFatVet) | **PUT** /api/V1.1/veterinarios/{id}/paramsfat | 


# **doCreateInfFatVet**
> \VertisConnect\Model\ModelParcInfFaturamento doCreateInfFatVet($parametrizao_de_faturamento, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_de_faturamento = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doCreateInfFatVet($parametrizao_de_faturamento, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoVeterinrioApi->doCreateInfFatVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_de_faturamento** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfFatVet**
> \VertisConnect\Model\ModelParcInfFaturamento doDeleteInfFatVet($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfFatVet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoVeterinrioApi->doDeleteInfFatVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfFatVet**
> \VertisConnect\Model\ModelParcInfFaturamento doGetInfFatVet($id)



Retorna configurações de faturamento para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfFatVet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoVeterinrioApi->doGetInfFatVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfFatVet**
> \VertisConnect\Model\ModelParcInfFaturamento doUpdateInfFatVet($parametros_fiscais___veterinrio, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___veterinrio = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfFatVet($parametros_fiscais___veterinrio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoVeterinrioApi->doUpdateInfFatVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___veterinrio** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

