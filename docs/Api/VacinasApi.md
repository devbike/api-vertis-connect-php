# VertisConnect\VacinasApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVacina**](VacinasApi.md#createVacina) | **POST** /api/V1.1/vacinas | 
[**deleteVacina**](VacinasApi.md#deleteVacina) | **DELETE** /api/V1.1/vacinas/{id} | 
[**getVacina**](VacinasApi.md#getVacina) | **GET** /api/V1.1/vacinas/{id} | 
[**getVacinas**](VacinasApi.md#getVacinas) | **GET** /api/V1.1/vacinas | 
[**updateVacina**](VacinasApi.md#updateVacina) | **PUT** /api/V1.1/vacinas/{id} | 


# **createVacina**
> \VertisConnect\Model\ModelVacinas createVacina($vacinas)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VacinasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$vacinas = new \VertisConnect\Model\ModelVacinas(); // \VertisConnect\Model\ModelVacinas | Objeto prod_vacina_nv

try {
    $result = $apiInstance->createVacina($vacinas);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VacinasApi->createVacina: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vacinas** | [**\VertisConnect\Model\ModelVacinas**](../Model/ModelVacinas.md)| Objeto prod_vacina_nv |

### Return type

[**\VertisConnect\Model\ModelVacinas**](../Model/ModelVacinas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVacina**
> \VertisConnect\Model\ModelVacinas deleteVacina($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VacinasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Vacinas

try {
    $result = $apiInstance->deleteVacina($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VacinasApi->deleteVacina: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Vacinas |

### Return type

[**\VertisConnect\Model\ModelVacinas**](../Model/ModelVacinas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVacina**
> \VertisConnect\Model\ModelVacinas getVacina($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VacinasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Vacinas

try {
    $result = $apiInstance->getVacina($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VacinasApi->getVacina: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Vacinas |

### Return type

[**\VertisConnect\Model\ModelVacinas**](../Model/ModelVacinas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVacinas**
> \VertisConnect\Model\ModelVacinas[] getVacinas()



Retorna registros do objeto prod_vacina_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VacinasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getVacinas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VacinasApi->getVacinas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelVacinas[]**](../Model/ModelVacinas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVacina**
> \VertisConnect\Model\ModelVacinas updateVacina($vacinas, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VacinasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$vacinas = new \VertisConnect\Model\ModelVacinas(); // \VertisConnect\Model\ModelVacinas | Objeto prod_vacina_nv
$id = 56; // int | ID do Vacinas

try {
    $result = $apiInstance->updateVacina($vacinas, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VacinasApi->updateVacina: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vacinas** | [**\VertisConnect\Model\ModelVacinas**](../Model/ModelVacinas.md)| Objeto prod_vacina_nv |
 **id** | **int**| ID do Vacinas |

### Return type

[**\VertisConnect\Model\ModelVacinas**](../Model/ModelVacinas.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

