# VertisConnect\AlocaoDeProdutosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLocal**](AlocaoDeProdutosApi.md#createLocal) | **POST** /api/V1.1/locais | 
[**deleteLocal**](AlocaoDeProdutosApi.md#deleteLocal) | **DELETE** /api/V1.1/locais | 
[**getLocal**](AlocaoDeProdutosApi.md#getLocal) | **GET** /api/V1.1/locais/{id} | 


# **createLocal**
> \VertisConnect\Model\ModelProdutoLocal createLocal($alocao_de_produtos)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AlocaoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$alocao_de_produtos = new \VertisConnect\Model\ModelProdutoLocal(); // \VertisConnect\Model\ModelProdutoLocal | Objeto prod_local

try {
    $result = $apiInstance->createLocal($alocao_de_produtos);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AlocaoDeProdutosApi->createLocal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alocao_de_produtos** | [**\VertisConnect\Model\ModelProdutoLocal**](../Model/ModelProdutoLocal.md)| Objeto prod_local |

### Return type

[**\VertisConnect\Model\ModelProdutoLocal**](../Model/ModelProdutoLocal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteLocal**
> \VertisConnect\Model\ModelProdutoLocal deleteLocal($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AlocaoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Alocação de Produtos

try {
    $result = $apiInstance->deleteLocal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AlocaoDeProdutosApi->deleteLocal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Alocação de Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutoLocal**](../Model/ModelProdutoLocal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLocal**
> \VertisConnect\Model\ModelProdutoLocal getLocal($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AlocaoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Alocação de Produtos

try {
    $result = $apiInstance->getLocal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AlocaoDeProdutosApi->getLocal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Alocação de Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutoLocal**](../Model/ModelProdutoLocal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

