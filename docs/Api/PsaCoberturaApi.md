# VertisConnect\PsaCoberturaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPsaCobertura**](PsaCoberturaApi.md#createPsaCobertura) | **POST** /api/V1.1/psa-cobertura | 
[**deletePsaCobertura**](PsaCoberturaApi.md#deletePsaCobertura) | **DELETE** /api/V1.1/psa-cobertura/{id} | 
[**getPsaCobertura**](PsaCoberturaApi.md#getPsaCobertura) | **GET** /api/V1.1/psa-cobertura/{id} | 
[**getPsaCoberturas**](PsaCoberturaApi.md#getPsaCoberturas) | **GET** /api/V1.1/psa-cobertura | 
[**updatePsaCobertura**](PsaCoberturaApi.md#updatePsaCobertura) | **PUT** /api/V1.1/psa-cobertura/{id} | 


# **createPsaCobertura**
> \VertisConnect\Model\ModelPsaCobertura createPsaCobertura($psa_cobertura)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_cobertura = new \VertisConnect\Model\ModelPsaCobertura(); // \VertisConnect\Model\ModelPsaCobertura | Objeto fin_conta

try {
    $result = $apiInstance->createPsaCobertura($psa_cobertura);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaCoberturaApi->createPsaCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_cobertura** | [**\VertisConnect\Model\ModelPsaCobertura**](../Model/ModelPsaCobertura.md)| Objeto fin_conta |

### Return type

[**\VertisConnect\Model\ModelPsaCobertura**](../Model/ModelPsaCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePsaCobertura**
> \VertisConnect\Model\ModelPsaCobertura deletePsaCobertura($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaCobertura

try {
    $result = $apiInstance->deletePsaCobertura($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaCoberturaApi->deletePsaCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaCobertura |

### Return type

[**\VertisConnect\Model\ModelPsaCobertura**](../Model/ModelPsaCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaCobertura**
> \VertisConnect\Model\ModelPsaCobertura getPsaCobertura($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PsaCobertura

try {
    $result = $apiInstance->getPsaCobertura($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaCoberturaApi->getPsaCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PsaCobertura |

### Return type

[**\VertisConnect\Model\ModelPsaCobertura**](../Model/ModelPsaCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPsaCoberturas**
> \VertisConnect\Model\ModelPsaCobertura[] getPsaCoberturas()



Retorna registros do objeto fin_conta

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPsaCoberturas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaCoberturaApi->getPsaCoberturas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelPsaCobertura[]**](../Model/ModelPsaCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePsaCobertura**
> \VertisConnect\Model\ModelPsaCobertura updatePsaCobertura($psa_cobertura, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PsaCoberturaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$psa_cobertura = new \VertisConnect\Model\ModelPsaCobertura(); // \VertisConnect\Model\ModelPsaCobertura | Objeto fin_conta
$id = 56; // int | ID do PsaCobertura

try {
    $result = $apiInstance->updatePsaCobertura($psa_cobertura, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PsaCoberturaApi->updatePsaCobertura: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **psa_cobertura** | [**\VertisConnect\Model\ModelPsaCobertura**](../Model/ModelPsaCobertura.md)| Objeto fin_conta |
 **id** | **int**| ID do PsaCobertura |

### Return type

[**\VertisConnect\Model\ModelPsaCobertura**](../Model/ModelPsaCobertura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

