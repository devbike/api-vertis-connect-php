# VertisConnect\PlanoSaudeApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPlanoSaude**](PlanoSaudeApi.md#createPlanoSaude) | **POST** /api/V1.1/psa-plano-saude | 
[**deletePlanoSaude**](PlanoSaudeApi.md#deletePlanoSaude) | **DELETE** /api/V1.1/psa-plano-saude/{id} | 
[**getPlanoSaude**](PlanoSaudeApi.md#getPlanoSaude) | **GET** /api/V1.1/psa-plano-saude/{id} | 
[**getPlanoSaudes**](PlanoSaudeApi.md#getPlanoSaudes) | **GET** /api/V1.1/psa-plano-saude | 
[**updatePlanoSaude**](PlanoSaudeApi.md#updatePlanoSaude) | **PUT** /api/V1.1/psa-plano-saude/{id} | 


# **createPlanoSaude**
> \VertisConnect\Model\ModelPlanoSaude createPlanoSaude($plano_saude)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PlanoSaudeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$plano_saude = new \VertisConnect\Model\ModelPlanoSaude(); // \VertisConnect\Model\ModelPlanoSaude | Objeto fin_conta

try {
    $result = $apiInstance->createPlanoSaude($plano_saude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanoSaudeApi->createPlanoSaude: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plano_saude** | [**\VertisConnect\Model\ModelPlanoSaude**](../Model/ModelPlanoSaude.md)| Objeto fin_conta |

### Return type

[**\VertisConnect\Model\ModelPlanoSaude**](../Model/ModelPlanoSaude.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePlanoSaude**
> \VertisConnect\Model\ModelPlanoSaude deletePlanoSaude($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PlanoSaudeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PlanoSaude

try {
    $result = $apiInstance->deletePlanoSaude($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanoSaudeApi->deletePlanoSaude: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PlanoSaude |

### Return type

[**\VertisConnect\Model\ModelPlanoSaude**](../Model/ModelPlanoSaude.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPlanoSaude**
> \VertisConnect\Model\ModelPlanoSaude getPlanoSaude($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PlanoSaudeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do PlanoSaude

try {
    $result = $apiInstance->getPlanoSaude($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanoSaudeApi->getPlanoSaude: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do PlanoSaude |

### Return type

[**\VertisConnect\Model\ModelPlanoSaude**](../Model/ModelPlanoSaude.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPlanoSaudes**
> \VertisConnect\Model\ModelPlanoSaude[] getPlanoSaudes()



Retorna registros do objeto fin_conta

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PlanoSaudeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPlanoSaudes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanoSaudeApi->getPlanoSaudes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelPlanoSaude[]**](../Model/ModelPlanoSaude.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePlanoSaude**
> \VertisConnect\Model\ModelPlanoSaude updatePlanoSaude($plano_saude, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\PlanoSaudeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$plano_saude = new \VertisConnect\Model\ModelPlanoSaude(); // \VertisConnect\Model\ModelPlanoSaude | Objeto fin_conta
$id = 56; // int | ID do PlanoSaude

try {
    $result = $apiInstance->updatePlanoSaude($plano_saude, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlanoSaudeApi->updatePlanoSaude: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plano_saude** | [**\VertisConnect\Model\ModelPlanoSaude**](../Model/ModelPlanoSaude.md)| Objeto fin_conta |
 **id** | **int**| ID do PlanoSaude |

### Return type

[**\VertisConnect\Model\ModelPlanoSaude**](../Model/ModelPlanoSaude.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

