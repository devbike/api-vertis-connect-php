# VertisConnect\ParametrizaoFiscalMotoboyApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoFiscalMBoy**](ParametrizaoFiscalMotoboyApi.md#doCreateInfoFiscalMBoy) | **POST** /api/V1.1/motoboys/{id}/paramsfiscais | 
[**doDeleteInfoFiscalMBoy**](ParametrizaoFiscalMotoboyApi.md#doDeleteInfoFiscalMBoy) | **DELETE** /api/V1.1/motoboys/{id}/paramsfiscais | 
[**doGetInfoFiscalMBoy**](ParametrizaoFiscalMotoboyApi.md#doGetInfoFiscalMBoy) | **GET** /api/V1.1/motoboys/{id}/paramsfiscais | 
[**doUpdateInfoFiscalMBoy**](ParametrizaoFiscalMotoboyApi.md#doUpdateInfoFiscalMBoy) | **PUT** /api/V1.1/motoboys/{id}/paramsfiscais | 


# **doCreateInfoFiscalMBoy**
> \VertisConnect\Model\ModelFiscal doCreateInfoFiscalMBoy($parametrizao_fiscal, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_fiscal = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do Motoboy

try {
    $result = $apiInstance->doCreateInfoFiscalMBoy($parametrizao_fiscal, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalMotoboyApi->doCreateInfoFiscalMBoy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_fiscal** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do Motoboy |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoFiscalMBoy**
> \VertisConnect\Model\ModelFiscal doDeleteInfoFiscalMBoy($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoFiscalMBoy($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalMotoboyApi->doDeleteInfoFiscalMBoy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoFiscalMBoy**
> \VertisConnect\Model\ModelFiscal doGetInfoFiscalMBoy($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoFiscalMBoy($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalMotoboyApi->doGetInfoFiscalMBoy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoFiscalMBoy**
> \VertisConnect\Model\ModelFiscal doUpdateInfoFiscalMBoy($parametros_fiscais___motoboy, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalMotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___motoboy = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoFiscalMBoy($parametros_fiscais___motoboy, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalMotoboyApi->doUpdateInfoFiscalMBoy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___motoboy** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

