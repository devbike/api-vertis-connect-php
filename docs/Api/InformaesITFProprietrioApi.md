# VertisConnect\InformaesITFProprietrioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoITFVertisProp**](InformaesITFProprietrioApi.md#doCreateInfoITFVertisProp) | **POST** /api/V1.1/proprietarios/{id}/itf-vertis | 
[**doDeleteInfoITFVertisPro**](InformaesITFProprietrioApi.md#doDeleteInfoITFVertisPro) | **DELETE** /api/V1.1/proprietarios/{id}/itf-vertis | 
[**doGetInfoITFVertisProp**](InformaesITFProprietrioApi.md#doGetInfoITFVertisProp) | **GET** /api/V1.1/proprietarios/{id}/itf-vertis | 
[**doUpdateInfoITFVertisProp**](InformaesITFProprietrioApi.md#doUpdateInfoITFVertisProp) | **PUT** /api/V1.1/proprietarios/{id}/itf-vertis | 


# **doCreateInfoITFVertisProp**
> \VertisConnect\Model\ModelParcInfITFVertis doCreateInfoITFVertisProp($informaes_itf, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFProprietrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$informaes_itf = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doCreateInfoITFVertisProp($informaes_itf, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFProprietrioApi->doCreateInfoITFVertisProp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **informaes_itf** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoITFVertisPro**
> \VertisConnect\Model\ModelParcInfITFVertis doDeleteInfoITFVertisPro($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFProprietrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoITFVertisPro($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFProprietrioApi->doDeleteInfoITFVertisPro: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoITFVertisProp**
> \VertisConnect\Model\ModelParcInfITFVertis doGetInfoITFVertisProp($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFProprietrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoITFVertisProp($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFProprietrioApi->doGetInfoITFVertisProp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoITFVertisProp**
> \VertisConnect\Model\ModelParcInfITFVertis doUpdateInfoITFVertisProp($parametros_fiscais___proprietrio, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFProprietrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___proprietrio = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoITFVertisProp($parametros_fiscais___proprietrio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFProprietrioApi->doUpdateInfoITFVertisProp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___proprietrio** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

