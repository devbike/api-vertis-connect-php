# VertisConnect\ExameEquipamentosUnidadesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createEquipUnidade**](ExameEquipamentosUnidadesApi.md#createEquipUnidade) | **POST** /api/V1.1/eq-x-uoper | 
[**deleteEquipUnidade**](ExameEquipamentosUnidadesApi.md#deleteEquipUnidade) | **DELETE** /api/V1.1/eq-x-uoper/{id} | 
[**getEquipUnidade**](ExameEquipamentosUnidadesApi.md#getEquipUnidade) | **GET** /api/V1.1/eq-x-uoper/{id} | 
[**getEquipamentos**](ExameEquipamentosUnidadesApi.md#getEquipamentos) | **GET** /api/V1.1/eq-x-uoper | 
[**getEquipsExame**](ExameEquipamentosUnidadesApi.md#getEquipsExame) | **GET** /api/V1.1/exames/{ex}/equipamentos | 
[**getEquipsUnidNeg**](ExameEquipamentosUnidadesApi.md#getEquipsUnidNeg) | **GET** /api/V1.1/uneg/{un}/equipamentos | 
[**getEquipsUnidOpe**](ExameEquipamentosUnidadesApi.md#getEquipsUnidOpe) | **GET** /api/V1.1/uoper/{uo}/equipamentos | 
[**getEquipsUnidades**](ExameEquipamentosUnidadesApi.md#getEquipsUnidades) | **GET** /api/V1.1/equipamento/{id}/equipamentos | 
[**updateEquipUnidade**](ExameEquipamentosUnidadesApi.md#updateEquipUnidade) | **PUT** /api/V1.1/eq-x-uoper/{id} | 


# **createEquipUnidade**
> \VertisConnect\Model\ModelEquipamentosUnidades createEquipUnidade($exame_equipamentos_unidades)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exame_equipamentos_unidades = new \VertisConnect\Model\ModelEquipamentosUnidades(); // \VertisConnect\Model\ModelEquipamentosUnidades | Objeto prod_exa_x_eqp_x_uoper_nv

try {
    $result = $apiInstance->createEquipUnidade($exame_equipamentos_unidades);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->createEquipUnidade: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exame_equipamentos_unidades** | [**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)| Objeto prod_exa_x_eqp_x_uoper_nv |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteEquipUnidade**
> \VertisConnect\Model\ModelEquipamentosUnidades deleteEquipUnidade($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame Equipamentos Unidades

try {
    $result = $apiInstance->deleteEquipUnidade($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->deleteEquipUnidade: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame Equipamentos Unidades |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEquipUnidade**
> \VertisConnect\Model\ModelEquipamentosUnidades getEquipUnidade($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame Equipamentos Unidades

try {
    $result = $apiInstance->getEquipUnidade($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->getEquipUnidade: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame Equipamentos Unidades |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEquipamentos**
> \VertisConnect\Model\ModelEquipamentosUnidades getEquipamentos($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Exame Equipamentos Unidades

try {
    $result = $apiInstance->getEquipamentos($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->getEquipamentos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Exame Equipamentos Unidades |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEquipsExame**
> \VertisConnect\Model\ModelEquipamentosUnidades[] getEquipsExame($ex)



Retorna Equipamentos e Unidades para exame definido pelo parametro EX prod_exa_x_eqp_x_uoper_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ex = 56; // int | Codigo do exame

try {
    $result = $apiInstance->getEquipsExame($ex);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->getEquipsExame: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ex** | **int**| Codigo do exame |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades[]**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEquipsUnidNeg**
> \VertisConnect\Model\ModelEquipamentosUnidades[] getEquipsUnidNeg($un)



Retorna equipamentos na unidade de negocio definida pelo parametro UN prod_exa_x_eqp_x_uoper_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$un = 56; // int | Codigo da unidade de negocio

try {
    $result = $apiInstance->getEquipsUnidNeg($un);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->getEquipsUnidNeg: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **un** | **int**| Codigo da unidade de negocio |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades[]**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEquipsUnidOpe**
> \VertisConnect\Model\ModelEquipamentosUnidades[] getEquipsUnidOpe($uo)



Retorna equipamentos na unidade operacional definida pelo parametro UO prod_exa_x_eqp_x_uoper_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uo = 56; // int | Codigo da unidade operacional

try {
    $result = $apiInstance->getEquipsUnidOpe($uo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->getEquipsUnidOpe: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uo** | **int**| Codigo da unidade operacional |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades[]**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEquipsUnidades**
> \VertisConnect\Model\ModelEquipamentosUnidades getEquipsUnidades($id)



Retorna informações do equipamento, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID Equipamentos

try {
    $result = $apiInstance->getEquipsUnidades($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->getEquipsUnidades: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID Equipamentos |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateEquipUnidade**
> \VertisConnect\Model\ModelEquipamentosUnidades updateEquipUnidade($exame_equipamentos_unidades, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExameEquipamentosUnidadesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exame_equipamentos_unidades = new \VertisConnect\Model\ModelEquipamentosUnidades(); // \VertisConnect\Model\ModelEquipamentosUnidades | Objeto prod_exa_x_eqp_x_uoper_nv
$id = 56; // int | ID do Exame Equipamentos Unidades

try {
    $result = $apiInstance->updateEquipUnidade($exame_equipamentos_unidades, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExameEquipamentosUnidadesApi->updateEquipUnidade: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exame_equipamentos_unidades** | [**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)| Objeto prod_exa_x_eqp_x_uoper_nv |
 **id** | **int**| ID do Exame Equipamentos Unidades |

### Return type

[**\VertisConnect\Model\ModelEquipamentosUnidades**](../Model/ModelEquipamentosUnidades.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

