# VertisConnect\InformaesITFBanhistaETosadorApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoITFVertisBnh**](InformaesITFBanhistaETosadorApi.md#doCreateInfoITFVertisBnh) | **POST** /api/V1.1/banhotosa/{id}/itf-vertis | 
[**doDeleteInfoITFVertisBnh**](InformaesITFBanhistaETosadorApi.md#doDeleteInfoITFVertisBnh) | **DELETE** /api/V1.1/banhotosa/{id}/itf-vertis | 
[**doGetInfoITFVertisBnh**](InformaesITFBanhistaETosadorApi.md#doGetInfoITFVertisBnh) | **GET** /api/V1.1/banhotosa/{id}/itf-vertis | 
[**doUpdateInfoITFVertisBnh**](InformaesITFBanhistaETosadorApi.md#doUpdateInfoITFVertisBnh) | **PUT** /api/V1.1/banhotosa/{id}/itf-vertis | 


# **doCreateInfoITFVertisBnh**
> \VertisConnect\Model\ModelParcInfITFVertis doCreateInfoITFVertisBnh($informaes_itf, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFBanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$informaes_itf = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do Cliente

try {
    $result = $apiInstance->doCreateInfoITFVertisBnh($informaes_itf, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFBanhistaETosadorApi->doCreateInfoITFVertisBnh: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **informaes_itf** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do Cliente |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoITFVertisBnh**
> \VertisConnect\Model\ModelParcInfITFVertis doDeleteInfoITFVertisBnh($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFBanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoITFVertisBnh($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFBanhistaETosadorApi->doDeleteInfoITFVertisBnh: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoITFVertisBnh**
> \VertisConnect\Model\ModelParcInfITFVertis doGetInfoITFVertisBnh($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFBanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoITFVertisBnh($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFBanhistaETosadorApi->doGetInfoITFVertisBnh: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoITFVertisBnh**
> \VertisConnect\Model\ModelParcInfITFVertis doUpdateInfoITFVertisBnh($parametros_fiscais___banhista_e_tosador, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFBanhistaETosadorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___banhista_e_tosador = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoITFVertisBnh($parametros_fiscais___banhista_e_tosador, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFBanhistaETosadorApi->doUpdateInfoITFVertisBnh: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___banhista_e_tosador** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

