# VertisConnect\AnimaisXTutoresIdAnimaltutoresApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAnimalXTutores**](AnimaisXTutoresIdAnimaltutoresApi.md#getAnimalXTutores) | **GET** /api/V1.1/animais_x_tutores/{idAnimal}/tutores | 


# **getAnimalXTutores**
> \VertisConnect\Model\ModelAnimalXTutor getAnimalXTutores($id_animal)



Retorna informações de todos os tutores de um animal.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisXTutoresIdAnimaltutoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id_animal = 56; // int | ID do Animal

try {
    $result = $apiInstance->getAnimalXTutores($id_animal);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisXTutoresIdAnimaltutoresApi->getAnimalXTutores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_animal** | **int**| ID do Animal |

### Return type

[**\VertisConnect\Model\ModelAnimalXTutor**](../Model/ModelAnimalXTutor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

