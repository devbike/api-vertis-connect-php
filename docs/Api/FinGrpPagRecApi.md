# VertisConnect\FinGrpPagRecApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createfingrppagrec**](FinGrpPagRecApi.md#createfingrppagrec) | **POST** /api/V1.1/fin-grps-pagrec | 
[**deletefingrppagrec**](FinGrpPagRecApi.md#deletefingrppagrec) | **DELETE** /api/V1.1/fin-grps-pagrec/{id} | 
[**getfingrppagrec**](FinGrpPagRecApi.md#getfingrppagrec) | **GET** /api/V1.1/fin-grps-pagrec/{id} | 
[**getfingrppagrecs**](FinGrpPagRecApi.md#getfingrppagrecs) | **GET** /api/V1.1/fin-grps-pagrec | 
[**updatefingrppagrec**](FinGrpPagRecApi.md#updatefingrppagrec) | **PUT** /api/V1.1/fin-grps-pagrec/{id} | 


# **createfingrppagrec**
> \VertisConnect\Model\ModelFinGrpPagRec createfingrppagrec($fin_grp_pag_rec)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinGrpPagRecApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_grp_pag_rec = new \VertisConnect\Model\ModelFinGrpPagRec(); // \VertisConnect\Model\ModelFinGrpPagRec | Objeto fin_grp_pagrec

try {
    $result = $apiInstance->createfingrppagrec($fin_grp_pag_rec);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinGrpPagRecApi->createfingrppagrec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_grp_pag_rec** | [**\VertisConnect\Model\ModelFinGrpPagRec**](../Model/ModelFinGrpPagRec.md)| Objeto fin_grp_pagrec |

### Return type

[**\VertisConnect\Model\ModelFinGrpPagRec**](../Model/ModelFinGrpPagRec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletefingrppagrec**
> \VertisConnect\Model\ModelFinGrpPagRec deletefingrppagrec($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinGrpPagRecApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinGrpPagRec

try {
    $result = $apiInstance->deletefingrppagrec($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinGrpPagRecApi->deletefingrppagrec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinGrpPagRec |

### Return type

[**\VertisConnect\Model\ModelFinGrpPagRec**](../Model/ModelFinGrpPagRec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfingrppagrec**
> \VertisConnect\Model\ModelFinGrpPagRec getfingrppagrec($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinGrpPagRecApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do FinGrpPagRec

try {
    $result = $apiInstance->getfingrppagrec($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinGrpPagRecApi->getfingrppagrec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do FinGrpPagRec |

### Return type

[**\VertisConnect\Model\ModelFinGrpPagRec**](../Model/ModelFinGrpPagRec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getfingrppagrecs**
> \VertisConnect\Model\ModelFinGrpPagRec[] getfingrppagrecs()



Retorna registros do objeto fin_grp_pagrec

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinGrpPagRecApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getfingrppagrecs();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinGrpPagRecApi->getfingrppagrecs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelFinGrpPagRec[]**](../Model/ModelFinGrpPagRec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatefingrppagrec**
> \VertisConnect\Model\ModelFinGrpPagRec updatefingrppagrec($fin_grp_pag_rec, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\FinGrpPagRecApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$fin_grp_pag_rec = new \VertisConnect\Model\ModelFinGrpPagRec(); // \VertisConnect\Model\ModelFinGrpPagRec | Objeto fin_grp_pagrec
$id = 56; // int | ID do FinGrpPagRec

try {
    $result = $apiInstance->updatefingrppagrec($fin_grp_pag_rec, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinGrpPagRecApi->updatefingrppagrec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fin_grp_pag_rec** | [**\VertisConnect\Model\ModelFinGrpPagRec**](../Model/ModelFinGrpPagRec.md)| Objeto fin_grp_pagrec |
 **id** | **int**| ID do FinGrpPagRec |

### Return type

[**\VertisConnect\Model\ModelFinGrpPagRec**](../Model/ModelFinGrpPagRec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

