# VertisConnect\TesteDeParaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTestesDPara**](TesteDeParaApi.md#createTestesDPara) | **POST** /api/V1.1/testesdpara | 
[**deleteTestesDPara**](TesteDeParaApi.md#deleteTestesDPara) | **DELETE** /api/V1.1/testesdpara/{id} | 
[**getTestesDPara**](TesteDeParaApi.md#getTestesDPara) | **GET** /api/V1.1/testesdpara/{id} | 
[**getTestesDParas**](TesteDeParaApi.md#getTestesDParas) | **GET** /api/V1.1/testesdpara | 
[**updateTestesDPara**](TesteDeParaApi.md#updateTestesDPara) | **PUT** /api/V1.1/testesdpara/{id} | 


# **createTestesDPara**
> \VertisConnect\Model\ModelTestesDPara createTestesDPara($teste_de__para)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TesteDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$teste_de__para = new \VertisConnect\Model\ModelTestesDPara(); // \VertisConnect\Model\ModelTestesDPara | Objeto prod_exa_teste_de_para_nv

try {
    $result = $apiInstance->createTestesDPara($teste_de__para);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TesteDeParaApi->createTestesDPara: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **teste_de__para** | [**\VertisConnect\Model\ModelTestesDPara**](../Model/ModelTestesDPara.md)| Objeto prod_exa_teste_de_para_nv |

### Return type

[**\VertisConnect\Model\ModelTestesDPara**](../Model/ModelTestesDPara.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteTestesDPara**
> \VertisConnect\Model\ModelTestesDPara deleteTestesDPara($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TesteDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Teste De > Para

try {
    $result = $apiInstance->deleteTestesDPara($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TesteDeParaApi->deleteTestesDPara: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Teste De &gt; Para |

### Return type

[**\VertisConnect\Model\ModelTestesDPara**](../Model/ModelTestesDPara.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTestesDPara**
> \VertisConnect\Model\ModelTestesDPara getTestesDPara($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TesteDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Teste De > Para

try {
    $result = $apiInstance->getTestesDPara($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TesteDeParaApi->getTestesDPara: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Teste De &gt; Para |

### Return type

[**\VertisConnect\Model\ModelTestesDPara**](../Model/ModelTestesDPara.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTestesDParas**
> \VertisConnect\Model\ModelTestesDPara[] getTestesDParas()



Retorna registros do objeto prod_exa_teste_de_para_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TesteDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getTestesDParas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TesteDeParaApi->getTestesDParas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelTestesDPara[]**](../Model/ModelTestesDPara.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateTestesDPara**
> \VertisConnect\Model\ModelTestesDPara updateTestesDPara($teste_de__para, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TesteDeParaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$teste_de__para = new \VertisConnect\Model\ModelTestesDPara(); // \VertisConnect\Model\ModelTestesDPara | Objeto prod_exa_teste_de_para_nv
$id = 56; // int | ID do Teste De > Para

try {
    $result = $apiInstance->updateTestesDPara($teste_de__para, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TesteDeParaApi->updateTestesDPara: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **teste_de__para** | [**\VertisConnect\Model\ModelTestesDPara**](../Model/ModelTestesDPara.md)| Objeto prod_exa_teste_de_para_nv |
 **id** | **int**| ID do Teste De &gt; Para |

### Return type

[**\VertisConnect\Model\ModelTestesDPara**](../Model/ModelTestesDPara.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

