# VertisConnect\GrupoDeProdutosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createGrpProdutos**](GrupoDeProdutosApi.md#createGrpProdutos) | **POST** /api/V1.1/grpprodutos | 
[**deleteGrpProdutos**](GrupoDeProdutosApi.md#deleteGrpProdutos) | **DELETE** /api/V1.1/grpprodutos/{id} | 
[**getGrpProdutos**](GrupoDeProdutosApi.md#getGrpProdutos) | **GET** /api/V1.1/grpprodutos/{id} | 
[**getGrpProdutoss**](GrupoDeProdutosApi.md#getGrpProdutoss) | **GET** /api/V1.1/grpprodutos | 
[**updateGrpProdutos**](GrupoDeProdutosApi.md#updateGrpProdutos) | **PUT** /api/V1.1/grpprodutos/{id} | 


# **createGrpProdutos**
> \VertisConnect\Model\ModelGrpProdutos createGrpProdutos($prod_grupo)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\GrupoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$prod_grupo = new \VertisConnect\Model\ModelGrpProdutos(); // \VertisConnect\Model\ModelGrpProdutos | Objeto prod_grupo

try {
    $result = $apiInstance->createGrpProdutos($prod_grupo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GrupoDeProdutosApi->createGrpProdutos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **prod_grupo** | [**\VertisConnect\Model\ModelGrpProdutos**](../Model/ModelGrpProdutos.md)| Objeto prod_grupo |

### Return type

[**\VertisConnect\Model\ModelGrpProdutos**](../Model/ModelGrpProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteGrpProdutos**
> \VertisConnect\Model\ModelGrpProdutos deleteGrpProdutos($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\GrupoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Grupo de Produtos

try {
    $result = $apiInstance->deleteGrpProdutos($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GrupoDeProdutosApi->deleteGrpProdutos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Grupo de Produtos |

### Return type

[**\VertisConnect\Model\ModelGrpProdutos**](../Model/ModelGrpProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGrpProdutos**
> \VertisConnect\Model\ModelGrpProdutos getGrpProdutos($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\GrupoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Grupo de Produtos

try {
    $result = $apiInstance->getGrpProdutos($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GrupoDeProdutosApi->getGrpProdutos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Grupo de Produtos |

### Return type

[**\VertisConnect\Model\ModelGrpProdutos**](../Model/ModelGrpProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGrpProdutoss**
> \VertisConnect\Model\ModelGrpProdutos[] getGrpProdutoss()



Retorna registros do objeto prod_grupo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\GrupoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getGrpProdutoss();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GrupoDeProdutosApi->getGrpProdutoss: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelGrpProdutos[]**](../Model/ModelGrpProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateGrpProdutos**
> \VertisConnect\Model\ModelGrpProdutos updateGrpProdutos($grupo_de_produtos, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\GrupoDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$grupo_de_produtos = new \VertisConnect\Model\ModelGrpProdutos(); // \VertisConnect\Model\ModelGrpProdutos | Objeto prod_grupo
$id = 56; // int | ID do Grupo de Produtos

try {
    $result = $apiInstance->updateGrpProdutos($grupo_de_produtos, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GrupoDeProdutosApi->updateGrpProdutos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grupo_de_produtos** | [**\VertisConnect\Model\ModelGrpProdutos**](../Model/ModelGrpProdutos.md)| Objeto prod_grupo |
 **id** | **int**| ID do Grupo de Produtos |

### Return type

[**\VertisConnect\Model\ModelGrpProdutos**](../Model/ModelGrpProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

