# VertisConnect\ParametrizaoFiscalVeterinrioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoFiscalVet**](ParametrizaoFiscalVeterinrioApi.md#doCreateInfoFiscalVet) | **POST** /api/V1.1/veterinarios/{id}/paramsfiscais | 
[**doDeleteInfoFiscalVet**](ParametrizaoFiscalVeterinrioApi.md#doDeleteInfoFiscalVet) | **DELETE** /api/V1.1/veterinarios/{id}/paramsfiscais | 
[**doGetInfoFiscalVet**](ParametrizaoFiscalVeterinrioApi.md#doGetInfoFiscalVet) | **GET** /api/V1.1/veterinarios/{id}/paramsfiscais | 
[**doUpdateInfoFiscalVet**](ParametrizaoFiscalVeterinrioApi.md#doUpdateInfoFiscalVet) | **PUT** /api/V1.1/veterinarios/{id}/paramsfiscais | 


# **doCreateInfoFiscalVet**
> \VertisConnect\Model\ModelFiscal doCreateInfoFiscalVet($parametrizao_fiscal, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_fiscal = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doCreateInfoFiscalVet($parametrizao_fiscal, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalVeterinrioApi->doCreateInfoFiscalVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_fiscal** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoFiscalVet**
> \VertisConnect\Model\ModelFiscal doDeleteInfoFiscalVet($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoFiscalVet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalVeterinrioApi->doDeleteInfoFiscalVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoFiscalVet**
> \VertisConnect\Model\ModelFiscal doGetInfoFiscalVet($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoFiscalVet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalVeterinrioApi->doGetInfoFiscalVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoFiscalVet**
> \VertisConnect\Model\ModelFiscal doUpdateInfoFiscalVet($parametros_fiscais___veterinrio, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoFiscalVeterinrioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___veterinrio = new \VertisConnect\Model\ModelFiscal(); // \VertisConnect\Model\ModelFiscal | Objeto parc_info_fiscal_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoFiscalVet($parametros_fiscais___veterinrio, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoFiscalVeterinrioApi->doUpdateInfoFiscalVet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___veterinrio** | [**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)| Objeto parc_info_fiscal_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelFiscal**](../Model/ModelFiscal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

