# VertisConnect\ControllerPelagemTControllerPelagemApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV11PelagensGet**](ControllerPelagemTControllerPelagemApi.md#apiV11PelagensGet) | **GET** /api/V1.1/pelagens | 
[**apiV11PelagensIdDelete**](ControllerPelagemTControllerPelagemApi.md#apiV11PelagensIdDelete) | **DELETE** /api/V1.1/pelagens/{id} | 
[**apiV11PelagensIdGet**](ControllerPelagemTControllerPelagemApi.md#apiV11PelagensIdGet) | **GET** /api/V1.1/pelagens/{id} | 
[**apiV11PelagensIdPut**](ControllerPelagemTControllerPelagemApi.md#apiV11PelagensIdPut) | **PUT** /api/V1.1/pelagens/{id} | 
[**apiV11PelagensPost**](ControllerPelagemTControllerPelagemApi.md#apiV11PelagensPost) | **POST** /api/V1.1/pelagens | 


# **apiV11PelagensGet**
> apiV11PelagensGet()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPelagemTControllerPelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11PelagensGet();
} catch (Exception $e) {
    echo 'Exception when calling ControllerPelagemTControllerPelagemApi->apiV11PelagensGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PelagensIdDelete**
> apiV11PelagensIdDelete($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPelagemTControllerPelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PelagensIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerPelagemTControllerPelagemApi->apiV11PelagensIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PelagensIdGet**
> apiV11PelagensIdGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPelagemTControllerPelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PelagensIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerPelagemTControllerPelagemApi->apiV11PelagensIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PelagensIdPut**
> apiV11PelagensIdPut($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPelagemTControllerPelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $apiInstance->apiV11PelagensIdPut($id);
} catch (Exception $e) {
    echo 'Exception when calling ControllerPelagemTControllerPelagemApi->apiV11PelagensIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiV11PelagensPost**
> apiV11PelagensPost()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ControllerPelagemTControllerPelagemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->apiV11PelagensPost();
} catch (Exception $e) {
    echo 'Exception when calling ControllerPelagemTControllerPelagemApi->apiV11PelagensPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

