# VertisConnect\ExamesResultadoPadroApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createResultadoPadrao**](ExamesResultadoPadroApi.md#createResultadoPadrao) | **POST** /api/V1.1/resultados_padrao | 
[**deleteResultadoPadrao**](ExamesResultadoPadroApi.md#deleteResultadoPadrao) | **DELETE** /api/V1.1/resultados_padrao/{id} | 
[**getResultadoPadrao**](ExamesResultadoPadroApi.md#getResultadoPadrao) | **GET** /api/V1.1/resultados_padrao/{id} | 
[**getResultadosPadrao**](ExamesResultadoPadroApi.md#getResultadosPadrao) | **GET** /api/V1.1/resultados_padrao | 
[**updateResultadoPadrao**](ExamesResultadoPadroApi.md#updateResultadoPadrao) | **PUT** /api/V1.1/resultados_padrao/{id} | 


# **createResultadoPadrao**
> \VertisConnect\Model\ModelResultadoPadrao createResultadoPadrao($exames___resultado_padro)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesResultadoPadroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exames___resultado_padro = new \VertisConnect\Model\ModelResultadoPadrao(); // \VertisConnect\Model\ModelResultadoPadrao | Objeto prod_exa_res_padrao_nv

try {
    $result = $apiInstance->createResultadoPadrao($exames___resultado_padro);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesResultadoPadroApi->createResultadoPadrao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exames___resultado_padro** | [**\VertisConnect\Model\ModelResultadoPadrao**](../Model/ModelResultadoPadrao.md)| Objeto prod_exa_res_padrao_nv |

### Return type

[**\VertisConnect\Model\ModelResultadoPadrao**](../Model/ModelResultadoPadrao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteResultadoPadrao**
> \VertisConnect\Model\ModelResultadoPadrao deleteResultadoPadrao($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesResultadoPadroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Resultado Padrao

try {
    $result = $apiInstance->deleteResultadoPadrao($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesResultadoPadroApi->deleteResultadoPadrao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Resultado Padrao |

### Return type

[**\VertisConnect\Model\ModelResultadoPadrao**](../Model/ModelResultadoPadrao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getResultadoPadrao**
> \VertisConnect\Model\ModelResultadoPadrao getResultadoPadrao($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesResultadoPadroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Resultado Padrao

try {
    $result = $apiInstance->getResultadoPadrao($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesResultadoPadroApi->getResultadoPadrao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Resultado Padrao |

### Return type

[**\VertisConnect\Model\ModelResultadoPadrao**](../Model/ModelResultadoPadrao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getResultadosPadrao**
> \VertisConnect\Model\ModelResultadoPadrao[] getResultadosPadrao()



Retorna registros do objeto prod_exa_res_padrao_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesResultadoPadroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getResultadosPadrao();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesResultadoPadroApi->getResultadosPadrao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelResultadoPadrao[]**](../Model/ModelResultadoPadrao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateResultadoPadrao**
> \VertisConnect\Model\ModelResultadoPadrao updateResultadoPadrao($exames___resultado_padro, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ExamesResultadoPadroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$exames___resultado_padro = new \VertisConnect\Model\ModelResultadoPadrao(); // \VertisConnect\Model\ModelResultadoPadrao | Objeto prod_exa_res_padrao_nv
$id = 56; // int | ID do Resultado Padrao

try {
    $result = $apiInstance->updateResultadoPadrao($exames___resultado_padro, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamesResultadoPadroApi->updateResultadoPadrao: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exames___resultado_padro** | [**\VertisConnect\Model\ModelResultadoPadrao**](../Model/ModelResultadoPadrao.md)| Objeto prod_exa_res_padrao_nv |
 **id** | **int**| ID do Resultado Padrao |

### Return type

[**\VertisConnect\Model\ModelResultadoPadrao**](../Model/ModelResultadoPadrao.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

