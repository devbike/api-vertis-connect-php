# VertisConnect\ProdutosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProduto**](ProdutosApi.md#createProduto) | **POST** /api/V1.1/produtos | 
[**deleteProduto**](ProdutosApi.md#deleteProduto) | **DELETE** /api/V1.1/produtos/{id} | 
[**getProduto**](ProdutosApi.md#getProduto) | **GET** /api/V1.1/produtos/{id} | 
[**getProdutos**](ProdutosApi.md#getProdutos) | **GET** /api/V1.1/produtos | 
[**updateProduto**](ProdutosApi.md#updateProduto) | **PUT** /api/V1.1/produtos/{id} | 


# **createProduto**
> \VertisConnect\Model\ModelProdutos createProduto($produtos)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$produtos = new \VertisConnect\Model\ModelProdutos(); // \VertisConnect\Model\ModelProdutos | Objeto produto_nv

try {
    $result = $apiInstance->createProduto($produtos);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosApi->createProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtos** | [**\VertisConnect\Model\ModelProdutos**](../Model/ModelProdutos.md)| Objeto produto_nv |

### Return type

[**\VertisConnect\Model\ModelProdutos**](../Model/ModelProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProduto**
> \VertisConnect\Model\ModelProdutos deleteProduto($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Produtos

try {
    $result = $apiInstance->deleteProduto($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosApi->deleteProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutos**](../Model/ModelProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProduto**
> \VertisConnect\Model\ModelProdutos getProduto($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Produtos

try {
    $result = $apiInstance->getProduto($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosApi->getProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutos**](../Model/ModelProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProdutos**
> \VertisConnect\Model\ModelProdutos[] getProdutos()



Retorna registros do objeto produto_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getProdutos();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosApi->getProdutos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelProdutos[]**](../Model/ModelProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProduto**
> \VertisConnect\Model\ModelProdutos updateProduto($produtos, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$produtos = new \VertisConnect\Model\ModelProdutos(); // \VertisConnect\Model\ModelProdutos | Objeto produto_nv
$id = 56; // int | ID do Produtos

try {
    $result = $apiInstance->updateProduto($produtos, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProdutosApi->updateProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtos** | [**\VertisConnect\Model\ModelProdutos**](../Model/ModelProdutos.md)| Objeto produto_nv |
 **id** | **int**| ID do Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutos**](../Model/ModelProdutos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

