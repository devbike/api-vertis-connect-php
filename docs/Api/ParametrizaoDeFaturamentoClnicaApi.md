# VertisConnect\ParametrizaoDeFaturamentoClnicaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfFatClin**](ParametrizaoDeFaturamentoClnicaApi.md#doCreateInfFatClin) | **POST** /api/V1.1/clinicas/{id}/paramsfat | 
[**doDeleteInfFatClint**](ParametrizaoDeFaturamentoClnicaApi.md#doDeleteInfFatClint) | **DELETE** /api/V1.1/clinicas/{id}/paramsfat | 
[**doGetInfFatClin**](ParametrizaoDeFaturamentoClnicaApi.md#doGetInfFatClin) | **GET** /api/V1.1/clinicas/{id}/paramsfat | 
[**doUpdateInfFatClin**](ParametrizaoDeFaturamentoClnicaApi.md#doUpdateInfFatClin) | **PUT** /api/V1.1/clinicas/{id}/paramsfat | 


# **doCreateInfFatClin**
> \VertisConnect\Model\ModelParcInfFaturamento doCreateInfFatClin($parametrizao_de_faturamento, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametrizao_de_faturamento = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doCreateInfFatClin($parametrizao_de_faturamento, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClnicaApi->doCreateInfFatClin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametrizao_de_faturamento** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfFatClint**
> \VertisConnect\Model\ModelParcInfFaturamento doDeleteInfFatClint($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfFatClint($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClnicaApi->doDeleteInfFatClint: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfFatClin**
> \VertisConnect\Model\ModelParcInfFaturamento doGetInfFatClin($id)



Retorna configurações de faturamento para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfFatClin($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClnicaApi->doGetInfFatClin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfFatClin**
> \VertisConnect\Model\ModelParcInfFaturamento doUpdateInfFatClin($parametros_fiscais___clnica, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrizaoDeFaturamentoClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___clnica = new \VertisConnect\Model\ModelParcInfFaturamento(); // \VertisConnect\Model\ModelParcInfFaturamento | Objeto parc_inf_faturamento_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfFatClin($parametros_fiscais___clnica, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrizaoDeFaturamentoClnicaApi->doUpdateInfFatClin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___clnica** | [**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)| Objeto parc_inf_faturamento_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfFaturamento**](../Model/ModelParcInfFaturamento.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

