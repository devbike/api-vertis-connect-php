# VertisConnect\ParametrosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createparametro**](ParametrosApi.md#createparametro) | **POST** /api/V1.1/parametros | 
[**deleteparametro**](ParametrosApi.md#deleteparametro) | **DELETE** /api/V1.1/parametros/{id} | 
[**getparametro**](ParametrosApi.md#getparametro) | **GET** /api/V1.1/parametros/{id} | 
[**getparametros**](ParametrosApi.md#getparametros) | **GET** /api/V1.1/parametros | 
[**updateparametro**](ParametrosApi.md#updateparametro) | **PUT** /api/V1.1/parametros/{id} | 


# **createparametro**
> \VertisConnect\Model\ModelParametros createparametro($parametros)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros = new \VertisConnect\Model\ModelParametros(); // \VertisConnect\Model\ModelParametros | Objeto unid_oper_param_sys

try {
    $result = $apiInstance->createparametro($parametros);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrosApi->createparametro: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros** | [**\VertisConnect\Model\ModelParametros**](../Model/ModelParametros.md)| Objeto unid_oper_param_sys |

### Return type

[**\VertisConnect\Model\ModelParametros**](../Model/ModelParametros.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteparametro**
> \VertisConnect\Model\ModelParametros deleteparametro($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Parametros

try {
    $result = $apiInstance->deleteparametro($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrosApi->deleteparametro: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Parametros |

### Return type

[**\VertisConnect\Model\ModelParametros**](../Model/ModelParametros.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getparametro**
> \VertisConnect\Model\ModelParametros getparametro($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Parametros

try {
    $result = $apiInstance->getparametro($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrosApi->getparametro: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Parametros |

### Return type

[**\VertisConnect\Model\ModelParametros**](../Model/ModelParametros.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getparametros**
> \VertisConnect\Model\ModelParametros[] getparametros()



Retorna registros do objeto unid_oper_param_sys

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getparametros();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrosApi->getparametros: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelParametros[]**](../Model/ModelParametros.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateparametro**
> \VertisConnect\Model\ModelParametros updateparametro($parametros, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\ParametrosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros = new \VertisConnect\Model\ModelParametros(); // \VertisConnect\Model\ModelParametros | Objeto unid_oper_param_sys
$id = 56; // int | ID do Parametros

try {
    $result = $apiInstance->updateparametro($parametros, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ParametrosApi->updateparametro: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros** | [**\VertisConnect\Model\ModelParametros**](../Model/ModelParametros.md)| Objeto unid_oper_param_sys |
 **id** | **int**| ID do Parametros |

### Return type

[**\VertisConnect\Model\ModelParametros**](../Model/ModelParametros.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

