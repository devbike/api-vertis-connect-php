# VertisConnect\TempoDeExecuoPorParceiroApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createParcTempExec**](TempoDeExecuoPorParceiroApi.md#createParcTempExec) | **POST** /api/V1.1/parc-temp-exec | 
[**deleteParcTempExec**](TempoDeExecuoPorParceiroApi.md#deleteParcTempExec) | **DELETE** /api/V1.1/parc-temp-exec/{id} | 
[**getParcTempExec**](TempoDeExecuoPorParceiroApi.md#getParcTempExec) | **GET** /api/V1.1/parc-temp-exec/{id} | 
[**getParcTempExecs**](TempoDeExecuoPorParceiroApi.md#getParcTempExecs) | **GET** /api/V1.1/parc-temp-exec | 
[**updateParcTempExec**](TempoDeExecuoPorParceiroApi.md#updateParcTempExec) | **PUT** /api/V1.1/parc-temp-exec/{id} | 


# **createParcTempExec**
> \VertisConnect\Model\ModelParcTempExec createParcTempExec($descricao)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TempoDeExecuoPorParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$descricao = new \VertisConnect\Model\ModelParcTempExec(); // \VertisConnect\Model\ModelParcTempExec | Objeto #tabela#

try {
    $result = $apiInstance->createParcTempExec($descricao);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TempoDeExecuoPorParceiroApi->createParcTempExec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | [**\VertisConnect\Model\ModelParcTempExec**](../Model/ModelParcTempExec.md)| Objeto #tabela# |

### Return type

[**\VertisConnect\Model\ModelParcTempExec**](../Model/ModelParcTempExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteParcTempExec**
> \VertisConnect\Model\ModelParcTempExec deleteParcTempExec($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TempoDeExecuoPorParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->deleteParcTempExec($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TempoDeExecuoPorParceiroApi->deleteParcTempExec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcTempExec**](../Model/ModelParcTempExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParcTempExec**
> \VertisConnect\Model\ModelParcTempExec getParcTempExec($id)



Retorna informações de um registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TempoDeExecuoPorParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do regoistro

try {
    $result = $apiInstance->getParcTempExec($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TempoDeExecuoPorParceiroApi->getParcTempExec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do regoistro |

### Return type

[**\VertisConnect\Model\ModelParcTempExec**](../Model/ModelParcTempExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getParcTempExecs**
> \VertisConnect\Model\ModelParcTempExec[] getParcTempExecs()



Retorna todos os registros.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TempoDeExecuoPorParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getParcTempExecs();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TempoDeExecuoPorParceiroApi->getParcTempExecs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelParcTempExec[]**](../Model/ModelParcTempExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateParcTempExec**
> \VertisConnect\Model\ModelParcTempExec updateParcTempExec($tempo_de_execuo_por_parceiro, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\TempoDeExecuoPorParceiroApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$tempo_de_execuo_por_parceiro = new \VertisConnect\Model\ModelParcTempExec(); // \VertisConnect\Model\ModelParcTempExec | Objeto parc_tmp_exec
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateParcTempExec($tempo_de_execuo_por_parceiro, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TempoDeExecuoPorParceiroApi->updateParcTempExec: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tempo_de_execuo_por_parceiro** | [**\VertisConnect\Model\ModelParcTempExec**](../Model/ModelParcTempExec.md)| Objeto parc_tmp_exec |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcTempExec**](../Model/ModelParcTempExec.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

