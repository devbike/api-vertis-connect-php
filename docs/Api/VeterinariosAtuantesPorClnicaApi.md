# VertisConnect\VeterinariosAtuantesPorClnicaApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createClinicaVeterinario**](VeterinariosAtuantesPorClnicaApi.md#createClinicaVeterinario) | **POST** /api/V1.1/clinicas/{c}/veterinarios | 
[**deleteClinicaVeterinario**](VeterinariosAtuantesPorClnicaApi.md#deleteClinicaVeterinario) | **DELETE** /api/V1.1/clinicas/{c}/veterinarios/{id} | 
[**getClinicaVeterinario**](VeterinariosAtuantesPorClnicaApi.md#getClinicaVeterinario) | **GET** /api/V1.1/clinicas/{c}/veterinarios/{id} | 
[**getClinicaVeterinarios**](VeterinariosAtuantesPorClnicaApi.md#getClinicaVeterinarios) | **GET** /api/V1.1/clinicas/{c}/veterinarios | 
[**updateClinicaVeterinario**](VeterinariosAtuantesPorClnicaApi.md#updateClinicaVeterinario) | **PUT** /api/V1.1/clinicas/{c}/veterinarios/{id} | 


# **createClinicaVeterinario**
> \VertisConnect\Model\ModelClinicaVeterinario createClinicaVeterinario($veterinarios_atuantes_por_clnica, $c)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinariosAtuantesPorClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$veterinarios_atuantes_por_clnica = new \VertisConnect\Model\ModelClinicaVeterinario(); // \VertisConnect\Model\ModelClinicaVeterinario | Objeto parc_vet_clinica
$c = 56; // int | 

try {
    $result = $apiInstance->createClinicaVeterinario($veterinarios_atuantes_por_clnica, $c);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinariosAtuantesPorClnicaApi->createClinicaVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **veterinarios_atuantes_por_clnica** | [**\VertisConnect\Model\ModelClinicaVeterinario**](../Model/ModelClinicaVeterinario.md)| Objeto parc_vet_clinica |
 **c** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelClinicaVeterinario**](../Model/ModelClinicaVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteClinicaVeterinario**
> \VertisConnect\Model\ModelClinicaVeterinario deleteClinicaVeterinario($id, $c)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinariosAtuantesPorClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Clinica Veterinarios
$c = 56; // int | 

try {
    $result = $apiInstance->deleteClinicaVeterinario($id, $c);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinariosAtuantesPorClnicaApi->deleteClinicaVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Clinica Veterinarios |
 **c** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelClinicaVeterinario**](../Model/ModelClinicaVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinicaVeterinario**
> \VertisConnect\Model\ModelClinicaVeterinario getClinicaVeterinario($id, $c)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinariosAtuantesPorClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Clinica Veterinarios
$c = 56; // int | 

try {
    $result = $apiInstance->getClinicaVeterinario($id, $c);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinariosAtuantesPorClnicaApi->getClinicaVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Clinica Veterinarios |
 **c** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelClinicaVeterinario**](../Model/ModelClinicaVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getClinicaVeterinarios**
> \VertisConnect\Model\ModelClinicaVeterinario[] getClinicaVeterinarios($c)



Retorna registros do objeto parc_vet_clinica

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinariosAtuantesPorClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$c = 56; // int | 

try {
    $result = $apiInstance->getClinicaVeterinarios($c);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinariosAtuantesPorClnicaApi->getClinicaVeterinarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **c** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelClinicaVeterinario[]**](../Model/ModelClinicaVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateClinicaVeterinario**
> \VertisConnect\Model\ModelClinicaVeterinario updateClinicaVeterinario($id, $veterinarios_atuantes_por_clnica, $c)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\VeterinariosAtuantesPorClnicaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Clinica Veterinarios
$veterinarios_atuantes_por_clnica = new \VertisConnect\Model\ModelClinicaVeterinario(); // \VertisConnect\Model\ModelClinicaVeterinario | Objeto parc_vet_clinica
$c = 56; // int | 

try {
    $result = $apiInstance->updateClinicaVeterinario($id, $veterinarios_atuantes_por_clnica, $c);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VeterinariosAtuantesPorClnicaApi->updateClinicaVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Clinica Veterinarios |
 **veterinarios_atuantes_por_clnica** | [**\VertisConnect\Model\ModelClinicaVeterinario**](../Model/ModelClinicaVeterinario.md)| Objeto parc_vet_clinica |
 **c** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelClinicaVeterinario**](../Model/ModelClinicaVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

