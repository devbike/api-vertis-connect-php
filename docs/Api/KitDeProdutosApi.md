# VertisConnect\KitDeProdutosApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createKitProduto**](KitDeProdutosApi.md#createKitProduto) | **POST** /api/V1.1/kitprodutos | 
[**deleteKitItem**](KitDeProdutosApi.md#deleteKitItem) | **DELETE** /api/V1.1/kitprodutos/{id}/item/{item} | 
[**deleteKitProduto**](KitDeProdutosApi.md#deleteKitProduto) | **DELETE** /api/V1.1/kitprodutos/{id} | 
[**getKitItem**](KitDeProdutosApi.md#getKitItem) | **GET** /api/V1.1/kitprodutos/{id}/item/{item} | 
[**getProdutosKit**](KitDeProdutosApi.md#getProdutosKit) | **GET** /api/V1.1/kitprodutos/{id} | 
[**updateKitProduto**](KitDeProdutosApi.md#updateKitProduto) | **PUT** /api/V1.1/kitprodutos/{id}/item/{item} | 


# **createKitProduto**
> \VertisConnect\Model\ModelProdutoKit createKitProduto($kit_de_produtos)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$kit_de_produtos = new \VertisConnect\Model\ModelProdutoKit(); // \VertisConnect\Model\ModelProdutoKit | Objeto prod_kit_nv

try {
    $result = $apiInstance->createKitProduto($kit_de_produtos);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitDeProdutosApi->createKitProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kit_de_produtos** | [**\VertisConnect\Model\ModelProdutoKit**](../Model/ModelProdutoKit.md)| Objeto prod_kit_nv |

### Return type

[**\VertisConnect\Model\ModelProdutoKit**](../Model/ModelProdutoKit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteKitItem**
> \VertisConnect\Model\ModelProdutoKit deleteKitItem($item, $id)



Apaga o item do kit determinado no parâmetro Item

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$item = 56; // int | ID do Item no Kit de Produtos
$id = 56; // int | ID do Kit de Produtos

try {
    $result = $apiInstance->deleteKitItem($item, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitDeProdutosApi->deleteKitItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item** | **int**| ID do Item no Kit de Produtos |
 **id** | **int**| ID do Kit de Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutoKit**](../Model/ModelProdutoKit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteKitProduto**
> \VertisConnect\Model\ModelProdutoKit deleteKitProduto($id)



Apaga o kit completo determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Kit de Produtos

try {
    $result = $apiInstance->deleteKitProduto($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitDeProdutosApi->deleteKitProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Kit de Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutoKit**](../Model/ModelProdutoKit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKitItem**
> \VertisConnect\Model\ModelProdutoKit getKitItem($item, $id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$item = 56; // int | 
$id = 56; // int | ID do Kit de Produtos

try {
    $result = $apiInstance->getKitItem($item, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitDeProdutosApi->getKitItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item** | **int**|  |
 **id** | **int**| ID do Kit de Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutoKit**](../Model/ModelProdutoKit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProdutosKit**
> \VertisConnect\Model\ModelProdutoKit[] getProdutosKit($id)



Retorna registros do objeto prod_kit_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | 

try {
    $result = $apiInstance->getProdutosKit($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitDeProdutosApi->getProdutosKit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\VertisConnect\Model\ModelProdutoKit[]**](../Model/ModelProdutoKit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateKitProduto**
> \VertisConnect\Model\ModelProdutoKit updateKitProduto($item, $kit_de_produtos, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitDeProdutosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$item = 56; // int | 
$kit_de_produtos = new \VertisConnect\Model\ModelProdutoKit(); // \VertisConnect\Model\ModelProdutoKit | Objeto prod_kit_nv
$id = 56; // int | ID do Kit de Produtos

try {
    $result = $apiInstance->updateKitProduto($item, $kit_de_produtos, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitDeProdutosApi->updateKitProduto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item** | **int**|  |
 **kit_de_produtos** | [**\VertisConnect\Model\ModelProdutoKit**](../Model/ModelProdutoKit.md)| Objeto prod_kit_nv |
 **id** | **int**| ID do Kit de Produtos |

### Return type

[**\VertisConnect\Model\ModelProdutoKit**](../Model/ModelProdutoKit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

