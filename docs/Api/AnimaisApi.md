# VertisConnect\AnimaisApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAnimal**](AnimaisApi.md#createAnimal) | **POST** /api/V1.1/animais | 
[**deleteAnimal**](AnimaisApi.md#deleteAnimal) | **DELETE** /api/V1.1/animais/{id} | 
[**getAnimal**](AnimaisApi.md#getAnimal) | **GET** /api/V1.1/animais/{id} | 
[**getAnimals**](AnimaisApi.md#getAnimals) | **GET** /api/V1.1/animais | 
[**updateAnimal**](AnimaisApi.md#updateAnimal) | **PUT** /api/V1.1/animais/{id} | 


# **createAnimal**
> \VertisConnect\Model\ModelAnimal createAnimal($animais)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$animais = new \VertisConnect\Model\ModelAnimal(); // \VertisConnect\Model\ModelAnimal | Objeto animal_nv

try {
    $result = $apiInstance->createAnimal($animais);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisApi->createAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **animais** | [**\VertisConnect\Model\ModelAnimal**](../Model/ModelAnimal.md)| Objeto animal_nv |

### Return type

[**\VertisConnect\Model\ModelAnimal**](../Model/ModelAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAnimal**
> \VertisConnect\Model\ModelAnimal deleteAnimal($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animais

try {
    $result = $apiInstance->deleteAnimal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisApi->deleteAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animais |

### Return type

[**\VertisConnect\Model\ModelAnimal**](../Model/ModelAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimal**
> \VertisConnect\Model\ModelAnimal getAnimal($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animais

try {
    $result = $apiInstance->getAnimal($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisApi->getAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animais |

### Return type

[**\VertisConnect\Model\ModelAnimal**](../Model/ModelAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimals**
> \VertisConnect\Model\ModelAnimal[] getAnimals()



Retorna registros do objeto animal_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAnimals();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisApi->getAnimals: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelAnimal[]**](../Model/ModelAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateAnimal**
> \VertisConnect\Model\ModelAnimal updateAnimal($animais, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimaisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$animais = new \VertisConnect\Model\ModelAnimal(); // \VertisConnect\Model\ModelAnimal | Objeto animal_nv
$id = 56; // int | ID do Animais

try {
    $result = $apiInstance->updateAnimal($animais, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimaisApi->updateAnimal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **animais** | [**\VertisConnect\Model\ModelAnimal**](../Model/ModelAnimal.md)| Objeto animal_nv |
 **id** | **int**| ID do Animais |

### Return type

[**\VertisConnect\Model\ModelAnimal**](../Model/ModelAnimal.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

