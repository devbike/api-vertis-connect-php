# VertisConnect\AnimalXVeterinarioApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAnimalXVeterinario**](AnimalXVeterinarioApi.md#createAnimalXVeterinario) | **POST** /api/V1.1/animais_x_veterinarios | 
[**getAnimaisXVeterinarios**](AnimalXVeterinarioApi.md#getAnimaisXVeterinarios) | **GET** /api/V1.1/animais_x_veterinarios | 
[**getAnimalXVeterinario**](AnimalXVeterinarioApi.md#getAnimalXVeterinario) | **GET** /api/V1.1/animais_x_veterinarios/{id} | 


# **createAnimalXVeterinario**
> \VertisConnect\Model\ModelAnimalXVeterinario createAnimalXVeterinario($anim_x_veterinario_nv)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimalXVeterinarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$anim_x_veterinario_nv = new \VertisConnect\Model\ModelAnimalXVeterinario(); // \VertisConnect\Model\ModelAnimalXVeterinario | Objeto anim_x_veterinario_nv

try {
    $result = $apiInstance->createAnimalXVeterinario($anim_x_veterinario_nv);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimalXVeterinarioApi->createAnimalXVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anim_x_veterinario_nv** | [**\VertisConnect\Model\ModelAnimalXVeterinario**](../Model/ModelAnimalXVeterinario.md)| Objeto anim_x_veterinario_nv |

### Return type

[**\VertisConnect\Model\ModelAnimalXVeterinario**](../Model/ModelAnimalXVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimaisXVeterinarios**
> \VertisConnect\Model\ModelAnimalXVeterinario[] getAnimaisXVeterinarios()



Retorna registros do objeto anim_x_veterinario_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimalXVeterinarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getAnimaisXVeterinarios();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimalXVeterinarioApi->getAnimaisXVeterinarios: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelAnimalXVeterinario[]**](../Model/ModelAnimalXVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAnimalXVeterinario**
> \VertisConnect\Model\ModelAnimalXVeterinario getAnimalXVeterinario($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\AnimalXVeterinarioApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Animal_x_Veterinario

try {
    $result = $apiInstance->getAnimalXVeterinario($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnimalXVeterinarioApi->getAnimalXVeterinario: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Animal_x_Veterinario |

### Return type

[**\VertisConnect\Model\ModelAnimalXVeterinario**](../Model/ModelAnimalXVeterinario.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

