# VertisConnect\MotoboyApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMotoboy**](MotoboyApi.md#createMotoboy) | **POST** /api/V1.1/motoboys | 
[**deleteMotoboy**](MotoboyApi.md#deleteMotoboy) | **DELETE** /api/V1.1/motoboys/{id} | 
[**getMotoboy**](MotoboyApi.md#getMotoboy) | **GET** /api/V1.1/motoboys/{id} | 
[**getMotoboys**](MotoboyApi.md#getMotoboys) | **GET** /api/V1.1/motoboys | 
[**updateMotoboy**](MotoboyApi.md#updateMotoboy) | **PUT** /api/V1.1/motoboys/{id} | 


# **createMotoboy**
> \VertisConnect\Model\ModelMotoboy createMotoboy($motoboy)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$motoboy = new \VertisConnect\Model\ModelMotoboy(); // \VertisConnect\Model\ModelMotoboy | Objeto parc_motoboy

try {
    $result = $apiInstance->createMotoboy($motoboy);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->createMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **motoboy** | [**\VertisConnect\Model\ModelMotoboy**](../Model/ModelMotoboy.md)| Objeto parc_motoboy |

### Return type

[**\VertisConnect\Model\ModelMotoboy**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteMotoboy**
> \VertisConnect\Model\ModelMotoboy deleteMotoboy($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do Registro

try {
    $result = $apiInstance->deleteMotoboy($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->deleteMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do Registro |

### Return type

[**\VertisConnect\Model\ModelMotoboy**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMotoboy**
> \VertisConnect\Model\ModelMotoboy[] getMotoboy($id)



Retorna o motoboy selecionado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do regoistro

try {
    $result = $apiInstance->getMotoboy($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->getMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do regoistro |

### Return type

[**\VertisConnect\Model\ModelMotoboy[]**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMotoboys**
> \VertisConnect\Model\ModelMotoboy[] getMotoboys()



Retorna todos os motoboys cadastrados.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getMotoboys();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->getMotoboys: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelMotoboy[]**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateMotoboy**
> \VertisConnect\Model\ModelMotoboy updateMotoboy($motoboy, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\MotoboyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$motoboy = new \VertisConnect\Model\MemoryStream(); // \VertisConnect\Model\MemoryStream | Objeto parc_motoboy
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateMotoboy($motoboy, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MotoboyApi->updateMotoboy: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **motoboy** | [**\VertisConnect\Model\MemoryStream**](../Model/MemoryStream.md)| Objeto parc_motoboy |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelMotoboy**](../Model/ModelMotoboy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

