# VertisConnect\RaasApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRaca**](RaasApi.md#createRaca) | **POST** /api/V1.1/raca | 
[**deleteRaca**](RaasApi.md#deleteRaca) | **DELETE** /api/V1.1/raca/{id} | 
[**getRaca**](RaasApi.md#getRaca) | **GET** /api/V1.1/raca/{id} | 
[**getRacas**](RaasApi.md#getRacas) | **GET** /api/V1.1/raca | 
[**updateRaca**](RaasApi.md#updateRaca) | **PUT** /api/V1.1/raca/{id} | 


# **createRaca**
> \VertisConnect\Model\ModelRaca createRaca($raas)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RaasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$raas = new \VertisConnect\Model\ModelRaca(); // \VertisConnect\Model\ModelRaca | Objeto raca

try {
    $result = $apiInstance->createRaca($raas);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RaasApi->createRaca: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **raas** | [**\VertisConnect\Model\ModelRaca**](../Model/ModelRaca.md)| Objeto raca |

### Return type

[**\VertisConnect\Model\ModelRaca**](../Model/ModelRaca.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteRaca**
> \VertisConnect\Model\ModelRaca deleteRaca($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RaasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Raças

try {
    $result = $apiInstance->deleteRaca($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RaasApi->deleteRaca: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Raças |

### Return type

[**\VertisConnect\Model\ModelRaca**](../Model/ModelRaca.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRaca**
> \VertisConnect\Model\ModelRaca getRaca($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RaasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Raças

try {
    $result = $apiInstance->getRaca($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RaasApi->getRaca: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Raças |

### Return type

[**\VertisConnect\Model\ModelRaca**](../Model/ModelRaca.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRacas**
> \VertisConnect\Model\ModelRaca[] getRacas($especie)



Retorna registros do objeto raca

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RaasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$especie = 56; // int | ID da Especie da qual deseja listar as raças

try {
    $result = $apiInstance->getRacas($especie);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RaasApi->getRacas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **especie** | **int**| ID da Especie da qual deseja listar as raças |

### Return type

[**\VertisConnect\Model\ModelRaca[]**](../Model/ModelRaca.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRaca**
> \VertisConnect\Model\ModelRaca updateRaca($raas, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\RaasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$raas = new \VertisConnect\Model\ModelRaca(); // \VertisConnect\Model\ModelRaca | Objeto raca
$id = 56; // int | ID do Raças

try {
    $result = $apiInstance->updateRaca($raas, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RaasApi->updateRaca: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **raas** | [**\VertisConnect\Model\ModelRaca**](../Model/ModelRaca.md)| Objeto raca |
 **id** | **int**| ID do Raças |

### Return type

[**\VertisConnect\Model\ModelRaca**](../Model/ModelRaca.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

