# VertisConnect\InformaesITFFornecedorApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**doCreateInfoITFVertisForn**](InformaesITFFornecedorApi.md#doCreateInfoITFVertisForn) | **POST** /api/V1.1/fornecedores/{id}/itf-vertis | 
[**doDeleteInfoITFVertisFor**](InformaesITFFornecedorApi.md#doDeleteInfoITFVertisFor) | **DELETE** /api/V1.1/fornecedores/{id}/itf-vertis | 
[**doGetInfoITFVertisForn**](InformaesITFFornecedorApi.md#doGetInfoITFVertisForn) | **GET** /api/V1.1/fornecedores/{id}/itf-vertis | 
[**doUpdateInfoITFVertisForn**](InformaesITFFornecedorApi.md#doUpdateInfoITFVertisForn) | **PUT** /api/V1.1/fornecedores/{id}/itf-vertis | 


# **doCreateInfoITFVertisForn**
> \VertisConnect\Model\ModelParcInfITFVertis doCreateInfoITFVertisForn($informaes_itf, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$informaes_itf = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID do Fornecedor

try {
    $result = $apiInstance->doCreateInfoITFVertisForn($informaes_itf, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFFornecedorApi->doCreateInfoITFVertisForn: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **informaes_itf** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID do Fornecedor |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doDeleteInfoITFVertisFor**
> \VertisConnect\Model\ModelParcInfITFVertis doDeleteInfoITFVertisFor($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doDeleteInfoITFVertisFor($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFFornecedorApi->doDeleteInfoITFVertisFor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doGetInfoITFVertisForn**
> \VertisConnect\Model\ModelParcInfITFVertis doGetInfoITFVertisForn($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doGetInfoITFVertisForn($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFFornecedorApi->doGetInfoITFVertisForn: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **doUpdateInfoITFVertisForn**
> \VertisConnect\Model\ModelParcInfITFVertis doUpdateInfoITFVertisForn($parametros_fiscais___fornecedor, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFFornecedorApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___fornecedor = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->doUpdateInfoITFVertisForn($parametros_fiscais___fornecedor, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFFornecedorApi->doUpdateInfoITFVertisForn: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___fornecedor** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

