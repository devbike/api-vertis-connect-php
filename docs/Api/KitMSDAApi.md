# VertisConnect\KitMSDAApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createKitMsda**](KitMSDAApi.md#createKitMsda) | **POST** /api/V1.1/kitmsda | 
[**deleteKitMsda**](KitMSDAApi.md#deleteKitMsda) | **DELETE** /api/V1.1/kitmsda/{id} | 
[**getKitMsda**](KitMSDAApi.md#getKitMsda) | **GET** /api/V1.1/kitmsda/{id} | 
[**getKitMsdas**](KitMSDAApi.md#getKitMsdas) | **GET** /api/V1.1/kitmsda | 
[**updateKitMsda**](KitMSDAApi.md#updateKitMsda) | **PUT** /api/V1.1/kitmsda/{id} | 


# **createKitMsda**
> \VertisConnect\Model\ModelKitMsda createKitMsda($kit_msda)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitMSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$kit_msda = new \VertisConnect\Model\ModelKitMsda(); // \VertisConnect\Model\ModelKitMsda | Objeto prod_exa_msda_kit_nv

try {
    $result = $apiInstance->createKitMsda($kit_msda);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitMSDAApi->createKitMsda: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kit_msda** | [**\VertisConnect\Model\ModelKitMsda**](../Model/ModelKitMsda.md)| Objeto prod_exa_msda_kit_nv |

### Return type

[**\VertisConnect\Model\ModelKitMsda**](../Model/ModelKitMsda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteKitMsda**
> \VertisConnect\Model\ModelKitMsda deleteKitMsda($id)



Apaga o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitMSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Kit MSDA

try {
    $result = $apiInstance->deleteKitMsda($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitMSDAApi->deleteKitMsda: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Kit MSDA |

### Return type

[**\VertisConnect\Model\ModelKitMsda**](../Model/ModelKitMsda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKitMsda**
> \VertisConnect\Model\ModelKitMsda getKitMsda($id)



Retorna informações de um único registro, determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitMSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID do Kit MSDA

try {
    $result = $apiInstance->getKitMsda($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitMSDAApi->getKitMsda: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID do Kit MSDA |

### Return type

[**\VertisConnect\Model\ModelKitMsda**](../Model/ModelKitMsda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getKitMsdas**
> \VertisConnect\Model\ModelKitMsda[] getKitMsdas()



Retorna registros do objeto prod_exa_msda_kit_nv

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitMSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getKitMsdas();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitMSDAApi->getKitMsdas: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\VertisConnect\Model\ModelKitMsda[]**](../Model/ModelKitMsda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateKitMsda**
> \VertisConnect\Model\ModelKitMsda updateKitMsda($kit_msda, $id)



Atualiza o registro determinado no parâmetro ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\KitMSDAApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$kit_msda = new \VertisConnect\Model\ModelKitMsda(); // \VertisConnect\Model\ModelKitMsda | Objeto prod_exa_msda_kit_nv
$id = 56; // int | ID do Kit MSDA

try {
    $result = $apiInstance->updateKitMsda($kit_msda, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KitMSDAApi->updateKitMsda: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kit_msda** | [**\VertisConnect\Model\ModelKitMsda**](../Model/ModelKitMsda.md)| Objeto prod_exa_msda_kit_nv |
 **id** | **int**| ID do Kit MSDA |

### Return type

[**\VertisConnect\Model\ModelKitMsda**](../Model/ModelKitMsda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

