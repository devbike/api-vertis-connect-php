# VertisConnect\InformaesITFPropriedadeApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createInfITFVertis**](InformaesITFPropriedadeApi.md#createInfITFVertis) | **POST** /api/V1.1/propriedades/{id}/itf-vertis | 
[**deleteInfITFVertis**](InformaesITFPropriedadeApi.md#deleteInfITFVertis) | **DELETE** /api/V1.1/propriedades/{id}/itf-vertis | 
[**getInfITFVertis**](InformaesITFPropriedadeApi.md#getInfITFVertis) | **GET** /api/V1.1/propriedades/{id}/itf-vertis | 
[**updateInfITFVertis**](InformaesITFPropriedadeApi.md#updateInfITFVertis) | **PUT** /api/V1.1/propriedades/{id}/itf-vertis | 


# **createInfITFVertis**
> \VertisConnect\Model\ModelParcInfITFVertis createInfITFVertis($informaes_itf, $id)



Insere um novo registro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$informaes_itf = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave da Properiedade

try {
    $result = $apiInstance->createInfITFVertis($informaes_itf, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFPropriedadeApi->createInfITFVertis: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **informaes_itf** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave da Properiedade |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteInfITFVertis**
> \VertisConnect\Model\ModelParcInfITFVertis deleteInfITFVertis($id)



Apaga o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->deleteInfITFVertis($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFPropriedadeApi->deleteInfITFVertis: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getInfITFVertis**
> \VertisConnect\Model\ModelParcInfITFVertis getInfITFVertis($id)



Retorna configurações fiscais para o registro determinado no parâmetro IG.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->getInfITFVertis($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFPropriedadeApi->getInfITFVertis: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateInfITFVertis**
> \VertisConnect\Model\ModelParcInfITFVertis updateInfITFVertis($parametros_fiscais___propriedade, $id)



Atualiza o registro determinado no parâmetro ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new VertisConnect\Api\InformaesITFPropriedadeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$parametros_fiscais___propriedade = new \VertisConnect\Model\ModelParcInfITFVertis(); // \VertisConnect\Model\ModelParcInfITFVertis | Objeto parc_info_itfvertis_nv
$id = 56; // int | ID chave do registro

try {
    $result = $apiInstance->updateInfITFVertis($parametros_fiscais___propriedade, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformaesITFPropriedadeApi->updateInfITFVertis: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametros_fiscais___propriedade** | [**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)| Objeto parc_info_itfvertis_nv |
 **id** | **int**| ID chave do registro |

### Return type

[**\VertisConnect\Model\ModelParcInfITFVertis**](../Model/ModelParcInfITFVertis.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

