<?php
/**
 * ModelFatDoctos
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisConnect\Model;

use \ArrayAccess;
use \VertisConnect\ObjectSerializer;

/**
 * ModelFatDoctos Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelFatDoctos implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelFatDoctos';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id_fat_doctos' => 'int',
        'fkid_faturamento' => 'int',
        'ind_tipo_doc' => 'string',
        'link_docto' => 'string',
        'dsc_docto' => 'string',
        'docto_b64' => 'string',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id_fat_doctos' => 'int64',
        'fkid_faturamento' => 'int64',
        'ind_tipo_doc' => null,
        'link_docto' => null,
        'dsc_docto' => null,
        'docto_b64' => null,
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id_fat_doctos' => 'id_fat_doctos',
        'fkid_faturamento' => 'fkid_faturamento',
        'ind_tipo_doc' => 'ind_tipo_doc',
        'link_docto' => 'link_docto',
        'dsc_docto' => 'dsc_docto',
        'docto_b64' => 'docto_B64',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id_fat_doctos' => 'setIdFatDoctos',
        'fkid_faturamento' => 'setFkidFaturamento',
        'ind_tipo_doc' => 'setIndTipoDoc',
        'link_docto' => 'setLinkDocto',
        'dsc_docto' => 'setDscDocto',
        'docto_b64' => 'setDoctoB64',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id_fat_doctos' => 'getIdFatDoctos',
        'fkid_faturamento' => 'getFkidFaturamento',
        'ind_tipo_doc' => 'getIndTipoDoc',
        'link_docto' => 'getLinkDocto',
        'dsc_docto' => 'getDscDocto',
        'docto_b64' => 'getDoctoB64',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id_fat_doctos'] = isset($data['id_fat_doctos']) ? $data['id_fat_doctos'] : null;
        $this->container['fkid_faturamento'] = isset($data['fkid_faturamento']) ? $data['fkid_faturamento'] : null;
        $this->container['ind_tipo_doc'] = isset($data['ind_tipo_doc']) ? $data['ind_tipo_doc'] : null;
        $this->container['link_docto'] = isset($data['link_docto']) ? $data['link_docto'] : null;
        $this->container['dsc_docto'] = isset($data['dsc_docto']) ? $data['dsc_docto'] : null;
        $this->container['docto_b64'] = isset($data['docto_b64']) ? $data['docto_b64'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['id_fat_doctos'] === null) {
            $invalidProperties[] = "'id_fat_doctos' can't be null";
        }
        if ($this->container['fkid_faturamento'] === null) {
            $invalidProperties[] = "'fkid_faturamento' can't be null";
        }
        if ($this->container['ind_tipo_doc'] === null) {
            $invalidProperties[] = "'ind_tipo_doc' can't be null";
        }
        if ($this->container['link_docto'] === null) {
            $invalidProperties[] = "'link_docto' can't be null";
        }
        if ($this->container['dsc_docto'] === null) {
            $invalidProperties[] = "'dsc_docto' can't be null";
        }
        if ($this->container['docto_b64'] === null) {
            $invalidProperties[] = "'docto_b64' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id_fat_doctos
     *
     * @return int
     */
    public function getIdFatDoctos()
    {
        return $this->container['id_fat_doctos'];
    }

    /**
     * Sets id_fat_doctos
     *
     * @param int $id_fat_doctos #field_definition#
     *
     * @return $this
     */
    public function setIdFatDoctos($id_fat_doctos)
    {
        $this->container['id_fat_doctos'] = $id_fat_doctos;

        return $this;
    }

    /**
     * Gets fkid_faturamento
     *
     * @return int
     */
    public function getFkidFaturamento()
    {
        return $this->container['fkid_faturamento'];
    }

    /**
     * Sets fkid_faturamento
     *
     * @param int $fkid_faturamento #field_definition#
     *
     * @return $this
     */
    public function setFkidFaturamento($fkid_faturamento)
    {
        $this->container['fkid_faturamento'] = $fkid_faturamento;

        return $this;
    }

    /**
     * Gets ind_tipo_doc
     *
     * @return string
     */
    public function getIndTipoDoc()
    {
        return $this->container['ind_tipo_doc'];
    }

    /**
     * Sets ind_tipo_doc
     *
     * @param string $ind_tipo_doc #field_definition#
     *
     * @return $this
     */
    public function setIndTipoDoc($ind_tipo_doc)
    {
        $this->container['ind_tipo_doc'] = $ind_tipo_doc;

        return $this;
    }

    /**
     * Gets link_docto
     *
     * @return string
     */
    public function getLinkDocto()
    {
        return $this->container['link_docto'];
    }

    /**
     * Sets link_docto
     *
     * @param string $link_docto #field_definition#
     *
     * @return $this
     */
    public function setLinkDocto($link_docto)
    {
        $this->container['link_docto'] = $link_docto;

        return $this;
    }

    /**
     * Gets dsc_docto
     *
     * @return string
     */
    public function getDscDocto()
    {
        return $this->container['dsc_docto'];
    }

    /**
     * Sets dsc_docto
     *
     * @param string $dsc_docto #field_definition#
     *
     * @return $this
     */
    public function setDscDocto($dsc_docto)
    {
        $this->container['dsc_docto'] = $dsc_docto;

        return $this;
    }

    /**
     * Gets docto_b64
     *
     * @return string
     */
    public function getDoctoB64()
    {
        return $this->container['docto_b64'];
    }

    /**
     * Sets docto_b64
     *
     * @param string $docto_b64 #field_definition#
     *
     * @return $this
     */
    public function setDoctoB64($docto_b64)
    {
        $this->container['docto_b64'] = $docto_b64;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


