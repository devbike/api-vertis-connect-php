<?php
/**
 * ModelFiscal
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisConnect\Model;

use \ArrayAccess;
use \VertisConnect\ObjectSerializer;

/**
 * ModelFiscal Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelFiscal implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelFiscal';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cod_parceiro' => 'int',
        'ind_tip_parceiro' => 'string',
        'ind_sit_rps' => 'string',
        'ind_opt_simples' => 'string',
        'nom_repr_simples' => 'string',
        'fkcod_nat_operacao' => 'int',
        'reg_esp_tributacao' => 'int',
        'ind_frm_irrfpj' => 'string',
        'ind_susp_exig_pis' => 'string',
        'dat_susp_exig_pis' => 'string',
        'ind_susp_exig_cofins' => 'string',
        'dat_susp_exig_cofins' => 'string',
        'ind_susp_exig_csll' => 'string',
        'dat_susp_exig_csll' => 'string',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cod_parceiro' => 'int64',
        'ind_tip_parceiro' => null,
        'ind_sit_rps' => null,
        'ind_opt_simples' => null,
        'nom_repr_simples' => null,
        'fkcod_nat_operacao' => 'int64',
        'reg_esp_tributacao' => 'int64',
        'ind_frm_irrfpj' => null,
        'ind_susp_exig_pis' => null,
        'dat_susp_exig_pis' => null,
        'ind_susp_exig_cofins' => null,
        'dat_susp_exig_cofins' => null,
        'ind_susp_exig_csll' => null,
        'dat_susp_exig_csll' => null,
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cod_parceiro' => 'cod_parceiro',
        'ind_tip_parceiro' => 'ind_tip_parceiro',
        'ind_sit_rps' => 'ind_sit_rps',
        'ind_opt_simples' => 'ind_opt_simples',
        'nom_repr_simples' => 'nom_repr_simples',
        'fkcod_nat_operacao' => 'fkcod_nat_operacao',
        'reg_esp_tributacao' => 'reg_esp_tributacao',
        'ind_frm_irrfpj' => 'ind_frm_irrfpj',
        'ind_susp_exig_pis' => 'ind_susp_exig_pis',
        'dat_susp_exig_pis' => 'dat_susp_exig_pis',
        'ind_susp_exig_cofins' => 'ind_susp_exig_cofins',
        'dat_susp_exig_cofins' => 'dat_susp_exig_cofins',
        'ind_susp_exig_csll' => 'ind_susp_exig_csll',
        'dat_susp_exig_csll' => 'dat_susp_exig_csll',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cod_parceiro' => 'setCodParceiro',
        'ind_tip_parceiro' => 'setIndTipParceiro',
        'ind_sit_rps' => 'setIndSitRps',
        'ind_opt_simples' => 'setIndOptSimples',
        'nom_repr_simples' => 'setNomReprSimples',
        'fkcod_nat_operacao' => 'setFkcodNatOperacao',
        'reg_esp_tributacao' => 'setRegEspTributacao',
        'ind_frm_irrfpj' => 'setIndFrmIrrfpj',
        'ind_susp_exig_pis' => 'setIndSuspExigPis',
        'dat_susp_exig_pis' => 'setDatSuspExigPis',
        'ind_susp_exig_cofins' => 'setIndSuspExigCofins',
        'dat_susp_exig_cofins' => 'setDatSuspExigCofins',
        'ind_susp_exig_csll' => 'setIndSuspExigCsll',
        'dat_susp_exig_csll' => 'setDatSuspExigCsll',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cod_parceiro' => 'getCodParceiro',
        'ind_tip_parceiro' => 'getIndTipParceiro',
        'ind_sit_rps' => 'getIndSitRps',
        'ind_opt_simples' => 'getIndOptSimples',
        'nom_repr_simples' => 'getNomReprSimples',
        'fkcod_nat_operacao' => 'getFkcodNatOperacao',
        'reg_esp_tributacao' => 'getRegEspTributacao',
        'ind_frm_irrfpj' => 'getIndFrmIrrfpj',
        'ind_susp_exig_pis' => 'getIndSuspExigPis',
        'dat_susp_exig_pis' => 'getDatSuspExigPis',
        'ind_susp_exig_cofins' => 'getIndSuspExigCofins',
        'dat_susp_exig_cofins' => 'getDatSuspExigCofins',
        'ind_susp_exig_csll' => 'getIndSuspExigCsll',
        'dat_susp_exig_csll' => 'getDatSuspExigCsll',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cod_parceiro'] = isset($data['cod_parceiro']) ? $data['cod_parceiro'] : null;
        $this->container['ind_tip_parceiro'] = isset($data['ind_tip_parceiro']) ? $data['ind_tip_parceiro'] : null;
        $this->container['ind_sit_rps'] = isset($data['ind_sit_rps']) ? $data['ind_sit_rps'] : null;
        $this->container['ind_opt_simples'] = isset($data['ind_opt_simples']) ? $data['ind_opt_simples'] : null;
        $this->container['nom_repr_simples'] = isset($data['nom_repr_simples']) ? $data['nom_repr_simples'] : null;
        $this->container['fkcod_nat_operacao'] = isset($data['fkcod_nat_operacao']) ? $data['fkcod_nat_operacao'] : null;
        $this->container['reg_esp_tributacao'] = isset($data['reg_esp_tributacao']) ? $data['reg_esp_tributacao'] : null;
        $this->container['ind_frm_irrfpj'] = isset($data['ind_frm_irrfpj']) ? $data['ind_frm_irrfpj'] : null;
        $this->container['ind_susp_exig_pis'] = isset($data['ind_susp_exig_pis']) ? $data['ind_susp_exig_pis'] : null;
        $this->container['dat_susp_exig_pis'] = isset($data['dat_susp_exig_pis']) ? $data['dat_susp_exig_pis'] : null;
        $this->container['ind_susp_exig_cofins'] = isset($data['ind_susp_exig_cofins']) ? $data['ind_susp_exig_cofins'] : null;
        $this->container['dat_susp_exig_cofins'] = isset($data['dat_susp_exig_cofins']) ? $data['dat_susp_exig_cofins'] : null;
        $this->container['ind_susp_exig_csll'] = isset($data['ind_susp_exig_csll']) ? $data['ind_susp_exig_csll'] : null;
        $this->container['dat_susp_exig_csll'] = isset($data['dat_susp_exig_csll']) ? $data['dat_susp_exig_csll'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cod_parceiro
     *
     * @return int
     */
    public function getCodParceiro()
    {
        return $this->container['cod_parceiro'];
    }

    /**
     * Sets cod_parceiro
     *
     * @param int $cod_parceiro #field_definition#
     *
     * @return $this
     */
    public function setCodParceiro($cod_parceiro)
    {
        $this->container['cod_parceiro'] = $cod_parceiro;

        return $this;
    }

    /**
     * Gets ind_tip_parceiro
     *
     * @return string
     */
    public function getIndTipParceiro()
    {
        return $this->container['ind_tip_parceiro'];
    }

    /**
     * Sets ind_tip_parceiro
     *
     * @param string $ind_tip_parceiro #field_definition#
     *
     * @return $this
     */
    public function setIndTipParceiro($ind_tip_parceiro)
    {
        $this->container['ind_tip_parceiro'] = $ind_tip_parceiro;

        return $this;
    }

    /**
     * Gets ind_sit_rps
     *
     * @return string
     */
    public function getIndSitRps()
    {
        return $this->container['ind_sit_rps'];
    }

    /**
     * Sets ind_sit_rps
     *
     * @param string $ind_sit_rps #field_definition#
     *
     * @return $this
     */
    public function setIndSitRps($ind_sit_rps)
    {
        $this->container['ind_sit_rps'] = $ind_sit_rps;

        return $this;
    }

    /**
     * Gets ind_opt_simples
     *
     * @return string
     */
    public function getIndOptSimples()
    {
        return $this->container['ind_opt_simples'];
    }

    /**
     * Sets ind_opt_simples
     *
     * @param string $ind_opt_simples #field_definition#
     *
     * @return $this
     */
    public function setIndOptSimples($ind_opt_simples)
    {
        $this->container['ind_opt_simples'] = $ind_opt_simples;

        return $this;
    }

    /**
     * Gets nom_repr_simples
     *
     * @return string
     */
    public function getNomReprSimples()
    {
        return $this->container['nom_repr_simples'];
    }

    /**
     * Sets nom_repr_simples
     *
     * @param string $nom_repr_simples #field_definition#
     *
     * @return $this
     */
    public function setNomReprSimples($nom_repr_simples)
    {
        $this->container['nom_repr_simples'] = $nom_repr_simples;

        return $this;
    }

    /**
     * Gets fkcod_nat_operacao
     *
     * @return int
     */
    public function getFkcodNatOperacao()
    {
        return $this->container['fkcod_nat_operacao'];
    }

    /**
     * Sets fkcod_nat_operacao
     *
     * @param int $fkcod_nat_operacao #field_definition#
     *
     * @return $this
     */
    public function setFkcodNatOperacao($fkcod_nat_operacao)
    {
        $this->container['fkcod_nat_operacao'] = $fkcod_nat_operacao;

        return $this;
    }

    /**
     * Gets reg_esp_tributacao
     *
     * @return int
     */
    public function getRegEspTributacao()
    {
        return $this->container['reg_esp_tributacao'];
    }

    /**
     * Sets reg_esp_tributacao
     *
     * @param int $reg_esp_tributacao #field_definition#
     *
     * @return $this
     */
    public function setRegEspTributacao($reg_esp_tributacao)
    {
        $this->container['reg_esp_tributacao'] = $reg_esp_tributacao;

        return $this;
    }

    /**
     * Gets ind_frm_irrfpj
     *
     * @return string
     */
    public function getIndFrmIrrfpj()
    {
        return $this->container['ind_frm_irrfpj'];
    }

    /**
     * Sets ind_frm_irrfpj
     *
     * @param string $ind_frm_irrfpj #field_definition#
     *
     * @return $this
     */
    public function setIndFrmIrrfpj($ind_frm_irrfpj)
    {
        $this->container['ind_frm_irrfpj'] = $ind_frm_irrfpj;

        return $this;
    }

    /**
     * Gets ind_susp_exig_pis
     *
     * @return string
     */
    public function getIndSuspExigPis()
    {
        return $this->container['ind_susp_exig_pis'];
    }

    /**
     * Sets ind_susp_exig_pis
     *
     * @param string $ind_susp_exig_pis #field_definition#
     *
     * @return $this
     */
    public function setIndSuspExigPis($ind_susp_exig_pis)
    {
        $this->container['ind_susp_exig_pis'] = $ind_susp_exig_pis;

        return $this;
    }

    /**
     * Gets dat_susp_exig_pis
     *
     * @return string
     */
    public function getDatSuspExigPis()
    {
        return $this->container['dat_susp_exig_pis'];
    }

    /**
     * Sets dat_susp_exig_pis
     *
     * @param string $dat_susp_exig_pis #field_definition#
     *
     * @return $this
     */
    public function setDatSuspExigPis($dat_susp_exig_pis)
    {
        $this->container['dat_susp_exig_pis'] = $dat_susp_exig_pis;

        return $this;
    }

    /**
     * Gets ind_susp_exig_cofins
     *
     * @return string
     */
    public function getIndSuspExigCofins()
    {
        return $this->container['ind_susp_exig_cofins'];
    }

    /**
     * Sets ind_susp_exig_cofins
     *
     * @param string $ind_susp_exig_cofins #field_definition#
     *
     * @return $this
     */
    public function setIndSuspExigCofins($ind_susp_exig_cofins)
    {
        $this->container['ind_susp_exig_cofins'] = $ind_susp_exig_cofins;

        return $this;
    }

    /**
     * Gets dat_susp_exig_cofins
     *
     * @return string
     */
    public function getDatSuspExigCofins()
    {
        return $this->container['dat_susp_exig_cofins'];
    }

    /**
     * Sets dat_susp_exig_cofins
     *
     * @param string $dat_susp_exig_cofins #field_definition#
     *
     * @return $this
     */
    public function setDatSuspExigCofins($dat_susp_exig_cofins)
    {
        $this->container['dat_susp_exig_cofins'] = $dat_susp_exig_cofins;

        return $this;
    }

    /**
     * Gets ind_susp_exig_csll
     *
     * @return string
     */
    public function getIndSuspExigCsll()
    {
        return $this->container['ind_susp_exig_csll'];
    }

    /**
     * Sets ind_susp_exig_csll
     *
     * @param string $ind_susp_exig_csll #field_definition#
     *
     * @return $this
     */
    public function setIndSuspExigCsll($ind_susp_exig_csll)
    {
        $this->container['ind_susp_exig_csll'] = $ind_susp_exig_csll;

        return $this;
    }

    /**
     * Gets dat_susp_exig_csll
     *
     * @return string
     */
    public function getDatSuspExigCsll()
    {
        return $this->container['dat_susp_exig_csll'];
    }

    /**
     * Sets dat_susp_exig_csll
     *
     * @param string $dat_susp_exig_csll #field_definition#
     *
     * @return $this
     */
    public function setDatSuspExigCsll($dat_susp_exig_csll)
    {
        $this->container['dat_susp_exig_csll'] = $dat_susp_exig_csll;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


