<?php
/**
 * ModelParcInfFaturamento
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisConnect\Model;

use \ArrayAccess;
use \VertisConnect\ObjectSerializer;

/**
 * ModelParcInfFaturamento Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelParcInfFaturamento implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelParcInfFaturamento';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cod_parceiro' => 'int',
        'ind_tip_parceiro' => 'string',
        'num_dias_fat' => 'int',
        'ind_tipo_vcto' => 'string',
        'ind_fmt_pgto_conv' => 'string',
        'ind_nome_boleto' => 'string',
        'ind_meio_fat_conv' => 'string',
        'email1_fat_conv' => 'string',
        'email2_fat_conv' => 'string',
        'ind_gerar_rps' => 'string',
        'ind_anexar_rps' => 'string',
        'ind_anexar_nfse' => 'string',
        'ind_anexar_boleto' => 'string',
        'nom_logra_cob' => 'string',
        'nro_logra_cob' => 'string',
        'compl_logra_cob' => 'string',
        'nom_bairro_cob' => 'string',
        'nom_cidade_cob' => 'string',
        'sigla_uf_cob' => 'string',
        'cod_cep_cob' => 'string',
        'cod_cep_compl_cob' => 'string',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cod_parceiro' => 'int64',
        'ind_tip_parceiro' => null,
        'num_dias_fat' => 'int64',
        'ind_tipo_vcto' => null,
        'ind_fmt_pgto_conv' => null,
        'ind_nome_boleto' => null,
        'ind_meio_fat_conv' => null,
        'email1_fat_conv' => null,
        'email2_fat_conv' => null,
        'ind_gerar_rps' => null,
        'ind_anexar_rps' => null,
        'ind_anexar_nfse' => null,
        'ind_anexar_boleto' => null,
        'nom_logra_cob' => null,
        'nro_logra_cob' => null,
        'compl_logra_cob' => null,
        'nom_bairro_cob' => null,
        'nom_cidade_cob' => null,
        'sigla_uf_cob' => null,
        'cod_cep_cob' => null,
        'cod_cep_compl_cob' => null,
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cod_parceiro' => 'cod_parceiro',
        'ind_tip_parceiro' => 'ind_tip_parceiro',
        'num_dias_fat' => 'num_dias_fat',
        'ind_tipo_vcto' => 'ind_tipo_vcto',
        'ind_fmt_pgto_conv' => 'ind_fmt_pgto_conv',
        'ind_nome_boleto' => 'ind_nome_boleto',
        'ind_meio_fat_conv' => 'ind_meio_fat_conv',
        'email1_fat_conv' => 'email1_fat_conv',
        'email2_fat_conv' => 'email2_fat_conv',
        'ind_gerar_rps' => 'ind_gerar_rps',
        'ind_anexar_rps' => 'ind_anexar_rps',
        'ind_anexar_nfse' => 'ind_anexar_nfse',
        'ind_anexar_boleto' => 'ind_anexar_boleto',
        'nom_logra_cob' => 'nom_logra_cob',
        'nro_logra_cob' => 'nro_logra_cob',
        'compl_logra_cob' => 'compl_logra_cob',
        'nom_bairro_cob' => 'nom_bairro_cob',
        'nom_cidade_cob' => 'nom_cidade_cob',
        'sigla_uf_cob' => 'sigla_uf_cob',
        'cod_cep_cob' => 'cod_cep_cob',
        'cod_cep_compl_cob' => 'cod_cep_compl_cob',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cod_parceiro' => 'setCodParceiro',
        'ind_tip_parceiro' => 'setIndTipParceiro',
        'num_dias_fat' => 'setNumDiasFat',
        'ind_tipo_vcto' => 'setIndTipoVcto',
        'ind_fmt_pgto_conv' => 'setIndFmtPgtoConv',
        'ind_nome_boleto' => 'setIndNomeBoleto',
        'ind_meio_fat_conv' => 'setIndMeioFatConv',
        'email1_fat_conv' => 'setEmail1FatConv',
        'email2_fat_conv' => 'setEmail2FatConv',
        'ind_gerar_rps' => 'setIndGerarRps',
        'ind_anexar_rps' => 'setIndAnexarRps',
        'ind_anexar_nfse' => 'setIndAnexarNfse',
        'ind_anexar_boleto' => 'setIndAnexarBoleto',
        'nom_logra_cob' => 'setNomLograCob',
        'nro_logra_cob' => 'setNroLograCob',
        'compl_logra_cob' => 'setComplLograCob',
        'nom_bairro_cob' => 'setNomBairroCob',
        'nom_cidade_cob' => 'setNomCidadeCob',
        'sigla_uf_cob' => 'setSiglaUfCob',
        'cod_cep_cob' => 'setCodCepCob',
        'cod_cep_compl_cob' => 'setCodCepComplCob',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cod_parceiro' => 'getCodParceiro',
        'ind_tip_parceiro' => 'getIndTipParceiro',
        'num_dias_fat' => 'getNumDiasFat',
        'ind_tipo_vcto' => 'getIndTipoVcto',
        'ind_fmt_pgto_conv' => 'getIndFmtPgtoConv',
        'ind_nome_boleto' => 'getIndNomeBoleto',
        'ind_meio_fat_conv' => 'getIndMeioFatConv',
        'email1_fat_conv' => 'getEmail1FatConv',
        'email2_fat_conv' => 'getEmail2FatConv',
        'ind_gerar_rps' => 'getIndGerarRps',
        'ind_anexar_rps' => 'getIndAnexarRps',
        'ind_anexar_nfse' => 'getIndAnexarNfse',
        'ind_anexar_boleto' => 'getIndAnexarBoleto',
        'nom_logra_cob' => 'getNomLograCob',
        'nro_logra_cob' => 'getNroLograCob',
        'compl_logra_cob' => 'getComplLograCob',
        'nom_bairro_cob' => 'getNomBairroCob',
        'nom_cidade_cob' => 'getNomCidadeCob',
        'sigla_uf_cob' => 'getSiglaUfCob',
        'cod_cep_cob' => 'getCodCepCob',
        'cod_cep_compl_cob' => 'getCodCepComplCob',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cod_parceiro'] = isset($data['cod_parceiro']) ? $data['cod_parceiro'] : null;
        $this->container['ind_tip_parceiro'] = isset($data['ind_tip_parceiro']) ? $data['ind_tip_parceiro'] : null;
        $this->container['num_dias_fat'] = isset($data['num_dias_fat']) ? $data['num_dias_fat'] : null;
        $this->container['ind_tipo_vcto'] = isset($data['ind_tipo_vcto']) ? $data['ind_tipo_vcto'] : null;
        $this->container['ind_fmt_pgto_conv'] = isset($data['ind_fmt_pgto_conv']) ? $data['ind_fmt_pgto_conv'] : null;
        $this->container['ind_nome_boleto'] = isset($data['ind_nome_boleto']) ? $data['ind_nome_boleto'] : null;
        $this->container['ind_meio_fat_conv'] = isset($data['ind_meio_fat_conv']) ? $data['ind_meio_fat_conv'] : null;
        $this->container['email1_fat_conv'] = isset($data['email1_fat_conv']) ? $data['email1_fat_conv'] : null;
        $this->container['email2_fat_conv'] = isset($data['email2_fat_conv']) ? $data['email2_fat_conv'] : null;
        $this->container['ind_gerar_rps'] = isset($data['ind_gerar_rps']) ? $data['ind_gerar_rps'] : null;
        $this->container['ind_anexar_rps'] = isset($data['ind_anexar_rps']) ? $data['ind_anexar_rps'] : null;
        $this->container['ind_anexar_nfse'] = isset($data['ind_anexar_nfse']) ? $data['ind_anexar_nfse'] : null;
        $this->container['ind_anexar_boleto'] = isset($data['ind_anexar_boleto']) ? $data['ind_anexar_boleto'] : null;
        $this->container['nom_logra_cob'] = isset($data['nom_logra_cob']) ? $data['nom_logra_cob'] : null;
        $this->container['nro_logra_cob'] = isset($data['nro_logra_cob']) ? $data['nro_logra_cob'] : null;
        $this->container['compl_logra_cob'] = isset($data['compl_logra_cob']) ? $data['compl_logra_cob'] : null;
        $this->container['nom_bairro_cob'] = isset($data['nom_bairro_cob']) ? $data['nom_bairro_cob'] : null;
        $this->container['nom_cidade_cob'] = isset($data['nom_cidade_cob']) ? $data['nom_cidade_cob'] : null;
        $this->container['sigla_uf_cob'] = isset($data['sigla_uf_cob']) ? $data['sigla_uf_cob'] : null;
        $this->container['cod_cep_cob'] = isset($data['cod_cep_cob']) ? $data['cod_cep_cob'] : null;
        $this->container['cod_cep_compl_cob'] = isset($data['cod_cep_compl_cob']) ? $data['cod_cep_compl_cob'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cod_parceiro
     *
     * @return int
     */
    public function getCodParceiro()
    {
        return $this->container['cod_parceiro'];
    }

    /**
     * Sets cod_parceiro
     *
     * @param int $cod_parceiro #field_definition#
     *
     * @return $this
     */
    public function setCodParceiro($cod_parceiro)
    {
        $this->container['cod_parceiro'] = $cod_parceiro;

        return $this;
    }

    /**
     * Gets ind_tip_parceiro
     *
     * @return string
     */
    public function getIndTipParceiro()
    {
        return $this->container['ind_tip_parceiro'];
    }

    /**
     * Sets ind_tip_parceiro
     *
     * @param string $ind_tip_parceiro #field_definition#
     *
     * @return $this
     */
    public function setIndTipParceiro($ind_tip_parceiro)
    {
        $this->container['ind_tip_parceiro'] = $ind_tip_parceiro;

        return $this;
    }

    /**
     * Gets num_dias_fat
     *
     * @return int
     */
    public function getNumDiasFat()
    {
        return $this->container['num_dias_fat'];
    }

    /**
     * Sets num_dias_fat
     *
     * @param int $num_dias_fat #field_definition#
     *
     * @return $this
     */
    public function setNumDiasFat($num_dias_fat)
    {
        $this->container['num_dias_fat'] = $num_dias_fat;

        return $this;
    }

    /**
     * Gets ind_tipo_vcto
     *
     * @return string
     */
    public function getIndTipoVcto()
    {
        return $this->container['ind_tipo_vcto'];
    }

    /**
     * Sets ind_tipo_vcto
     *
     * @param string $ind_tipo_vcto #field_definition#
     *
     * @return $this
     */
    public function setIndTipoVcto($ind_tipo_vcto)
    {
        $this->container['ind_tipo_vcto'] = $ind_tipo_vcto;

        return $this;
    }

    /**
     * Gets ind_fmt_pgto_conv
     *
     * @return string
     */
    public function getIndFmtPgtoConv()
    {
        return $this->container['ind_fmt_pgto_conv'];
    }

    /**
     * Sets ind_fmt_pgto_conv
     *
     * @param string $ind_fmt_pgto_conv #field_definition#
     *
     * @return $this
     */
    public function setIndFmtPgtoConv($ind_fmt_pgto_conv)
    {
        $this->container['ind_fmt_pgto_conv'] = $ind_fmt_pgto_conv;

        return $this;
    }

    /**
     * Gets ind_nome_boleto
     *
     * @return string
     */
    public function getIndNomeBoleto()
    {
        return $this->container['ind_nome_boleto'];
    }

    /**
     * Sets ind_nome_boleto
     *
     * @param string $ind_nome_boleto #field_definition#
     *
     * @return $this
     */
    public function setIndNomeBoleto($ind_nome_boleto)
    {
        $this->container['ind_nome_boleto'] = $ind_nome_boleto;

        return $this;
    }

    /**
     * Gets ind_meio_fat_conv
     *
     * @return string
     */
    public function getIndMeioFatConv()
    {
        return $this->container['ind_meio_fat_conv'];
    }

    /**
     * Sets ind_meio_fat_conv
     *
     * @param string $ind_meio_fat_conv #field_definition#
     *
     * @return $this
     */
    public function setIndMeioFatConv($ind_meio_fat_conv)
    {
        $this->container['ind_meio_fat_conv'] = $ind_meio_fat_conv;

        return $this;
    }

    /**
     * Gets email1_fat_conv
     *
     * @return string
     */
    public function getEmail1FatConv()
    {
        return $this->container['email1_fat_conv'];
    }

    /**
     * Sets email1_fat_conv
     *
     * @param string $email1_fat_conv #field_definition#
     *
     * @return $this
     */
    public function setEmail1FatConv($email1_fat_conv)
    {
        $this->container['email1_fat_conv'] = $email1_fat_conv;

        return $this;
    }

    /**
     * Gets email2_fat_conv
     *
     * @return string
     */
    public function getEmail2FatConv()
    {
        return $this->container['email2_fat_conv'];
    }

    /**
     * Sets email2_fat_conv
     *
     * @param string $email2_fat_conv #field_definition#
     *
     * @return $this
     */
    public function setEmail2FatConv($email2_fat_conv)
    {
        $this->container['email2_fat_conv'] = $email2_fat_conv;

        return $this;
    }

    /**
     * Gets ind_gerar_rps
     *
     * @return string
     */
    public function getIndGerarRps()
    {
        return $this->container['ind_gerar_rps'];
    }

    /**
     * Sets ind_gerar_rps
     *
     * @param string $ind_gerar_rps #field_definition#
     *
     * @return $this
     */
    public function setIndGerarRps($ind_gerar_rps)
    {
        $this->container['ind_gerar_rps'] = $ind_gerar_rps;

        return $this;
    }

    /**
     * Gets ind_anexar_rps
     *
     * @return string
     */
    public function getIndAnexarRps()
    {
        return $this->container['ind_anexar_rps'];
    }

    /**
     * Sets ind_anexar_rps
     *
     * @param string $ind_anexar_rps #field_definition#
     *
     * @return $this
     */
    public function setIndAnexarRps($ind_anexar_rps)
    {
        $this->container['ind_anexar_rps'] = $ind_anexar_rps;

        return $this;
    }

    /**
     * Gets ind_anexar_nfse
     *
     * @return string
     */
    public function getIndAnexarNfse()
    {
        return $this->container['ind_anexar_nfse'];
    }

    /**
     * Sets ind_anexar_nfse
     *
     * @param string $ind_anexar_nfse #field_definition#
     *
     * @return $this
     */
    public function setIndAnexarNfse($ind_anexar_nfse)
    {
        $this->container['ind_anexar_nfse'] = $ind_anexar_nfse;

        return $this;
    }

    /**
     * Gets ind_anexar_boleto
     *
     * @return string
     */
    public function getIndAnexarBoleto()
    {
        return $this->container['ind_anexar_boleto'];
    }

    /**
     * Sets ind_anexar_boleto
     *
     * @param string $ind_anexar_boleto #field_definition#
     *
     * @return $this
     */
    public function setIndAnexarBoleto($ind_anexar_boleto)
    {
        $this->container['ind_anexar_boleto'] = $ind_anexar_boleto;

        return $this;
    }

    /**
     * Gets nom_logra_cob
     *
     * @return string
     */
    public function getNomLograCob()
    {
        return $this->container['nom_logra_cob'];
    }

    /**
     * Sets nom_logra_cob
     *
     * @param string $nom_logra_cob #field_definition#
     *
     * @return $this
     */
    public function setNomLograCob($nom_logra_cob)
    {
        $this->container['nom_logra_cob'] = $nom_logra_cob;

        return $this;
    }

    /**
     * Gets nro_logra_cob
     *
     * @return string
     */
    public function getNroLograCob()
    {
        return $this->container['nro_logra_cob'];
    }

    /**
     * Sets nro_logra_cob
     *
     * @param string $nro_logra_cob #field_definition#
     *
     * @return $this
     */
    public function setNroLograCob($nro_logra_cob)
    {
        $this->container['nro_logra_cob'] = $nro_logra_cob;

        return $this;
    }

    /**
     * Gets compl_logra_cob
     *
     * @return string
     */
    public function getComplLograCob()
    {
        return $this->container['compl_logra_cob'];
    }

    /**
     * Sets compl_logra_cob
     *
     * @param string $compl_logra_cob #field_definition#
     *
     * @return $this
     */
    public function setComplLograCob($compl_logra_cob)
    {
        $this->container['compl_logra_cob'] = $compl_logra_cob;

        return $this;
    }

    /**
     * Gets nom_bairro_cob
     *
     * @return string
     */
    public function getNomBairroCob()
    {
        return $this->container['nom_bairro_cob'];
    }

    /**
     * Sets nom_bairro_cob
     *
     * @param string $nom_bairro_cob #field_definition#
     *
     * @return $this
     */
    public function setNomBairroCob($nom_bairro_cob)
    {
        $this->container['nom_bairro_cob'] = $nom_bairro_cob;

        return $this;
    }

    /**
     * Gets nom_cidade_cob
     *
     * @return string
     */
    public function getNomCidadeCob()
    {
        return $this->container['nom_cidade_cob'];
    }

    /**
     * Sets nom_cidade_cob
     *
     * @param string $nom_cidade_cob #field_definition#
     *
     * @return $this
     */
    public function setNomCidadeCob($nom_cidade_cob)
    {
        $this->container['nom_cidade_cob'] = $nom_cidade_cob;

        return $this;
    }

    /**
     * Gets sigla_uf_cob
     *
     * @return string
     */
    public function getSiglaUfCob()
    {
        return $this->container['sigla_uf_cob'];
    }

    /**
     * Sets sigla_uf_cob
     *
     * @param string $sigla_uf_cob #field_definition#
     *
     * @return $this
     */
    public function setSiglaUfCob($sigla_uf_cob)
    {
        $this->container['sigla_uf_cob'] = $sigla_uf_cob;

        return $this;
    }

    /**
     * Gets cod_cep_cob
     *
     * @return string
     */
    public function getCodCepCob()
    {
        return $this->container['cod_cep_cob'];
    }

    /**
     * Sets cod_cep_cob
     *
     * @param string $cod_cep_cob #field_definition#
     *
     * @return $this
     */
    public function setCodCepCob($cod_cep_cob)
    {
        $this->container['cod_cep_cob'] = $cod_cep_cob;

        return $this;
    }

    /**
     * Gets cod_cep_compl_cob
     *
     * @return string
     */
    public function getCodCepComplCob()
    {
        return $this->container['cod_cep_compl_cob'];
    }

    /**
     * Sets cod_cep_compl_cob
     *
     * @param string $cod_cep_compl_cob #field_definition#
     *
     * @return $this
     */
    public function setCodCepComplCob($cod_cep_compl_cob)
    {
        $this->container['cod_cep_compl_cob'] = $cod_cep_compl_cob;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


