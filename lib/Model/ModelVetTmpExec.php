<?php
/**
 * ModelVetTmpExec
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisConnect\Model;

use \ArrayAccess;
use \VertisConnect\ObjectSerializer;

/**
 * ModelVetTmpExec Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelVetTmpExec implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelVetTmpExec';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cod_veterinario' => 'int',
        'cod_exame' => 'int',
        'tmp_med_execucao' => 'int',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cod_veterinario' => 'int64',
        'cod_exame' => 'int64',
        'tmp_med_execucao' => 'int64',
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cod_veterinario' => 'cod_veterinario',
        'cod_exame' => 'cod_exame',
        'tmp_med_execucao' => 'tmp_med_execucao',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cod_veterinario' => 'setCodVeterinario',
        'cod_exame' => 'setCodExame',
        'tmp_med_execucao' => 'setTmpMedExecucao',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cod_veterinario' => 'getCodVeterinario',
        'cod_exame' => 'getCodExame',
        'tmp_med_execucao' => 'getTmpMedExecucao',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cod_veterinario'] = isset($data['cod_veterinario']) ? $data['cod_veterinario'] : null;
        $this->container['cod_exame'] = isset($data['cod_exame']) ? $data['cod_exame'] : null;
        $this->container['tmp_med_execucao'] = isset($data['tmp_med_execucao']) ? $data['tmp_med_execucao'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cod_veterinario
     *
     * @return int
     */
    public function getCodVeterinario()
    {
        return $this->container['cod_veterinario'];
    }

    /**
     * Sets cod_veterinario
     *
     * @param int $cod_veterinario #field_definition#
     *
     * @return $this
     */
    public function setCodVeterinario($cod_veterinario)
    {
        $this->container['cod_veterinario'] = $cod_veterinario;

        return $this;
    }

    /**
     * Gets cod_exame
     *
     * @return int
     */
    public function getCodExame()
    {
        return $this->container['cod_exame'];
    }

    /**
     * Sets cod_exame
     *
     * @param int $cod_exame #field_definition#
     *
     * @return $this
     */
    public function setCodExame($cod_exame)
    {
        $this->container['cod_exame'] = $cod_exame;

        return $this;
    }

    /**
     * Gets tmp_med_execucao
     *
     * @return int
     */
    public function getTmpMedExecucao()
    {
        return $this->container['tmp_med_execucao'];
    }

    /**
     * Sets tmp_med_execucao
     *
     * @param int $tmp_med_execucao #field_definition#
     *
     * @return $this
     */
    public function setTmpMedExecucao($tmp_med_execucao)
    {
        $this->container['tmp_med_execucao'] = $tmp_med_execucao;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


