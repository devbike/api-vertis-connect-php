<?php
/**
 * ModelCodigoServico
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisConnect\Model;

use \ArrayAccess;
use \VertisConnect\ObjectSerializer;

/**
 * ModelCodigoServico Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelCodigoServico implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelCodigoServico';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cod_servico' => 'string',
        'desc_servico' => 'string',
        'ind_exige_intermediario' => 'string',
        'cod_ibpt' => 'int',
        'ind_categoria' => 'string',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cod_servico' => null,
        'desc_servico' => null,
        'ind_exige_intermediario' => null,
        'cod_ibpt' => 'int64',
        'ind_categoria' => null,
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cod_servico' => 'cod_servico',
        'desc_servico' => 'desc_servico',
        'ind_exige_intermediario' => 'ind_exige_intermediario',
        'cod_ibpt' => 'cod_ibpt',
        'ind_categoria' => 'ind_categoria',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cod_servico' => 'setCodServico',
        'desc_servico' => 'setDescServico',
        'ind_exige_intermediario' => 'setIndExigeIntermediario',
        'cod_ibpt' => 'setCodIbpt',
        'ind_categoria' => 'setIndCategoria',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cod_servico' => 'getCodServico',
        'desc_servico' => 'getDescServico',
        'ind_exige_intermediario' => 'getIndExigeIntermediario',
        'cod_ibpt' => 'getCodIbpt',
        'ind_categoria' => 'getIndCategoria',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cod_servico'] = isset($data['cod_servico']) ? $data['cod_servico'] : null;
        $this->container['desc_servico'] = isset($data['desc_servico']) ? $data['desc_servico'] : null;
        $this->container['ind_exige_intermediario'] = isset($data['ind_exige_intermediario']) ? $data['ind_exige_intermediario'] : null;
        $this->container['cod_ibpt'] = isset($data['cod_ibpt']) ? $data['cod_ibpt'] : null;
        $this->container['ind_categoria'] = isset($data['ind_categoria']) ? $data['ind_categoria'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cod_servico
     *
     * @return string
     */
    public function getCodServico()
    {
        return $this->container['cod_servico'];
    }

    /**
     * Sets cod_servico
     *
     * @param string $cod_servico #field_definition#
     *
     * @return $this
     */
    public function setCodServico($cod_servico)
    {
        $this->container['cod_servico'] = $cod_servico;

        return $this;
    }

    /**
     * Gets desc_servico
     *
     * @return string
     */
    public function getDescServico()
    {
        return $this->container['desc_servico'];
    }

    /**
     * Sets desc_servico
     *
     * @param string $desc_servico #field_definition#
     *
     * @return $this
     */
    public function setDescServico($desc_servico)
    {
        $this->container['desc_servico'] = $desc_servico;

        return $this;
    }

    /**
     * Gets ind_exige_intermediario
     *
     * @return string
     */
    public function getIndExigeIntermediario()
    {
        return $this->container['ind_exige_intermediario'];
    }

    /**
     * Sets ind_exige_intermediario
     *
     * @param string $ind_exige_intermediario #field_definition#
     *
     * @return $this
     */
    public function setIndExigeIntermediario($ind_exige_intermediario)
    {
        $this->container['ind_exige_intermediario'] = $ind_exige_intermediario;

        return $this;
    }

    /**
     * Gets cod_ibpt
     *
     * @return int
     */
    public function getCodIbpt()
    {
        return $this->container['cod_ibpt'];
    }

    /**
     * Sets cod_ibpt
     *
     * @param int $cod_ibpt #field_definition#
     *
     * @return $this
     */
    public function setCodIbpt($cod_ibpt)
    {
        $this->container['cod_ibpt'] = $cod_ibpt;

        return $this;
    }

    /**
     * Gets ind_categoria
     *
     * @return string
     */
    public function getIndCategoria()
    {
        return $this->container['ind_categoria'];
    }

    /**
     * Sets ind_categoria
     *
     * @param string $ind_categoria #field_definition#
     *
     * @return $this
     */
    public function setIndCategoria($ind_categoria)
    {
        $this->container['ind_categoria'] = $ind_categoria;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


