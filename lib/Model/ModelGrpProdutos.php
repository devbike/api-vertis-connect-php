<?php
/**
 * ModelGrpProdutos
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisConnect\Model;

use \ArrayAccess;
use \VertisConnect\ObjectSerializer;

/**
 * ModelGrpProdutos Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelGrpProdutos implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelGrpProdutos';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cod_grp_prod' => 'int',
        'cod_unid_negoc' => 'int',
        'nom_grp_produto' => 'string',
        'sigla_grp_prod' => 'string',
        'ind_tip_produto' => 'string',
        'ind_frm_apura_prc' => 'string',
        'cod_grp_prod_pai' => 'int',
        'perc_custo_prod' => 'float',
        'ind_tab_preco' => 'string',
        'ind_lista_in35' => 'string',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cod_grp_prod' => 'int64',
        'cod_unid_negoc' => 'int64',
        'nom_grp_produto' => null,
        'sigla_grp_prod' => null,
        'ind_tip_produto' => null,
        'ind_frm_apura_prc' => null,
        'cod_grp_prod_pai' => 'int64',
        'perc_custo_prod' => null,
        'ind_tab_preco' => null,
        'ind_lista_in35' => null,
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cod_grp_prod' => 'cod_grp_prod',
        'cod_unid_negoc' => 'cod_unid_negoc',
        'nom_grp_produto' => 'nom_grp_produto',
        'sigla_grp_prod' => 'sigla_grp_prod',
        'ind_tip_produto' => 'ind_tip_produto',
        'ind_frm_apura_prc' => 'ind_frm_apura_prc',
        'cod_grp_prod_pai' => 'cod_grp_prod_pai',
        'perc_custo_prod' => 'perc_custo_prod',
        'ind_tab_preco' => 'ind_tab_preco',
        'ind_lista_in35' => 'ind_lista_in35',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cod_grp_prod' => 'setCodGrpProd',
        'cod_unid_negoc' => 'setCodUnidNegoc',
        'nom_grp_produto' => 'setNomGrpProduto',
        'sigla_grp_prod' => 'setSiglaGrpProd',
        'ind_tip_produto' => 'setIndTipProduto',
        'ind_frm_apura_prc' => 'setIndFrmApuraPrc',
        'cod_grp_prod_pai' => 'setCodGrpProdPai',
        'perc_custo_prod' => 'setPercCustoProd',
        'ind_tab_preco' => 'setIndTabPreco',
        'ind_lista_in35' => 'setIndListaIn35',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cod_grp_prod' => 'getCodGrpProd',
        'cod_unid_negoc' => 'getCodUnidNegoc',
        'nom_grp_produto' => 'getNomGrpProduto',
        'sigla_grp_prod' => 'getSiglaGrpProd',
        'ind_tip_produto' => 'getIndTipProduto',
        'ind_frm_apura_prc' => 'getIndFrmApuraPrc',
        'cod_grp_prod_pai' => 'getCodGrpProdPai',
        'perc_custo_prod' => 'getPercCustoProd',
        'ind_tab_preco' => 'getIndTabPreco',
        'ind_lista_in35' => 'getIndListaIn35',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cod_grp_prod'] = isset($data['cod_grp_prod']) ? $data['cod_grp_prod'] : null;
        $this->container['cod_unid_negoc'] = isset($data['cod_unid_negoc']) ? $data['cod_unid_negoc'] : null;
        $this->container['nom_grp_produto'] = isset($data['nom_grp_produto']) ? $data['nom_grp_produto'] : null;
        $this->container['sigla_grp_prod'] = isset($data['sigla_grp_prod']) ? $data['sigla_grp_prod'] : null;
        $this->container['ind_tip_produto'] = isset($data['ind_tip_produto']) ? $data['ind_tip_produto'] : null;
        $this->container['ind_frm_apura_prc'] = isset($data['ind_frm_apura_prc']) ? $data['ind_frm_apura_prc'] : null;
        $this->container['cod_grp_prod_pai'] = isset($data['cod_grp_prod_pai']) ? $data['cod_grp_prod_pai'] : null;
        $this->container['perc_custo_prod'] = isset($data['perc_custo_prod']) ? $data['perc_custo_prod'] : null;
        $this->container['ind_tab_preco'] = isset($data['ind_tab_preco']) ? $data['ind_tab_preco'] : null;
        $this->container['ind_lista_in35'] = isset($data['ind_lista_in35']) ? $data['ind_lista_in35'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cod_grp_prod
     *
     * @return int
     */
    public function getCodGrpProd()
    {
        return $this->container['cod_grp_prod'];
    }

    /**
     * Sets cod_grp_prod
     *
     * @param int $cod_grp_prod #field_definition#
     *
     * @return $this
     */
    public function setCodGrpProd($cod_grp_prod)
    {
        $this->container['cod_grp_prod'] = $cod_grp_prod;

        return $this;
    }

    /**
     * Gets cod_unid_negoc
     *
     * @return int
     */
    public function getCodUnidNegoc()
    {
        return $this->container['cod_unid_negoc'];
    }

    /**
     * Sets cod_unid_negoc
     *
     * @param int $cod_unid_negoc #field_definition#
     *
     * @return $this
     */
    public function setCodUnidNegoc($cod_unid_negoc)
    {
        $this->container['cod_unid_negoc'] = $cod_unid_negoc;

        return $this;
    }

    /**
     * Gets nom_grp_produto
     *
     * @return string
     */
    public function getNomGrpProduto()
    {
        return $this->container['nom_grp_produto'];
    }

    /**
     * Sets nom_grp_produto
     *
     * @param string $nom_grp_produto #field_definition#
     *
     * @return $this
     */
    public function setNomGrpProduto($nom_grp_produto)
    {
        $this->container['nom_grp_produto'] = $nom_grp_produto;

        return $this;
    }

    /**
     * Gets sigla_grp_prod
     *
     * @return string
     */
    public function getSiglaGrpProd()
    {
        return $this->container['sigla_grp_prod'];
    }

    /**
     * Sets sigla_grp_prod
     *
     * @param string $sigla_grp_prod #field_definition#
     *
     * @return $this
     */
    public function setSiglaGrpProd($sigla_grp_prod)
    {
        $this->container['sigla_grp_prod'] = $sigla_grp_prod;

        return $this;
    }

    /**
     * Gets ind_tip_produto
     *
     * @return string
     */
    public function getIndTipProduto()
    {
        return $this->container['ind_tip_produto'];
    }

    /**
     * Sets ind_tip_produto
     *
     * @param string $ind_tip_produto #field_definition#
     *
     * @return $this
     */
    public function setIndTipProduto($ind_tip_produto)
    {
        $this->container['ind_tip_produto'] = $ind_tip_produto;

        return $this;
    }

    /**
     * Gets ind_frm_apura_prc
     *
     * @return string
     */
    public function getIndFrmApuraPrc()
    {
        return $this->container['ind_frm_apura_prc'];
    }

    /**
     * Sets ind_frm_apura_prc
     *
     * @param string $ind_frm_apura_prc #field_definition#
     *
     * @return $this
     */
    public function setIndFrmApuraPrc($ind_frm_apura_prc)
    {
        $this->container['ind_frm_apura_prc'] = $ind_frm_apura_prc;

        return $this;
    }

    /**
     * Gets cod_grp_prod_pai
     *
     * @return int
     */
    public function getCodGrpProdPai()
    {
        return $this->container['cod_grp_prod_pai'];
    }

    /**
     * Sets cod_grp_prod_pai
     *
     * @param int $cod_grp_prod_pai #field_definition#
     *
     * @return $this
     */
    public function setCodGrpProdPai($cod_grp_prod_pai)
    {
        $this->container['cod_grp_prod_pai'] = $cod_grp_prod_pai;

        return $this;
    }

    /**
     * Gets perc_custo_prod
     *
     * @return float
     */
    public function getPercCustoProd()
    {
        return $this->container['perc_custo_prod'];
    }

    /**
     * Sets perc_custo_prod
     *
     * @param float $perc_custo_prod #field_definition#
     *
     * @return $this
     */
    public function setPercCustoProd($perc_custo_prod)
    {
        $this->container['perc_custo_prod'] = $perc_custo_prod;

        return $this;
    }

    /**
     * Gets ind_tab_preco
     *
     * @return string
     */
    public function getIndTabPreco()
    {
        return $this->container['ind_tab_preco'];
    }

    /**
     * Sets ind_tab_preco
     *
     * @param string $ind_tab_preco #field_definition#
     *
     * @return $this
     */
    public function setIndTabPreco($ind_tab_preco)
    {
        $this->container['ind_tab_preco'] = $ind_tab_preco;

        return $this;
    }

    /**
     * Gets ind_lista_in35
     *
     * @return string
     */
    public function getIndListaIn35()
    {
        return $this->container['ind_lista_in35'];
    }

    /**
     * Sets ind_lista_in35
     *
     * @param string $ind_lista_in35 #field_definition#
     *
     * @return $this
     */
    public function setIndListaIn35($ind_lista_in35)
    {
        $this->container['ind_lista_in35'] = $ind_lista_in35;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


