<?php
/**
 * ModelOrdemServico
 *
 * PHP version 5
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Documentação - API Vertis Connect
 *
 * API Vertis Connect
 *
 * OpenAPI spec version: /V1.1
 * Contact: gpi@gpiti.com.br
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace VertisConnect\Model;

use \ArrayAccess;
use \VertisConnect\ObjectSerializer;

/**
 * ModelOrdemServico Class Doc Comment
 *
 * @category Class
 * @package  VertisConnect
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ModelOrdemServico implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'ModelOrdemServico';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'cod_ord_servico' => 'int',
        'cod_ord_serv_solic' => 'string',
        'fkcod_unid_negoc' => 'int',
        'fkcod_unid_oper' => 'int',
        'fkcod_unid_oper_prestadora' => 'int',
        'ind_serv_prod' => 'string',
        'ind_liquidante' => 'string',
        'cpf_liquidante' => 'string',
        'fkcod_parc_liquidante' => 'int',
        'info_liquidante' => '\VertisConnect\Model\ModelOrdemServicoInfoLiquidante',
        'fkcod_tab_preco' => 'int',
        'tip_faturamento' => 'string',
        'dth_vencimento' => 'string',
        'fkcod_faturamento' => 'int',
        'fkusr_cod_criacao_os' => 'int',
        'fkusr_cod_modificou_os' => 'int',
        'obs_ordem_servico' => 'string',
        'dth_inclusao' => 'string',
        'dth_exclusao' => 'string',
        'ind_retira_amostra' => 'string',
        'nro_protocolo' => 'string',
        'vlr_adiantamento' => 'float',
        'lock_vlr_adiantamento' => 'string',
        'os_med' => '\VertisConnect\Model\ModelOrdemServicoOsMed',
        'itens_os' => '\VertisConnect\Model\ModelOrdemServicoItensOs[]',
        'itens_excluidos' => '\VertisConnect\Model\ModelOrdemServicoItensExcluidos[]',
        'ind_permite_excluir' => 'string',
        'statusinicial' => 'string',
        'recordcount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'cod_ord_servico' => 'int64',
        'cod_ord_serv_solic' => null,
        'fkcod_unid_negoc' => 'int64',
        'fkcod_unid_oper' => 'int64',
        'fkcod_unid_oper_prestadora' => 'int64',
        'ind_serv_prod' => null,
        'ind_liquidante' => null,
        'cpf_liquidante' => null,
        'fkcod_parc_liquidante' => 'int64',
        'info_liquidante' => null,
        'fkcod_tab_preco' => 'int64',
        'tip_faturamento' => null,
        'dth_vencimento' => null,
        'fkcod_faturamento' => 'int64',
        'fkusr_cod_criacao_os' => 'int64',
        'fkusr_cod_modificou_os' => 'int64',
        'obs_ordem_servico' => null,
        'dth_inclusao' => null,
        'dth_exclusao' => null,
        'ind_retira_amostra' => null,
        'nro_protocolo' => null,
        'vlr_adiantamento' => null,
        'lock_vlr_adiantamento' => null,
        'os_med' => null,
        'itens_os' => null,
        'itens_excluidos' => null,
        'ind_permite_excluir' => null,
        'statusinicial' => null,
        'recordcount' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'cod_ord_servico' => 'cod_ord_servico',
        'cod_ord_serv_solic' => 'cod_ord_serv_solic',
        'fkcod_unid_negoc' => 'fkcod_unid_negoc',
        'fkcod_unid_oper' => 'fkcod_unid_oper',
        'fkcod_unid_oper_prestadora' => 'fkcod_unid_oper_prestadora',
        'ind_serv_prod' => 'ind_serv_prod',
        'ind_liquidante' => 'ind_liquidante',
        'cpf_liquidante' => 'cpf_liquidante',
        'fkcod_parc_liquidante' => 'fkcod_parc_liquidante',
        'info_liquidante' => 'info_liquidante',
        'fkcod_tab_preco' => 'fkcod_tab_preco',
        'tip_faturamento' => 'tip_faturamento',
        'dth_vencimento' => 'dth_vencimento',
        'fkcod_faturamento' => 'fkcod_faturamento',
        'fkusr_cod_criacao_os' => 'fkusr_cod_criacao_os',
        'fkusr_cod_modificou_os' => 'fkusr_cod_modificou_os',
        'obs_ordem_servico' => 'obs_ordem_servico',
        'dth_inclusao' => 'dth_inclusao',
        'dth_exclusao' => 'dth_exclusao',
        'ind_retira_amostra' => 'ind_retira_amostra',
        'nro_protocolo' => 'nro_protocolo',
        'vlr_adiantamento' => 'vlr_adiantamento',
        'lock_vlr_adiantamento' => 'lock_vlr_adiantamento',
        'os_med' => 'os_med',
        'itens_os' => 'itens_os',
        'itens_excluidos' => 'itens_excluidos',
        'ind_permite_excluir' => 'ind_permite_excluir',
        'statusinicial' => 'statusinicial',
        'recordcount' => 'recordcount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'cod_ord_servico' => 'setCodOrdServico',
        'cod_ord_serv_solic' => 'setCodOrdServSolic',
        'fkcod_unid_negoc' => 'setFkcodUnidNegoc',
        'fkcod_unid_oper' => 'setFkcodUnidOper',
        'fkcod_unid_oper_prestadora' => 'setFkcodUnidOperPrestadora',
        'ind_serv_prod' => 'setIndServProd',
        'ind_liquidante' => 'setIndLiquidante',
        'cpf_liquidante' => 'setCpfLiquidante',
        'fkcod_parc_liquidante' => 'setFkcodParcLiquidante',
        'info_liquidante' => 'setInfoLiquidante',
        'fkcod_tab_preco' => 'setFkcodTabPreco',
        'tip_faturamento' => 'setTipFaturamento',
        'dth_vencimento' => 'setDthVencimento',
        'fkcod_faturamento' => 'setFkcodFaturamento',
        'fkusr_cod_criacao_os' => 'setFkusrCodCriacaoOs',
        'fkusr_cod_modificou_os' => 'setFkusrCodModificouOs',
        'obs_ordem_servico' => 'setObsOrdemServico',
        'dth_inclusao' => 'setDthInclusao',
        'dth_exclusao' => 'setDthExclusao',
        'ind_retira_amostra' => 'setIndRetiraAmostra',
        'nro_protocolo' => 'setNroProtocolo',
        'vlr_adiantamento' => 'setVlrAdiantamento',
        'lock_vlr_adiantamento' => 'setLockVlrAdiantamento',
        'os_med' => 'setOsMed',
        'itens_os' => 'setItensOs',
        'itens_excluidos' => 'setItensExcluidos',
        'ind_permite_excluir' => 'setIndPermiteExcluir',
        'statusinicial' => 'setStatusinicial',
        'recordcount' => 'setRecordcount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'cod_ord_servico' => 'getCodOrdServico',
        'cod_ord_serv_solic' => 'getCodOrdServSolic',
        'fkcod_unid_negoc' => 'getFkcodUnidNegoc',
        'fkcod_unid_oper' => 'getFkcodUnidOper',
        'fkcod_unid_oper_prestadora' => 'getFkcodUnidOperPrestadora',
        'ind_serv_prod' => 'getIndServProd',
        'ind_liquidante' => 'getIndLiquidante',
        'cpf_liquidante' => 'getCpfLiquidante',
        'fkcod_parc_liquidante' => 'getFkcodParcLiquidante',
        'info_liquidante' => 'getInfoLiquidante',
        'fkcod_tab_preco' => 'getFkcodTabPreco',
        'tip_faturamento' => 'getTipFaturamento',
        'dth_vencimento' => 'getDthVencimento',
        'fkcod_faturamento' => 'getFkcodFaturamento',
        'fkusr_cod_criacao_os' => 'getFkusrCodCriacaoOs',
        'fkusr_cod_modificou_os' => 'getFkusrCodModificouOs',
        'obs_ordem_servico' => 'getObsOrdemServico',
        'dth_inclusao' => 'getDthInclusao',
        'dth_exclusao' => 'getDthExclusao',
        'ind_retira_amostra' => 'getIndRetiraAmostra',
        'nro_protocolo' => 'getNroProtocolo',
        'vlr_adiantamento' => 'getVlrAdiantamento',
        'lock_vlr_adiantamento' => 'getLockVlrAdiantamento',
        'os_med' => 'getOsMed',
        'itens_os' => 'getItensOs',
        'itens_excluidos' => 'getItensExcluidos',
        'ind_permite_excluir' => 'getIndPermiteExcluir',
        'statusinicial' => 'getStatusinicial',
        'recordcount' => 'getRecordcount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['cod_ord_servico'] = isset($data['cod_ord_servico']) ? $data['cod_ord_servico'] : null;
        $this->container['cod_ord_serv_solic'] = isset($data['cod_ord_serv_solic']) ? $data['cod_ord_serv_solic'] : null;
        $this->container['fkcod_unid_negoc'] = isset($data['fkcod_unid_negoc']) ? $data['fkcod_unid_negoc'] : null;
        $this->container['fkcod_unid_oper'] = isset($data['fkcod_unid_oper']) ? $data['fkcod_unid_oper'] : null;
        $this->container['fkcod_unid_oper_prestadora'] = isset($data['fkcod_unid_oper_prestadora']) ? $data['fkcod_unid_oper_prestadora'] : null;
        $this->container['ind_serv_prod'] = isset($data['ind_serv_prod']) ? $data['ind_serv_prod'] : null;
        $this->container['ind_liquidante'] = isset($data['ind_liquidante']) ? $data['ind_liquidante'] : null;
        $this->container['cpf_liquidante'] = isset($data['cpf_liquidante']) ? $data['cpf_liquidante'] : null;
        $this->container['fkcod_parc_liquidante'] = isset($data['fkcod_parc_liquidante']) ? $data['fkcod_parc_liquidante'] : null;
        $this->container['info_liquidante'] = isset($data['info_liquidante']) ? $data['info_liquidante'] : null;
        $this->container['fkcod_tab_preco'] = isset($data['fkcod_tab_preco']) ? $data['fkcod_tab_preco'] : null;
        $this->container['tip_faturamento'] = isset($data['tip_faturamento']) ? $data['tip_faturamento'] : null;
        $this->container['dth_vencimento'] = isset($data['dth_vencimento']) ? $data['dth_vencimento'] : null;
        $this->container['fkcod_faturamento'] = isset($data['fkcod_faturamento']) ? $data['fkcod_faturamento'] : null;
        $this->container['fkusr_cod_criacao_os'] = isset($data['fkusr_cod_criacao_os']) ? $data['fkusr_cod_criacao_os'] : null;
        $this->container['fkusr_cod_modificou_os'] = isset($data['fkusr_cod_modificou_os']) ? $data['fkusr_cod_modificou_os'] : null;
        $this->container['obs_ordem_servico'] = isset($data['obs_ordem_servico']) ? $data['obs_ordem_servico'] : null;
        $this->container['dth_inclusao'] = isset($data['dth_inclusao']) ? $data['dth_inclusao'] : null;
        $this->container['dth_exclusao'] = isset($data['dth_exclusao']) ? $data['dth_exclusao'] : null;
        $this->container['ind_retira_amostra'] = isset($data['ind_retira_amostra']) ? $data['ind_retira_amostra'] : null;
        $this->container['nro_protocolo'] = isset($data['nro_protocolo']) ? $data['nro_protocolo'] : null;
        $this->container['vlr_adiantamento'] = isset($data['vlr_adiantamento']) ? $data['vlr_adiantamento'] : null;
        $this->container['lock_vlr_adiantamento'] = isset($data['lock_vlr_adiantamento']) ? $data['lock_vlr_adiantamento'] : null;
        $this->container['os_med'] = isset($data['os_med']) ? $data['os_med'] : null;
        $this->container['itens_os'] = isset($data['itens_os']) ? $data['itens_os'] : null;
        $this->container['itens_excluidos'] = isset($data['itens_excluidos']) ? $data['itens_excluidos'] : null;
        $this->container['ind_permite_excluir'] = isset($data['ind_permite_excluir']) ? $data['ind_permite_excluir'] : null;
        $this->container['statusinicial'] = isset($data['statusinicial']) ? $data['statusinicial'] : null;
        $this->container['recordcount'] = isset($data['recordcount']) ? $data['recordcount'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['fkcod_unid_negoc'] === null) {
            $invalidProperties[] = "'fkcod_unid_negoc' can't be null";
        }
        if ($this->container['fkcod_unid_oper'] === null) {
            $invalidProperties[] = "'fkcod_unid_oper' can't be null";
        }
        if ($this->container['fkcod_tab_preco'] === null) {
            $invalidProperties[] = "'fkcod_tab_preco' can't be null";
        }
        if ($this->container['tip_faturamento'] === null) {
            $invalidProperties[] = "'tip_faturamento' can't be null";
        }
        if ($this->container['dth_inclusao'] === null) {
            $invalidProperties[] = "'dth_inclusao' can't be null";
        }
        if ($this->container['ind_retira_amostra'] === null) {
            $invalidProperties[] = "'ind_retira_amostra' can't be null";
        }
        if ($this->container['nro_protocolo'] === null) {
            $invalidProperties[] = "'nro_protocolo' can't be null";
        }
        if ($this->container['vlr_adiantamento'] === null) {
            $invalidProperties[] = "'vlr_adiantamento' can't be null";
        }
        if ($this->container['lock_vlr_adiantamento'] === null) {
            $invalidProperties[] = "'lock_vlr_adiantamento' can't be null";
        }
        if ($this->container['os_med'] === null) {
            $invalidProperties[] = "'os_med' can't be null";
        }
        if ($this->container['itens_os'] === null) {
            $invalidProperties[] = "'itens_os' can't be null";
        }
        if ($this->container['itens_excluidos'] === null) {
            $invalidProperties[] = "'itens_excluidos' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets cod_ord_servico
     *
     * @return int
     */
    public function getCodOrdServico()
    {
        return $this->container['cod_ord_servico'];
    }

    /**
     * Sets cod_ord_servico
     *
     * @param int $cod_ord_servico #field_definition#
     *
     * @return $this
     */
    public function setCodOrdServico($cod_ord_servico)
    {
        $this->container['cod_ord_servico'] = $cod_ord_servico;

        return $this;
    }

    /**
     * Gets cod_ord_serv_solic
     *
     * @return string
     */
    public function getCodOrdServSolic()
    {
        return $this->container['cod_ord_serv_solic'];
    }

    /**
     * Sets cod_ord_serv_solic
     *
     * @param string $cod_ord_serv_solic #field_definition#
     *
     * @return $this
     */
    public function setCodOrdServSolic($cod_ord_serv_solic)
    {
        $this->container['cod_ord_serv_solic'] = $cod_ord_serv_solic;

        return $this;
    }

    /**
     * Gets fkcod_unid_negoc
     *
     * @return int
     */
    public function getFkcodUnidNegoc()
    {
        return $this->container['fkcod_unid_negoc'];
    }

    /**
     * Sets fkcod_unid_negoc
     *
     * @param int $fkcod_unid_negoc #field_definition#
     *
     * @return $this
     */
    public function setFkcodUnidNegoc($fkcod_unid_negoc)
    {
        $this->container['fkcod_unid_negoc'] = $fkcod_unid_negoc;

        return $this;
    }

    /**
     * Gets fkcod_unid_oper
     *
     * @return int
     */
    public function getFkcodUnidOper()
    {
        return $this->container['fkcod_unid_oper'];
    }

    /**
     * Sets fkcod_unid_oper
     *
     * @param int $fkcod_unid_oper #field_definition#
     *
     * @return $this
     */
    public function setFkcodUnidOper($fkcod_unid_oper)
    {
        $this->container['fkcod_unid_oper'] = $fkcod_unid_oper;

        return $this;
    }

    /**
     * Gets fkcod_unid_oper_prestadora
     *
     * @return int
     */
    public function getFkcodUnidOperPrestadora()
    {
        return $this->container['fkcod_unid_oper_prestadora'];
    }

    /**
     * Sets fkcod_unid_oper_prestadora
     *
     * @param int $fkcod_unid_oper_prestadora #field_definition#
     *
     * @return $this
     */
    public function setFkcodUnidOperPrestadora($fkcod_unid_oper_prestadora)
    {
        $this->container['fkcod_unid_oper_prestadora'] = $fkcod_unid_oper_prestadora;

        return $this;
    }

    /**
     * Gets ind_serv_prod
     *
     * @return string
     */
    public function getIndServProd()
    {
        return $this->container['ind_serv_prod'];
    }

    /**
     * Sets ind_serv_prod
     *
     * @param string $ind_serv_prod #field_definition#
     *
     * @return $this
     */
    public function setIndServProd($ind_serv_prod)
    {
        $this->container['ind_serv_prod'] = $ind_serv_prod;

        return $this;
    }

    /**
     * Gets ind_liquidante
     *
     * @return string
     */
    public function getIndLiquidante()
    {
        return $this->container['ind_liquidante'];
    }

    /**
     * Sets ind_liquidante
     *
     * @param string $ind_liquidante #field_definition#
     *
     * @return $this
     */
    public function setIndLiquidante($ind_liquidante)
    {
        $this->container['ind_liquidante'] = $ind_liquidante;

        return $this;
    }

    /**
     * Gets cpf_liquidante
     *
     * @return string
     */
    public function getCpfLiquidante()
    {
        return $this->container['cpf_liquidante'];
    }

    /**
     * Sets cpf_liquidante
     *
     * @param string $cpf_liquidante #field_definition#
     *
     * @return $this
     */
    public function setCpfLiquidante($cpf_liquidante)
    {
        $this->container['cpf_liquidante'] = $cpf_liquidante;

        return $this;
    }

    /**
     * Gets fkcod_parc_liquidante
     *
     * @return int
     */
    public function getFkcodParcLiquidante()
    {
        return $this->container['fkcod_parc_liquidante'];
    }

    /**
     * Sets fkcod_parc_liquidante
     *
     * @param int $fkcod_parc_liquidante #field_definition#
     *
     * @return $this
     */
    public function setFkcodParcLiquidante($fkcod_parc_liquidante)
    {
        $this->container['fkcod_parc_liquidante'] = $fkcod_parc_liquidante;

        return $this;
    }

    /**
     * Gets info_liquidante
     *
     * @return \VertisConnect\Model\ModelOrdemServicoInfoLiquidante
     */
    public function getInfoLiquidante()
    {
        return $this->container['info_liquidante'];
    }

    /**
     * Sets info_liquidante
     *
     * @param \VertisConnect\Model\ModelOrdemServicoInfoLiquidante $info_liquidante info_liquidante
     *
     * @return $this
     */
    public function setInfoLiquidante($info_liquidante)
    {
        $this->container['info_liquidante'] = $info_liquidante;

        return $this;
    }

    /**
     * Gets fkcod_tab_preco
     *
     * @return int
     */
    public function getFkcodTabPreco()
    {
        return $this->container['fkcod_tab_preco'];
    }

    /**
     * Sets fkcod_tab_preco
     *
     * @param int $fkcod_tab_preco #field_definition#
     *
     * @return $this
     */
    public function setFkcodTabPreco($fkcod_tab_preco)
    {
        $this->container['fkcod_tab_preco'] = $fkcod_tab_preco;

        return $this;
    }

    /**
     * Gets tip_faturamento
     *
     * @return string
     */
    public function getTipFaturamento()
    {
        return $this->container['tip_faturamento'];
    }

    /**
     * Sets tip_faturamento
     *
     * @param string $tip_faturamento #field_definition#
     *
     * @return $this
     */
    public function setTipFaturamento($tip_faturamento)
    {
        $this->container['tip_faturamento'] = $tip_faturamento;

        return $this;
    }

    /**
     * Gets dth_vencimento
     *
     * @return string
     */
    public function getDthVencimento()
    {
        return $this->container['dth_vencimento'];
    }

    /**
     * Sets dth_vencimento
     *
     * @param string $dth_vencimento #field_definition#
     *
     * @return $this
     */
    public function setDthVencimento($dth_vencimento)
    {
        $this->container['dth_vencimento'] = $dth_vencimento;

        return $this;
    }

    /**
     * Gets fkcod_faturamento
     *
     * @return int
     */
    public function getFkcodFaturamento()
    {
        return $this->container['fkcod_faturamento'];
    }

    /**
     * Sets fkcod_faturamento
     *
     * @param int $fkcod_faturamento #field_definition#
     *
     * @return $this
     */
    public function setFkcodFaturamento($fkcod_faturamento)
    {
        $this->container['fkcod_faturamento'] = $fkcod_faturamento;

        return $this;
    }

    /**
     * Gets fkusr_cod_criacao_os
     *
     * @return int
     */
    public function getFkusrCodCriacaoOs()
    {
        return $this->container['fkusr_cod_criacao_os'];
    }

    /**
     * Sets fkusr_cod_criacao_os
     *
     * @param int $fkusr_cod_criacao_os #field_definition#
     *
     * @return $this
     */
    public function setFkusrCodCriacaoOs($fkusr_cod_criacao_os)
    {
        $this->container['fkusr_cod_criacao_os'] = $fkusr_cod_criacao_os;

        return $this;
    }

    /**
     * Gets fkusr_cod_modificou_os
     *
     * @return int
     */
    public function getFkusrCodModificouOs()
    {
        return $this->container['fkusr_cod_modificou_os'];
    }

    /**
     * Sets fkusr_cod_modificou_os
     *
     * @param int $fkusr_cod_modificou_os #field_definition#
     *
     * @return $this
     */
    public function setFkusrCodModificouOs($fkusr_cod_modificou_os)
    {
        $this->container['fkusr_cod_modificou_os'] = $fkusr_cod_modificou_os;

        return $this;
    }

    /**
     * Gets obs_ordem_servico
     *
     * @return string
     */
    public function getObsOrdemServico()
    {
        return $this->container['obs_ordem_servico'];
    }

    /**
     * Sets obs_ordem_servico
     *
     * @param string $obs_ordem_servico #field_definition#
     *
     * @return $this
     */
    public function setObsOrdemServico($obs_ordem_servico)
    {
        $this->container['obs_ordem_servico'] = $obs_ordem_servico;

        return $this;
    }

    /**
     * Gets dth_inclusao
     *
     * @return string
     */
    public function getDthInclusao()
    {
        return $this->container['dth_inclusao'];
    }

    /**
     * Sets dth_inclusao
     *
     * @param string $dth_inclusao #field_definition#
     *
     * @return $this
     */
    public function setDthInclusao($dth_inclusao)
    {
        $this->container['dth_inclusao'] = $dth_inclusao;

        return $this;
    }

    /**
     * Gets dth_exclusao
     *
     * @return string
     */
    public function getDthExclusao()
    {
        return $this->container['dth_exclusao'];
    }

    /**
     * Sets dth_exclusao
     *
     * @param string $dth_exclusao #field_definition#
     *
     * @return $this
     */
    public function setDthExclusao($dth_exclusao)
    {
        $this->container['dth_exclusao'] = $dth_exclusao;

        return $this;
    }

    /**
     * Gets ind_retira_amostra
     *
     * @return string
     */
    public function getIndRetiraAmostra()
    {
        return $this->container['ind_retira_amostra'];
    }

    /**
     * Sets ind_retira_amostra
     *
     * @param string $ind_retira_amostra #field_definition#
     *
     * @return $this
     */
    public function setIndRetiraAmostra($ind_retira_amostra)
    {
        $this->container['ind_retira_amostra'] = $ind_retira_amostra;

        return $this;
    }

    /**
     * Gets nro_protocolo
     *
     * @return string
     */
    public function getNroProtocolo()
    {
        return $this->container['nro_protocolo'];
    }

    /**
     * Sets nro_protocolo
     *
     * @param string $nro_protocolo #field_definition#
     *
     * @return $this
     */
    public function setNroProtocolo($nro_protocolo)
    {
        $this->container['nro_protocolo'] = $nro_protocolo;

        return $this;
    }

    /**
     * Gets vlr_adiantamento
     *
     * @return float
     */
    public function getVlrAdiantamento()
    {
        return $this->container['vlr_adiantamento'];
    }

    /**
     * Sets vlr_adiantamento
     *
     * @param float $vlr_adiantamento #field_definition#
     *
     * @return $this
     */
    public function setVlrAdiantamento($vlr_adiantamento)
    {
        $this->container['vlr_adiantamento'] = $vlr_adiantamento;

        return $this;
    }

    /**
     * Gets lock_vlr_adiantamento
     *
     * @return string
     */
    public function getLockVlrAdiantamento()
    {
        return $this->container['lock_vlr_adiantamento'];
    }

    /**
     * Sets lock_vlr_adiantamento
     *
     * @param string $lock_vlr_adiantamento #field_definition#
     *
     * @return $this
     */
    public function setLockVlrAdiantamento($lock_vlr_adiantamento)
    {
        $this->container['lock_vlr_adiantamento'] = $lock_vlr_adiantamento;

        return $this;
    }

    /**
     * Gets os_med
     *
     * @return \VertisConnect\Model\ModelOrdemServicoOsMed
     */
    public function getOsMed()
    {
        return $this->container['os_med'];
    }

    /**
     * Sets os_med
     *
     * @param \VertisConnect\Model\ModelOrdemServicoOsMed $os_med os_med
     *
     * @return $this
     */
    public function setOsMed($os_med)
    {
        $this->container['os_med'] = $os_med;

        return $this;
    }

    /**
     * Gets itens_os
     *
     * @return \VertisConnect\Model\ModelOrdemServicoItensOs[]
     */
    public function getItensOs()
    {
        return $this->container['itens_os'];
    }

    /**
     * Sets itens_os
     *
     * @param \VertisConnect\Model\ModelOrdemServicoItensOs[] $itens_os #field_definition#
     *
     * @return $this
     */
    public function setItensOs($itens_os)
    {
        $this->container['itens_os'] = $itens_os;

        return $this;
    }

    /**
     * Gets itens_excluidos
     *
     * @return \VertisConnect\Model\ModelOrdemServicoItensExcluidos[]
     */
    public function getItensExcluidos()
    {
        return $this->container['itens_excluidos'];
    }

    /**
     * Sets itens_excluidos
     *
     * @param \VertisConnect\Model\ModelOrdemServicoItensExcluidos[] $itens_excluidos #field_definition#
     *
     * @return $this
     */
    public function setItensExcluidos($itens_excluidos)
    {
        $this->container['itens_excluidos'] = $itens_excluidos;

        return $this;
    }

    /**
     * Gets ind_permite_excluir
     *
     * @return string
     */
    public function getIndPermiteExcluir()
    {
        return $this->container['ind_permite_excluir'];
    }

    /**
     * Sets ind_permite_excluir
     *
     * @param string $ind_permite_excluir ind_permite_excluir
     *
     * @return $this
     */
    public function setIndPermiteExcluir($ind_permite_excluir)
    {
        $this->container['ind_permite_excluir'] = $ind_permite_excluir;

        return $this;
    }

    /**
     * Gets statusinicial
     *
     * @return string
     */
    public function getStatusinicial()
    {
        return $this->container['statusinicial'];
    }

    /**
     * Sets statusinicial
     *
     * @param string $statusinicial statusinicial
     *
     * @return $this
     */
    public function setStatusinicial($statusinicial)
    {
        $this->container['statusinicial'] = $statusinicial;

        return $this;
    }

    /**
     * Gets recordcount
     *
     * @return int
     */
    public function getRecordcount()
    {
        return $this->container['recordcount'];
    }

    /**
     * Sets recordcount
     *
     * @param int $recordcount Total de registros
     *
     * @return $this
     */
    public function setRecordcount($recordcount)
    {
        $this->container['recordcount'] = $recordcount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


